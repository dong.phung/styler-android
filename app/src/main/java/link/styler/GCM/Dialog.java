package link.styler.GCM;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.WindowManager;

/**
 * Created by admin on 4/6/17.
 */

public class Dialog extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        String message = getIntent().getExtras().getString("message");

        this.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("通知があります")
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
