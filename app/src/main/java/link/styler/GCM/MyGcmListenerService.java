package link.styler.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import link.styler.Foreground;
import link.styler.Utils.Logger;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.R;

/**
 * Created by admin on 3/24/17.
 */

public class MyGcmListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data){

        String message = data.getString("message");
        Logger.print("From "+from);
        Logger.print("Message "+message);

        if(from.startsWith("/topics")){
            Logger.print("message received from some topic");
        }
        else {
            Logger.print("normal dowstream message");
        }

        sendNotification(message);

    }

    private void sendNotification(final String message){

        if(Foreground.get().isBackground())
        {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.icon_star)
                    .setContentTitle("GCM Message")
                    .setContentText(message)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0,notificationBuilder.build());

            Logger.print("Foreground background");
        }

        if(Foreground.get().isForeground()){
            Intent dialog = new Intent(this, Dialog.class);
            dialog.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            dialog.putExtra("message",message);
            getApplicationContext().startActivity(dialog);
        }
    }
}
