package link.styler.GCM;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.DataSharing;
import link.styler.Utils.Logger;
import link.styler.styler_android.R;

/**
 * Created by admin on 3/24/17.
 */

public class RegistrationIntentService extends IntentService{

    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService(){
        super("RegistrationIntentService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */

    public RegistrationIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE,null);

            Logger.print("token RegistrationIntentService "+token);

            sendRegistrationToServer(token);
            subscribeTopics(token);

            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER,true).apply();

        } catch (IOException e) {
            e.printStackTrace();
            Logger.print("FAIL");
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER,false).apply();
        }


        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token){
        //save to reference android
        DataSharing.getsIntstance().setTokenGCM(token);
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for(String topic : TOPICS){
            pubSub.subscribe(token,"/topics/"+topic,null);
        }
    }
}
