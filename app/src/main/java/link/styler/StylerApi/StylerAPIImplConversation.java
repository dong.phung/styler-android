package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplConversation {
    public static void getConversations(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/conversations";
        Map<String,String> param =  new HashMap<>();
        if(lastObjID != null)
           param.put("last_object_id",lastObjID.toString());

        StylerAPIBaseRequests.sendGet(path,param,completion);

    }

    public static void postConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
    }

    public static void deleteConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
    }
}
