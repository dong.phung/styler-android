package link.styler.StylerApi;

import com.loopj.android.http.RequestParams;

import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplFeedback {
    public static void createFeedback(int rate, int replyID, final STCompletion.Base completion){
        String path = "/feedbacks";
        RequestParams params = new RequestParams();
        params.put("rate",rate);
        params.put("reply_id",replyID);

        StylerAPIBaseRequests.sendPost(path,params,completion);
    }
    public static void postFeedback(Map<String, String> param, final STCompletion.Base completion) {

    }
}
