package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplFollow {
    public static void getFollowers(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/users/"+(userID)+"/followers";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getFollows(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/users/"+(userID)+"/follows";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void createFollow(int staffID, final STCompletion.WithJson completion) {
        String path = "/follows";
        Map<String,String> param = new HashMap<>();
        param.put("staff_id",Integer.toString(staffID));
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }

    public static void deleteFollow(int id, final STCompletion.Base completion) {
        String path = "/follows/"+id;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }
}
