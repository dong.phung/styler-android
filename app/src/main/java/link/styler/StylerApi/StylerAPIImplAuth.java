package link.styler.StylerApi;

import android.util.Log;

import com.loopj.android.http.RequestParams;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.styler_android.New_TabHost.Constants;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplAuth {
    public static void login(String email, String password, final STCompletion.WithJsonToken completion) {
        Log.i("Tan006 ", "in login");
        String path = "/login";
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);
        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void facebookLogin(String accessToken, final STCompletion.WithJsonToken completion) {
        String path = "/facebook";
        RequestParams params = new RequestParams();
        params.put("access_token", accessToken);
        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void logout(STCompletion.Base completion) {
        String path = "/auth";
        StylerAPIBaseRequests.sendDelete(path,completion);
    }

    public static void refreshToken(STCompletion.WithJsonToken completion) {
        String path = "/token_reference";
        Map<String, String> params = new HashMap<String, String>();
        params.put("version_num", Constants.appVersion());

        // TODO: device token
        params.put("device_token", "" );

        params.put("device_platform","android");
        StylerAPIBaseRequests.sendGet(path, params, completion);
    }

    public static void reset(String email, final STCompletion.Base completion) {
        String path = "/auth/reset";
        RequestParams params = new RequestParams();
        params.put("email", email);
        StylerAPIBaseRequests.sendPost(path, params, completion);
    }
}
