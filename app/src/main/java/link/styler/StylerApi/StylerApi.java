package link.styler.StylerApi;

/**
 * Created by dongphung on 1/4/17.
 */

import android.app.Activity;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import link.styler.CompletionInterfaces.*;
import link.styler.STStructs.*;

import org.json.JSONObject;

public final class StylerApi {
    /* StylerAPI */

    public static String getAccessTokenBlock = null;
    public static String getUserEmailBlock = null;
    public static String getUserPasswordBlock = null;

    public static void setCurrentActivity(Activity activity) {
        StylerAPIBaseRequests.setCurrentActivity(activity);
    }

	public static Activity getCurrentActivity(){
		return StylerAPIBaseRequests.getCurrentActivity();
	}

    /* StylerAPI+Auth */
    // TODO: @Tan

	public static void login(String email, String password, final STCompletion.WithJsonToken completion) {
		StylerAPIImplAuth.login(email, password, completion);
	}

	public static void facebookLogin(String accessToken, final STCompletion.WithJsonToken completion) {
		StylerAPIImplAuth.facebookLogin(accessToken, completion);
	}

	public static void logout(STCompletion.Base completion) {
		StylerAPIImplAuth.logout(completion);
	}

	public static void refreshToken(STCompletion.WithJsonToken completion) {
		StylerAPIImplAuth.refreshToken(completion);
	}

	public static void reset(String email, final STCompletion.Base completion) {
		StylerAPIImplAuth.reset(email, completion);
	}

	public static void createFeedback(Integer rate, Integer replyID, STCompletion.Base completion){
		StylerAPIImplFeedback.createFeedback(rate,replyID,completion);
	}

    /* StylerAPI+Users */

    public static void getUser(int userID, final STCompletion.WithJson completion) {
        StylerAPIImplUsers.getUser(userID, completion);
    }

    public static void postUser(String email, String password, final STCompletion.WithJsonToken completion) {
		StylerAPIImplUsers.postUser(email, password, completion);
    }

	public static void patchUser(File file, String name, int location, String birthday, String  gender, String profile, final STCompletion.WithJson completion) {

		try {
			StylerAPIImplUsers.patchUser(file, name, location, birthday, gender, profile, completion);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

    public static void deleteUser(String userID, final STCompletion.Base completion) {
        //TODO: not using on iOS
		StylerAPIImplUsers.deleteUser(userID, completion);
    }

    public static void getPostsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplUsers.getPostsOfUser(userID, lastObjectID, completion);
    }

    public static void getWatchesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
		StylerAPIImplUsers.getWatchesOfUser(userID, lastObjectID, completion);
    }

    public static void getRepliesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplUsers.getRepliesOfUser(userID, lastObjectID, completion);
    }

    public static void getLikesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
		StylerAPIImplUsers.getLikesOfUser(userID, lastObjectID, completion);
    }

    public static void getInterestsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplUsers.getInterestsOfUser(userID, lastObjectID, completion);
    }

    public static void postDeviceToken(String token,STCompletion.Base completion) {
		StylerAPIImplUsers.postDeviceToken(token, completion);
    }

    /* StylerAPI+Posts */

    public static void getPosts(Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplPosts.getPosts(lastObjectID, completion);
    }

    public static void getLatestPosts(Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplPosts.getLatestPosts(lastObjectID, completion);
    }

    public static void getUnrepliedPosts(int category, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplPosts.getUnrepliedPosts(category, lastObjectID, completion);
    }

    public static void getPost(Integer postID, final STCompletion.WithJson completion) {
		StylerAPIImplPosts.getPost(postID, completion);
    }

    public static void createPost(String text, int categoryID, final STCompletion.WithJson completion) {
		StylerAPIImplPosts.createPost(text, categoryID, completion);
    }

    public static void patchPost(int postID, String text, int categoryID, final STCompletion.WithJson completion) {
		StylerAPIImplPosts.patchPost(postID, text, categoryID, completion);
    }

    public static void deletePost(int postID, final STCompletion.Base completion) {
		StylerAPIImplPosts.deletePost(postID, completion);
    }

    /* StylerAPI+Reply */

    public static void getRepliesOfPost(int postID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplReply.getRepliesOfPost(postID, lastObjectID, completion);
    }

	// create by tan
	public static void getRepliesOfPost(int postID, Integer lastObjectID, final STCompletion.WithJsonArrayNullableInt completion) {
		//StylerAPIImplReply.getRepliesOfPost(postID, lastObjectID, completion);
		//// TODO: 2/10/17 by Tan create getRepliesOfPost with jsonArray;
	}
	//end create by tan

    public static void createReply(STStructs.StylerReplyStruct replyStruct, final STCompletion.Base completion) {
		StylerAPIImplReply.createReply(replyStruct, completion);
    }

    public static void createReplyAndItem(STStructs.StylerReplyAndItemStruct replyAndItemStruct, File photos, final STCompletion.WithJson completion) {
		Log.i("clickSend", "StylerApi createReplyAndItem");
		StylerAPIImplReply.createReplyAndItem(replyAndItemStruct, photos, completion);
    }

    public static void patchReply(STStructs.StylerUpdateItemAndReplyStruct params, File photos, final STCompletion.WithJson completion) {
		StylerAPIImplReply.patchReply(params, photos, completion);
    }

    public static void getReply(int replyID, final STCompletion.WithJson completion) {
		StylerAPIImplReply.getReply(replyID, completion);
    }

    public static void deleteReply(int postID, int replyID, final STCompletion.Base completion) {
		StylerAPIImplReply.deleteReply(postID, replyID, completion);
    }

    /* StylerAPI+Conversation */

    public static void getConversations(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplConversation.getConversations(lastObjID, completion);
    }

	// create by tan
	public static void getConversations(Integer lastObjID, final STCompletion.WithJsonArrayNullableInt completion) {
		//StylerAPIImplReply.getRepliesOfPost(postID, lastObjectID, completion);
		//// TODO: 2/13/17 by Tan create getRepliesOfPost with jsonArray;
	}
	// end create by tan




    public static void postConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
		StylerAPIImplConversation.postConversation(conversationID, completion);
    }

    public static void deleteConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
		StylerAPIImplConversation.deleteConversation(conversationID, completion);
    }

    /* StylerAPI+Message */

    public static void getMessages(int conversationID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplMessage.getMessages(conversationID, lastObjID, completion);
    }

    public static void getMessagesWithUserID(int userID, Integer lastObjID, Integer latestObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplMessage.getMessagesWithUserID(userID, lastObjID, latestObjID, completion);
    }

    public static void postMessages(JSONObject param, final STCompletion.WithNullableInt completion) {
		StylerAPIImplMessage.postMessages(param, completion);
    }

    /* StylerAPI+Follow */

	public static void getFollowers(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplFollow.getFollowers(userID, lastObjID, completion);
	}
	
	public static void getFollows(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplFollow.getFollows(userID, lastObjID, completion);
	}
	
	public static void createFollow(int staffID, final STCompletion.WithJson completion) {
		StylerAPIImplFollow.createFollow(staffID, completion);
	}
	
	public static void deleteFollow(int id, final STCompletion.Base completion) {
		StylerAPIImplFollow.deleteFollow(id, completion);
	}

    /* StylerAPI+Favorite */

    public static void getFavoriteIds(STCompletion.Base completion) {
		//TODO: not using on iOS
		StylerAPIImplFavourite.getFavoriteIds(completion);
    }

    public static void getFavorites(String userID, Map<String, String> param, final STCompletion.WithJson completion) {
		//TODO: not using on iOS
		StylerAPIImplFavourite.getFavorites(userID, param, completion);
    }
	
	public static void postFavorite(String objectName, String objectId, final STCompletion.Base completion) {
		//TODO: not using on iOS
		StylerAPIImplFavourite.postFavorite(objectName, objectId, completion);
    }
	
	public static void deleteFavorite(String objectName, String objectId, final STCompletion.Base completion) {
		//TODO: not using on iOS
		StylerAPIImplFavourite.deleteFavorite(objectName, objectId, completion);
    }

    /* StylerAPI+Notification */

	public static void checkNewNotification(STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplNotification.checkNewNotification(completion);
    }
	
	public static void getActivities(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplNotification.getActivities(lastObjID, completion);
    }

		// create by tan
	public static void getActivities(Integer lastObjID, final STCompletion.WithJsonArrayNullableInt completion) {
		//StylerAPIImplReply.get(postID, lastObjectID, completion);
		//// TODO: 2/14/17 by Tan create getRepliesOfPost with jsonArray;

	}
		// end create by tan
	
	public static void getMessageList(Integer lastObjID, final STCompletion.WithNullableInt completion) {
		//TODO: not using on iOS
		StylerAPIImplNotification.getMessageList(lastObjID, completion);
    }

    /* StylerAPI+Shop */

	public static void getLocations(STCompletion.WithJson completion) {
		StylerAPIImplShop.getLocations(completion);
	}
	
	public static void getShops(int locationID, String order, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplShop.getShops(locationID, order, lastObjID, completion);
	}
	
	public static void getShop(int shopID, final STCompletion.WithJson completion) {
		StylerAPIImplShop.getShop(shopID, completion);
	}
	
	public static void getRepliesOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		// TODO: need to check [STReply] - array instead of json obj
		// TODO: not using on iOS
		StylerAPIImplShop.getRepliesOfShop(shopID, lastObjID, completion);
	}
	
	public static void getItemsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplShop.getItemsOfShop(shopID, lastObjID, completion);
	}
	
	public static void getItemsOfShopWithCategory(int shopID, int categoryID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplShop.getItemsOfShopWithCategory(shopID, categoryID, lastObjID, completion);
	}
	
	public static void getColleagueOfStaff(int staffID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		// TODO: need to check [STUser] - array instead of json obj
		// TODO: not using on iOS
		StylerAPIImplShop.getColleagueOfStaff(staffID, lastObjID, completion);
	}
	
	public static void getStaffsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplShop.getStaffsOfShop(shopID, lastObjID, completion);
	}
	
	public static void updateShop(int shopID, Map<String, Object> param, File photo, final STCompletion.WithJson completion) {
		StylerAPIImplShop.updateShop(shopID, param, photo, completion);
	}

    /* StylerAPI+Watch */

	public static void getWatches(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplWatch.getWatches(lastObjID, completion);
	}
	
	public static void postWatch(int postID, final STCompletion.WithJson completion) {
		StylerAPIImplWatch.postWatch(postID, completion);
	}
	
	public static void deleteWatch(int watchID, final STCompletion.Base completion) {
		StylerAPIImplWatch.deleteWatch(watchID, completion);
	}

    /* StylerAPI+Reachable */

	public static boolean isReachable() {
		return StylerAPIImplReachable.isReachable();
	}

    /* StylerAPI+Report */
	
	public static void postReport(STEnums.ReportType type, int objectId, String text, final STCompletion.WithJson completion) {
		StylerAPIImplReport.postReport(type, objectId, text, completion);
	}

    /* StylerAPI+Versions */

	public static void getIsLatestVersion(String versionNumber, final STCompletion.Base completion) {
		StylerAPIImplVersion.getIsLatestVersion(versionNumber, completion);
	}

    /* StylerAPI+Comment */

	public static void getComments(int postID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplComment.getComments(postID, lastObjID, completion);
	}
	
	public static void postComment(int postID, String text, Integer itemID, final STCompletion.WithJson completion) {
		StylerAPIImplComment.postComment(postID, text, itemID, completion);
	}
	
	public static void deleteComment(int commentID, final STCompletion.Base completion) {
		StylerAPIImplComment.deleteComment(commentID, completion);
	}

    /* StylerAPI+Todo */

	public static void getTodos(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplTodo.getTodos(lastObjID, completion);
	}
	
	public static void postTodo(int postID, final STCompletion.WithJson completion) {
		StylerAPIImplTodo.postTodo(postID, completion);
	}
	
	public static void deleteTodo(int todoID, final STCompletion.Base completion) {
		StylerAPIImplTodo.deleteTodo(todoID, completion);
	}

    /* StylerAPI+Like */

	public static void postLike(int itemID, Integer replyID, final STCompletion.WithJson completion) {
		StylerAPIImplLike.postLike(itemID, replyID, completion);
	}
	
	public static void deleteLike(int likeID, final STCompletion.Base completion) {
		StylerAPIImplLike.deleteLike(likeID, completion);
	}

    /* StylerAPI+Task */

	public static void postTask(int postID, final STCompletion.WithJson completion) {
		StylerAPIImplTask.postTask(postID, completion);
	}

    /* StylerAPI+Event */

	public static void createEvent(STStructs.StylerCreateEventParam param, final STCompletion.WithJson completion) {
		StylerAPIImplEvent.createEvent(param, completion);
	}
	
	public static void patchEvent(STStructs.StylerPatchEventParam param, final STCompletion.WithJson completion) {
		StylerAPIImplEvent.patchEvent(param, completion);
	}
	
	public static void getFeatureEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplEvent.getFeatureEvents(lastObjID, completion);
	}
	
	public static void getOngoingEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplEvent.getOngoingEvents(lastObjID, completion);
	}

	public static void getEventDetail(int eventID, final STCompletion.WithJson completion) {
		StylerAPIImplEvent.getEventDetail(eventID, completion);
	}
	
	public static void deleteEvent(int eventID, final STCompletion.Base completion) {
		StylerAPIImplEvent.deleteEvent(eventID, completion);
	}
	
	public static void getEventsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplEvent.getEventsOfShop(shopID, lastObjID, completion);
	}
	
	public static void getEventsOfUserWithID(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplEvent.getEventsOfUserWithID(userID, lastObjID, completion);
	}
	
	public static void createInterest(int eventID, final STCompletion.WithJson completion) {
		StylerAPIImplEvent.createInterest(eventID, completion);
	}
	
	public static void deleteInterest(int interestID, final STCompletion.Base completion) {
		StylerAPIImplEvent.deleteInterest(interestID, completion);
	}
	
    /* StylerAPI+Item */

	public static void getPopularItems(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
		StylerAPIImplItem.getPopularItems(lastObjID, completion);
	}
	
	public static void getItem(int itemID, final STCompletion.WithJson completion) {
		StylerAPIImplItem.getItem(itemID, completion);
	}
	
	public static void patchItem(int itemID, Map<String, String> param, final STCompletion.WithJson completion) {
		StylerAPIImplItem.patchItem(itemID, param, completion);
	}

    /* StylerAPI+Category */

	public static void getCategories(STCompletion.WithJson completion) {
		StylerAPIImplCategory.getCategories(completion);
	}

    /* StylerAPI+Article */

	public static void getArticle(int articleID, final STCompletion.WithJson completion) {
		StylerAPIImplArticle.getArticle(articleID, completion);
	}
	
	public static void getArticles(Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplArticle.getArticles(lastObjID, completion);
	}
	
	public static void getCategoryArticles(int categoryID, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplArticle.getCategoryArticles(categoryID, lastObjID, completion);
	}
	
	public static void getTagArticles(int tagID, Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplArticle.getTagArticles(tagID, lastObjID, completion);
	}
	
	public static void getTopArticles(STCompletion.WithJson completion) {
		StylerAPIImplArticle.getTopArticles(completion);
	}

    /* StylerAPIFeedback */

	public static void postFeedback(Map<String, String> param, final STCompletion.Base completion) {
		StylerAPIImplFeedback.postFeedback(param, completion);
	}

    /* StylerAPI+Tag */

	public static void getTags(Integer lastObjID, final STCompletion.WithJson completion) {
		StylerAPIImplTag.getTags(lastObjID, completion);
	}

    /* StylerAPI+Setting */

	public static void sendNotificationSetting(boolean pushNotification, boolean mailNotification, boolean mailMagazine) {
		StylerAPIImplSetting.sendNotificationSetting(pushNotification, mailNotification, mailMagazine);
	}

	public static void sendEmail(String email, final STCompletion.WithJson completion) {
		StylerAPIImplSetting.sendEmail(email, completion);
	}
	
	public static void sendPassword(String currentPassword, String newPassword, String confirmPassword, final STCompletion.WithJson completion) {
		StylerAPIImplSetting.sendPassword(currentPassword, newPassword, confirmPassword, completion);
	}

}
