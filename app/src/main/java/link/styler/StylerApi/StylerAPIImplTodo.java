package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplTodo {
    public static void getTodos(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/todo_posts";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void postTodo(int postID, final STCompletion.WithJson completion) {
        String path = "/todos";
        Map<String,String> param = new HashMap<>();
        param.put("post_id",Integer.toString(postID));
        StylerAPIBaseRequests.creatPost(path,param,completion);

    }

    public static void deleteTodo(int todoID, final STCompletion.Base completion) {
        String path = "/todos/"+todoID;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }
}
