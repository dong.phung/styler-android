package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplItem {
    public static void getPopularItems(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/items";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getItem(int itemID, final STCompletion.WithJson completion) {
        String  path = "/items/"+itemID;
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }

    public static void patchItem(int itemID, Map<String, String> param, final STCompletion.WithJson completion) {
        String path = "/items/" +itemID;
        StylerAPIBaseRequests.sendPatch(path,param,null,completion);
    }
}
