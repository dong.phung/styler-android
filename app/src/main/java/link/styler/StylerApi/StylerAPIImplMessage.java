package link.styler.StylerApi;

import android.util.Log;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplMessage {
    public static void getMessages(int conversationID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/conversations/" + conversationID + "/messages";
        Map<String, String> param = new HashMap<>();
        if (lastObjID != null)
            param.put("last_object_id", lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path, param, completion);
    }

    public static void getMessagesWithUserID(int userID, Integer lastObjID, Integer latestObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/messages";
        Map<String, String> param = new HashMap<>();
        param.put("destination_user_id", "" + userID);
        if (lastObjID != null)
            param.put("last_object_id", lastObjID.toString());
        else if (latestObjID != null)
            param.put("latest_object_id", latestObjID.toString());

        // ReloadIgnoringLocalAndRemoteCacheData
        StylerAPIBaseRequests.sendGet(path, param, completion);
    }

    public static void postMessages(JSONObject param, final STCompletion.WithNullableInt completion) {
        String path = "/messages";

        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        Iterator<String> keys = param.keys();
        Log.i("postMessages00", "param: " + param);
        RequestParams map = new RequestParams();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            Log.i("postMessages00", "tag: " + key);
            Log.i("postMessages00", "value: " + param.opt(key));
            map.put(key, param.opt(key));
        }
        request.params = map;

        request.httpMethod = STEnums.HTTPRequestType.POST;
        Log.i("postMessages", "request: " + request);

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, null);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(false, null);
                } else {
                    if (json.isNull("message")) {
                        try {
                            // TODO:
                            completion.onRequestComplete(true, null);
                        } catch (Exception parseJsonException) {
                            completion.onRequestComplete(false, null);
                        }
                    } else {
                        try {
                            completion.onRequestComplete(false, null);
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
            }
        });
    }
}
