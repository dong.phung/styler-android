package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplLike {
    public static void postLike(int itemID, Integer replyID, final STCompletion.WithJson completion) {
        String path = "/likes";
        Map<String,String> param = new HashMap<>();
        param.put("item_id",Integer.toString(itemID));
        if(replyID != null)
            param.put("reply_id",replyID.toString());
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }

    public static void deleteLike(int likeID, final STCompletion.Base completion) {
        String path = "/likes/" +likeID;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }
}
