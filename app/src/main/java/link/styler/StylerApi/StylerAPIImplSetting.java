package link.styler.StylerApi;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplSetting {
    public static void sendNotificationSetting(boolean pushNotification, boolean mailNotification, boolean mailMagazine) {
        String path = "/users/setting";
        RequestParams params = new RequestParams();
        params.put("push_notification",pushNotification);
        params.put("mail_notification",mailNotification);
        params.put("mail_magazine",mailMagazine);

        StylerAPIBaseRequests.sendPatch(path, null, params, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {

            }
        });
    }

    public static void sendEmail(String email, final STCompletion.WithJson completion) {
        String path = "/users/email";
        RequestParams params = new RequestParams();
        params.put("email",email);
        StylerAPIBaseRequests.sendPatch(path,null,params,completion);
    }

    public static void sendPassword(String currentPassword, String newPassword, String confirmPassword, final STCompletion.WithJson completion) {
        String path = "/users/password";
        RequestParams params = new RequestParams();
        params.put("current_password",currentPassword);
        params.put("password",newPassword);
        params.put("password_confirmation",confirmPassword);
        StylerAPIBaseRequests.sendPatch(path,null,params,completion);
    }
}
