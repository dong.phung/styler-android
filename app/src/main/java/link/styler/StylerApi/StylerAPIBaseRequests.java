package link.styler.StylerApi;

import android.app.Activity;
import android.util.Log;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.auth.AuthScope;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;
import link.styler.Utils.Logger;
import link.styler.styler_android.New_TabHost.Constants;

/**
 * Created by dongphung on 1/17/17.
 */

class StylerAPIBaseRequests {
    private static WeakReference<Activity> currentActivity;

    private static AsyncHttpClient requestClient = new AsyncHttpClient();

    public static void setCurrentActivity(Activity activity) {
        currentActivity = new WeakReference<Activity>(activity);
    }

    public static Activity getCurrentActivity() {
        return currentActivity.get();
    }

    public static String urlWithPath(String path, Map<String, String> param) {
        String urlString = Constants.apiUrl(path);
        String query = "";
        if (param != null) {
            for (Map.Entry<String, String> entry : param.entrySet()) {
                if (!query.isEmpty()) {
                    query += "&";
                }
                try {
                    query += entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Logger.print("Encode error message " + e.getMessage());
                    e.printStackTrace();
                }

            }
        }

        if (!query.isEmpty()) {
            urlString += "?" + query;
        }

        return urlString;
    }

    public static void startRequest(STURLRequest request, final STCompletion.WithJsonInt callback) {

        Logger.print("startRequest " + request.url);

        requestClient.removeAllHeaders();
        requestClient.addHeader("X-STYLER-APP-VERSION", Constants.appVersion());


        if (StylerApi.getAccessTokenBlock != null) {
            Logger.print("X-STYLER-TOKEN " + StylerApi.getAccessTokenBlock);
            requestClient.addHeader("X-STYLER-TOKEN", StylerApi.getAccessTokenBlock);
        }

        requestClient.setBasicAuth(Constants.kusername, Constants.kpassword, AuthScope.ANY);

        AsyncHttpResponseHandler respondhandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response == null) {
                    Log.e("startRequest1", "response == null");
                    Log.e("[STyler Error]", "Can not parse respond into JSON object");
                    callback.onRequestComplete(null, false, new STError("Could not parse json from respond" + headers.toString()), statusCode);
                    return;
                }
                callback.onRequestComplete(response, true, null, statusCode);
                Logger.print("startRequest response " + response);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                Log.e("startRequest2", "timelinel " + timeline);
                try {
                    callback.onRequestComplete(new JSONObject(timeline.toString()), true, null, statusCode);
                } catch (JSONException e) {
                    Log.e("[STyler Error]", "Can not parse respond into JSON array");
                    callback.onRequestComplete(null, false, new STError("Can not parse respond into JSON array"), statusCode);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.e("startRequest3", "onFailure " + errorResponse);
                Log.w("Quang", "status code " + statusCode + " ");
                Log.w("Quang", "errorRespone " + errorResponse);
                Logger.print("startRequest onFailure " + errorResponse + " statusCode " + statusCode + " errorResponse " + errorResponse);
                callback.onRequestComplete(null, false, new STError(errorResponse + ""), statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("startRequest4", "onFailure " + responseString);
                Log.w("[STyler Warning]", "resquest failed, respond: " + responseString);
                Logger.print("startRequest onFailure " + responseString);
                callback.onRequestComplete(null, false, new STError(responseString), statusCode);
            }
        };

        Logger.print("setUseSynchronousMode " + respondhandler.getUseSynchronousMode());

        requestClient.setTimeout(150000);
        switch (request.httpMethod) {
            case GET:
                requestClient.get(request.url, request.params, respondhandler);
                break;
            case POST:
                requestClient.post(request.url, request.params, respondhandler);
                break;
            case PUT:
                requestClient.put(request.url, request.params, respondhandler);
                break;
            case PATCH:
                requestClient.patch(request.url, request.params, respondhandler);
                break;
            case DELETE:
                requestClient.delete(request.url, request.params, respondhandler);
                break;
            default:
                Log.e("[Styler Error]", "Unknown HTTP method: " + request.httpMethod.name());
                callback.onRequestComplete(null, false, null, -1);
                break;
        }
        Log.i("StylerAPIBaseRequests", "request url " + request.url);
        Log.i("StylerAPIBaseRequests", "request params " + request.params);
        Log.i("StylerAPIBaseRequests", "requestClient " + requestClient);
        Log.i("StylerAPIBaseRequests", "requestClient " + request);
    }

    public static void creatPost(String path, Map<String, String> params, final STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);
        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int intValue) {
                completion.onRequestComplete(json, isSuccess, err);
                if (json.has("message"))
                    Log.w("StylerAPIBaseRequests", "creatPost [message] " + json.optString("message"));
            }
        });


    }

    public static void sendGet(String path, Map<String, String> params, final STCompletion.Base completion) {

        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        Logger.print("Home " + url);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int intValue) {
                Logger.print("onRequestComplete json " + json + " err " + err);

                if (err != null) {
                    completion.onRequestComplete(false, null);
                    return;
                }


                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(false, null);
                } else {
                    if (json.isNull("message")) {
                        completion.onRequestComplete(true, null);
                    } else {
                        try {
                            completion.onRequestComplete(false, new STError(json.getString("message")));
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
            }
        });

    }

    public static void sendGet(String path, Map<String, String> params, final STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError error, int statusCode) {
                if (error != null) {
                    completion.onRequestComplete(null, false, error);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null);
                } else {
                    if (json.isNull("message")) {
                        completion.onRequestComplete(json, true, null);
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(json.getString("message")));
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
            }
        });
    }

    public static void sendGet(String path, Map<String, String> params, final STCompletion.WithJsonNullableInt completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        if (url == null) {
            completion.onRequestComplete(null, false, null, null);
            return;
        }

        Logger.print("url " + url);

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError error, int statusCode) {
                if (error != null) {
                    completion.onRequestComplete(null, false, error, null);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null, null);
                } else {
                    if (json.isNull("message")) {
                        Integer lastObjID = null;
                        try {
                            lastObjID = json.getInt("last_object_id");
                        } catch (Exception e) {
                        }
                        completion.onRequestComplete(json, true, null, lastObjID);
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(json.getString("message")), null);
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
            }
        });
    }

    public static void postComment(String path, RequestParams params, final STCompletion.WithJson completion) {
        //ToDo : Handle errorcode 409 . Display message : "このアイテムは既におすすめ済みです"
    }

    // sendGet create by Tan:
    public static void sendGet(String path, Map<String, String> params, final STCompletion.WithJsonToken completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        if (url == null) {
            completion.onRequestComplete(null, false, null, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;
        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError error, int statusCode) {
                if (error != null) {
                    completion.onRequestComplete(null, false, error, null);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null, null);
                } else {
                    if (json.isNull("message")) {
                        String token = null;
                        try {
                            token = json.getString("token");
                        } catch (Exception e) {
                        }
                        completion.onRequestComplete(json, true, null, token);
                    }
                }
            }
        });
    }
    // end sendGet create by Tan:

    public static void sendPost(String path, RequestParams params, final STCompletion.Base completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.params = params;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }
                completion.onRequestComplete(true, null);
            }
        });
    }

    public static void sendPost(String path, RequestParams params, final STCompletion.WithJson completion) {
        Log.i("clickSend", "StylerAPIBaseRequests sendPost");
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        Logger.print("send post url " + url);
        Log.i("sendPost", "params" + params);

        STURLRequest request = new STURLRequest(url);
        request.params = params;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                Logger.print("send post startRequset");
                if (err != null) {
                    completion.onRequestComplete(null, false, err);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null);
                } else {
                    Logger.print("1111111111111");
                    Log.i("onRequestComplete", "json " + json);
                    Log.i("onRequestComplete", "status " + status);
                    if (json.isNull("message")) {
                        Log.i("onRequestComplete", "json.isNull message " + json.isNull("message"));
                        Log.i("onRequestComplete", "json.isNull message " + json);
                        //try {
                        Log.i("onRequestComplete", "completion.onRequestComplete(json, true, null);");
                        completion.onRequestComplete(json, true, null);
                        // } catch (Exception parseJsonException) {
                        //    Logger.print("sendPost parseJsonException "+parseJsonException.getMessage());
                        //     completion.onRequestComplete(null, false, new STError("No token received!"));
                        //   }
                    } else {
                        Log.i("onRequestComplete", "eel  ");
                        try {
                            completion.onRequestComplete(null, false, new STError(401, json.getString("message")));
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
                Log.i("startRequest", "isSuccess " + isSuccess);
            }
        });
    }

    public static void sendPost(String path, RequestParams params, final STCompletion.WithJsonToken completion) {
        Log.i("Tan006 ", "in sendPost");
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(null, false, null, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.params = params;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                Log.i("Tan006 ", "startRequest");
                if (err != null) {
                    completion.onRequestComplete(null, false, err, null);
                    Log.i("Tan006 ", "err + isSuc " + isSuccess);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null, null);
                } else {
                    Logger.print("message starRequest " + json);
                    if (json.isNull("message")) {
                        try {
                            String token = json.optString("token");
                            //Logger.print("token "+token);
                            //if(err == null)
                            //    err = new STError();
                            completion.onRequestComplete(json, true, err, token);
                        } catch (Exception parseJsonException) {
                            Logger.print(" parseJsonException " + parseJsonException.getMessage().toString());
                            completion.onRequestComplete(null, false, new STError("No token received!"), null);
                        }
                    } else {
                        try {
                            Logger.print("AAAAAAAAAAAAAAAAAAA");
                            completion.onRequestComplete(null, false, new STError(401, json.getString("message")), null);
                        } catch (Exception dunnoWthHappenHere) {
                        }
                    }
                }
            }
        });
    }

    public static void sendPatch(String path, Map<String, String> param, RequestParams requsetParams, final STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, param);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        Logger.print("sendPatch " + url);

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.PATCH;
        request.params = requsetParams;
        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(null, false, err);
                    return;
                }

                if (json != null)
                    completion.onRequestComplete(json, true, null);
                else
                    completion.onRequestComplete(null, false, null);
            }
        });
    }

    public static void sendDelete(String path, final STCompletion.Base completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.DELETE;
        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }

                if (json != null)
                    completion.onRequestComplete(true, null);
                else
                    completion.onRequestComplete(false, null);
            }
        });
    }

    public static void patchUser(String path, RequestParams params, final STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        Log.i("Quang", "url " + url);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.PATCH;
        request.params = params;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(null, true, err);
                    return;
                }
                if (json != null)
                    completion.onRequestComplete(json, true, err);
                else
                    completion.onRequestComplete(null, false, err);
            }
        });
    }

}
