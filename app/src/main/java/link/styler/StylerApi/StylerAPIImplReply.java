package link.styler.StylerApi;

import android.graphics.Bitmap;
import android.util.Log;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STStructs;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplReply {
    public static void getRepliesOfPost(int postID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts/" + postID + "/replies";
        Map<String, String> querryString = null;
        if (lastObjectID != null) {
            querryString = new HashMap<String, String>();
            querryString.put("last_object_id", lastObjectID.toString());
        }
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    //create by tan

    //end create by tan

    public static void createReply(STStructs.StylerReplyStruct replyStruct, final STCompletion.Base completion) {
        String path = "/replies";

        RequestParams params = new RequestParams();
        params.put("post_id", replyStruct.postID);
        params.put("comment", replyStruct.comment);
        params.put("item_id", replyStruct.itemID);

        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void createReplyAndItem(STStructs.StylerReplyAndItemStruct replyAndItemStruct, File photos, final STCompletion.WithJson completion) {

        String path = "/replies";
        RequestParams params = new RequestParams();
        params.put("post_id", replyAndItemStruct.postID);  //
        params.put("comment", replyAndItemStruct.comment);  //
        params.put("brand", replyAndItemStruct.brand);  //
        params.put("price", replyAndItemStruct.price); //
        params.put("name", replyAndItemStruct.name);  //
        params.put("text", replyAndItemStruct.text); //
        params.put("category_id", replyAndItemStruct.categoryID);  //
        if (replyAndItemStruct.itemID != 0) {
            params.put("item_id", replyAndItemStruct.itemID);
        }
        try {
            params.put("photo_1", photos, "jpeg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.i("StylerAPIImplReply", "path " + path);
        Log.i("StylerAPIImplReply", "params " + params);
        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void patchReply(STStructs.StylerUpdateItemAndReplyStruct stylerUpdateItemAndReplyStruct, File photos, final STCompletion.WithJson completion) {
        String path = "/replies/" + stylerUpdateItemAndReplyStruct.replyID;

        RequestParams params = new RequestParams();
        params.put("comment", stylerUpdateItemAndReplyStruct.comment);
        params.put("brand", stylerUpdateItemAndReplyStruct.brand);
        params.put("name", stylerUpdateItemAndReplyStruct.name);
        params.put("price", stylerUpdateItemAndReplyStruct.price);
        params.put("text", stylerUpdateItemAndReplyStruct.text);
        params.put("category_id", stylerUpdateItemAndReplyStruct.categoryID);
        StylerAPIBaseRequests.sendPatch(path, null, params, completion);
    }

    public static void getReply(int replyID, final STCompletion.WithJson completion) {
        String path = "/replies/" + replyID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void deleteReply(int postID, int replyID, final STCompletion.Base completion) {
        String path = "/posts/" + postID + "/replies/" + replyID;
        StylerAPIBaseRequests.sendDelete(path, completion);
    }
}
