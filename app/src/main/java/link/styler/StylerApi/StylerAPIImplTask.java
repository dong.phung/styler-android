package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplTask {
    public static void postTask(int postID, final STCompletion.WithJson completion) {
        String path = "/tasks";
        Map<String,String> param = new HashMap<>();
        param.put("post_id",Integer.toString(postID));
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }
}
