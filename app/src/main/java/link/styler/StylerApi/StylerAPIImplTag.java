package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplTag {
    public static void getTags(Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/tags";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }
}
