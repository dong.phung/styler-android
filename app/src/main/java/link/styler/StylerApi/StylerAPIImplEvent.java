package link.styler.StylerApi;

import com.loopj.android.http.RequestParams;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STStructs;

/**
 * Created by dongphung on 1/11/17.
 * public String title;
 public String text;
 public String start_date;
 public String finish_date;
 public String location;
 */


final class StylerAPIImplEvent {
    public static void createEvent(STStructs.StylerCreateEventParam param, final STCompletion.WithJson completion) {
        String path = "/events";
        RequestParams params =  param.appendContentDataEvent();
        StylerAPIBaseRequests.sendPost(path,params,completion);

    }

    public static void patchEvent(STStructs.StylerPatchEventParam param, final STCompletion.WithJson completion) {
        String path = "/events/"+param.eventID ;

        RequestParams params = param.appendContentPatchEvent();
        StylerAPIBaseRequests.sendPatch(path,null,params,completion);
    }

    public static void getFeatureEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/events";
        Map<String,String> param = new HashMap<>();
        param.put("type","future");
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());

        StylerAPIBaseRequests.sendGet(path,param,completion);

    }

    public static void getOngoingEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/events";
        Map<String,String> param = new HashMap<>();
        param.put("type","ongoing");
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());

        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getEventDetail(int eventID, final STCompletion.WithJson completion) {
        String path = "/events/" +eventID ;
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }

    public static void deleteEvent(int eventID, final STCompletion.Base completion) {
        String path = "/events/" + eventID;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }

    public static void getEventsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/shops/"+shopID+"/events";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());

        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getEventsOfUserWithID(int userID, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/users/" +(userID)+"/interests";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void createInterest(int eventID, final STCompletion.WithJson completion) {
        String path = "/interests";
        Map<String,String> param = new HashMap<>();
        param.put("event_id",Integer.toString(eventID));
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }

    public static void deleteInterest(int interestID, final STCompletion.Base completion) {
        String path = "/interests/" + interestID;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }
}
