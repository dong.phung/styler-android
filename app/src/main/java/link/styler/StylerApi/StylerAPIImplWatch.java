package link.styler.StylerApi;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplWatch {
    public static void getWatches(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/watch_posts";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void postWatch(int postID, final STCompletion.WithJson completion) {
        Log.i("StylerAPIImplWatch", "postWatch" );
        String path = "/watches";
        Map<String,String> param = new HashMap<>();
        param.put("post_id",Integer.toString(postID));
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }

    public static void deleteWatch(int watchID, final STCompletion.Base completion) {
        String path = "/watches/" +watchID;
        StylerAPIBaseRequests.sendDelete(path,completion);
    }
}
