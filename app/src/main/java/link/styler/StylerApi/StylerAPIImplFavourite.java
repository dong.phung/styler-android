package link.styler.StylerApi;

import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplFavourite {
    // noted: whole of these methods not using anywhere on iOS staging-3.0.0
    public static void getFavoriteIds(STCompletion.Base completion) {
        //TODO: not using on iOS
    }

    public static void getFavorites(String userID, Map<String, String> param, final STCompletion.WithJson completion) {
        //TODO: not using on iOS
    }

    public static void postFavorite(String objectName, String objectId, final STCompletion.Base completion) {
        //TODO: not using on iOS
    }

    public static void deleteFavorite(String objectName, String objectId, final STCompletion.Base completion) {
        //TODO: not using on iOS
    }
}
