package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplCategory {
    public static void getCategories(STCompletion.WithJson completion) {
        String path = "/categories";
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }
}
