package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplReport {
    public static void postReport(STEnums.ReportType type, int objectId, String text, final STCompletion.WithJson completion) {
        String path = "/feedbacks";
        String object = "";
        switch (type)
        {
            case Post:
                object = "Post";
                break;
            case Reply:
                object = "Reply";
                break;
            case Comment:
                object = "Comment";
                break;
            case Item:
                object = "Item";
                break;
        }
        Map<String,String> param = new HashMap<>();
        param.put("object",object);
        param.put("generic_id",Integer.toString(objectId));
        if(text != null)
            param.put("text",text);
        StylerAPIBaseRequests.creatPost(path,param,completion);
    }
}
