package link.styler.StylerApi;

import com.loopj.android.http.RequestParams;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplPosts {
    public static void getPosts(Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());

        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getLatestPosts(Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts";
        Map<String, String> querryString = new HashMap<String, String>();
        querryString.put("type","latest");
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());

        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getUnrepliedPosts(int category, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts/unreplied";
        Map<String, String> querryString = new HashMap<String, String>();
        querryString.put("category_id", category+"");
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());

        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getPost(Integer postID, final STCompletion.WithJson completion) {
        String path = "/posts/" + postID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void createPost(String text, int categoryID, final STCompletion.WithJson completion) {
        String path = "/posts";

        RequestParams params = new RequestParams();
        params.put("text",text);
        params.put("category_id",categoryID);

        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void patchPost(int postID, String text, int categoryID, final STCompletion.WithJson completion) {
        String path = "/posts/" + postID;
        Map<String, String> querryString = new HashMap<String, String>();
        querryString.put("text", text);
        querryString.put("category_id", "" + categoryID);
        StylerAPIBaseRequests.sendPatch(path, querryString,new RequestParams(),completion);
    }

    public static void deletePost(int postID, final STCompletion.Base completion) {
        String path = "/posts/" + postID;
        StylerAPIBaseRequests.sendDelete(path, completion);
    }
}
