package link.styler.StylerApi;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.*;
import link.styler.styler_android.New_TabHost.Constants;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplUsers {
    public static void getUser(int userID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void postUser(String email, String password, final STCompletion.WithJsonToken completion) {
        String path = "/sign_up";

        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);
        StylerAPIBaseRequests.sendPost(path, params, completion);
    }

    public static void patchUser(File file, String name, int location, String birthday, String  gender, String profile, final STCompletion.WithJson completion) throws FileNotFoundException {
        // TODO:null
        String path = "/users/profile";

        RequestParams params = new RequestParams();
        params.put("name",name);
        params.put("location",location);

        params.put("birthday",birthday);
        params.put("gender",gender);
        params.put("profile",profile);

        if (file != null)
            params.put("photo", file, "image.jpeg");

        StylerAPIBaseRequests.patchUser(path,params,completion);

    }

    public static void deleteUser(String userID, final STCompletion.Base completion) {
        //TODO: not using on iOS
    }

    public static void getPostsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/posts";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getWatchesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID +"/watches";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getRepliesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/replies";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getLikesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID +"/likes";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getInterestsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/interests";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void postDeviceToken(String token, STCompletion.Base completion) {
        String path = "/token_reference";
        Map<String,String> params = new HashMap<>();
        params.put("version_num", Constants.appVersion());

        // TODO: device token
        if(token != null)
            params.put("device_token",token);

        params.put("device_platform","android");

        StylerAPIBaseRequests.sendGet(path, params, completion);
    }
}
