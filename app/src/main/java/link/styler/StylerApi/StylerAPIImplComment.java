package link.styler.StylerApi;

import android.util.Log;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplComment {
    public static void getComments(int postID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts/" + postID + "/comments";

        Map<String, String> param = new HashMap<String, String>();
        if (lastObjID != null)
            param.put("last_object_id", lastObjID.toString());

        StylerAPIBaseRequests.sendGet(path, param, completion);
    }

    public static void postComment(int postID, String text, Integer itemID, final STCompletion.WithJson completion) {
        Log.i("sendCM", "StylerAPIImplComment");

        String path = "/comments";
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.params = new RequestParams();
        Log.i("sendCM", "text: " + text);
        if (text != null) {
            request.params.put("text", text);
            request.params.put("post_id", postID);
        } else if (itemID != null) {
            request.params.put("item_id", itemID);
            request.params.put("post_id", postID);
        }
        request.httpMethod = STEnums.HTTPRequestType.POST;

        Log.i("sendCM", "request: text: " + request.params);

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(json, false, null);
                    return;
                }
                if (status == 409) {
                    Log.w("[STyler Warning]", "[\"message\": \"このアイテムは既におすすめ済みです\"]");
                    completion.onRequestComplete(json, false, null);
                    return;
                }
                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null);
                } else {
                    completion.onRequestComplete(json, true, null);
                }
            }
        });

    }

    public static void deleteComment(int commentID, final STCompletion.Base completion) {
        String path = "/comments/" + commentID;
        StylerAPIBaseRequests.sendDelete(path, completion);
    }
}
