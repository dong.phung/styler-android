package link.styler.StylerApi;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplNotification {
    public static void checkNewNotification(STCompletion.WithJsonNullableInt completion) {
        String path = "/check_notifications";
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }

    public static void getActivities(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/notifications";

        Map<String,String> param = new HashMap<>();
        if(lastObjID != null){
            param.put("last_object_id",lastObjID.toString());
        }


        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getMessageList(Integer lastObjID, final STCompletion.WithNullableInt completion) {
        //TODO: not using on iOS
    }
}
