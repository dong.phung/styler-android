package link.styler.StylerApi;

import android.graphics.Bitmap;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplShop {
    public static void getLocations(STCompletion.WithJson completion) {
        String path = "/shops";
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }

    public static void getShops(int locationID, String order, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/shops";
        Map<String,String> param = new HashMap<>();
        param.put("location",Integer.toString(locationID));
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        if(order != null)
            param.put("order",order);
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getShop(int shopID, final STCompletion.WithJson completion) {
        String path = "/shops/" +shopID;
        StylerAPIBaseRequests.sendGet(path,null,completion);
    }

    public static void getRepliesOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        // TODO: need to check [STReply] - array instead of json obj
        // TODO: not using on iOS
    }

    public static void getItemsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/shops/"+shopID+"/items";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);
    }

    public static void getItemsOfShopWithCategory(int shopID, int categoryID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/shops/"+shopID+"/items";
        Map<String,String> param = new HashMap<>();
        param.put("category_id",Integer.toString(categoryID));
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);

    }

    public static void getColleagueOfStaff(int staffID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        // TODO: need to check [STUser] - array instead of json obj
        // TODO: not using on iOS
    }

    public static void getStaffsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        String path =  "/shops/"+shopID+"/staffs";
        Map<String,String> param = new HashMap<>();
        if(lastObjID != null)
            param.put("last_object_id",lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path,param,completion);

    }

    public static void updateShop(int shopID, Map<String, Object> param, File photo, final STCompletion.WithJson completion) {
        String path = "/shops/"+shopID;
        RequestParams params = new RequestParams();
        for (Map.Entry<String,Object> entry: param.entrySet())
        {
            params.put(entry.getKey(),entry.getValue());
        }

        try
        {
            if(photo.exists())
                params.put("photo",photo,"image/jpg","image.jpeg");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        StylerAPIBaseRequests.sendPatch(path,null,params,completion);

    }
}
