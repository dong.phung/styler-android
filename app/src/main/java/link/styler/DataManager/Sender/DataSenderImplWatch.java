package link.styler.DataManager.Sender;

import android.provider.ContactsContract;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STPost;
import link.styler.models.STUser;
import link.styler.models.STWatch;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplWatch {

    public static void createWatch(final Integer postID, final STCompletion.Base completion){
        Log.i("Tan007-1","in createWatch");
        StylerApi.postWatch(postID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                //main_queue
                Log.i("Tan007 ","watch json" + json);
                final JSONObject watchObj = json.optJSONObject("watch");
                final STPost post         = DataLoader.getPost(postID);
                if(json == null && post == null && post == null){
                    completion.onRequestComplete(false,null);
                    return;
                }
                Realm realm = Realm.getDefaultInstance();
                int watchID = 0;
                final STWatch watch = new STWatch();
                watch.fillInfo(watchObj,post);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(watch);
                    }
                });
                watchID = watch.watchID;
                if(watch.watchID != 0){
                    final int finalWatchID = watchID;
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            post.watchID = finalWatchID;
                            post.watchesCount += 1;
                            realm.copyToRealmOrUpdate(post);
                        }
                    });
                }
                STUser me = realm.where(STUser.class).equalTo("userID",watch.watchID).findFirst();
                if(me != null)
                    me.watchCount += 1;
                if(watchID != 0)
                    completion.onRequestComplete(true,null);
                else
                    completion.onRequestComplete(false,null);
            }
        });
    }

    public static void deleteWatch(final Integer postId, final Integer watchID, final STCompletion.Base completion){
        StylerApi.deleteWatch(watchID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                //main_queue
                if(isSuccess){
                    final STPost post = DataLoader.getPost(postId);
                    if(post == null) {
                        completion.onRequestComplete(false,null);
                        return;
                    }

                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            post.watchID = 0;
                            post.watchesCount -= 1;
                            if(post.watchesCount < 0)
                                post.watchesCount = 0;

                            STWatch watch = DataLoader.getWatch(watchID);
                            if(watch != null){
                                STUser me = realm.where(STUser.class).equalTo("userID",watch.userID).findFirst();
                                me.watchCount -= 1;
                                if(me.watchCount < 0)
                                    me.watchCount = 0;

                                watch.deleteFromRealm();
                            }
                        }
                    });
                    completion.onRequestComplete(true,null);
                }
            }
        });
    }
}

