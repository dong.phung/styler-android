package link.styler.DataManager.Sender;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.LoginManager;
import link.styler.models.STComment;
import link.styler.models.STItem;
import link.styler.models.STPost;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplComment {

    public static void createComment(final Integer postID, final String text, final Integer itemID, final STCompletion.Base completion) {
        Log.i("sendCM", "DataSenderImplComment");
        StylerApi.postComment(postID, text, itemID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }
                Log.i("DataSenderImplComment","err:+ "+err);
                Log.i("DataSenderImplComment","json:+ "+json);
                if (json==null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                final int commentID = json.optInt("id");
                if (commentID != 0) {
                    Realm realm = Realm.getDefaultInstance();
                    final STPost stPost = DataLoader.getPost(postID);
                    if (stPost != null) {
                        final STComment stComment = new STComment();
                        stComment.commentID = commentID;
                        stComment.postID = postID;
                        stComment.text = text;
                        if (itemID != null) {
                            STItem item = DataLoader.getItemWithItemID(itemID);
                            if (item != null)
                                stComment.item = item;
                        }
                        STUser me = LoginManager.getsIntstance().me();
                        if (me != null)
                            stComment.user = me;

                        Date now = new Date();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        stComment.createdAt = format.format(now);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stComment);
                            }
                        });


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                stPost.commentsCount += 1;
                                //stPost.comment = stComment;
                                realm.copyToRealmOrUpdate(stPost);
                            }
                        });
                    }
                    completion.onRequestComplete(true, err);
                } else {
                    completion.onRequestComplete(false, err);
                }
            }
        });
    }

    public static void deleteComment(Integer commentID, final STCompletion.Base completion) {
        StylerApi.deleteComment(commentID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                completion.onRequestComplete(isSuccess, err);
            }
        });
    }


}
