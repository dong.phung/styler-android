package link.styler.DataManager.Sender;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STItem;
import link.styler.models.STLike;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplLike {
    public static void createLike(final Integer itemID, Integer replyID, final STCompletion.Base completion){
        StylerApi.postLike(itemID, replyID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if(err != null){
                    completion.onRequestComplete(false,err);
                    return;
                }

                //main_queue
                Log.i("DataSenderImplLike","json: "+json);
                    final JSONObject likeObj = json.optJSONObject("like");
                    final STItem item        = DataLoader.getItemWithItemID(itemID);
                    if(json == null && likeObj == null && item == null)
                    {
                        completion.onRequestComplete(false,null);
                        return;
                    }

                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            STLike like = new STLike();
                            like.fillInfo(likeObj,item);
                            realm.copyToRealmOrUpdate(like);

                            if(like.likeID != 0){
                                item.likeID = like.likeID;
                                item.likeCount += 1;
                                realm.copyToRealmOrUpdate(item);
                            }

                            STUser me = realm.where(STUser.class).equalTo("userID",like.userID).findFirst();
                            if(me != null){
                                me.likeCount += 1;
                                realm.copyToRealmOrUpdate(me);
                            }
                            completion.onRequestComplete(true,null);
                        }
                    });
            }
        });
    }

    public static void deleteLike(final Integer itemID, final Integer likeID, final STCompletion.Base completion){
        StylerApi.deleteLike(likeID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if(err != null) {
                    completion.onRequestComplete(false,err);
                    return;
                }
                //main_queue
                if(isSuccess){
                    final STItem item = DataLoader.getItemWithItemID(itemID);
                    if(item == null)
                    {
                        completion.onRequestComplete(false,null);
                        return;
                    }

                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            item.likeID = 0;
                            item.likeCount -= 1;
                            if(item.likeCount < 0)
                                item.likeCount = 0;

                            STLike like = DataLoader.getLike(likeID);
                            if(like != null){
                                STUser me = realm.where(STUser.class).equalTo("userID",like.userID).findFirst();
                                if(me != null){
                                    me.likeCount -= 1;
                                    if(me.likeCount < 0)
                                        me.likeCount = 0;
                                }
                                like.deleteFromRealm();
                            }
                        }
                    });

                    completion.onRequestComplete(true,null);
                }
            }
        });
    }

}
