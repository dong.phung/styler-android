package link.styler.DataManager.Sender;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplPost {
    public static void createPost(String text, Integer categoryID, final STCompletion.WithNullableInt completion) {
        if (categoryID == null)
            categoryID = 1;
        StylerApi.createPost(text, categoryID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                Logger.print("create post json "+json);
                if(json != null){
                    Realm realm = Realm.getDefaultInstance();
                    final STPostSort postSort =  new STPostSort();
                    final STPost post         =  new STPost();

                    post.fillInfo(realm,json.optJSONObject("post"));

                    final STUser user = new STUser(); //realm.createObject(STUser.class,post.user.userID);
                    JSONObject infoUser = json.optJSONObject("post").optJSONObject("user");
                    if(infoUser != null){
                        user.fillInfo(realm,infoUser);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(user);
                            }
                        });
                        postSort.userID = user.userID;
                    }

                    post.user = user;
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Logger.print("copy to Realm post");
                            realm.copyToRealmOrUpdate(post);
                        }
                    });

                    postSort.postID = post.postID;
                    postSort.newestTime = post.newestTime;
                    postSort.post   = post;
                    postSort.postSortKey = "{PostSortType.User.rawValue}-{"+post.postID+"}-{"+post.user.userID+"}";
                    postSort.type       = STEnums.PostSortType.User.toString();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Logger.print("copy to Realm post sort");
                            realm.copyToRealmOrUpdate(postSort);
                        }
                    });

                    completion.onRequestComplete(isSuccess,post.postID);
                }
                else {
                    completion.onRequestComplete(isSuccess,0);
                }
            }
        });
    }

    public static void editPost(int postID, String text, Integer categoryID, final STCompletion.Base completion){
        if(categoryID == null)
            categoryID = 1;
        StylerApi.patchPost(postID, text, categoryID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if(err != null){
                    completion.onRequestComplete(false,err);
                }
                if(json != null){
                    Realm realm = Realm.getDefaultInstance();
                    final STPost stPost = new STPost();
                    stPost.fillInfo(realm,json);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stPost);
                        }
                    });
                    completion.onRequestComplete(true,null);
                }
                else {
                    completion.onRequestComplete(false,err);
                }
            }
        });
    }

    public static void deletePost(final int postID, final STCompletion.Base completion){
        StylerApi.deletePost(postID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if(err != null)
                    completion.onRequestComplete(false,err);
                if(isSuccess){

                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            STPostSort stPostSort = realm.where(STPostSort.class).equalTo("postID",postID).findFirst();
                            stPostSort.deleteFromRealm();

                            STPost stPost = realm.where(STPost.class).equalTo("postID",postID).findFirst();
                            stPost.deleteFromRealm();
                        }
                    });

                    completion.onRequestComplete(isSuccess,err);
                }
                else {
                    completion.onRequestComplete(isSuccess,err);
                }
            }
        });
    }



}
