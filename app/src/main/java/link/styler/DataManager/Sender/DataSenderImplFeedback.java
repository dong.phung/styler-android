package link.styler.DataManager.Sender;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplFeedback {

    public static void createFeedBack(Integer rate, Integer replyID, final STCompletion.Base completion){
        StylerApi.createFeedback(rate, replyID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                completion.onRequestComplete(isSuccess,err);
            }
        });
    }
}
