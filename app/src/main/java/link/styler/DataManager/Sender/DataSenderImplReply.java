package link.styler.DataManager.Sender;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STItem;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplReply {

    public static void createReply (final STStructs.StylerReplyStruct replyStruct, final STCompletion.Base completion){
        StylerApi.createReply(replyStruct, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if(err != null) {
                    completion.onRequestComplete(isSuccess, err);
                    return;
                }
                if(isSuccess){
                    DataPreserver.savePost(replyStruct.postID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            completion.onRequestComplete(isSuccess,err);
                        }
                    });
                }
                else {
                    completion.onRequestComplete(isSuccess,err);
                }
            }
        });
    }

    //Note Bitmap
    public static void createReplyAndItem(STStructs.StylerReplyAndItemStruct replyAndItemStruct, File photo, final  STCompletion.WithNullableInt completion){
        Log.i("clickSend", "DataSenderImplReply createReplyAndItem");
        StylerApi.createReplyAndItem(replyAndItemStruct, photo, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                Log.i("clickSend", "DataSenderImplReply createReplyAndItem isSuccess: " + isSuccess);
                if(err != null){
                    completion.onRequestComplete(false,null);
                    return;
                }
                if(isSuccess) {
                    if(json != null && json.has("reply")){
                        Realm realm = Realm.getDefaultInstance();
                        JSONObject replyObj = json.optJSONObject("reply");
                        int itemID = 0;
                        if(!replyObj.has("shop")){
                            completion.onRequestComplete(false,null);
                            Log.i("clickSend", "DataSenderImplReply createReplyAndItem shop = null");
                            return;
                        }
                        if(!replyObj.has("user")){
                            completion.onRequestComplete(false,null);
                            Log.i("clickSend", "DataSenderImplReply createReplyAndItem user = null");

                            return;
                        }
                        if(!replyObj.has("item")){
                            completion.onRequestComplete(false,null);
                            Log.i("clickSend", "DataSenderImplReply createReplyAndItem item = null");
                            return;
                        }
                        final STShop stShop = new STShop();
                        stShop.fillInfo(realm,replyObj.optJSONObject("shop"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stShop);
                            }
                        });

                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm,replyObj.optJSONObject("user"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });

                        final STReply stReply = new STReply();
                        stReply.shop    = stShop;
                        stReply.user    = stUser;
                        stReply.fillInfo(json.optJSONObject("reply"),replyObj.optJSONObject("item").optInt("id"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stReply);
                            }
                        });

                        RealmResults<STReply> replies = realm.where(STReply.class).equalTo("postID",json.optJSONObject("reply").optInt("post_id")).findAll();
                        final STItem stItem = new STItem();
                        stItem.fillInfo(realm,replyObj.optJSONObject("item"));
                        stItem.replies.addAll(replies);
                        itemID = stItem.itemID;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItem);
                            }
                        });
                        if(itemID != 0){
                            completion.onRequestComplete(true,itemID);
                            Log.i("clickSend", "DataSenderImplReply createReplyAndItem isSuccess = true");
                        }else {
                            completion.onRequestComplete(false,null);
                            Log.i("clickSend", "DataSenderImplReply createReplyAndItem itemID = 0");
                        }
                    } else {
                        completion.onRequestComplete(false,null);
                        Log.i("clickSend", "DataSenderImplReply createReplyAndItem json = null || !json.has(\"reply\")");
                    }
                } else {
                    completion.onRequestComplete(false,null);
                }
            }
        });
    }

    public static void updateReply(STStructs.StylerUpdateItemAndReplyStruct stylerUpdateItemAndReplyStruct,File photo, final STCompletion.Base completion){
        StylerApi.patchReply(stylerUpdateItemAndReplyStruct, photo, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, final STError err) {
                Log.i("aaaaaasdsdsdww","22: "+isSuccess);
                //main_queue
                if(err != null){
                    completion.onRequestComplete(false,err);
                    return;
                }
                if (isSuccess) {
                    if (json != null && json.has("reply")) {
                        Realm realm = Realm.getDefaultInstance();
                        JSONObject reply = json.optJSONObject("reply");
                        if(!reply.has("shop")){
                            completion.onRequestComplete(false,null);
                            return;
                        }
                        if(!reply.has("user")){
                            completion.onRequestComplete(false,null);
                            return;
                        }
                        if(!reply.has("item")){
                            completion.onRequestComplete(false,null);
                            return;
                        }

                        final STShop stShop = new STShop();
                        stShop.fillInfo(realm,reply.optJSONObject("shop"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stShop);
                            }
                        });

                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm,reply.optJSONObject("user"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });

                        final STReply stReply = new STReply();
                        stReply.shop    = stShop;
                        stReply.user    = stUser;
                        stReply.fillInfo(json.optJSONObject("reply"),reply.optJSONObject("item").optInt("id"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stReply);
                            }
                        });

                        final STItem stItem = new STItem();
                        stItem.fillInfo(realm,reply.optJSONObject("item"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItem);
                            }
                        });

                        completion.onRequestComplete(true,null);

                    } else {
                        completion.onRequestComplete(false, null);
                    }
                }
                else {
                    completion.onRequestComplete(false,null);
                }
            }
        });
    }

    public static void updateItem(final Integer itemID, STStructs.StylerUpdateItemStruct stylerUpdateItemStruct, final STCompletion.Base completion){
        Map<String,String> sendParam = new HashMap<>();
        if(stylerUpdateItemStruct.brand != null){
            sendParam.put("brand",stylerUpdateItemStruct.brand);
        }
        if(stylerUpdateItemStruct.name != null){
            sendParam.put("name",stylerUpdateItemStruct.name);
        }
        if(stylerUpdateItemStruct.price != null){
            sendParam.put("price",stylerUpdateItemStruct.price);
        }
        if(stylerUpdateItemStruct.text != null){
            sendParam.put("text",stylerUpdateItemStruct.text);
        }
        if(stylerUpdateItemStruct.categoryID != null){
            sendParam.put("category_id",Integer.toString(stylerUpdateItemStruct.categoryID));
        }

        StylerApi.patchItem(itemID, sendParam, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if(err != null){
                    completion.onRequestComplete(false,err);
                    return;
                }
                if(isSuccess){
                    DataPreserver.saveItem(itemID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            if(err != null){
                                completion.onRequestComplete(false,err);
                                return;
                            }
                            if(isSuccess){
                                completion.onRequestComplete(true,null);
                            }
                            else {
                                completion.onRequestComplete(false,null);
                            }
                        }
                    });
                }
                else {
                    completion.onRequestComplete(false,null);
                }
            }
        });
    }
}
