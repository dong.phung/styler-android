package link.styler.DataManager.Sender;

import android.content.Intent;
import android.provider.ContactsContract;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.LoginManager;
import link.styler.models.STFollow;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplFollow {

    public static void createFollowStaff(final Integer staffID, final STCompletion.Base completion){
        StylerApi.createFollow(staffID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                //main_queue
                    final JSONObject followObj = json.optJSONObject("follow");
                    final STUser staff         = DataLoader.getUser(staffID);
                    if(json == null && followObj == null && staff == null){
                        completion.onRequestComplete(false,null);
                        return;
                    }

                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            int followID = 0;
                            STFollow stFollow = new STFollow();
                            stFollow.fillInfoStaff(followObj,staff);

                            STUser me = LoginManager.getsIntstance().me();
                            if(me != null && me.userID == stFollow.userID && me.userID != 0){
                                stFollow.user = me;
                            }
                            realm.copyToRealmOrUpdate(stFollow);

                            followID = stFollow.followID;
                            if(stFollow.followID != 0){
                                staff.followID = stFollow.followID;
                                staff.followerCount += 1;
                                realm.copyToRealmOrUpdate(staff);
                            }
                        }
                    });
                    completion.onRequestComplete(true,null);
            }
        });
    }

    public static void deleteFollowStaff(final Integer staffID, final Integer followID, final STCompletion.Base completion){
        StylerApi.deleteFollow(followID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                //main_queue
                if(isSuccess){
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            STUser staff = DataLoader.getUser(staffID);
                            if(staff != null){
                                staff.followID = 0;
                                staff.followerCount -=1;
                                if(staff.followerCount < 0)
                                    staff.followerCount = 0;
                            }

                            realm.copyToRealmOrUpdate(staff);

                            STFollow follow = DataLoader.getFollow(followID);
                            if(follow != null)
                            {
                                follow.deleteFromRealm();
                            }
                        }
                    });
                    completion.onRequestComplete(true,null);
                }
                else {
                    completion.onRequestComplete(false,err);
                }
            }
        });
    }
}
