package link.styler.DataManager.Sender;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.StylerApi.StylerApi;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplMessage {

    public static void createMesseage(Integer destinationUserID, String text, Integer itemID, final STCompletion.WithNullableInt completion){
        JSONObject param = new JSONObject();

        try {
            param.put("destination_user_id",destinationUserID);
            if(text != null){
                param.put("text",text);
            }
            if(itemID != null && itemID !=0){
                param.put("item_id",itemID);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        Log.i("DataSenderImplMessage", "param: " + param);

        StylerApi.postMessages(param, new STCompletion.WithNullableInt() {
            @Override
            public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                if(isSuccess){
                    completion.onRequestComplete(true,nullableInt);
                }
                else {
                    completion.onRequestComplete(false ,null);
                }
            }
        });
    }

}
