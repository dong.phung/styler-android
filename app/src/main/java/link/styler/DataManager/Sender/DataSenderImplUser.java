package link.styler.DataManager.Sender;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.FileNotFoundException;

import io.realm.Realm;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.DataSharing;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.models.STShop;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplUser {

    public static void signUp(String email, String password, final STCompletion.WithNullableIntString completion)
    {
        //Logger.method() : side ios
        StylerApi.postUser(email, password, new STCompletion.WithJsonToken() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final String token) {
                if(err != null){
                    Logger.print(err.toString());
                    completion.onRequestComplete(err,null,null);
                    return;
                }
                if(json != null){
                    Logger.print("Json = "+json);
                    saveMyDataToRealm(json, new STCompletion.WithNullableInt() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                            if(token != null && nullableInt != null) {
                                Logger.print("nullableInt "+ nullableInt);
                                Logger.print("nullableInt "+ token);
                                completion.onRequestComplete(null,nullableInt,token);
                            }
                            else {
                                completion.onRequestComplete(null,null,null);
                            }
                        }
                    });
                }
                else {
                    completion.onRequestComplete(err,null,null);
                    return;
                }
            }
        });
    }

    public static void login(final String email, final String password, final STCompletion.WithNullableIntString completion){
        //Logger.method() : side ios
        StylerApi.login(email, password, new STCompletion.WithJsonToken() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, final STError err, final String token) {
                if(err != null){
                    completion.onRequestComplete(err,null,null);
                    return;
                }
                if(json != null){
                    Log.i("Tan006 ", isSuccess + "");
                    Log.i("Tan006 ", token + "");
                    Log.i("Tan006 ", json + "");
                    StylerApi.getAccessTokenBlock = token;
                    DataSharing.getsIntstance().setAccessToken(token);
                    DataSharing.getsIntstance().setUserEmail(email);
                    DataSharing.getsIntstance().setUserPassword(password);
                    saveMyDataToRealm(json, new STCompletion.WithNullableInt() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                            Logger.print(token);
                            Logger.print(nullableInt.toString());
                            if(token != null && nullableInt != null){ //Success
                                Logger.print("Success");
                                completion.onRequestComplete(null,nullableInt,token);
                            }
                            else {//fail
                                Logger.print("Fail");
                                completion.onRequestComplete(err,null,null);
                            }
                        }
                    });
                }
                else {
                    completion.onRequestComplete(err,null,null);
                }
            }
        });
    }

    public static void facebookLogin(String accessToken, final STCompletion.WithNullableIntString completion){
        //Logger.method() : side ios
        StylerApi.facebookLogin(accessToken, new STCompletion.WithJsonToken() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, final STError err, final String token) {
                if(err != null){
                    completion.onRequestComplete(err,null,null);
                    return;
                }
                if(json != null){
                    saveMyDataToRealm(json, new STCompletion.WithNullableInt() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                            if(token != null && nullableInt != null){
                                completion.onRequestComplete(null,nullableInt,token);
                            }
                            else {
                                completion.onRequestComplete(err,null,null);
                            }
                        }
                    });
                }
                else {
                    completion.onRequestComplete(err,null,null);
                }
            }
        });
    }

    public static void refreshToken(final STCompletion.WithNullableIntString completion){
        //Logger.method() : side ios
        StylerApi.refreshToken(new STCompletion.WithJsonToken() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, final STError err, final String token) {
                if(err != null){
                    completion.onRequestComplete(err,null,null);
                    return;
                }
                if(json != null){
                    saveMyDataToRealm(json, new STCompletion.WithNullableInt() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                            if(token != null && nullableInt != null){
                                completion.onRequestComplete(null,nullableInt,token);
                            }
                            else {
                                completion.onRequestComplete(err,null,null);
                            }
                        }
                    });
                }
                else {
                    completion.onRequestComplete(err,null,null);
                }
            }
        });
    }

    public static void updateUser(File photo, final String name, Integer location, String birthday, String gender, String profile, final STCompletion.Base completion){
        //Logger.method() : side ios
        StylerApi.patchUser(photo,name,location,birthday,gender,profile, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (json == null) {
                    completion.onRequestComplete(false, err);
                }

                if (json.has("user")) {
                    Realm realm = Realm.getDefaultInstance();
                    Integer meID = LoginManager.getsIntstance().me().userID;
                    if (meID != null) {
                        final STUser stUser = realm.where(STUser.class).equalTo("userID", meID).findFirst();
                        if (stUser != null) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    stUser.fillInfo(realm, json.optJSONObject("user"));
                                }
                            });
                        }
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });

                        completion.onRequestComplete(true, null);
                    } else {
                        completion.onRequestComplete(false, err);
                    }
                }
                else
                    completion.onRequestComplete(false,err);
            }
        });
    }


    private static void saveMyDataToRealm(final JSONObject json, final STCompletion.WithNullableInt completion)
    {
        Logger.print("saveMyDataToRealm");
        Realm realm = Realm.getDefaultInstance();
        int shopID = 0;
        if(json.has("shop")){
            shopID = json.optJSONObject("shop").optInt("id");
            final STShop stShop = new STShop();
            stShop.fillInfo(realm,json.optJSONObject("shop"));
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stShop);
                }
            });
        }

        if(json.has("user")){
            final STUser stUser = new STUser();
            stUser.fillInfo(realm,json.optJSONObject("user"));
            stUser.shopID =  shopID;
            STShop shop = realm.where(STShop.class).equalTo("shopID",shopID).findFirst();
            if(shop != null){
                stUser.shop = shop;
            }

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stUser);
                }
            });
        }

        if(json.has("user")){
            completion.onRequestComplete(true,json.optJSONObject("user").optInt("id"));
        }
        else{
            completion.onRequestComplete(false,null);
        }
    }
}
