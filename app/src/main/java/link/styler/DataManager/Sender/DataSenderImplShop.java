package link.styler.DataManager.Sender;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.LoginManager;
import link.styler.models.STShop;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplShop {

    public static void updateShop(File photo, STStructs.StylerShopStruct param, final STCompletion.Base completion){
        //Logger.method() [ios side]
        int shopID = LoginManager.getsIntstance().shopID;
        if(shopID != 0)
        {
            Map<String,Object> sendParam = new HashMap<>();
            if(param.name != null)
                sendParam.put("name",param.name);
            if(param.profile != null)
                sendParam.put("profile",param.profile);
            if(param.address != null)
                sendParam.put("address",param.address);
            if(param.tel != null)
                sendParam.put("tel",param.tel);
            if(param.website != null)
                sendParam.put("website",param.website);
            if(param.station != null)
                sendParam.put("station",param.station);
            if(param.location != null)
                sendParam.put("location",param.location);
            if(param.business_hours != null)
                sendParam.put("business_hours",param.business_hours);

            StylerApi.updateShop(shopID, sendParam, photo, new STCompletion.WithJson() {
                @Override
                public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                    Log.i("updateShop","isSuccess: " + isSuccess);
                    Log.i("updateShop","json: " + json);
                    Log.i("updateShop","err: " + err);
                    if(err != null)
                    {
                        completion.onRequestComplete(false,err);
                        return;
                    }
                    if(json == null)
                    {
                        completion.onRequestComplete(false,null);
                        return;
                    }
                    final JSONObject shop = json.optJSONObject("shop");
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            STShop stShop = new STShop();
                            stShop.fillInfo(realm,shop);
                            realm.copyToRealmOrUpdate(stShop);
                            }
                        });
                    completion.onRequestComplete(true,null);
                }
            });
        }
        else {
            completion.onRequestComplete(false,null);
        }
    }

}
