package link.styler.DataManager.Sender;

import org.json.JSONObject;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.LoginManager;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplSetting {

    public static void sendNotificationSetting(Boolean pushNotification, Boolean mailNotification, Boolean mailMagazine){
        StylerApi.sendNotificationSetting(pushNotification,mailNotification,mailMagazine);
    }

    public static void sendEmail(final String email, final STCompletion.Base completion){
        StylerApi.sendEmail(email, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if(isSuccess){
                    LoginManager.getsIntstance().setUserEmail(email);
                }
                completion.onRequestComplete(isSuccess,err);
            }
        });
    }

    public static void sendPassWord(String currentPassword, String newPassword, String confirmPassword, final STCompletion.Base completion){
        StylerApi.sendPassword(currentPassword, newPassword, confirmPassword, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                completion.onRequestComplete(isSuccess,err);
            }
        });
    }
}
