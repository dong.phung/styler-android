package link.styler.DataManager.Sender;

import org.json.JSONObject;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplReport {

    public static void sendReport(STEnums.ReportType type, Integer objectID, String text, final STCompletion.Base completion){
        StylerApi.postReport(type, objectID, text, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                completion.onRequestComplete(isSuccess,err);
            }
        });
    }
}
