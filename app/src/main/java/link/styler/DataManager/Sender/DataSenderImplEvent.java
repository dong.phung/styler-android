package link.styler.DataManager.Sender;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Struct;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.models.STShop;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplEvent {

    public static void createEvent(STStructs.StylerCreateEventParam stylerCreateEventParam, final STCompletion.WithLastObjectID completion) {
        StylerApi.createEvent(stylerCreateEventParam, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                //main_queue
                Log.i("DataSenderImplEvent", "createEvent json: " + json);
                final JSONObject eventObj = json.optJSONObject("event");
                final JSONObject shopObj = eventObj.optJSONObject("shop");

                if (json == null && eventObj == null && shopObj == null) {
                    completion.onRequestComplete(false, err, null);
                    return;
                }

                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        int eventID = 0;
                        STEvent stEvent = new STEvent();
                        stEvent.fillInfo(realm, eventObj, shopObj.optInt("id"));
                        realm.copyToRealmOrUpdate(stEvent);
                        eventID = stEvent.eventID;

                        if (DataLoader.getShop(shopObj.optInt("id")) == null) {
                            STShop stShop = new STShop();
                            stShop.fillInfo(realm, shopObj);
                            realm.copyToRealmOrUpdate(stShop);
                        }
                        completion.onRequestComplete(true, null, eventID);
                    }
                });
            }
        });
    }

    public static void patchEvent(final STStructs.StylerPatchEventParam param, final STCompletion.Base completion) {
        StylerApi.patchEvent(param, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {

                if (err != null) {
                    completion.onRequestComplete(isSuccess, err);
                    return;
                }
                if (isSuccess) {
                    //main_queue
                    DataPreserver.saveEvent(param.eventID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            if (err != null) {
                                completion.onRequestComplete(isSuccess, err);
                                return;
                            } else {
                                completion.onRequestComplete(isSuccess, null);
                                return;
                            }

                        }
                    });
                } else {
                    completion.onRequestComplete(isSuccess, null);
                    return;
                }
            }
        });
    }

    public static void deleteEvent(final Integer eventID, final STCompletion.Base completion) {
        StylerApi.deleteEvent(eventID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if (err != null) {
                    completion.onRequestComplete(isSuccess, err);
                    return;
                }
                if (isSuccess) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            STEvent stEvent = realm.where(STEvent.class).equalTo("eventID", eventID).findFirst();
                            STEventSort stEventSort = realm.where(STEventSort.class).equalTo("eventID", eventID).findFirst();
                            stEvent.deleteFromRealm();
                            stEventSort.deleteFromRealm();
                        }
                    });
                    completion.onRequestComplete(true, null);
                } else {
                    completion.onRequestComplete(isSuccess, null);
                }
            }
        });
    }
}
