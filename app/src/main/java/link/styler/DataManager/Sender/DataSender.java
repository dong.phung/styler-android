package link.styler.DataManager.Sender;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STStructs;

/**
 * Created by Tan Nguyen on 2/7/17.
 */

public class DataSender {
    //  DataSender


    //  DataSender+User
    public static void signUp(String email, String password, STCompletion.WithNullableIntString completion) {
        DataSenderImplUser.signUp(email, password, completion);
    }

    public static void login(String email, String password, STCompletion.WithNullableIntString completion) {
        DataSenderImplUser.login(email, password, completion);
    }

    public static void facebookLogin(String accessToken, STCompletion.WithNullableIntString completion) {
        DataSenderImplUser.facebookLogin(accessToken, completion);
    }

    public static void refreshToken(STCompletion.WithNullableIntString completion) {
        DataSenderImplUser.refreshToken(completion);
    }

    public static void updateUser(File photo, String name, Integer location, String birthday, String gender, String profile, STCompletion.Base completion) {
        DataSenderImplUser.updateUser(photo, name, location, birthday, gender, profile, completion);
    }

    //  DataSender+Post
    public static void createPost(String text, Integer categoryID, STCompletion.WithNullableInt completion) {
        DataSenderImplPost.createPost(text, categoryID, completion);
    }

    public static void editPost(Integer postID, String text, Integer categoryID, STCompletion.Base completion) {
        DataSenderImplPost.editPost(postID, text, categoryID, completion);
    }

    public static void deletePost(Integer postID, STCompletion.Base completion) {
        DataSenderImplPost.deletePost(postID, completion);
    }

    //DataSender+Reply
    public static void createReply(STStructs.StylerReplyStruct replyStruct, STCompletion.Base completion) {
        DataSenderImplReply.createReply(replyStruct, completion);
    }

    //Note Bitmap
    public static void createReplyAndItem(STStructs.StylerReplyAndItemStruct replyAndItemStruct, File photo, STCompletion.WithNullableInt completion) {
        Log.i("clickSend", "DataSender");
        DataSenderImplReply.createReplyAndItem(replyAndItemStruct, photo, completion);
    }

    public static void updateReply(STStructs.StylerUpdateItemAndReplyStruct stylerUpdateItemAndReplyStruct, File photo, STCompletion.Base completion) {
        DataSenderImplReply.updateReply(stylerUpdateItemAndReplyStruct, photo, completion);
    }

    public static void updateItem(Integer itemID, STStructs.StylerUpdateItemStruct stylerUpdateItemStruct, STCompletion.Base completion) {
        DataSenderImplReply.updateItem(itemID, stylerUpdateItemStruct, completion);
    }

    //  DataSender+Conversation

    //  DataSender+Message
    public static void createMesseage(Integer destinationUserID, String text, Integer itemID, final STCompletion.WithNullableInt completion) {
        DataSenderImplMessage.createMesseage(destinationUserID, text, itemID, completion);
    }

    //  DataSender+Event
    public static void createEvent(STStructs.StylerCreateEventParam stylerCreateEventParam, final STCompletion.WithLastObjectID completion) {
        DataSenderImplEvent.createEvent(stylerCreateEventParam, completion);
    }

    public static void patchEvent(final STStructs.StylerPatchEventParam param, final STCompletion.Base completion) {
        DataSenderImplEvent.patchEvent(param, completion);
    }

    public static void deleteEvent(final Integer eventID, final STCompletion.Base completion) {
        DataSenderImplEvent.deleteEvent(eventID, completion);
    }

    //  DataSender+Follow
    public static void createFollowStaff(final Integer staffID, final STCompletion.Base completion) {
        DataSenderImplFollow.createFollowStaff(staffID, completion);
    }

    public static void deleteFollowStaff(final Integer staffID, final Integer followID, final STCompletion.Base completion) {
        DataSenderImplFollow.deleteFollowStaff(staffID, followID, completion);
    }

    //  DataSender+Comment
    public static void createComment(final Integer postID, final String text, final Integer itemID, final STCompletion.Base completion) {
        Log.i("sendCM", "DataSender");
        DataSenderImplComment.createComment(postID, text, itemID, completion);
    }

    public static void deleteComment(Integer commentID, final STCompletion.Base completion) {
        DataSenderImplComment.deleteComment(commentID, completion);
    }

    //  DataSender+Watch
    public static void createWatch(Integer postID, STCompletion.Base completion) {
        DataSenderImplWatch.createWatch(postID, completion);
    }

    public static void deleteWatch(Integer postId, Integer watchID, STCompletion.Base completion) {
        DataSenderImplWatch.deleteWatch(postId, watchID, completion);
    }

    //  DataSender+Like
    public static void createLike(Integer itemID, Integer replyID, STCompletion.Base completion) {
        DataSenderImplLike.createLike(itemID, replyID, completion);
    }

    public static void deleteLike(Integer itemID, Integer likeID, STCompletion.Base completion) {
        DataSenderImplLike.deleteLike(itemID, likeID, completion);
    }

    //  DataSender+Feedback
    public static void createFeedBack(Integer rate, Integer replyID, final STCompletion.Base completion) {
        DataSenderImplFeedback.createFeedBack(rate, replyID, completion);
    }

    //  DataSender+Interest
    public static void createInterest(final Integer eventID, final STCompletion.WithSTEvent completion) {
        DataSenderImplInterest.createInterest(eventID, completion);
    }

    public static void deleteInterest(final Integer eventID, Integer interestID, final STCompletion.WithSTEvent completion) {
        DataSenderImplInterest.deleteInterest(eventID, interestID, completion);
    }

    //  DataSender+Shop
    public static void updateShop(File photo, STStructs.StylerShopStruct param, final STCompletion.Base completion) {
        DataSenderImplShop.updateShop(photo, param, completion);
    }

    //  DataSender+Report
    public static void sendReport(STEnums.ReportType type, Integer objectID, String text, final STCompletion.Base completion){
        DataSenderImplReport.sendReport(type, objectID, text, completion);
    }

    //  DataSender+Setting
    public static void sendNotificationSetting(Boolean pushNotification, Boolean mailNotification, Boolean mailMagazine){
        DataSenderImplSetting.sendNotificationSetting(pushNotification, mailNotification, mailMagazine);
    }

    public static void sendEmail(final String email, final STCompletion.Base completion){
        DataSenderImplSetting.sendEmail(email, completion);
    }

    public static void sendPassWord(String currentPassword, String newPassword, String confirmPassword, final STCompletion.Base completion){
        DataSenderImplSetting.sendPassWord(currentPassword, newPassword, confirmPassword, completion);
    }
}
