package link.styler.DataManager.Sender;

import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.models.STUser;

/**
 * Created by macOS on 2/9/17.
 */

public class DataSenderImplInterest {

    public static void createInterest(final Integer eventID, final STCompletion.WithSTEvent completion){
        StylerApi.createInterest(eventID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if(err != null){
                    completion.onRequestComplete(null);
                    return;
                }
                else {
                    //main_queue
                    if(json != null){
                           final Integer interestID = json.optInt("id");
                            if(interestID != null){
                                Realm realm = Realm.getDefaultInstance();
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        STEvent event = new STEvent();
                                        STEvent stEvent = realm.where(STEvent.class).equalTo("eventID",eventID).findFirst();
                                        if(stEvent != null){
                                            stEvent.interestID = interestID;
                                            stEvent.interestsCount += 1;
                                            realm.copyToRealmOrUpdate(stEvent);
                                            event = stEvent;
                                        }

                                        Integer meID = link.styler.Utils.LoginManager.getsIntstance().me().userID;
                                        if(meID != null) {
                                            String eventSortType = STEnums.EventSortType.Interest.toString();
                                            STEventSort stEventSort = new STEventSort();
                                            stEventSort.eventSortKey = eventSortType +"-" +stEvent.eventID;
                                            stEventSort.type        = eventSortType;
                                            stEventSort.eventID     = stEvent.eventID;
                                            stEventSort.event       = stEvent;
                                            stEventSort.startDate   = stEvent.startDate;
                                            stEventSort.shopID      = stEvent.shopID;
                                            stEventSort.userID      = meID;

                                            realm.copyToRealmOrUpdate(stEventSort);
                                        }

                                        completion.onRequestComplete(event);
                                        return;
                                    }
                                });
                            }
                            else {
                                completion.onRequestComplete(null);
                                return;
                            }
                    }
                }
            }
        });
    }

    public static void deleteInterest(final Integer eventID, Integer interestID, final STCompletion.WithSTEvent completion){
        StylerApi.deleteInterest(interestID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if(err != null){
                    completion.onRequestComplete(null);
                    return;
                }
                else {
                    //main_queue
                    if(isSuccess){
                        Realm realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                STEvent event = new STEvent();
                                STEvent stEvent = realm.where(STEvent.class).equalTo("eventID",eventID).findFirst();
                                if(stEvent != null){
                                    stEvent.interestID = 0;
                                    stEvent.interestsCount -= 1;
                                    if(stEvent.interestsCount < 0)
                                        stEvent.interestsCount = 0;

                                    realm.copyToRealmOrUpdate(stEvent);
                                    event = stEvent;

                                    String filter = STEnums.EventSortType.Interest.toString() +"-"+stEvent.eventID;
                                    STEventSort eventSort = realm.where(STEventSort.class).equalTo("eventSortKey", filter).findFirst();
                                    if(eventSort != null){
                                        eventSort.deleteFromRealm();
                                    }

                                    completion.onRequestComplete(event);
                                    return;
                                }
                            }
                        });
                    }
                    else {
                        completion.onRequestComplete(null);
                        return;
                    }
                }
            }
        });

    }

}
