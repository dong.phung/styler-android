package link.styler.DataManager.Preserver;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.models.STUserSort;


/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplEvent {

    public static void saveFetureEvents(Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion) {
        StylerApi.getFeatureEvents(lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json != null) {
                    JSONArray events = json.optJSONArray("events");
                    if (events != null) {
                        saveEventsToRealm(0, events, STEnums.EventSortType.Feture.getEventSortType(), shouldDelete, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                if (isSuccess) {
                                    completion.onRequestComplete(true, null, nullableInt);
                                    return;
                                } else {
                                    completion.onRequestComplete(false, null, null);
                                    return;
                                }
                            }
                        });
                    } else {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                } else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

    public static void saveOngoingEvents(Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion) {
        StylerApi.getOngoingEvents(lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json != null) {
                    JSONArray events = json.optJSONArray("events");
                    if (events != null) {
                        saveEventsToRealm(0, events, STEnums.EventSortType.Ongoing.getEventSortType(), shouldDelete, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                if (isSuccess) {
                                    completion.onRequestComplete(true, null, nullableInt);
                                    return;
                                } else {
                                    completion.onRequestComplete(false, null, null);
                                    return;
                                }
                            }
                        });
                    } else {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                } else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });

    }

    public static void saveEventsOfShop(Integer shopID, Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion) {
        StylerApi.getEventsOfShop(shopID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json != null) {
                    JSONArray events = json.optJSONArray("events");
                    if (events != null) {
                        saveEventsToRealm(0, events, STEnums.EventSortType.Shop.getEventSortType(), shouldDelete, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                if (isSuccess) {
                                    completion.onRequestComplete(true, null, nullableInt);
                                    return;
                                } else {
                                    completion.onRequestComplete(false, null, null);
                                    return;
                                }
                            }
                        });
                    } else {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                } else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

    public static void saveInterestsOfUser(final Integer userID, final Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion) {
        StylerApi.getInterestsOfUser(userID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                Log.i("DataPreserver.Event01", "json: " + json);
                Log.i("DataPreserver.Event02", "isSuccess: " + isSuccess);
                Log.i("DataPreserver.Event03", "err: " + err);
                Log.i("DataPreserver.Event04", "nullableInt: " + nullableInt);

                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null) {
                    JSONArray events = json.optJSONArray("interests");
                    Realm realm = Realm.getDefaultInstance();
                    if (events != null) {
                        Log.i("DataPreserver.Event05", "events: " + events);
                        for (int i = 0; i < events.length(); i++) {
                            JSONObject event = events.optJSONObject(i);
                            if (event.has("shop") && event.has("event")) {
                                JSONObject shop = event.optJSONObject("shop");
                                JSONObject eventObj = event.optJSONObject("event");
                                final STShop stShop = new STShop();
                                stShop.fillInfo(realm, shop);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stShop);
                                    }
                                });
                                STEvent stEvent = new STEvent();
                                stEvent.fillInfo(realm, eventObj, stShop.shopID);
                                stEvent.shop = stShop;
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stShop);
                                    }
                                });

                                final STEventSort stEventSort = new STEventSort();
                                stEventSort.eventSortKey = STEnums.EventSortType.Interest.getEventSortType() + "-" + (stEvent.eventID);
                                stEventSort.type = STEnums.EventSortType.Interest.getEventSortType();
                                stEventSort.eventID = stEvent.eventID;
                                stEventSort.event = stEvent;
                                stEventSort.startDate = stEvent.startDate;
                                stEventSort.shopID = shop.optInt("id");
                                stEventSort.userID = userID;
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stEventSort);
                                    }
                                });
                            }
                        }
                        completion.onRequestComplete(true,null, nullableInt);

                    } else {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                } else {
                    completion.onRequestComplete(false, null, null);
                }
            }
        });
    }

    private static void saveEventsToRealm(final Integer userID, final JSONArray events, final String eventSortType, final boolean shouldDelete, final STCompletion.Base completion) {
        Realm realm = Realm.getDefaultInstance();
        if (shouldDelete) {
            final RealmResults<STEventSort> realmResults = realm.where(STEventSort.class).equalTo("type", eventSortType).findAll();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realmResults.deleteAllFromRealm();
                    Log.i("Tan020", "delete saveEventsToRealm");
                }
            });
        }
        for (int i = 0; i < events.length(); i++) {

            JSONObject event = events.optJSONObject(i);
            JSONObject shop = event.optJSONObject("shop");
            if (shop == null) {
                completion.onRequestComplete(false, null);
                return;
            }
            final STShop stShop = new STShop();
            stShop.fillInfo(realm, shop);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stShop);
                }
            });
            final STEvent stEvent = new STEvent();
            stEvent.fillInfo(realm, event, stShop.shopID);
            stEvent.shop = stShop;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stEvent);
                }
            });
            final STEventSort stEventSort = new STEventSort();
            stEventSort.eventSortKey = eventSortType + "-" + stEvent.eventID;
            stEventSort.type = eventSortType;
            stEventSort.eventID = stEvent.eventID;
            stEventSort.event = stEvent;
            stEventSort.startDate = stEvent.startDate;
            stEventSort.shopID = shop.optInt("id");
            stEventSort.userID = userID;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stEventSort);
                }
            });
        }
        completion.onRequestComplete(true, null);
        return;
    }

    public static void saveEvent(final Integer eventID, final STCompletion.Base completion) {
        StylerApi.getEventDetail(eventID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                }
                if (json != null) {
                    //// TODO: 2/14/17 chuyen ve main thread
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject event = json.optJSONObject("event");
                    if (event == null) {
                        completion.onRequestComplete(false, null);
                        return;
                    }
                    JSONObject shop = event.optJSONObject("shop");
                    if (shop == null) {
                        completion.onRequestComplete(false, null);
                        return;
                    }
                    final STShop stShop = new STShop();
                    stShop.fillInfo(realm, shop);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stShop);
                        }
                    });
                    JSONArray interestUsers = event.optJSONArray("interest_users");
                    if (interestUsers != null) {
                        RealmResults<STUserSort> realmResults = realm.where(STUserSort.class).equalTo("eventID", eventID).findAll();
                        realmResults = realmResults.where().equalTo("type", STEnums.UserSortType.Interest.getUserSortType()).findAll();
                        final RealmResults<STUserSort> finalRealmResults = realmResults;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                finalRealmResults.deleteAllFromRealm();
                                Log.i("Tan020", "delete saveEvent");
                            }
                        });
                        for (int i = 0; i < interestUsers.length(); i++) {
                            JSONObject interestUser = interestUsers.optJSONObject(i);
                            final STUser stUser = new STUser();
                            stUser.fillInfo(realm, interestUser);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stUser);
                                }
                            });
                            final STUserSort stUserSort = new STUserSort();
                            stUserSort.userSortKey = STEnums.UserSortType.Interest.getUserSortType() + "-" + stUser.userID;
                            stUserSort.userID = interestUser.optInt("id");
                            stUserSort.user = stUser;
                            stUserSort.type = STEnums.UserSortType.Interest.getUserSortType();
                            stUserSort.eventID = eventID;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stUserSort);
                                }
                            });
                        }
                    }
                    RealmResults<STUserSort> interestUsersResult = realm.where(STUserSort.class).equalTo("type", STEnums.UserSortType.Interest.getUserSortType()).findAll();
                    interestUsersResult = interestUsersResult.where().equalTo("eventID", eventID).findAll();
                    final STEvent stEvent = new STEvent();
                    stEvent.fillInfo(realm, event, shop.optInt("id"));
                    stEvent.shop = stShop;
                    stEvent.interestUsers.addAll(interestUsersResult);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stEvent);
                        }
                    });
                    completion.onRequestComplete(true, null);
                } else {
                    completion.onRequestComplete(false, null);
                }
            }
        });

    }
}
