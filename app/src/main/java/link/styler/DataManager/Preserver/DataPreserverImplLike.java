package link.styler.DataManager.Preserver;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.models.STItem;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplLike {
    public static void saveLikeID(final Integer itemID, final Integer likeID, final STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        final STItem stItem = realm.where(STItem.class).equalTo("itemID", itemID).findFirst();
        if (stItem != null){
            stItem.likeID = likeID;
            stItem.likeCount += 1;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stItem);
                }
            });
            completion.onRequestComplete(true, null);
        }else {
            completion.onRequestComplete(false, null);
        }
    }

    public static void deleteLikeID(final Integer itemID, final STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        final STItem stItem = realm.where(STItem.class).equalTo("itemID", itemID).findFirst();
        if (stItem != null){
            stItem.likeID = 0;
            stItem.likeCount -= 1;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stItem);
                }
            });
            completion.onRequestComplete(true, null);
        }else {
            completion.onRequestComplete(false, null);
        }
    }
}
