package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STTag;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplTag {
    public static void saveTags(Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getTags(lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }

                JSONArray tags = json.optJSONArray("tags");
                if (tags== null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                //// TODO: 2/14/17 chuyen ve main thread
                saveTagsToRealm(tags, shouldDelete);
                if(json.has("last_object_id")){
                    int lastObjectID = json.optInt("last_object_id");
                    completion.onRequestComplete(true, null, lastObjectID);
                    return;
                }
            }
        });
    }

    private static void saveTagsToRealm(final JSONArray tags, final boolean shouldDelete){
        Realm realm = Realm.getDefaultInstance();
        if (shouldDelete){
            final RealmResults<STTag> realmResults = realm.where(STTag.class).findAll();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realmResults.deleteAllFromRealm();
                    Log.i("Tan020","delete saveTagsToRealm");
                }
            });
        }
        for (int i=0; i<tags.length(); i++){
            JSONObject tag = null;
            tag = tags.optJSONObject(i);
            final STTag stTag = new STTag();
            stTag.tagID = tag.optInt("id");
            stTag.name  = tag.optString("name");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stTag);
                }
            });
        }
    }
}
