package link.styler.DataManager.Preserver;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.models.STPost;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplWatch {
    public static void createWatchForPost(final Integer postID, final Integer watchID, STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        if (DataLoader.getPost(postID) != null){
            STPost post = new STPost();
            post = DataLoader.getPost(postID);
            post.watchesCount +=1;
            post.watchID = watchID;
            final STPost finalPost = post;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(finalPost);
                }
            });
            completion.onRequestComplete(true, null);
        }else {
            completion.onRequestComplete(false, null);
        }
    }

    public static void deleteWatchInPostWithWatchID(final Integer postID, STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        if (DataLoader.getPost(postID) != null){
            STPost post = new STPost();
            post = DataLoader.getPost(postID);
            post.watchesCount -=1;
            post.watchID = 0;
            final STPost finalPost = post;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(finalPost);
                }
            });
            completion.onRequestComplete(true, null);
        }else {
            completion.onRequestComplete(false, null);
        }
    }
}
