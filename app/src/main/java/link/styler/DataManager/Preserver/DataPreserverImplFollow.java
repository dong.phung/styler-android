package link.styler.DataManager.Preserver;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.models.STUser;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplFollow {
    public static void saveFollow(final Integer userID, final Integer followID, final STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        final STUser user = realm.where(STUser.class).equalTo("userID", userID).findFirst();
        if (user == null){
            completion.onRequestComplete(false, null);
            return;
        }
        user.followID = followID;
        user.followCount +=1;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
            }
        });
        completion.onRequestComplete(true, null);
    }

    public static void deleteFollow(final Integer followID, final STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        final STUser user = realm.where(STUser.class).equalTo("followID", followID).findFirst();
        if (user == null){
            completion.onRequestComplete(false, null);
            return;
        }
        user.followID = 0;
        user.followCount -=1;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
            }
        });
        completion.onRequestComplete(true, null);
    }
}
