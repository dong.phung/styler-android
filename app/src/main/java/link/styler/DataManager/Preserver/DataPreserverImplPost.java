package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.models.STReply;
import link.styler.models.STUser;
import link.styler.models.STWatch;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplPost {

    public static void savePosts(final Integer lastObjectID, final String postSortType, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        switch (postSortType){
            case "popular":
                Log.i("tan02","popular");
                StylerApi.getPosts(lastObjectID, new STCompletion.WithJsonNullableInt() {
                    @Override
                    public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt){
                        if(isSuccess){
                            if (json == null)
                            {
                                completion.onRequestComplete(false, err, null);
                            }
                            Log.i("tan02","json " + json);
                            if (json.has("posts"))
                            {
                                JSONArray posts = json.optJSONArray("posts");
                                if (posts.length()==0){
                                    completion.onRequestComplete(false, null, null);
                                }
                                savePostsToRealm(postSortType, 0, shouldDelete, posts, new STCompletion.Base() {
                                    @Override
                                    public void onRequestComplete(boolean isSuccess, STError err) {
                                        if (isSuccess){
                                            completion.onRequestComplete(true, null, nullableInt);
                                        }else{
                                            completion.onRequestComplete(false, null, nullableInt);
                                        }
                                    }
                                });
                            }
                        }else {
                            completion.onRequestComplete(false,err,null);
                        }
                    }
                });
                break;

            case "latest":
                Log.i("tan02","latest");
                StylerApi.getLatestPosts(lastObjectID, new STCompletion.WithJsonNullableInt() {
                        @Override
                        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt){
                            if(isSuccess){
                                JSONArray posts = json.optJSONArray("posts");
                                if(posts.length()==0){
                                    completion.onRequestComplete(false, null, null);
                                    return;
                                }
                                savePostsToRealm(postSortType, 0, shouldDelete, posts, new STCompletion.Base() {
                                    @Override
                                    public void onRequestComplete(boolean isSuccess, STError err) {
                                        if(isSuccess){
                                            completion.onRequestComplete(true, null, nullableInt);
                                        }else {
                                            completion.onRequestComplete(false, null, nullableInt);
                                        }
                                    }
                                });

                            }else {
                                completion.onRequestComplete(false, err, null);
                            }
                        }
                    });
                break;

            case "watch":
                StylerApi.getWatches(lastObjectID, new STCompletion.WithJsonNullableInt() {
                    @Override
                    public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                        if(isSuccess){
                            JSONArray posts = json.optJSONArray("posts");
                            if(posts.length()==0){
                                completion.onRequestComplete(false, null, null);
                                return;
                            }
                            savePostsToRealm(postSortType, 0, shouldDelete, posts, new STCompletion.Base() {
                                @Override
                                public void onRequestComplete(boolean isSuccess, STError err) {
                                    if(isSuccess){
                                        completion.onRequestComplete(true, null, nullableInt);
                                    }else {
                                        completion.onRequestComplete(false, null, nullableInt);
                                    }
                                }
                            });
                        }else {
                            completion.onRequestComplete(false, err, null);
                        }
                    }
                });
                break;

            default:
                completion.onRequestComplete(false, null, null);
                break;
        }
    }

    public static void saveWatchesOfUser(Integer userID, final Integer lastObjectID, boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        StylerApi.getWatchesOfUser(userID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, final STError err) {
                if(err != null){
                    completion.onRequestComplete(false, err, lastObjectID);
                    return;
                }
                if (json.has("watches")){
                    Realm realm = Realm.getDefaultInstance();
                    int lastObjectID = 0;
                    JSONArray watches = json.optJSONArray("watches");
                    lastObjectID = json.optInt("last_object_id");

                    for (int i=0;i<watches.length();i++) {
                        JSONObject watchObj = watches.optJSONObject(i);

                        final STUser user = new STUser();
                        JSONObject userObj = watchObj.optJSONObject("user");
                        if(userObj !=null){
                            user.fillInfo(realm,userObj);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(user);
                                }
                            });
                        }

                        final STPost post = new STPost();
                        JSONObject postObj = watchObj.optJSONObject("post");
                        if(postObj != null){
                            post.fillInfo(realm,postObj);

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(post);
                                }
                            });
                        }
                        post.user = user;

                        JSONArray items = watchObj.optJSONArray("items");
                        if (items != null){
                            for (int j = 0; j<items.length();j++){
                                JSONObject item = items.optJSONObject(j);
                                RealmResults<STReply> replies = realm.where(STReply.class).equalTo("postID",post.postID).findAll();
                                final STItem stItem = new STItem();
                                stItem.fillInfo(realm,item);
                                stItem.replies.addAll(replies);

                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stItem);
                                    }
                                });

                                final STItemSort stItemSort   = new STItemSort();
                                stItemSort.itemSortKey  = STEnums.ItemSortType.Post.getItemSortType() + "-" + stItem.itemID;
                                stItemSort.itemID       = stItem.itemID;
                                stItemSort.type         = STEnums.ItemSortType.Post.getItemSortType();
                                stItemSort.postID       = post.postID;
                                stItemSort.shopID       = stItem.shopID;
                                stItemSort.item         = stItem;
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stItemSort);
                                    }
                                });

                                post.itemSorts.add(stItemSort);
                            }
                        }
                        final STWatch watch = new STWatch();
                        watch.fillInfo(watchObj, post);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(watch);
                            }
                        });
                    }
                    completion.onRequestComplete(true,err,lastObjectID);
                }else {
                    completion.onRequestComplete(false, null, null);
                }
            }
        });
    }

    public static void savePostsOfUser(final Integer userID, final Integer lastObjectID, final String postSortType, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        switch (postSortType){
            case "user":
                StylerApi.getPostsOfUser(userID, lastObjectID, new STCompletion.WithJsonNullableInt() {
                    @Override
                    public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                        if (isSuccess){

                            JSONArray posts = json.optJSONArray("posts");
                            if(posts == null){
                                completion.onRequestComplete(false, null, null);
                                return;
                            }
                            if(posts.length()==0){
                                completion.onRequestComplete(false, null, null);
                                return;
                            }
                            savePostsToRealm(postSortType, userID, shouldDelete, posts, new STCompletion.Base() {
                                @Override
                                public void onRequestComplete(boolean isSuccess, STError err) {
                                    if (isSuccess){
                                        completion.onRequestComplete(true, null, nullableInt);
                                    }else {
                                        completion.onRequestComplete(false, null, null);
                                    }
                                }
                            });
                        }else {
                            completion.onRequestComplete(false, err ,null);
                        }
                    }
                });
                break;
            default:
                completion.onRequestComplete(false, null, null);
                break;
        }
    }

    public static void saveUnrepliedPosts(Integer categoryID, final Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getUnrepliedPosts(categoryID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {
            JSONArray posts = json.optJSONArray("posts");
            if(posts == null){
                completion.onRequestComplete(false, null, null);
            }
            if(posts.length()==0){
                completion.onRequestComplete(false ,null, null);
            }
            savePostsToRealm(STEnums.PostSortType.Unreplied.getPostSortType(), 0, shouldDelete, posts, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if(isSuccess){
                        completion.onRequestComplete(true, err, lastObjectID);
                    }else {
                        completion.onRequestComplete(false, err, lastObjectID);
                    }
                }
            });
            }
        });
    }

    public static void savePost(final Integer postID, final STCompletion.Base completion){
        //Logger.method();
        StylerApi.getPost(postID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, final boolean isSuccess, final STError err) {
                //// TODO: 2/10/17 ==> go to Main thread ?????????
                if(err!= null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                final JSONObject post = json;
                if(post != null){
                    Logger.print("DataPreseverPost run");
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray items = post.optJSONArray("items");
                    if (items != null){
                        for (int i=0; i<items.length();i++){
                            JSONObject item = items.optJSONObject(i);
                            RealmResults<STReply> replies = realm.where(STReply.class).equalTo("postID",postID).findAll();
                            final STItem stItem = new STItem();
                            stItem.fillInfo(realm, item);
                            stItem.replies.addAll(replies);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItem);
                                }
                            });
                            final STItemSort stItemSort   = new STItemSort();
                            stItemSort.itemSortKey  = STEnums.ItemSortType.Post.getItemSortType() +"-"+ stItem.itemID;
                            stItemSort.itemID       = stItem.itemID;
                            stItemSort.type         = STEnums.ItemSortType.Post.getItemSortType();
                            stItemSort.postID       = post.optInt("id");
                            stItemSort.shopID       = item.optInt("shop_id");
                            stItemSort.item         = stItem;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItemSort);
                                }
                            });
                        }
                    }
                    RealmResults<STItemSort> itemSorts = realm.where(STItemSort.class).equalTo("type", STEnums.ItemSortType.Post.getItemSortType()).findAll();
                    itemSorts = itemSorts.where().equalTo("postID", post.optInt("id")).findAll();
                    final STPost stPost = new STPost();
                    JSONObject userObj = post.optJSONObject("user");
                    if(userObj!=null){
                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm,userObj);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });
                        stPost.user = stUser;
                    }
                    stPost.fillInfo(realm,post);
                    stPost.itemSorts.addAll(itemSorts);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stPost);
                        }
                    });
                    completion.onRequestComplete(true,null);
                    return;
                }else {
                    completion.onRequestComplete(false,err);
                }
            }
        });
    }

    private static void savePostsToRealm(final String postSortType, final Integer userID, final boolean shouldDelete, final JSONArray posts, STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        if(shouldDelete){
            final RealmResults<STPostSort>realmResults = realm.where(STPostSort.class).equalTo("type",postSortType).findAll();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realmResults.deleteAllFromRealm();
                    Log.i("Tan020","delete savePostsToRealm");
                }
            });
        }
        for (int i=0; i<posts.length();i++){
            JSONObject post = posts.optJSONObject(i);
            JSONArray items = post.optJSONArray("items");
            if(items!= null){
                for (int j=0; j<items.length(); j++){
                    JSONObject item = items.optJSONObject(j);
                    RealmResults<STReply>replies = realm.where(STReply.class).equalTo("postID", post.optInt("id")).findAll();
                    final STItem stItem = new STItem();
                    stItem.fillInfo(realm, item);
                    stItem.replies.addAll(replies);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stItem);
                        }
                    });
                    final STItemSort stItemSort   = new STItemSort();
                    stItemSort.itemSortKey  = STEnums.ItemSortType.Post.getItemSortType() +"-"+stItem.itemID;
                    stItemSort.itemID       = stItem.itemID;
                    stItemSort.type         = STEnums.ItemSortType.Post.getItemSortType();
                    stItemSort.postID       = post.optInt("id");
                    stItemSort.shopID       = item.optInt("shop_id");
                    stItemSort.item         = stItem;
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stItemSort);
                        }
                    });
                }
            }
            RealmResults<STItemSort>itemSorts = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Post.getItemSortType()).findAll();
            itemSorts = itemSorts.where().equalTo("postID", post.optInt("id")).findAll();
            final STPost stPost = new STPost();
            stPost.fillInfo(realm, post);
            stPost.itemSorts.addAll(itemSorts);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stPost);
                }
            });
            JSONObject userObj = post.optJSONObject("user");
            if (userObj!=null)
            {
                final STUser stUser = new STUser();
                stUser.fillInfo(realm, userObj);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(stUser);
                    }
                });
                stPost.user = stUser;
            }
            final STPostSort sort = new STPostSort();
            sort.postSortKey    = postSortType +"-"+ stPost.postID +"-"+ userID;
            sort.newestTime     = post.optInt("newest_time");
            sort.post           = stPost;
            sort.type           = postSortType;
            sort.postID         = post.optInt("id");
            sort.userID         = userID;
            sort.categoryID     = post.optInt("category_id");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(sort);
                }
            });
        }
        completion.onRequestComplete(true, null);
    }
}
