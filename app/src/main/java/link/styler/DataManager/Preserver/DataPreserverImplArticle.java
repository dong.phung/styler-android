package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STArticle;
import link.styler.models.STArticleSort;
import link.styler.models.STTag;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplArticle {

    public static void saveArticles(Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getArticles(lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }


                JSONArray articles = json.optJSONArray("articles");
                if (articles==null){
                        completion.onRequestComplete(false, null, null);
                        return;
                }
                saveArticlesToRealm(0, STEnums.ArticleSortType.Timeline.getArticleSortType(), articles, shouldDelete);
                Integer lastObjectID = json.optInt("last_object_id");
                completion.onRequestComplete(true, null, lastObjectID);
                return;
            }
        });

    }

    public static void saveCategorizedArticles(final Integer categoryID, Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        StylerApi.getCategoryArticles(categoryID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                Log.i("tan", "saveCategorizedArticles " +json );
                JSONArray articles = json.optJSONArray("articles");
                if (articles==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                saveArticlesToRealm(categoryID, STEnums.ArticleSortType.Categorized.getArticleSortType(), articles, shouldDelete);
                Integer lastObjectID = json.optInt("last_object_id");
                completion.onRequestComplete(true, null, lastObjectID);
                return;
            }
        });
    }

    public static void saveTopArticles(final boolean shouldDelete, final STCompletion.Base completion){
        StylerApi.getTopArticles(new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null);
                    return;
                }

                JSONArray features = json.optJSONArray("features");
                if (features!=null){
                    saveArticlesToRealm(0, STEnums.ArticleSortType.Feature.getArticleSortType(), features, shouldDelete);
                }
                JSONArray articles = json.optJSONArray("articles");
                if (articles!=null){
                    saveArticlesToRealm(0, STEnums.ArticleSortType.Timeline.getArticleSortType(), articles, shouldDelete);
                }
                completion.onRequestComplete(true, null);
            }
        });
    }

    public static void saveRelateKeywordArticles(final Integer tagID, Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        StylerApi.getTagArticles(tagID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                Log.i("tan", "saveRelateKeywordArticles " +json );

                JSONArray articles = json.optJSONArray("articles");
                if (articles==null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                saveArticlesToRealm(tagID, STEnums.ArticleSortType.Tagged.getArticleSortType(), articles, shouldDelete);
                Integer lastObjectID = json.optInt("last_object_id");
                completion.onRequestComplete(true, null, lastObjectID);
                return;
            }
        });
    }

    public static void saveArticle(Integer articleID, final STCompletion.Base completion){
        StylerApi.getArticle(articleID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err!=null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (json==null){
                    completion.onRequestComplete(false, null);
                    return;
                }

                final JSONObject article = json.optJSONObject("article");
                if (article==null){
                    completion.onRequestComplete(false, null);
                    return;
                }
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        STArticle stArticle = new STArticle();
                        stArticle.fillInfo(realm, article);

                    }
                });
                completion.onRequestComplete(true, null);
            }
        });

    }

    private static void saveArticlesToRealm(final Integer miscID, final String type, final JSONArray articles, final boolean shouldDelete){
        Realm realm = Realm.getDefaultInstance();
        if (shouldDelete){
            if (type == STEnums.ArticleSortType.Categorized.getArticleSortType()){
                RealmResults<STArticleSort>realmResults = realm.where(STArticleSort.class).equalTo("categoryID",miscID).findAll();
                realmResults=realmResults.where().equalTo("type",type).findAll();
                final RealmResults<STArticleSort> finalRealmResults = realmResults;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        finalRealmResults.deleteAllFromRealm();
                        Log.i("Tan020","delete saveArticlesToRealm");

                    }
                });
            }else if (type == STEnums.ArticleSortType.Tagged.getArticleSortType()){
                RealmResults<STArticleSort>realmResults = realm.where(STArticleSort.class).equalTo("tagID",miscID).findAll();
                realmResults=realmResults.where().equalTo("type",type).findAll();
                final RealmResults<STArticleSort> finalRealmResults = realmResults;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        finalRealmResults.deleteAllFromRealm();
                        Log.i("Tan020","delete saveArticlesToRealm");
                    }
                });
            }else {
                final RealmResults<STArticleSort>realmResults = realm.where(STArticleSort.class).equalTo("type",type).findAll();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realmResults.deleteAllFromRealm();
                        Log.i("Tan020","delete saveArticlesToRealm");
                    }
                });
            }
        }
        for (int i=0; i<articles.length();i++){
            JSONObject article = articles.optJSONObject(i);
            final STArticle stArticle =new STArticle();
            stArticle.fillInfo(realm, article);
            JSONArray tags = article.optJSONArray("tags");
            if (tags!=null){
                for (int j=0; j<tags.length(); j++){
                    JSONObject tag = tags.optJSONObject(j);
                    final STTag stTag = new STTag();
                    stTag.tagID = tag.optInt("id");
                    stTag.name  = tag.optString("name");
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stTag);
                        }
                    });
                    stArticle.tags.add(stTag);
                }
            }
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stArticle);
                }
            });
            final STArticleSort stArticleSort = new STArticleSort();
            stArticleSort.articleSortKey = type + "-" + stArticle.articleID + "-" + miscID;
            stArticleSort.articleID      = stArticle.articleID;
            stArticleSort.article        = stArticle;
            if (type == STEnums.ArticleSortType.Categorized.getArticleSortType()){
                stArticleSort.categoryID = miscID;
            }else if (type == STEnums.ArticleSortType.Tagged.getArticleSortType()){
                stArticleSort.tagID = miscID;
            }
            stArticleSort.type           = type;
            stArticleSort.publishedAt    = stArticle.publishedAt;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stArticleSort);
                }
            });
        }
    }

    private static void saveTagsToRealm(final JSONArray tags, final boolean shouldDelete){
        //// TODO: 2/14/17 not using on iOS
        Realm realm = Realm.getDefaultInstance();
        if (shouldDelete){
            RealmResults<STTag>realmResults = realm.where(STTag.class).findAll();
            realmResults.deleteAllFromRealm();
            Log.i("Tan020","delete saveTagsToRealm");
        }
        for (int i=0; i<tags.length(); i++){
            JSONObject tag = null;
            tag = tags.optJSONObject(i);
            final STTag stTag = new STTag();
            stTag.tagID = tag.optInt("id");
            stTag.name  = tag.optString("name");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stTag);
                }
            });
        }
    }
}
