package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STCategory;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplCategory {

    public static void saveCategoris(final STCompletion.Base completion){
        StylerApi.getCategories(new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false,err);
                    return;
                }
                if (json != null) {
                    final JSONArray categories = json.optJSONArray("categories");
                    Log.i("tan", "saveCategoris " +json );
                    Realm realm = Realm.getDefaultInstance();
                    final RealmResults<STCategory> realmResults = realm.where(STCategory.class).findAll();
                    if(realmResults != null);
                    {
                        Log.i("tan", "non null " );
                    }
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Log.i("tan", "delete " );
                            realmResults.deleteAllFromRealm();
                            Log.i("Tan020","delete saveCategoris");
                        }
                    });
                    for (int i=0; i<categories.length(); i++) {
                        JSONObject category = categories.optJSONObject(i);
                        final STCategory stCategory = new STCategory();
                        stCategory.fillInfo(category);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stCategory);
                            }
                        });
                    }
                    completion.onRequestComplete(true,null);
                }else {
                    completion.onRequestComplete(false,null);
                    return;
                }
            }
        });
    }
}
