package link.styler.DataManager.Preserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STComment;
import link.styler.models.STItem;
import link.styler.models.STUser;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplComment {

    public static void getComments(Integer postID, Integer lastObjectID, final STCompletion.WithLastObjectID completion){
        StylerApi.getComments(postID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                //// TODO: 2/14/17 chuyen ve main thread
                if (err!=null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json!=null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray comments = json.optJSONArray("comments");
                    if (comments!= null){
                        for (int i = 0; i<comments.length(); i++){
                            JSONObject comment = comments.optJSONObject(i);
                            final STComment stComment = new STComment();
                            stComment.fillInfo(comment);

                            JSONObject user = comment.optJSONObject("user");
                            if (user!= null){
                                final STUser stUser = new STUser();
                                stUser.fillInfo(realm, user);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stUser);
                                    }
                                });
                                stComment.user = stUser;
                            }

                            JSONObject item = comment.optJSONObject("item");
                            if (item!= null){
                                final STItem stItem = new STItem();
                                stItem.fillInfo(realm, item);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stItem);
                                    }
                                });
                                stComment.item = stItem;
                            }
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stComment);
                                }
                            });
                        }
                        completion.onRequestComplete(true, null, nullableInt);
                    }else {
                        completion.onRequestComplete(false, null, null);
                    }
                }else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

}
