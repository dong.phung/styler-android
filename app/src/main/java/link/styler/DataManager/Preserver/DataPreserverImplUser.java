package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STFollow;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STLike;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.models.STUserSort;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplUser {
    public static void saveUser(Integer userID, final STCompletion.Base completion){
        //Logger.method();
        StylerApi.getUser(userID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, null);
                    return;
                }
                if (json != null){

                    final JSONObject user = json.optJSONObject("user");
                    if (user == null){
                        completion.onRequestComplete(false, err);
                        return;
                    }
                    //// TODO: 2/13/17 chuyen ve main thread ??????
                    Realm realm = Realm.getDefaultInstance();
                    final STUser stUser = new STUser();
                    stUser.fillInfo(realm, user);
                    JSONObject shop = json.optJSONObject("shop");
                    if (shop != null){
                        final STShop stShop = new STShop();
                        stShop.fillInfo(realm, shop);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stShop);
                            }
                        });
                        stUser.shop = stShop;
                    }
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stUser);
                        }
                    });
                    completion.onRequestComplete(true, null);
                }else {
                    completion.onRequestComplete(false, err);
                    return;
                }
            }
        });
    }

    public static void saveLikesOfUser(Integer userID, Integer lastObjectID, boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getLikesOfUser(userID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                Integer lastID = json.optInt("last_object_id");
                final JSONArray likes = json.optJSONArray("likes");
                if (json == null || lastID == null || likes == null){
                    completion.onRequestComplete(false, null, null);
                    return;
                }
                Realm realm = Realm.getDefaultInstance();
                for (int i=0; i<likes.length(); i++){
                    JSONObject likeObj = likes.optJSONObject(i);
                    final STShop shop = new STShop();
                    JSONObject shopObj = likeObj.optJSONObject("shop");
                    if(shopObj!= null){
                        shop.fillInfo(realm, shopObj);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(shop);
                            }
                        });
                    }
                    final STItem item = new STItem();
                    JSONObject itemObj = likeObj.optJSONObject("item");
                    if (itemObj != null){
                        item.fillInfo(realm, itemObj);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(item);
                            }
                        });
                    }
                    item.shop = shop;
                    final STLike like = new STLike();
                    like.fillInfo(likeObj, item);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(like);
                        }
                    });
                }
                completion.onRequestComplete(true, null, lastID);
            }
        });
    }

    public static void saveStaffsOfShop(final Integer shopID, final Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        Log.i("saveStaffsOfShop","shopID: " + shopID);
        StylerApi.getStaffsOfShop(shopID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err, nullableInt);
                    return;
                }
                if (json != null){
                    final JSONArray users = json.optJSONArray("users");
                    if (users==null){
                        completion.onRequestComplete(false, null, nullableInt);
                        return;
                    }
                    Realm realm = Realm.getDefaultInstance();
                    if (shouldDelete){
                        RealmResults<STUserSort> realmResults = realm.where(STUserSort.class).equalTo("shopID",shopID).findAll();
                        realmResults = realmResults.where().equalTo("type", STEnums.UserSortType.Staff.getUserSortType()).findAll();
                        final RealmResults<STUserSort> finalRealmResults = realmResults;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                finalRealmResults.deleteAllFromRealm();
                                Log.i("Tan020","delete saveStaffsOfShop");
                            }
                        });
                    }
                    for (int i=0; i<users.length(); i++){
                        JSONObject user = users.optJSONObject(i);
                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm, user);
                        JSONObject shop = user.optJSONObject("shop");
                        if (shop!=null){
                            final STShop stShop = new STShop();
                            stShop.fillInfo(realm, shop);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stShop);
                                }
                            });
                            stUser.shop = stShop;
                        }
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });
                        final STUserSort stUserSort = new STUserSort();
                        stUserSort.userSortKey  = STEnums.UserSortType.Staff.getUserSortType() + "-" + stUser.userID;
                        stUserSort.type         = STEnums.UserSortType.Staff.getUserSortType();
                        stUserSort.userID       = stUser.userID;
                        stUserSort.user         = stUser;
                        stUserSort.shopID       = shopID;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUserSort);
                            }
                        });
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                }else {
                    completion.onRequestComplete(false, null, nullableInt);
                    return;
                }
            }
        });
    }

    public static void saveFollowingOfUser(Integer userID, Integer lastObjectID, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getFollows(userID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json!= null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray follows = json.optJSONArray("follows");
                    Integer lastID = json.optInt("last_object_id");
                    if (follows == null || lastID == null){
                        completion.onRequestComplete(false, null, lastID);
                        return;
                    }
                    for (int i=0; i<follows.length(); i++){
                        JSONObject followObj = follows.optJSONObject(i);
                        final STUser staff = new STUser();
                        JSONObject staffObj = followObj.optJSONObject("staff");
                        if (staffObj!=null){
                            staff.fillInfo(realm, staffObj);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(staff);
                                }
                            });
                        }
                        final STFollow follow = new STFollow();
                        follow.fillInfoStaff(followObj, staff);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(follow);
                            }
                        });
                    }
                    completion.onRequestComplete(true, null, lastID);
                }else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

    public static void saveRepliesOfUser(final Integer userID, Integer lastObjectID, boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        StylerApi.getRepliesOfUser(userID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json!= null){
                    Log.i("saveRepliesOfUser","json:" + json);
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray replies = json.optJSONArray("replies");
                    if (replies == null){
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                    for (int i=0; i<replies.length(); i++){
                        JSONObject reply = replies.optJSONObject(i);

                        final STItem stItem = new STItem();
                        JSONObject item = reply.optJSONObject("item");
                        Log.i("saveRepliesOfUser","item:" + item);
                        if (item != null){
                            stItem.fillInfo(realm, item);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItem);
                                }
                            });
                        }

                        final STShop stShop = new STShop();
                        JSONObject shop = reply.optJSONObject("shop");
                        if (shop != null){
                            stShop.fillInfo(realm, shop);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stShop);
                                }
                            });
                        }

                        final STUser stUser = new STUser();
                        JSONObject user = reply.optJSONObject("user");
                        if (user != null){
                            stUser.fillInfo(realm, user);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stUser);
                                }
                            });
                        }

                        final STReply stReply = new STReply();
                        stReply.fillInfo(reply, stItem.itemID);
                        stReply.shop    = stShop;
                        stReply.user    = stUser;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stReply);
                            }
                        });
                        final STItemSort stItemSort = new STItemSort();
                        stItemSort.itemSortKey = STEnums.ItemSortType.Reply.getItemSortType() + "-"+ stItem.itemID;
                        stItemSort.itemID      = stItem.itemID;
                        stItemSort.type        = STEnums.ItemSortType.Reply.getItemSortType();
                        stItemSort.postID      = stReply.postID;
                        stItemSort.replyID     = stReply.replyID;
                        stItemSort.shopID      = stShop.shopID;
                        stItemSort.item        = stItem;
                        stItemSort.userID      = userID;
                        stItemSort.itemCreatedAt = stItem.createdAt;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItemSort);
                            }
                        });

                    }
                    completion.onRequestComplete(true, null, nullableInt);
                }else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

    public static void saveFollowerOfUser(final Integer userID, final Integer lastObjectID, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getFollowers(userID, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                //// TODO: 2/13/17  chuyen ve main thread
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json!= null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray followers = json.optJSONArray("followers");
                    Integer lastID = json.optInt("last_object_id");
                    if (followers == null || lastID == null){
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                    for (int i=0; i<followers.length(); i++){
                        JSONObject followerObj = followers.optJSONObject(i);
                        final STUser user = new STUser();
                        JSONObject userObj = followerObj.optJSONObject("user");
                        if (userObj != null){
                            user.fillInfo(realm, userObj);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(user);
                                }
                            });
                        }
                        final STFollow follow = new STFollow();
                        follow.fillInfoUser(followerObj, user);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(follow);
                            }
                        });
                    }
                    completion.onRequestComplete(true, null, lastID);
                    return;
                }else {
                    completion.onRequestComplete(false, null, lastObjectID);
                    return;
                }
            }
        });

    }
}
