package link.styler.DataManager.Preserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.models.STNotification;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplNotification {
    public static void saveNotificationState(final STCompletion.Base completion){
        StylerApi.checkNewNotification(new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                //// TODO: 2/14/17 not using on iOS
            }
        });
    }

    public static void saveNotificationActivity(Integer lastObjectID, final STCompletion.WithLastObjectID completion){
        Logger.print("saveNotificationActivity");
        StylerApi.getActivities(lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {
                Logger.print("json "+json);
                Logger.print("isSuccess "+isSuccess);
                Logger.print("err "+err);
                Logger.print("nullableInt "+nullableInt);
                if (err!=null || !isSuccess){
                    completion.onRequestComplete(false, err, nullableInt);
                    return;
                }

                Realm realm = Realm.getDefaultInstance();
                JSONArray notificationObjs = json.optJSONArray("notifications");
                if (notificationObjs != null){
                    for (int i =0; i<notificationObjs.length(); i++){
                        JSONObject notificationObj = notificationObjs.optJSONObject(i);
                        if(notificationObj.has("notification_id")){
                            if(realm.where(STNotification.class).equalTo("notificationID",notificationObj.optInt("notification_id")).findFirst() != null && nullableInt != null ){
                                LoginManager.getsIntstance().hasUnOpenNotification = true;
                            }
                        }
                        final STNotification notification = new STNotification();
                        notification.fillInfo(realm, notificationObj);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(notification);
                            }
                        });
                    }
                }
                completion.onRequestComplete(true, null, nullableInt);
            }
        });
    }

}
