package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.models.STImage;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STPopularItem;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.models.STUser;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplReply {

    public static void saveReplies(Integer postID, final Integer lastObjectID, final String itemSortType, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getRepliesOfPost(postID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                Log.i("Tan004"," Json " + json);
                if (json != null){
                    //// TODO: 2/10/17 by Tan create getRepliesOfPost with jsonArray;


                    JSONArray replies = null;
                    try {
                        replies = json.getJSONArray("replies");
                        Log.i("Tan004"," replies " + replies);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(replies != null){
                        saveRepliesToRealm(itemSortType, false, replies, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                completion.onRequestComplete(isSuccess,err,nullableInt);
                                return;
                            }
                        });
                    }else {
                        completion.onRequestComplete(false, null, null);
                    }
                }
            }
        });
    }

    public static void saveReply(Integer replyID, final STCompletion.Base completion){
        //Logger.method();
        STCompletion.WithJson completionGetReply = new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false,err);
                }
                if (json != null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject replyObj = json.optJSONObject("reply");
                    if (replyObj != null){
                        JSONObject shop = replyObj.optJSONObject("shop");
                        if (shop == null){
                            completion.onRequestComplete(false, null);
                            return;
                        }
                        JSONObject user = replyObj.optJSONObject("user");
                        if (user == null){
                            completion.onRequestComplete(false, null);
                            return;
                        }
                        JSONObject item = replyObj.optJSONObject("item");
                        if (item == null){
                            completion.onRequestComplete(false, null);
                            return;
                        }

                        final STShop stShop = new STShop();
                        stShop.fillInfo(realm, shop);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stShop);
                            }
                        });
                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm, user);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });
                        final STReply stReply = new STReply();
                        stReply.shop    = stShop;
                        stReply.user    = stUser;
                        stReply.fillInfo(replyObj, item.optInt("id"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stReply);
                            }
                        });
                        RealmResults<STReply> replies = realm.where(STReply.class).equalTo("postID", replyObj.optInt("post_id")).findAll();
                        final STItem stItem = new STItem();
                        stItem.fillInfo(realm, item);
                        stItem.replies.addAll(replies);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItem);
                            }
                        });
                        completion.onRequestComplete(true, null);
                    }
                    return;
                }
            }
        };
        StylerApi.getReply(replyID, completionGetReply);
    }

    public static void saveReplyAndGetID(final Integer replyID, final STCompletion.WithPostIDAndItemID completion){
        //Logger.method():
        StylerApi.getReply(replyID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, err, null, null);
                    return;
                }
                if(json != null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject replyObj = json.optJSONObject("reply");
                    if (replyObj != null){
                        JSONObject shop = replyObj.optJSONObject("shop");
                        if (shop == null){
                            completion.onRequestComplete(false, null, null, null);
                            return;
                        }
                        JSONObject user = replyObj.optJSONObject("user");
                        if (user == null){
                            completion.onRequestComplete(false, null, null, null);
                            return;
                        }
                        JSONObject item = replyObj.optJSONObject("item");
                        if (item == null){
                            completion.onRequestComplete(false, null, null, null);
                            return;
                        }

                        final STShop stShop = new STShop();
                        stShop.fillInfo(realm, shop);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stShop);
                            }
                        });
                        final STUser stUser = new STUser();
                        stUser.fillInfo(realm, user);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stUser);
                            }
                        });
                        final STReply stReply = new STReply();
                        stReply.shop    = stShop;
                        stReply.user    = stUser;
                        stReply.fillInfo(replyObj, item.optInt("id"));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stReply);
                            }
                        });
                        RealmResults<STReply> replies = realm.where(STReply.class).equalTo("postID", replyObj.optInt("post_id")).findAll();
                        final STItem stItem = new STItem();
                        stItem.fillInfo(realm, item);
                        stItem.shop = stShop;
                        stItem.replies.addAll(replies);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItem);
                            }
                        });
                        completion.onRequestComplete(true, null,replyObj.optInt("post_id"), item.optInt("id"));
                    }
                    return;
                }
            }
        });
    }

    public static void savePopularItems(Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getPopularItems(lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, final STError err, final Integer nullableInt) {
                Log.i("Tan0201","json " + json);
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null){
                    Realm realm = Realm.getDefaultInstance();
                    if (shouldDelete){
                        final RealmResults<STItemSort>realmResults = realm.where(STItemSort.class).equalTo("type", STEnums.ItemSortType.Popular.getItemSortType()).findAll();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realmResults.deleteAllFromRealm();
                                Log.i("Tan020","delete savePopularItems");
                            }
                        });
                    }
                    JSONArray items = json.optJSONArray("items");
                    if (items != null){
                        for (int i = 0; i<items.length(); i++){
                            JSONObject item = items.optJSONObject(i);
                            RealmResults<STReply> replies = realm.where(STReply.class).equalTo("itemID", item.optInt("id")).findAll();
                            final STPopularItem stPopularItem = new STPopularItem();
                            stPopularItem.fillInfo(realm, item);

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stPopularItem);
                                }
                            });

                            final STItem stItem = new STItem();
                            stItem.fillInfo(realm, item);
                            stItem.replies.addAll(replies);
                            final STItemSort stItemSort   = new STItemSort();
                            stItemSort.itemSortKey  = STEnums.ItemSortType.Popular.getItemSortType() +"-"+ stItem.itemID;
                            stItemSort.itemID       = stItem.itemID;
                            stItemSort.type         = STEnums.ItemSortType.Popular.getItemSortType();
                            stItemSort.postID       = 0;
                            stItemSort.replyID      = 0;
                            stItemSort.shopID       = item.optInt("shop_id");
                            stItemSort.item         = stItem;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItemSort);
                                }
                            });
                            JSONObject shop = item.optJSONObject("shop");
                            if (shop != null){
                                final STShop stShop = new STShop();
                                stShop.fillInfo(realm, shop);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(stShop);
                                    }
                                });
                                stItem.shop = stShop;
                            }
                            JSONArray replies2 = item.optJSONArray("replies");
                            if (replies2 != null){
                                for (int j = 0; j<replies2.length(); j++){
                                    JSONObject replyObj = replies2.optJSONObject(j);
                                    final STShop stShop = new STShop();
                                    JSONObject shopObj = replyObj.optJSONObject("shop");
                                    if (shopObj != null){
                                        stShop.fillInfo(realm, shopObj);
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.copyToRealmOrUpdate(stShop);
                                            }
                                        });
                                    }
                                    final STUser stUser = new STUser();
                                    JSONObject userObj = replyObj.optJSONObject("user");
                                    if (userObj != null) {
                                        stUser.fillInfo(realm, userObj);
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.copyToRealmOrUpdate(stUser);
                                            }
                                        });
                                    }

                                    final STReply stReply = new STReply();
                                    stReply.fillAllInfo(replyObj, stItem.shopID, stShop, stUser);
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.copyToRealmOrUpdate(stReply);
                                        }
                                    });
                                    stItem.replies.add(stReply);
                                }
                            }
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItem);
                                }
                            });
                        }
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                }else {
                    completion.onRequestComplete(false, null, null);
                }
            }
        });
    }

    public static void saveItemsOfShop(final Integer shopID, Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getItemsOfShop(shopID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null){
                    Realm realm = Realm.getDefaultInstance();
                    if (shouldDelete){
                        RealmResults<STItemSort>realmResults = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Shop.getItemSortType()).findAll();
                        realmResults = realmResults.where().equalTo("shopID", shopID).findAll();
                        final RealmResults<STItemSort> finalRealmResults = realmResults;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                finalRealmResults.deleteAllFromRealm();
                                Log.i("Tan020","delete saveItemsOfShop");
                            }
                        });
                    }
                    JSONArray items = json.optJSONArray("items");
                    if (items!=null){
                        Log.i("Tan019","saveItemsOfShop items " + items);
                        for (int i=0; i<items.length(); i++){
                            JSONObject item = items.optJSONObject(i);
                            RealmResults<STReply> replies = realm.where(STReply.class).equalTo("itemID",item.optInt("id")).findAll();
                            final STItem stItem = new STItem();
                            stItem.fillInfo(realm, item);
                            stItem.replies.addAll(replies);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItem);
                                }
                            });
                            final STItemSort stItemSort   = new STItemSort();
                            stItemSort.itemSortKey  = STEnums.ItemSortType.Shop.getItemSortType() +"-"+stItem.itemID;
                            stItemSort.itemID = stItem.itemID;
                            stItemSort.type         = STEnums.ItemSortType.Shop.getItemSortType();
                            stItemSort.postID       = 0;
                            stItemSort.replyID      = 0;
                            stItemSort.shopID       = shopID;
                            stItemSort.item         = stItem;
                            //// TODO: 3/14/17 add category ID to test create reply of shop - bug in STReplySelectPastItemViewControllerFragment
                            //stItemSort.categoryID   = stItem.categoryID;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItemSort);
                                }
                            });
                        }
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                }else {
                    completion.onRequestComplete(false, null, null);
                }
            }
        });
    }

    public static void saveItemsOfShopWithCategory(final Integer shopID, Integer categoryID, Integer lastObjectID, final boolean shouldDelete, final STCompletion.WithLastObjectID completion) {
        //Logger.method();
        StylerApi.getItemsOfShopWithCategory(shopID, categoryID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null){
                    Log.i("Tan018","json " + json);
                    Realm realm = Realm.getDefaultInstance();
                    if (shouldDelete){
                        RealmResults<STItemSort>realmResults = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Shop.getItemSortType()).findAll();
                        realmResults =realmResults.where().equalTo("shopID", shopID).findAll();
                        final RealmResults<STItemSort> finalRealmResults = realmResults;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                finalRealmResults.deleteAllFromRealm();
                                Log.i("Tan020","delete saveItemsOfShopWithCategory");
                            }
                        });
                    }

                    JSONArray items = json.optJSONArray("items");
                    if (items!=null){
                        for (int i=0; i<items.length(); i++){
                            Log.i("Tan018","items " + items);
                            JSONObject item = items.optJSONObject(i);
                            RealmResults<STReply> replies = realm.where(STReply.class).equalTo("itemID",item.optInt("id")).findAll();
                            final STItem stItem = new STItem();
                            stItem.fillInfo(realm, item);
                            stItem.replies.addAll(replies);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItem);
                                }
                            });
                            final STItemSort stItemSort   = new STItemSort();
                            stItemSort.itemSortKey  = STEnums.ItemSortType.Shop.getItemSortType() +"-"+stItem.itemID;
                            stItemSort.type         = STEnums.ItemSortType.Shop.getItemSortType();
                            stItemSort.postID       = 0;
                            stItemSort.replyID      = 0;
                            stItemSort.shopID       = shopID;
                            stItemSort.item         = stItem;
                            stItemSort.categoryID   = stItem.categoryID;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stItemSort);
                                    Log.i("Tan018","stItemSort " + stItemSort);
                                    Log.i("Tan018","stItemSort " + stItemSort.categoryID);
                                }
                            });
                        }
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                }else {
                    completion.onRequestComplete(false, null, null);
                }
            }
        });
    }

    public static void saveItem(final Integer itemID, final STCompletion.Base completion){
        //Logger.method();
        StylerApi.getItem(itemID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                Logger.print("json "+json+" success "+isSuccess+" err "+err);
                if (err != null){
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (json != null){
                    //// TODO: 2/13/17 chuyen ve main thread ?????
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject itemObj = json.optJSONObject("item");
                    if (itemObj != null) {
                        final STItem item = new STItem();
                        item.fillInfo(realm, itemObj);

                        JSONObject shopObj = itemObj.optJSONObject("shop");
                        if (shopObj != null) {
                            final STShop shop = new STShop();
                            shop.fillInfo(realm, shopObj);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(shop);
                                }
                            });
                            item.shop = shop;
                        }

                        JSONArray replies = itemObj.optJSONArray("replies");
                        if (replies != null) {
                            for (int i = 0; i < replies.length(); i++) {
                                JSONObject replyObj = replies.optJSONObject(i);
                                final STUser user = new STUser();

                                JSONObject userObj = replyObj.optJSONObject("user");
                                if (userObj != null) {
                                    user.fillInfo(realm, userObj);
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.copyToRealmOrUpdate(user);
                                        }
                                    });
                                }

                                final STReply reply = new STReply();
                                reply.fillInfo(replyObj, item.itemID);
                                reply.user = user;
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(reply);
                                    }
                                });
                                item.replies.add(reply);
                            }
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(item);
                            }
                        });

                        final STItemSort stItemSort = new STItemSort();
                        stItemSort.itemSortKey = STEnums.ItemSortType.Popular.getItemSortType() + "-" + item.itemID;
                        stItemSort.itemID = item.itemID;
                        stItemSort.type = STEnums.ItemSortType.Popular.getItemSortType();
                        stItemSort.postID = 0;
                        stItemSort.replyID = 0;
                        stItemSort.shopID = item.shopID;
                        stItemSort.item = item;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stItemSort);
                            }
                        });
                        completion.onRequestComplete(true, null);
                    }
                } else {
                    completion.onRequestComplete(false , null);
                    return;
                }
            }
        });
    }

    private static void saveRepliesToRealm(final String replySortType, final boolean shouldDelete, final JSONArray replies, final STCompletion.Base completion){
        Realm realm = Realm.getDefaultInstance();
        if (shouldDelete){
            final RealmResults<STItemSort> realmResults = realm.where(STItemSort.class).equalTo("type", replySortType).findAll();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realmResults.deleteAllFromRealm();
                    Log.i("Tan020","delete saveRepliesToRealm");
                }
            });
        }
        for (int i=0; i<replies.length(); i++){
            JSONObject reply = replies.optJSONObject(i);
            JSONObject shop = reply.optJSONObject("shop");
            final STShop stShop = new STShop();
            stShop.fillInfo(realm, shop);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stShop);
                }
            });
            final STUser stUser = new STUser();
            stUser.fillInfo(realm, reply.optJSONObject("user"));
            stUser.shop = stShop;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stUser);
                }
            });
            JSONObject item = reply.optJSONObject("item");
            final STReply stReply = new STReply();
            stReply.fillInfo(reply, item.optInt("id"));
            stReply.shop = stShop;
            stReply.user = stUser;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stReply);
                }
            });
            final STItem stItem = new STItem();
            stItem.fillInfo(realm , item);
            stItem.shop = stShop;
            stItem.replies.add(stReply);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stItem);
                }
            });
            final STItemSort stItemSort   = new STItemSort();
            stItemSort.itemSortKey  = replySortType +"-"+stItem.itemID;
            stItemSort.itemID       = stItem.itemID;
            stItemSort.type         = replySortType;
            stItemSort.postID       = reply.optInt("post_id");
            stItemSort.replyID      = reply.optInt("id");
            stItemSort.shopID       = shop.optInt("id");
            stItemSort.item         = stItem;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stItemSort);
                }
            });
            Log.i("Tan004","type " + replySortType);

        }
    completion.onRequestComplete(true, null);
    }
}
