package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STConversation;
import link.styler.models.STMessage;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplMessage {

    public static void saveMessages(final Integer conversationID, Integer lastObjectID, final STCompletion.WithLastObjectID completion) {

        StylerApi.getMessages(conversationID, lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {
                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null) {
                    //// TODO: 2/13/17 chuyen ve main thread ????
                    final JSONArray messages = json.optJSONArray("messages");
                    if (messages == null) {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                    Realm realm = Realm.getDefaultInstance();
                    final STConversation stConversation = realm.where(STConversation.class).equalTo("conversationID", conversationID).findFirst();
                    if (stConversation != null) {
                        for (int i = 0; i < messages.length(); i++) {
                            final JSONObject message = messages.optJSONObject(i);
                            final STMessage stMessage = new STMessage();
                            stMessage.fillInfo(realm, message);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stMessage);
                                }
                            });
                            stConversation.messages.add(stMessage);
                        }
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(stConversation);
                            }
                        });
                    } else {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                    return;
                } else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

    public static void saveMessagesWithUserID(final Integer userID, Integer lastObjectID, Integer latestObjectID, final STCompletion.WithLastObjectID completion) {
        Log.i("DataPreserver", "saveMessages: " + " userID: " + userID + " lastObjectID: " + lastObjectID + " latestObjectID: " + latestObjectID);
        StylerApi.getMessagesWithUserID(userID, lastObjectID, latestObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err, final Integer nullableInt) {
                Log.i("saveMessagesWithUserID", "json" + json);
                if (err != null) {
                    completion.onRequestComplete(false, err, null);
                    return;
                }
                if (json != null) {
                    JSONArray messages = json.optJSONArray("messages");
                    Log.i("saveMessagesWithUserID", "messages" + messages);
                    if (messages == null) {
                        completion.onRequestComplete(false, null, null);
                        return;
                    }
                    Realm realm = Realm.getDefaultInstance();
                    for (int i = 0; i < messages.length(); i++) {
                        final JSONObject message = messages.optJSONObject(i);
                        final STMessage stMessage = new STMessage();
                        stMessage.isReadMessage = true;
                        stMessage.destinationUserID = userID;
                        stMessage.fillInfo(realm, message);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate( stMessage);
                            }
                        });
                    }
                    completion.onRequestComplete(true, null, nullableInt);
                } else {
                    completion.onRequestComplete(false, null, null);
                    return;
                }
            }
        });
    }

}
