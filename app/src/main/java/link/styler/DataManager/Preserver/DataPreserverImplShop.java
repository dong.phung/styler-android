package link.styler.DataManager.Preserver;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STLocation;
import link.styler.models.STPostSort;
import link.styler.models.STShop;
import link.styler.models.STShopSort;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplShop {
    public static void saveLocations(final STCompletion.Base completion){
        //Logger.method();
        StylerApi.getLocations(new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, err);
                }
                if (json != null){
                    final JSONArray locations = json.optJSONArray("locations");
                    if (locations != null) {
                        Realm realm = Realm.getDefaultInstance();
                        for (int i = 0; i < locations.length(); i++) {
                            JSONObject location = locations.optJSONObject(i);
                            final STLocation stLocation = new STLocation();
                            stLocation.fillInfo(location);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stLocation);
                                }
                            });
                        }
                        completion.onRequestComplete(true, null);
                    }else {
                        completion.onRequestComplete(false, null);
                    }
                }else {
                    completion.onRequestComplete(false, null);
                }
            }
        });
    }

    public static void saveShops(final Integer locationID, String order, Integer lastObjectID, final String shopSortType, final boolean shouldDelete, final STCompletion.WithLastObjectID completion){
        //Logger.method();
        StylerApi.getShops(locationID, order, lastObjectID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                if (err != null){
                    completion.onRequestComplete(false, err, null);
                }
                if (json != null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONArray shops = json.optJSONArray("shops");
                    if (shops != null){
                        if (shouldDelete){
                            RealmResults<STShopSort> realmResults = realm.where(STShopSort.class).equalTo("locationID",locationID).findAll();
                            realmResults = realmResults.where().equalTo("type", shopSortType).findAll();
                            final RealmResults<STShopSort> finalRealmResults = realmResults;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    finalRealmResults.deleteAllFromRealm();
                                    Log.i("Tan020","delete saveShops");
                                }
                            });
                        }
                        for (int i=0; i<shops.length(); i++){
                            JSONObject shop = shops.optJSONObject(i);
                            final STShop stShop = new STShop();
                            stShop.fillInfo(realm, shop);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stShop);
                                }
                            });
                            final STShopSort stShopSort   = new STShopSort();
                            stShopSort.objectID     = shop.optInt("id");
                            stShopSort.type         = STEnums.ShopSortType.None.getShopSortType();
                            stShopSort.locationID   = locationID;
                            stShopSort.shopID       = shop.optInt("id");
                            stShopSort.shop         = stShop;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(stShopSort);
                                }
                            });
                        }
                        if(json.has("last_object_id")){
                            int lastObjectID = json.optInt("last_object_id");
                            completion.onRequestComplete(true, null, lastObjectID);
                        }
                    }else {
                        completion.onRequestComplete(false, null ,null);
                    }
                } else {
                    completion.onRequestComplete(false, null ,null);
                }
            }
        });
    }

    public static void saveShop(Integer shopID, final STCompletion.Base completion){
        //Logger.method();
        StylerApi.getShop(shopID, new STCompletion.WithJson() {
            @Override
            public void onRequestComplete(final JSONObject json, boolean isSuccess, STError err) {
                //// TODO: 2/13/17 chuyen ve main thread ?????
                if (err != null){
                    completion.onRequestComplete(false, err);
                }
                if (json != null){
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject _json = json;
                    final STShop stShop = new STShop();
                    stShop.fillInfo(realm, _json);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stShop);
                        }
                    });
                    completion.onRequestComplete(true, null);
                }
                else {
                    completion.onRequestComplete(false, null);
                }
            }
        });

    }
}
