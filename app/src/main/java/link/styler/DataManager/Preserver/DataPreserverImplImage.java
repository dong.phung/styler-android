package link.styler.DataManager.Preserver;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.models.STImage;

/**
 * Created by macOS on 2/28/17.
 */

public class DataPreserverImplImage {
    public static void saveImageWithUrl(String url, Bitmap bitmap){
        Bitmap bmp = bitmap;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] data = stream.toByteArray();
        Realm realm = Realm.getDefaultInstance();
        final STImage stImage = new STImage();
        stImage.url = url;
        stImage.data = data;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(stImage);
            }
        });
    }
}
