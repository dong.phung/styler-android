package link.styler.DataManager.Preserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.models.STConversation;

/**
 * Created by Tan Nguyen on 2/8/17.
 */

public class DataPreserverImplConversation {
    public static void saveConversation(Integer lastObjectID, final STCompletion.WithLastObjectID completion){
        //// TODO: 2/13/17 JSONObject to JsonArray
        StylerApi.getConversations(lastObjectID, new STCompletion.WithJsonNullableInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt) {

                Logger.print("json "+json);

                if (err!=null || json == null){
                    completion.onRequestComplete(false, err, nullableInt);
                    Logger.print("err "+ err.message);
                    return;
                }
                Realm realm = Realm.getDefaultInstance();
                if (json != null) {
                    JSONArray conversationObjs = json.optJSONArray("conversations");
                    if (conversationObjs != null) {
                        for (int i = 0; i < conversationObjs.length(); i++) {
                            JSONObject conversationObj = conversationObjs.optJSONObject(i);
                            final STConversation conversation = new STConversation();
                            conversation.fillInfo(conversationObj);
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(conversation);
                                }
                            });
                        }
                        completion.onRequestComplete(true, null, nullableInt);
                    } else {
                        completion.onRequestComplete(false, null, nullableInt);
                    }
                } else {
                    completion.onRequestComplete(false, null, nullableInt);
                }
            }
        });
    }
}
