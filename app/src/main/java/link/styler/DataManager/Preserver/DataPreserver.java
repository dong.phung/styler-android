package link.styler.DataManager.Preserver;

import android.graphics.Bitmap;

import org.json.JSONObject;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoaderImplImage;

/**
 * Created by Tan Nguyen on 2/7/17.
 */

public class DataPreserver {
    /* DataPreserver */

    public static void deleteAll(){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


    // STCompletion.Base completion được dùng chung completion: (success: Bool) -> Void)
    // STCompletion.WithLastObjectID completion đc dùng với 2 trường hợp có STError và không STError
    /*--*/

    /* DataPreserver+Category */
    public static void saveCategoris(STCompletion.Base completion){
        DataPreserverImplCategory.saveCategoris(completion);
    }

    /* DataPreserver+Post */
    public static void savePosts(Integer lastObjectID, String postSortType, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplPost.savePosts(lastObjectID, postSortType, shouldDelete, completion);
    }

    public static void saveWatchesOfUser(Integer userID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplPost.saveWatchesOfUser(userID, lastObjectID, shouldDelete, completion);
    }

    public static void savePostsOfUser(Integer userID, Integer lastObjectID, String postSortType, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplPost.savePostsOfUser(userID, lastObjectID, postSortType, shouldDelete, completion);
    }

    public static void saveUnrepliedPosts(Integer categoryID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplPost.saveUnrepliedPosts(categoryID, lastObjectID, shouldDelete, completion);
    }

    public static void savePost(Integer postID, STCompletion.Base completion){
        DataPreserverImplPost.savePost(postID, completion);
    }

    /* DataPreserver+Reply */
    public static void saveReplies(Integer postID, Integer lastObjectID, String itemSortType, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplReply.saveReplies(postID, lastObjectID, itemSortType, shouldDelete, completion);
    }

    public static void saveReply(Integer replyID, STCompletion.Base completion){
        DataPreserverImplReply.saveReply(replyID, completion);
    }

    public static void saveReplyAndGetID(Integer replyID, STCompletion.WithPostIDAndItemID completion){
        DataPreserverImplReply.saveReplyAndGetID(replyID, completion);
    }

    public static void savePopularItems(Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplReply.savePopularItems(lastObjectID, shouldDelete, completion);
    }

    public static void saveItemsOfShop(Integer shopID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplReply.saveItemsOfShop(shopID, lastObjectID, shouldDelete, completion);
    }

    public static void saveItemsOfShopWithCategory(Integer shopID, Integer categoryID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion) {
        DataPreserverImplReply.saveItemsOfShopWithCategory(shopID, categoryID, lastObjectID, shouldDelete, completion);
    }

    public static void saveItem(Integer itemID, STCompletion.Base completion){
        DataPreserverImplReply.saveItem(itemID, completion);
    }

    /* DataPreserver+Shop */
    public static void saveLocations(STCompletion.Base completion){
        DataPreserverImplShop.saveLocations(completion);
    }

    public static void saveShops(Integer locationID, String order, Integer lastObjectID, String shopSortType, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplShop.saveShops(locationID, order, lastObjectID, shopSortType, shouldDelete, completion);
    }

    public static void saveShop(Integer shopID, STCompletion.Base completion){
        DataPreserverImplShop.saveShop(shopID, completion);
    }

    /* DataPreserver+User */
    public static void saveUser(Integer userID, STCompletion.Base completion){
        DataPreserverImplUser.saveUser(userID, completion);
    }

    public static void saveLikesOfUser(Integer userID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplUser.saveLikesOfUser(userID, lastObjectID, shouldDelete, completion);
    }

    public static void saveStaffsOfShop(Integer shopID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplUser.saveStaffsOfShop(shopID, lastObjectID, shouldDelete, completion);
    }

    public static void saveFollowingOfUser(Integer userID, Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplUser.saveFollowingOfUser(userID, lastObjectID, completion);
    }

    public static void saveRepliesOfUser(Integer userID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplUser.saveRepliesOfUser(userID, lastObjectID, shouldDelete, completion);
    }

    public static void saveFollowerOfUser(Integer userID, Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplUser.saveFollowerOfUser(userID, lastObjectID, completion);
    }

    /* DataPreserver+Conversation */
    public static void saveConversation(Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplConversation.saveConversation(lastObjectID, completion);
    }

    /* DataPreserver+Message */
    public static void saveMessages(Integer conversationID, Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplMessage.saveMessages(conversationID, lastObjectID, completion);
    }

    public static void saveMessagesWithUserID(Integer userID, Integer lastObjectID, Integer latestObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplMessage.saveMessagesWithUserID(userID, lastObjectID, latestObjectID, completion);
    }

    /* DataPreserver+Event */
    public static void saveFetureEvents(Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplEvent.saveFetureEvents(lastObjectID, shouldDelete, completion);
    }

    public static void saveOngoingEvents(Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplEvent.saveOngoingEvents(lastObjectID, shouldDelete, completion);
    }

    public static void saveEventsOfShop(Integer shopID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplEvent.saveEventsOfShop(shopID, lastObjectID, shouldDelete, completion);
    }

    public static void saveInterestsOfUser(Integer userID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplEvent.saveInterestsOfUser(userID, lastObjectID, shouldDelete, completion);
    }

    public static void saveEvent(Integer eventID, STCompletion.Base completion){
        DataPreserverImplEvent.saveEvent(eventID, completion);
    }

    /* DataPreserver+Follow */

    public static void saveFollow(Integer userID, Integer followID, STCompletion.Base completion){
        DataPreserverImplFollow.saveFollow(userID, followID, completion);
    }

    public static void deleteFollow(Integer followID, STCompletion.Base completion){
        DataPreserverImplFollow.deleteFollow(followID, completion);
    }

    /* DataPreserver+Comment */
    public static void getComments(Integer postID, Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplComment.getComments(postID, lastObjectID, completion);
    }

    /* DataPreserver+Watch */
    public static void createWatchForPost(Integer postID, Integer watchID, STCompletion.Base completion){
        DataPreserverImplWatch.createWatchForPost(postID, watchID, completion);
    }

    public static void deleteWatchInPostWithWatchID(Integer postID, STCompletion.Base completion){
        DataPreserverImplWatch.deleteWatchInPostWithWatchID(postID, completion);
    }

    /* DataPreserver+Notification */
    public static void saveNotificationState(STCompletion.Base completion){
        DataPreserverImplNotification.saveNotificationState(completion);
    }

    public static void saveNotificationActivity(Integer lastObjectID, STCompletion.WithLastObjectID completion){
        DataPreserverImplNotification.saveNotificationActivity(lastObjectID, completion);
    }

    /* DataPreserver+Article */
    public static void saveArticles(Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplArticle.saveArticles(lastObjectID, shouldDelete, completion);
    }

    public static void saveCategorizedArticles(Integer categoryID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplArticle.saveCategorizedArticles(categoryID, lastObjectID, shouldDelete, completion);
    }

    public static void saveTopArticles(boolean shouldDelete, STCompletion.Base completion){
        DataPreserverImplArticle.saveTopArticles(shouldDelete, completion);
    }

    public static void saveRelateKeywordArticles(Integer tagID, Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplArticle.saveRelateKeywordArticles(tagID, lastObjectID, shouldDelete, completion);
    }

    public static void saveArticle(Integer articleID, STCompletion.Base completion){
        DataPreserverImplArticle.saveArticle(articleID, completion);
    }

    /* DataPreserver+Like */
    public static void saveLikeID(Integer itemID, Integer likeID, STCompletion.Base completion){
        DataPreserverImplLike.saveLikeID(itemID, likeID, completion);
    }

    public static void deleteLikeID(Integer itemID, STCompletion.Base completion){
        DataPreserverImplLike.deleteLikeID(itemID, completion);
    }

    /* DataPreserver+Tag */
    public static void saveTags(Integer lastObjectID, boolean shouldDelete, STCompletion.WithLastObjectID completion){
        DataPreserverImplTag.saveTags(lastObjectID, shouldDelete, completion);
    }

    /* DataPreserver+Image */
    public static void saveImageWithUrl(String url, Bitmap bitmap){
        DataPreserverImplImage.saveImageWithUrl(url, bitmap);
    }

}
