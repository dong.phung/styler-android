package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STFollow;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplFollow {

    public static STFollow getFollow(int followID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STFollow.class).equalTo("followID",followID).findFirst();
    }

    public static RealmResults<STFollow> getFollowsOfUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STFollow> realmResults = realm.where(STFollow.class).equalTo("userID",userID).findAll();
        realmResults = realmResults.sort("createdAt", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STFollow> getFollowersOfStaff(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STFollow> realmResults = realm.where(STFollow.class).equalTo("staffID",userID).findAll();
        realmResults = realmResults.sort("createdAt", Sort.DESCENDING);
        return realmResults;
    }
}
