package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STNotification;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplNotification {
    public static Boolean checkNotificationState(){
        return false;
        //TODO: not using on iOS
    }
    public static RealmResults<STNotification> getNotificationActivities(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STNotification> realmResults = realm.where(STNotification.class).findAll();
        realmResults = realmResults.sort("notificationID", Sort.DESCENDING);
        return realmResults;
    }
}
