package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STPost;
import link.styler.models.STWatch;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplWatch {
    public static STWatch getWatch(int watchID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STWatch.class).equalTo("watchID",watchID).findFirst();
    }

    public static RealmResults<STWatch> getWatchesOfUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STWatch> realmResults = realm.where(STWatch.class).equalTo("userID",userID).findAll();
        realmResults = realmResults.sort("createdAt", Sort.DESCENDING);
        return realmResults;
    }

    public static STPost getPostFromWatch(int watchID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STPost.class).equalTo("watchID",watchID).findFirst();
    }

    public static int getWatchIDFromPostID(int postID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        STPost post = new STPost();
        if(realm.where(STPost.class).equalTo("postID",postID).findFirst() != null){
            post = realm.where(STPost.class).equalTo("postID",postID).findFirst();
            return post.watchID;
        } else {
            return 0;
        }
    }
}
