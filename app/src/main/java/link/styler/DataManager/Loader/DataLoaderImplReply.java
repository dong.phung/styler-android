package link.styler.DataManager.Loader;

import io.realm.Realm;
import link.styler.models.STReply;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplReply {
    public static STReply getReply(int replyID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STReply.class).equalTo("replyID",replyID).findFirst();
    }
}
