package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.STStructs.STEnums;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STPopularItem;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplItem {
    public static RealmResults<STItemSort> getItemsWithPostID(int postID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STItemSort>realmResults = realm.where(STItemSort.class).equalTo("type", STEnums.ItemSortType.None.getItemSortType()).findAll();
        realmResults = realmResults.where().equalTo("postID",postID).findAll();
        realmResults = realmResults.sort("itemSortKey", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STPopularItem> getPopularItems(){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        double oneWeakAgo = System.currentTimeMillis()/1000 + -60*60*24*7;
        RealmResults<STPopularItem> realmResults = realm.where(STPopularItem.class).greaterThan("updateAt", oneWeakAgo).findAll();
        realmResults =realmResults.sort("latestLikeCount",Sort.DESCENDING);
        return realmResults;
    }

    public static STItem getItemWithItemID(int itemID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STItem.class).equalTo("itemID",itemID).findFirst();
    }

    public static RealmResults<STItemSort> getRepliesOfUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STItemSort> realmResults = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Reply.getItemSortType()).findAll();
        realmResults = realmResults.where().equalTo("userID",userID).findAll();
        realmResults = realmResults.sort("itemCreatedAt",Sort.DESCENDING);
        return realmResults;
    }

    // <=> getItemsOfShop
    public static RealmResults<STItemSort> getItemsOfShopWithSTItemSort(int shopID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STItemSort> realmResults = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Shop.getItemSortType()).findAll();
        realmResults = realmResults.where().equalTo("shopID",shopID).findAll();
        return realmResults;
    }
    // <=> getItemsOfShop
    public static RealmResults<STItem> getItemsOfShopWithSTItem(int shopID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STItem.class).equalTo("shopID",shopID).findAll();

    }

    public static RealmResults<STItemSort> getItemsOfShopWithCategory(int shopID, int categoryID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STItemSort> realmResults = realm.where(STItemSort.class).equalTo("type",STEnums.ItemSortType.Shop.getItemSortType()).findAll();
        realmResults = realmResults.where().equalTo("shopID",shopID).findAll();
        realmResults = realmResults.where().equalTo("categoryID",categoryID).findAll();
        return realmResults;
    }
}
