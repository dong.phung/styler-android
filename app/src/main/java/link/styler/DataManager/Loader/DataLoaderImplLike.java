package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STLike;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplLike {
    public static STLike getLike(int likeID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STLike.class).equalTo("likeID",likeID).findFirst();
    }

    public static RealmResults<STLike> getLikesOfUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STLike>realmResults = realm.where(STLike.class).equalTo("userID",userID).findAll();
        realmResults = realmResults.sort("createdAt", Sort.DESCENDING);
        return realmResults;
    }
}
