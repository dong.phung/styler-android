package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.STStructs.STEnums;
import link.styler.models.STCategory;
import link.styler.models.STPost;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplCategory {
    public static STCategory getCategory (int categoryID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STCategory.class).equalTo("categoryID",categoryID).findFirst();
    }

    public static RealmResults<STCategory> getPostCategories(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STCategory.class).equalTo("type", STEnums.CategoryType.Post.getCategoryType()).findAll();
    }

    public static RealmResults<STCategory> getArticleCategories(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STCategory.class).equalTo("type", STEnums.CategoryType.Article.getCategoryType()).findAll();
    }

    public static RealmResults<STCategory> getArticleChildCategories(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STCategory.class).equalTo("type", STEnums.CategoryType.ArticleChild.getCategoryType()).findAll();
    }
}
