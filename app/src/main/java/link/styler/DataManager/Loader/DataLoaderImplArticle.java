package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.STStructs.STEnums;
import link.styler.models.STArticle;
import link.styler.models.STArticleSort;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplArticle {
    public static STArticle getArticle(int articleID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STArticle.class).equalTo("articleID", articleID).findFirst();
    }

    public static RealmResults<STArticleSort> getArticles(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STArticleSort> realmResults = realm.where(STArticleSort.class).equalTo("type", STEnums.ArticleSortType.Timeline.getArticleSortType()).findAll();
        realmResults = realmResults.sort("publishedAt", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STArticleSort> getCategoriezedArticles(int categoryID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STArticleSort> realmResults = realm.where(STArticleSort.class).equalTo("type", STEnums.ArticleSortType.Categorized.getArticleSortType()).findAll();
        realmResults = realmResults.where().equalTo("categoryID", categoryID).findAll();
        realmResults = realmResults.sort("publishedAt", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STArticleSort> getRelateKeywordArticles(int tagID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STArticleSort> realmResults = realm.where(STArticleSort.class).equalTo("type", STEnums.ArticleSortType.Tagged.getArticleSortType()).findAll();
        realmResults = realmResults.where().equalTo("tagID", tagID).findAll();
        realmResults = realmResults.sort("publishedAt", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STArticleSort> getFeatureArticles(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STArticleSort> realmResults = realm.where(STArticleSort.class).equalTo("type", STEnums.ArticleSortType.Feature.getArticleSortType()).findAll();
        realmResults = realmResults.where().equalTo("categoryID", 0).findAll();
        realmResults = realmResults.sort("publishedAt", Sort.DESCENDING);
        return realmResults;
    }

    public static STArticleSort getFeatureArticle(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STArticleSort> realmResults = realm.where(STArticleSort.class).equalTo("type",STEnums.ArticleSortType.Feature.getArticleSortType()).findAll();
        realmResults = realmResults.sort("publishedAt",Sort.DESCENDING);
        return realmResults.where().equalTo("categoryID",0).findFirst();
    }
}
