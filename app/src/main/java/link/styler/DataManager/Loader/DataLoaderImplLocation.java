package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STEvent;
import link.styler.models.STLocation;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplLocation {
    public static RealmResults<STLocation> getLocationAll(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STLocation> realmResults = realm.where(STLocation.class).greaterThan("count",0).findAll();
        realmResults = realmResults.sort("count", Sort.DESCENDING);
        return realmResults;
    }

    public static STLocation getLocation(int locationID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STLocation.class).equalTo("locationID",locationID).findFirst();
    }

}
