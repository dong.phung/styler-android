package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STComment;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplComment {
    public static RealmResults<STComment> getComments(int postID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STComment>realmResults = realm.where(STComment.class).equalTo("postID",postID).findAll();
        realmResults = realmResults.sort("commentID", Sort.ASCENDING);
        return realmResults;
    }
}
