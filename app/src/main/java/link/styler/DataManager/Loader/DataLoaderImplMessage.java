package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STMessage;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplMessage {
    public static STMessage getMessage(int id){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STMessage.class).equalTo("messageID", id).findFirst();
    }

    public static RealmResults<STMessage> getMessages(int conversationID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STMessage> realmResults = realm.where(STMessage.class).equalTo("conversationID",conversationID).findAll();
        realmResults = realmResults.sort("messageID", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STMessage> getMessagesFromDestination(int destinationUserID){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STMessage> realmResults = realm.where(STMessage.class).equalTo("destinationUserID",destinationUserID).findAll();
        realmResults = realmResults.sort("messageID", Sort.ASCENDING);
        return realmResults;
    }
}
