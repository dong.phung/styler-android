package link.styler.DataManager.Loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.models.STImage;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplImage {

    public static STImage getSTImage(String url){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STImage.class).equalTo("url",url).findFirst();
    }

    public static void getSTImage(final String url, final STCompletion.WithSTImage completion){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = Realm.getDefaultInstance();
                completion.onRequestComplete(realm.where(STImage.class).equalTo("url",url).findFirst());
            }
        });
        thread.start();
    }

    public static void getSTImageInAsync (String url, STCompletion.WithSTImage completion){
        loadSTImageDoInBackground(url, completion);
    }

    public static void getImageDataWithBlock(String url, STCompletion.WithByteArray completion){
        //Logger.method();
        //TODO: not using on iOS
    }

    // UIImage => bitmap
    public static Bitmap getBitmapImage(String url){
        //Logger.method();
        Bitmap bitmap=null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (IOException e) { e.printStackTrace();}
        return bitmap;
    }

    private static void loadSTImageDoInBackground(final String url, final STCompletion.WithSTImage completion){
        if (url == "") {
            completion.onRequestComplete(null);
            return;
        }
        final Realm realm = Realm.getDefaultInstance();
        if(realm.where(STImage.class).equalTo("url",url).findFirst() != null){
            STImage stImage = new STImage();
            stImage = realm.where(STImage.class).equalTo("url",url).findFirst();
            completion.onRequestComplete(stImage);
            return;
        }else {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            final STImage stImage = new STImage();
                            try {
                                stImage.fillByURL(realm,url);
                                realm.copyToRealmOrUpdate(stImage);
                                Thread thread1 = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        completion.onRequestComplete(stImage);
                                    }
                                });
                                thread1.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            });
            thread.start();

        }

    }

    public static void deleteOldImages(){
        //Logger.method();
        double filterTime = System.currentTimeMillis()/1000 + -60*60*24*7;
        Realm realm = Realm.getDefaultInstance();
        final RealmResults realmResults = realm.where(STImage.class).lessThan("registeredAt",filterTime).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
                Log.i("Tan020","delete deleteOldImages");
            }
        });
    }

    public static void deleteAllImages(){
        Realm realm = Realm.getDefaultInstance();
        final RealmResults realmResults = realm.where(STImage.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
                Log.i("Tan020","delete deleteAllImages");
            }
        });
    }
}
