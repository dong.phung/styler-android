package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STTag;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplTag {
    public static RealmResults<STTag> getTags(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STTag>realmResults = realm.where(STTag.class).findAll();
        realmResults = realmResults.sort("name", Sort.ASCENDING);
        return realmResults;
    }
}
