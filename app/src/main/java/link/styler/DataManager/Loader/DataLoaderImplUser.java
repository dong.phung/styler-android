package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.STStructs.STEnums;
import link.styler.models.STUser;
import link.styler.models.STUserSort;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplUser {

    public static STUser getUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STUser.class).equalTo("userID", userID).findFirst();
    }

    public static STUserSort getUserSort(String key){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STUserSort.class).equalTo("userSortKey", key).findFirst();
    }

    public static RealmResults<STUserSort> getStaffsFromShop(int shopID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STUserSort> realmResults = realm.where(STUserSort.class).equalTo("type", STEnums.UserSortType.Staff.getUserSortType()).findAll();
        realmResults = realmResults.where().equalTo("shopID", shopID).findAll();
        return realmResults;
    }
}
