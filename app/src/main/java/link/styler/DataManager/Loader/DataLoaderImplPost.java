package link.styler.DataManager.Loader;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.STStructs.STEnums;
import link.styler.models.STPost;
import link.styler.models.STPostSort;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplPost {
    public static RealmResults<STPostSort> getPopularPosts(){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.Popular.getPostSortType()).findAll();
        realmResults = realmResults.sort("newestTime", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STPostSort> getLatestPosts(){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.Latest.getPostSortType()).findAll();
        realmResults = realmResults.sort("postID", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STPostSort> getUserPosts(int userID){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.User.getPostSortType()).findAll();
        realmResults = realmResults.where().equalTo("userID", userID).findAll();
        realmResults = realmResults.sort("postID", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STPostSort> getUserWatches(int userID){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.Watch.getPostSortType()).findAll();
        realmResults = realmResults.where().equalTo("userID", userID).findAll();
        realmResults = realmResults.sort("newestTime", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STPostSort> getCategorizedPosts(int categoryID){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("categoryID", categoryID).findAll();
        realmResults = realmResults.sort("newestTime", Sort.DESCENDING);
        return realmResults;
    }

    public static void deleteUnrepliedPosts(int categoryID){

        Realm realm = Realm.getDefaultInstance();
        final RealmResults<STPostSort> realmResults= realm.where(STPostSort.class).equalTo("type",STEnums.PostSortType.Unreplied.getPostSortType()).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
                Log.i("Tan020","delete deleteUnrepliedPosts");
            }
        });
    }

    public static RealmResults<STPostSort> getCategorizedPostsWithoutReply(int categoryID){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        if(categoryID != 0){
            RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.Unreplied.getPostSortType()).findAll();
            realmResults = realmResults.where().equalTo("categoryID", categoryID).findAll();
            realmResults = realmResults.sort("postID", Sort.DESCENDING);
            return realmResults;
        }else {
            RealmResults<STPostSort> realmResults = realm.where(STPostSort.class).equalTo("type", STEnums.PostSortType.Unreplied.getPostSortType()).findAll();
            realmResults = realmResults.sort("postID", Sort.DESCENDING);
            return realmResults;
        }
    }

    public static STPost getPost(int postID){
        //Logger.method()
        Realm realm = Realm.getDefaultInstance();
        STPost data = realm.where(STPost.class).equalTo("postID", postID).findFirst();
        if(data != null)
        {
            return data;
        }else {
            return null;
        }
    }
}
