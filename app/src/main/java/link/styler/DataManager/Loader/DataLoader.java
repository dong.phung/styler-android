package link.styler.DataManager.Loader;

import android.graphics.Bitmap;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.models.STArticle;
import link.styler.models.STArticleSort;
import link.styler.models.STCategory;
import link.styler.models.STComment;
import link.styler.models.STConversation;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.models.STFollow;
import link.styler.models.STImage;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STLike;
import link.styler.models.STLocation;
import link.styler.models.STMessage;
import link.styler.models.STNotification;
import link.styler.models.STPopularItem;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.models.STTag;
import link.styler.models.STUser;
import link.styler.models.STUserSort;
import link.styler.models.STWatch;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoader {
    /* DataLoader */

    /* DataLoader+Post */
    public static RealmResults<STPostSort> getPopularPosts(){
        return DataLoaderImplPost.getPopularPosts();
    }

    public static RealmResults<STPostSort> getLatestPosts(){
        return DataLoaderImplPost.getLatestPosts();
    }

    public static RealmResults<STPostSort> getUserPosts(int userID){
        return DataLoaderImplPost.getUserPosts(userID);
    }

    public static RealmResults<STPostSort> getUserWatches(int userID){
        return DataLoaderImplPost.getUserWatches(userID);
    }

    public static RealmResults<STPostSort> getCategorizedPosts(int categoryID){
        return DataLoaderImplPost.getCategorizedPosts(categoryID);
    }

    public static void deleteUnrepliedPosts(int categoryID){
        DataLoaderImplPost.deleteUnrepliedPosts(categoryID);
    }

    public static RealmResults<STPostSort> getCategorizedPostsWithoutReply(int categoryID){
        return DataLoaderImplPost.getCategorizedPostsWithoutReply(categoryID);
    }

    public static STPost getPost(int postID){
        return DataLoaderImplPost.getPost(postID);
    }

    /* DataLoader+Category */
    public static STCategory getCategory (int categoryID){
        return DataLoaderImplCategory.getCategory(categoryID);
    }

    public static RealmResults<STCategory> getPostCategories(){
        return DataLoaderImplCategory.getPostCategories();
    }

    public static RealmResults<STCategory> getArticleCategories(){
        return DataLoaderImplCategory.getArticleCategories();
    }

    public static RealmResults<STCategory> getArticleChildCategories(){
        return DataLoaderImplCategory.getArticleChildCategories();
    }

    /* DataLoader+User */
    public static STUser getUser(int userID){
        return DataLoaderImplUser.getUser(userID);
    }

    public static STUserSort getUserSort(String key){
        return DataLoaderImplUser.getUserSort(key);
    }

    public static RealmResults<STUserSort> getStaffsFromShop(int shopID){
        return DataLoaderImplUser.getStaffsFromShop(shopID);
    }

    /* DataLoader+Item */
    public static RealmResults<STItemSort> getItemsWithPostID(int postID){
        return DataLoaderImplItem.getItemsWithPostID(postID);
    }

    public static RealmResults<STPopularItem> getPopularItems(){
        return DataLoaderImplItem.getPopularItems();
    }

    public static STItem getItemWithItemID(int itemID){
        return DataLoaderImplItem.getItemWithItemID(itemID);
    }

    public static RealmResults<STItemSort> getRepliesOfUser(int userID){
        return DataLoaderImplItem.getRepliesOfUser(userID);
    }
        // <=> getItemsOfShop
    public static RealmResults<STItemSort> getItemsOfShopWithSTItemSort(int shopID){
        return DataLoaderImplItem.getItemsOfShopWithSTItemSort(shopID);
    }
        // <=> getItemsOfShop
    public static RealmResults<STItem> getItemsOfShopWithSTItem(int shopID){
        return DataLoaderImplItem.getItemsOfShopWithSTItem(shopID);
    }

    public static RealmResults<STItemSort> getItemsOfShopWithCategory(int shopID, int categoryID){
        return DataLoaderImplItem.getItemsOfShopWithCategory(shopID, categoryID);
    }

    /* DataLoader+Event */
    public static STEvent getEvent(int eventID){
        return DataLoaderImplEvent.getEvent(eventID);
    }

    public static STEventSort getMostPopularOngoingEvent(){
        return DataLoaderImplEvent.getMostPopularOngoingEvent();
    }

    public static RealmResults<STEventSort> getOngoingEvents(){
        return DataLoaderImplEvent.getOngoingEvents();
    }

    public static RealmResults<STEventSort> getFetureEvents(){
        return DataLoaderImplEvent.getFetureEvents();
    }

    public static RealmResults<STEventSort> getInterestEventsOfUser(int userID){
        return DataLoaderImplEvent.getInterestEventsOfUser(userID);
    }

    public static RealmResults<STEventSort> getEventsOfShop(int shopID){
        return DataLoaderImplEvent.getEventsOfShop(shopID);
    }

    /* DataLoader+Watch */
    public static STWatch getWatch(int watchID){
        return DataLoaderImplWatch.getWatch(watchID);
    }

    public static RealmResults<STWatch> getWatchesOfUser(int userID){
        return DataLoaderImplWatch.getWatchesOfUser(userID);
    }

    public static STPost getPostFromWatch(int watchID){
        return DataLoaderImplWatch.getPostFromWatch(watchID);
    }

    public static int getWatchIDFromPostID(int postID){
        return DataLoaderImplWatch.getWatchIDFromPostID(postID);
    }

    /* DataLoader+Notification */
    public static Boolean checkNotificationState(){
        return DataLoaderImplNotification.checkNotificationState();
    }
    public static RealmResults<STNotification> getNotificationActivities(){
        return DataLoaderImplNotification.getNotificationActivities();
    }

    /* DataLoader+Conversation */
    public static STConversation getConversation(int id){
        return DataLoaderImplConversation.getConversation(id);
    }

    public static STConversation getConversationFromDestinationUserID(int destinationUserID){
        return DataLoaderImplConversation.getConversationFromDestinationUserID(destinationUserID);
    }

    public static RealmResults<STConversation> getConversations(){
        return DataLoaderImplConversation.getConversations();
    }

    /* DataLoader+Message */
    public static STMessage getMessage(int id){
        return DataLoaderImplMessage.getMessage(id);
    }

    public static RealmResults<STMessage> getMessages(int conversationID){
        return DataLoaderImplMessage.getMessages(conversationID);
    }

    public static RealmResults<STMessage> getMessagesFromDestination(int destinationUserID){
        return DataLoaderImplMessage.getMessagesFromDestination(destinationUserID);
    }

    /* DataLoader+Article */
    public static STArticle getArticle(int articleID){
        return DataLoaderImplArticle.getArticle(articleID);
    }

    public static RealmResults<STArticleSort> getArticles(){
        return DataLoaderImplArticle.getArticles();
    }

    public static RealmResults<STArticleSort> getCategoriezedArticles(int categoryID){
        return DataLoaderImplArticle.getCategoriezedArticles(categoryID);
    }

    public static RealmResults<STArticleSort> getRelateKeywordArticles(int tagID){
        return DataLoaderImplArticle.getRelateKeywordArticles(tagID);
    }

    public static RealmResults<STArticleSort> getFeatureArticles(){
        return DataLoaderImplArticle.getFeatureArticles();
    }

    public static STArticleSort getFeatureArticle(){
        return DataLoaderImplArticle.getFeatureArticle();
    }

    /* DataLoader+Shop */
    public static STShop getShop(int shopID){
        return DataLoaderImplShop.getShop(shopID);
    }

    public static STLocation getLocation(int locationID){
        return DataLoaderImplLocation.getLocation(locationID);
    }

    public static RealmResults<STShop> getShopList(int locationID){
        return DataLoaderImplShop.getShopList(locationID);
    }

    /* DataLoader+Comment */
    public static RealmResults<STComment> getComments(int postID){
        return DataLoaderImplComment.getComments(postID);
    }

    /* DataLoader+Tag */
    public static RealmResults<STTag> getTags(){
        return DataLoaderImplTag.getTags();
    }

    /* DataLoader+Image */
    public static STImage getSTImage(String url){
        return DataLoaderImplImage.getSTImage(url);
    }

    public static void getSTImage(String url, STCompletion.WithSTImage completion){
        DataLoaderImplImage.getSTImage(url, completion);
    }

    public static void getSTImageInAsync(String url, STCompletion.WithSTImage completion){
        DataLoaderImplImage.getSTImageInAsync(url, completion);
    }

    public static void getImageDataWithBlock(String url, STCompletion.WithByteArray completion){
        DataLoaderImplImage.getImageDataWithBlock(url, completion);
    }
        // UIImage => bitmap
    public static Bitmap getBitmapImage(String url){
        return  DataLoaderImplImage.getBitmapImage(url);
    }
    public static Bitmap getUIImage(String url){
        return  DataLoaderImplImage.getBitmapImage(url);
    }
        // end (xài cái nào cũng đc =)) )

    public static void deleteOldImages(){
        DataLoaderImplImage.deleteOldImages();
    }

    public static void deleteAllImages(){
        DataLoaderImplImage.deleteAllImages();
    }

    /* DataLoader+Location */
    public static RealmResults<STLocation> getLocationAll(){
        return DataLoaderImplLocation.getLocationAll();
    }

    /* DataLoader+Reply */
    public static STReply getReply(int replyID){
        return DataLoaderImplReply.getReply(replyID);
    }

    /* DataLoader+Like */
    public static STLike getLike(int likeID){
        return DataLoaderImplLike.getLike(likeID);
    }

    public static RealmResults<STLike> getLikesOfUser(int userID){
        return DataLoaderImplLike.getLikesOfUser(userID);
    }

     /* DataLoader+Like */
     public static STFollow getFollow(int followID){
         return DataLoaderImplFollow.getFollow(followID);
     }

    public static RealmResults<STFollow> getFollowsOfUser(int userID){
        return DataLoaderImplFollow.getFollowsOfUser(userID);
    }

    public static RealmResults<STFollow> getFollowersOfStaff(int userID){
        return DataLoaderImplFollow.getFollowersOfStaff(userID);
    }
}
