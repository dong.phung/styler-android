package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.models.STShop;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplShop {
    public static STShop getShop(int shopID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STShop.class).equalTo("shopID",shopID).findFirst();
    }

    public static RealmResults<STShop> getShopList(int locationID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STShop.class).equalTo("locationID",locationID).findAll();
    }
}
