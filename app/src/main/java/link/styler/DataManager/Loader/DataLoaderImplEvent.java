package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.STStructs.STEnums;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplEvent {
    public static STEvent getEvent(int eventID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STEvent.class).equalTo("eventID",eventID).findFirst();
    }

    public static STEventSort getMostPopularOngoingEvent(){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STEventSort> eventSorts = realm.where(STEventSort.class).equalTo("type", STEnums.EventSortType.Ongoing.getEventSortType()).findAll();
        STEventSort eventSort = new STEventSort();
        int maxInterestCount = 0;
        for (STEventSort e: eventSorts) {
            int interestCount;
            if (e.event != null){
                interestCount = e.event.interestsCount;
                if (interestCount >= maxInterestCount){
                    maxInterestCount = interestCount;
                    eventSort = e;
                }
            }
        }
        return eventSort;
    }

    public static RealmResults<STEventSort> getOngoingEvents(){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STEventSort> realmResults = realm.where(STEventSort.class).equalTo("type", STEnums.EventSortType.Ongoing.getEventSortType()).findAll();
        realmResults = realmResults.sort("startDate", Sort.DESCENDING);
        return realmResults;
    }

    public static RealmResults<STEventSort> getFetureEvents(){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STEventSort> realmResults = realm.where(STEventSort.class).equalTo("type", STEnums.EventSortType.Feture.getEventSortType()).findAll();
        realmResults = realmResults.sort("startDate", Sort.ASCENDING);
        return realmResults;
    }

    public static RealmResults<STEventSort> getInterestEventsOfUser(int userID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STEventSort> realmResults = realm.where(STEventSort.class).equalTo("type", STEnums.EventSortType.Interest.getEventSortType()).findAll();
        realmResults = realmResults.where().equalTo("userID",userID).findAll();
        realmResults = realmResults.sort("startDate", Sort.ASCENDING);
        return realmResults;
    }

    public static RealmResults<STEventSort> getEventsOfShop(int shopID){
        //Logger.method();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STEventSort> realmResults = realm.where(STEventSort.class).equalTo("type", STEnums.EventSortType.Shop.getEventSortType()).findAll();
        realmResults = realmResults.where().equalTo("shopID",shopID).findAll();
        realmResults = realmResults.sort("startDate", Sort.DESCENDING);
        return realmResults;
    }
}
