package link.styler.DataManager.Loader;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.models.STConversation;

/**
 * Created by Tan Nguyen on 2/6/17.
 */

public class DataLoaderImplConversation {
    public static STConversation getConversation(int id){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STConversation.class).equalTo("conversationID",id).findFirst();
    }

    public static STConversation getConversationFromDestinationUserID(int destinationUserID){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(STConversation.class).equalTo("destinationID",destinationUserID).findFirst();
    }

    public static RealmResults<STConversation> getConversations(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<STConversation>realmResults = realm.where(STConversation.class).findAll();
        realmResults =realmResults.sort("newestTime", Sort.DESCENDING);
        return realmResults;
    }
}
