package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STUser;
import link.styler.models.STUserSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterStaff;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageStaffViewControllerFragment extends Fragment {

    //model
    private RealmResults<STUserSort> staffs;
    private RealmList<STUser> users = new RealmList<>();
    private int shopID = 0;
    private Integer lastObjectID = null;

    //view
    private GridView collectionView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterStaff adapterStaff = null;

    public void createAdapterStaff() {

        this.staffs = DataLoader.getStaffsFromShop(shopID);
        this.users = new RealmList<STUser>();
        for (STUserSort userSort : this.staffs) {
            this.users.add(userSort.user);
        }
        this.adapterStaff = new MyAdapterStaff(getActivity(), users);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (STMyPageStaffViewControllerFragment.this.adapterStaff == null)
            createAdapterStaff();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_staff_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_staff);
        collectionView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        collectionView.setAdapter(adapterStaff);
        if (adapterStaff.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        STMyPageStaffViewControllerFragment.this.collectionView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageStaffVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageStaffVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageStaffVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageStaffVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageStaffVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageStaffVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveStaffsOfShop(shopID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageStaffViewControllerFragment.this.staffs = DataLoader.getStaffsFromShop(shopID);
                    STMyPageStaffViewControllerFragment.this.users = new RealmList<STUser>();
                    for (STUserSort userSort : STMyPageStaffViewControllerFragment.this.staffs) {
                        STMyPageStaffViewControllerFragment.this.users.add(userSort.user);
                    }

                    if (STMyPageStaffViewControllerFragment.this.adapterStaff == null)
                        createAdapterStaff();

                    STMyPageStaffViewControllerFragment.this.adapterStaff = (MyAdapterStaff) STMyPageStaffViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageStaffViewControllerFragment.this.adapterStaff.setUsers(STMyPageStaffViewControllerFragment.this.users);
                    STMyPageStaffViewControllerFragment.this.adapterStaff.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageStaffViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveStaffsOfShop(shopID, STMyPageStaffViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageStaffViewControllerFragment.this.staffs = DataLoader.getStaffsFromShop(shopID);
                    STMyPageStaffViewControllerFragment.this.users = new RealmList<STUser>();
                    for (STUserSort userSort : STMyPageStaffViewControllerFragment.this.staffs) {
                        STMyPageStaffViewControllerFragment.this.users.add(userSort.user);
                    }

                    if (STMyPageStaffViewControllerFragment.this.adapterStaff == null)
                        createAdapterStaff();

                    STMyPageStaffViewControllerFragment.this.adapterStaff = (MyAdapterStaff) STMyPageStaffViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageStaffViewControllerFragment.this.adapterStaff.setUsers(STMyPageStaffViewControllerFragment.this.users);
                    STMyPageStaffViewControllerFragment.this.adapterStaff.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageStaffViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }
}