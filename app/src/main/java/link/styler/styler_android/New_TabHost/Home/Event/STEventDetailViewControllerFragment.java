package link.styler.styler_android.New_TabHost.Home.Event;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.GeocodeAddressString;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STShareAction;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STEvent;
import link.styler.models.STUser;
import link.styler.models.STUserSort;
import link.styler.styler_android.New_TabHost.Adapter.ListImageRecyclerViewAdapter;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Common.TabsPagerfragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.New_TabHost.PopUpMoreActionEvent;
import link.styler.styler_android.R;


public class STEventDetailViewControllerFragment extends Fragment {

    //model
    private int eventID = 0;
    private STEvent event;

    //view
    private LinearLayout linearLayoutFragment;
    private ScrollView scrollView;
    private ProgressBar progressBar;

    private STBaseNavigationController navigation;
    //Header Event
    private ImageView eventImageView;
    private TextView titleLabel;
    private ImageButton goingButton;
    private TextView goingCountLabel;
    private TextView goingLabel;
    private LinearLayout viewGoing;
    private TextView dateTitleLabel;
    private TextView dateLabel;
    private ImageButton shareButton;
    private TextView descriptionTitleLabel;
    private TextView descriptionLabel;
    private TextView locationTitleLabel;
    private TextView locationLabel;
    private LinearLayout showMapButton;

    //Event detail
    private TextView titleLabelEventDetail;
    //Shop status
    private ImageView shopImageView;
    private TextView replyCountLabel;
    private TextView eventCountLabel;
    private TextView staffCountLabel;
    private Button actionButton;
    private LinearLayout showView;
    //end shop status

    private TextView shopNameLabel;
    private TextView addressLabel;
    private TextView descriptionLabelShop;
    private TextView nearestStationLabel;
    private TextView contactLabelShop;
    private TextView phoneNumberLabelShop;
    private ImageView phoneImageViewShop;

    //Participation

    private ViewPager viewPagerMap;
    private LinearLayout showAllUserView;
    private TextView titleLabel1;
    private TextView countLabel1;


    //List image user
    private RecyclerView recyclerViewImageUser;

    //helper
    private boolean isGoing;
    private boolean isLoading = false;
    private int myShopID = 0;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;
    private static boolean getMap = false;

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public static void setResumeType(STEnums.resumeType resumeType) {
        STEventDetailViewControllerFragment.resumeType = resumeType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMap = false;
        try {
            myShopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_event_detail_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        ///////
        navigation = new STBaseNavigationController(view);

        eventImageView = (ImageView) view.findViewById(R.id.eventImageViewHeader);

        titleLabel = (TextView) view.findViewById(R.id.titleLabel);

        goingButton = (ImageButton) view.findViewById(R.id.goingButton);
        goingCountLabel = (TextView) view.findViewById(R.id.goingCountLabel);
        goingLabel = (TextView) view.findViewById(R.id.goingLabel);
        viewGoing = (LinearLayout) view.findViewById(R.id.viewGoing);

        dateTitleLabel = (TextView) view.findViewById(R.id.dateTitleLabel);
        dateLabel = (TextView) view.findViewById(R.id.dateLabel);
        shareButton = (ImageButton) view.findViewById(R.id.shareButton);

        descriptionTitleLabel = (TextView) view.findViewById(R.id.descriptionTitleLabel);
        descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
        locationTitleLabel = (TextView) view.findViewById(R.id.locationTitleLabel);
        locationLabel = (TextView) view.findViewById(R.id.locationLabel);

        showMapButton = (LinearLayout) view.findViewById(R.id.showMapButton);

        //Map Fragment
        viewPagerMap = (ViewPager) view.findViewById(R.id.mapViewPager);

        //List image user
        recyclerViewImageUser = (RecyclerView) view.findViewById(R.id.list_user_image_participation);

        showAllUserView = (LinearLayout) view.findViewById(R.id.showAllUserView);
        titleLabel1 = (TextView) view.findViewById(R.id.titleLabel1);
        countLabel1 = (TextView) view.findViewById(R.id.countLabel1);
        ///////
        titleLabelEventDetail = (TextView) view.findViewById(R.id.titleLabelShop);
        shopImageView = (ImageView) view.findViewById(R.id.shopImageViewShop);

        replyCountLabel = (TextView) view.findViewById(R.id.replyCountLabel);
        eventCountLabel = (TextView) view.findViewById(R.id.eventCountLabel);
        staffCountLabel = (TextView) view.findViewById(R.id.staffCountLabel);
        actionButton = (Button) view.findViewById(R.id.actionButton);
        actionButton.setText("ショップ詳細をみる");
        showView = (LinearLayout) view.findViewById(R.id.showView);

        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabelEvent);
        addressLabel = (TextView) view.findViewById(R.id.addressLabelEvent);
        descriptionLabelShop = (TextView) view.findViewById(R.id.descriptionLabelEvent);
        nearestStationLabel = (TextView) view.findViewById(R.id.nearestStationLabelEvent);
        contactLabelShop = (TextView) view.findViewById(R.id.contactLabelEvent);
        phoneNumberLabelShop = (TextView) view.findViewById(R.id.phoneNumberLabelEvent);
        phoneImageViewShop = (ImageView) view.findViewById(R.id.phoneImageViewEvent);
        ///////

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        getData();
        addAction();
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveEvent(eventID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if (isSuccess) {
                    event = DataLoader.getEvent(eventID);
                    setupView();
                }
                isLoading = false;
                scrollView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setupView() {
        navigation.getTitleNavigation().setText("Event");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getUpdateButton().setVisibility(View.GONE);
        navigation.getMoreButton().setEnabled(false);
        navigation.getMoreButton().setVisibility(View.INVISIBLE);
        Log.i("aaaaaaaaaaaaa", "event.shopID: " + event.shopID);
        Log.i("aaaaaaaaaaaaa", "myShopID: " + myShopID);

        if (event.shopID == myShopID) {
            navigation.getMoreButton().setVisibility(View.VISIBLE);
            navigation.getMoreButton().setImageResource(R.drawable.icon_more2x);
            navigation.getMoreButton().setEnabled(true);
        }
        if (event != null) {
            setUpHeaderView(event);
            if (!getMap) {
                setUpMapFragment(event.location);
                getMap = true;
            }
            setUpDetailEvent(event);

            List<String> listURLImage = new ArrayList<>();
            List<STUserSort> stUserSorts = event.interestUsers;
            STUser user;
            for (int i = 0; i < stUserSorts.size(); i++) {
                user = stUserSorts.get(i).user;
                if (user == null)
                    continue;
                String imageURL = user.imageURL;
                if (!imageURL.isEmpty())
                    listURLImage.add(imageURL);
            }

            Logger.print("listURLImage " + listURLImage);

            setRecyclerViewImageUser(listURLImage);

            titleLabel1.setText("参加者の一覧を見る ＞");
            countLabel1.setText(event.interestUsers.size() + "人");

            if (event.interestID == 0) {
                goingButton.setImageResource(R.drawable.button_to_go2x);
                goingCountLabel.setTextColor(0xFF9B9B9B);
            } else {
                goingButton.setImageResource(R.drawable.button_going2x);
                goingCountLabel.setTextColor(0xFF28ABEC);
            }

            isGoing = event.interestID == 0 ? true : false;
        }
    }

    private void setUpHeaderView(STEvent event) {
        String urlImage = event.imageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(getContext()).load(urlImage).transform(new TransformationUtils().new RoundedCornersTransform()).into(eventImageView);
        }
        titleLabel.setText(event.title);
        goingCountLabel.setText("" + event.interestsCount);

        String stringStartDate = "";
        String stringEndDate = "";

        if (!event.startDate.isEmpty()) {
            stringStartDate = event.startDate;
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(stringStartDate);
                format = new SimpleDateFormat("yyyy年MM月dd日");
                stringStartDate = format.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (!event.finishDate.isEmpty()) {
            stringEndDate = event.finishDate;

            DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = formatDate.parse(stringEndDate);
                formatDate = new SimpleDateFormat("yyyy年MM月dd日");
                stringEndDate = formatDate.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        dateLabel.setText(stringStartDate + "~" + stringEndDate);


        descriptionLabel.setText(event.text);

        if (!event.location.isEmpty()) {
            locationLabel.setText(event.location);
        } else {
            locationLabel.setText(event.shop.address);
        }
    }

    private void setUpMapFragment(String location) {

        new GeocodeAddressString(new GeocodeAddressString.AsyncResponse() {
            @Override
            public void processFinish(double lng, double lnt) {
                TabsPagerfragment tabsPagerfragment = new TabsPagerfragment(getChildFragmentManager(), lng, lnt);
                viewPagerMap.setAdapter(tabsPagerfragment);
            }
        }).execute(location);
    }

    private void setUpDetailEvent(STEvent event) {
        String urlEvent = event.shop.imageURL;
        if (!urlEvent.isEmpty())
            Picasso.with(getContext()).load(urlEvent).transform(new TransformationUtils().new RoundedCornersTransform()).into(shopImageView);

        replyCountLabel.setText("" + event.shop.itemCount);
        eventCountLabel.setText("" + event.shop.eventCount);
        staffCountLabel.setText("" + event.shop.staffCount);

        shopNameLabel.setText(event.shop.name);
        addressLabel.setText(event.shop.address);
        descriptionLabelShop.setText(event.shop.profile);
        phoneNumberLabelShop.setText(event.shop.tel);
    }

    private void setRecyclerViewImageUser(List<String> urlImage) {
        Logger.print("setRecyclerViewImageUser ");
        ListImageRecyclerViewAdapter listImageRecyclerViewAdapter = new ListImageRecyclerViewAdapter(getContext(), urlImage);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImageUser.setLayoutManager(layoutManager);
        recyclerViewImageUser.setItemAnimator(new DefaultItemAnimator());
        recyclerViewImageUser.setAdapter(listImageRecyclerViewAdapter);
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        navigation.getMoreButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event.shopID == myShopID) {
                    PopUpMoreActionEvent popUp = new PopUpMoreActionEvent(getActivity(), event);
                    popUp.show();
                }
            }
        });

        goingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goingAction();
            }
        });

        viewGoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goingAction();
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAction();
            }
        });

        showMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMapAction();
            }
        });

        showView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openShopPage();
            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openShopPage();
            }
        });

        showAllUserView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllUser();
            }
        });
    }

    private void goingAction() {
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
            status = "";
        }
        if (status == "" || status.equals("none")) {
            PopUpLogin login = new PopUpLogin(getContext());
            login.show();
        } else {
            if (isGoing) {
                DataSender.createInterest(eventID, new STCompletion.WithSTEvent() {
                    @Override
                    public void onRequestComplete(STEvent event) {
                        if (event != null) {
                            isGoing = !isGoing;
                            goingButton.setImageResource(R.drawable.button_going2x);
                            goingCountLabel.setTextColor(0xFF28ABEC);
                            int goingCount = Integer.parseInt(goingCountLabel.getText().toString());
                            goingCount++;
                            goingCountLabel.setText(goingCount + "");
                        } else {
                            Log.i("goingAction", "fail");
                        }
                    }
                });
            } else {

                DataSender.deleteInterest(eventID, event.interestID, new STCompletion.WithSTEvent() {
                    @Override
                    public void onRequestComplete(STEvent event) {
                        if (event != null) {
                            isGoing = !isGoing;
                            goingButton.setImageResource(R.drawable.button_to_go2x);
                            goingCountLabel.setTextColor(0xFF9B9B9B);
                            int goingCount = Integer.parseInt(goingCountLabel.getText().toString());
                            goingCount--;
                            goingCountLabel.setText(goingCount + "");
                        } else {
                            Log.i("goingAction", "fail");
                        }
                    }
                });
            }
        }
    }

    private void shareAction() {
        STShareAction shareAction = new STShareAction();
        String path = "events/" + eventID;
        shareAction.ShareAction(getActivity(), path);
    }

    private void showMapAction() {
        //o need to convert
        Intent intent = new Intent(getContext(), STMapViewController.class);
        intent.putExtra("address", event.location);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void openShopPage() {

        STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
        fragment.setShopID(event.shopID);
        getFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void showAllUser() {
        STEventGoingListViewControllerFragment fragment = new STEventGoingListViewControllerFragment();
        fragment.setEventID(eventID);
        getFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STEventDetailVCF", "onResume");
        if (resumeType == STEnums.resumeType.patchEvent) {
            getData();
            resumeType = STEnums.resumeType.none;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STEventDetailVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STEventDetailVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STEventDetailVCF", "onDestroy");
    }
}
