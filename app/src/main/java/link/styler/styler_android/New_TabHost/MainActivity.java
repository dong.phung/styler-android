package link.styler.styler_android.New_TabHost;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import io.realm.Realm;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.GCM.RegistrationIntentService;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.DataSharing;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.STStructs.STEnums;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Article.STArticleViewControllerFragment;
import link.styler.styler_android.New_TabHost.Create.STCreateSelectShopViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.Event.STEventDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.PostDetail.STPostDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.STHomePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.Account.STProfileInfoViewControllerActivity;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.Notification.STNotificationPageViewControllerFragment;
import link.styler.styler_android.R;

import link.styler.styler_android.New_TabHost.Create.STCreatePostViewControllerActivity;

import java.io.File;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_LOGS;
import static android.Manifest.permission.WAKE_LOCK;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

@SuppressWarnings("deprecation")

public class MainActivity extends AppCompatActivity {

    private FragmentTabHost tabHost;
    private TabSpec tabSpecHome, tabSpecArtice, tabSpecPost, tabSpecCustomer, tabSpecProfile;
    private String status = STEnums.UserStatus.None.getUserStatus();

    private static int tabHostID = 0;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;
    private static int resumeCreatePostID;
    private static int resumeCreateReplyID;
    private static Integer resumeCreateEventID;


    public static void setTabHostID(int tabHostID) {
        MainActivity.tabHostID = tabHostID;
    }

    public static void setResumeType(STEnums.resumeType resumeType) {
        MainActivity.resumeType = resumeType;
    }

    public static void setResumeCreatePostID(int resumeCreatePostID) {
        MainActivity.resumeCreatePostID = resumeCreatePostID;
    }

    public static void setResumeCreateReplyID(int resumeCreateReplyID) {
        MainActivity.resumeCreateReplyID = resumeCreateReplyID;
    }

    public static void setResumeCreateEventID(Integer resumeCreateEventID) {
        MainActivity.resumeCreateEventID = resumeCreateEventID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("MainActivity", "onCreate");
        setContentView(R.layout.activity_main);

        StylerApi.setCurrentActivity(this);
        DataSharing.getsIntstance();

        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(MainActivity.this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabSpecHome = tabHost.newTabSpec("Home");
        tabSpecArtice = tabHost.newTabSpec("Article");
        tabSpecPost = tabHost.newTabSpec("Post");
        tabSpecCustomer = tabHost.newTabSpec("Customer");
        tabSpecProfile = tabHost.newTabSpec("Profile");

        Realm.init(this);

        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }

        setupHomeController();
        initTabHost();
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                tabHostManager();
            }
        });

        //Load PageLogo
        Intent intent = new Intent(MainActivity.this, StylerActivity.class);
        startActivity(intent);

        //first setting
        if (resumeType == STEnums.resumeType.none) {
            STUser me;
            me = LoginManager.getsIntstance().me();
            if (me != null && me.userID != 0) {
                if (me.name.equals("null")) {
                    resumeType = STEnums.resumeType.firstSetting;
                }
                if (me.birthday.equals("null")) {
                    resumeType = STEnums.resumeType.firstSetting;
                }
                if (me.imageURL.equals("null")) {
                    resumeType = STEnums.resumeType.firstSetting;
                }
                if (me.gender.equals("null")) {
                    resumeType = STEnums.resumeType.firstSetting;
                }
            }
        }
        //check requestPermissions

    }

    private boolean checkPlayServices() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000).show();
            } else {
                Logger.print("This is deveice is not support.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void tabHostManager() {
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }
        Log.i("MainActivity", "status " + status);
        switch (tabHost.getCurrentTab()) {
            case 0:
                tabHostID = tabHost.getCurrentTab();
                break;
            case 1:
                tabHostID = tabHost.getCurrentTab();
                break;
            case 2:
                if (status.equals("none")) {
                    tabHost.setCurrentTab(tabHostID);
                    PopUpLogin login = new PopUpLogin(MainActivity.this);
                    login.show();
                } else if (status.equals("customer")) {
                    Intent intent = new Intent(MainActivity.this, STCreatePostViewControllerActivity.class);
                    startActivity(intent);
                } else {
                    tabHostID = tabHost.getCurrentTab();
                }
                break;
            case 3:
                if (status.equals("none")) {
                    tabHost.setCurrentTab(tabHostID);
                    PopUpLogin login = new PopUpLogin(MainActivity.this);
                    login.show();
                }
                tabHostID = tabHost.getCurrentTab();
                break;
            case 4:
                tabHostID = tabHost.getCurrentTab();
                break;
            default:
                break;
        }
    }

    private void initTabHost() {

        //home
        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
        ((TextView) tabIndicator.findViewById(R.id.title)).setText("ホーム");
        ((ImageView) tabIndicator.findViewById(R.id.icon)).setImageResource(R.drawable.icon_homefeed);
        tabSpecHome.setIndicator(tabIndicator);
        tabHost.addTab(tabSpecHome, STHomePageViewControllerFragment.class, null);

        //article
        View tabIndicator2 = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
        ((TextView) tabIndicator2.findViewById(R.id.title)).setText("記事");
        ((ImageView) tabIndicator2.findViewById(R.id.icon)).setImageResource(R.drawable.icon_article);
        tabSpecArtice.setIndicator(tabIndicator2);
        tabHost.addTab(tabSpecArtice, STArticleViewControllerFragment.class, null);

        //create
        View tabIndicator3 = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
        ((TextView) tabIndicator3.findViewById(R.id.title)).setText("作成");
        ((ImageView) tabIndicator3.findViewById(R.id.icon)).setImageResource(R.drawable.icon_create);
        tabSpecPost.setIndicator(tabIndicator3);
        tabHost.addTab(tabSpecPost, STCreateSelectShopViewControllerFragment.class, null);

        //notification
        View tabIndicator4 = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
        ((TextView) tabIndicator4.findViewById(R.id.title)).setText("お知らせ");
        ((ImageView) tabIndicator4.findViewById(R.id.icon)).setImageResource(R.drawable.icon_notification);
        tabSpecCustomer.setIndicator(tabIndicator4);
        tabHost.addTab(tabSpecCustomer, STNotificationPageViewControllerFragment.class, null);

        //mypage
        View tabIndicator5 = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null);
        ((TextView) tabIndicator5.findViewById(R.id.title)).setText("マイページ");
        ((ImageView) tabIndicator5.findViewById(R.id.icon)).setImageResource(R.drawable.icon_user);
        tabSpecProfile.setIndicator(tabIndicator5);
        tabHost.addTab(tabSpecProfile, STMyPagePageViewControllerFragment.class, null);

    }

    private void setupHomeController() {
        String token = DataSharing.getsIntstance().getTokenGCM();
        Logger.print("token token " + token);

        if (token != null) {
            StylerApi.postDeviceToken(token, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    Logger.print("setupHomeController isSuccess " + isSuccess + " err " + err);
                }
            });
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.i("MainActivity", "onResume");
        Log.i("aaa111", "StylerApi.getAccessTokenBlock: " + StylerApi.getAccessTokenBlock);

        tabHost.setCurrentTab(tabHostID);
        switch (resumeType) {
            case createPost:
                STPostDetailViewControllerFragment fragment = new STPostDetailViewControllerFragment();
                fragment.setPostID(resumeCreatePostID);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
                resumeType = STEnums.resumeType.none;
                break;
            case createReply:
                STPostDetailViewControllerFragment fragment1 = new STPostDetailViewControllerFragment();
                fragment1.setPostID(resumeCreatePostID);
                fragment1.setResumeType(STEnums.resumeType.createReply);
                fragment1.setResumeCreateReplyID(resumeCreateReplyID);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment1)
                        .addToBackStack(null)
                        .commit();
                resumeType = STEnums.resumeType.none;
                break;
            case createEvent:
                STEventDetailViewControllerFragment fragment2 = new STEventDetailViewControllerFragment();
                fragment2.setEventID(resumeCreateEventID);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment2)
                        .addToBackStack(null)
                        .commit();
                resumeType = STEnums.resumeType.none;
                break;
            case createAccount:
                resumeType = STEnums.resumeType.none;
                break;
            case firstSetting:
                resumeType = STEnums.resumeType.none;
                finish();
                Intent intent1 = new Intent(MainActivity.this, STProfileInfoViewControllerActivity.class);
                startActivity(intent1);
                break;
            case none:
                break;
            default:
                break;
        }
        //setupHomeController();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("MainActivity", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("MainActivity", "onStop");

        File exportRealm = null;
        exportRealm = new File(this.getExternalCacheDir(), "export.realm");
        Logger.print(exportRealm.getPath());
        if (exportRealm.exists())
            exportRealm.delete();

        Realm realm = Realm.getDefaultInstance();
        realm.writeCopyTo(exportRealm);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity", "onDestroy");

        File exportRealm = null;
        exportRealm = new File(this.getExternalCacheDir(), "export.realm");
        Logger.print(exportRealm.getPath());
        if (exportRealm.exists())
            exportRealm.delete();

        Realm realm = Realm.getDefaultInstance();
        realm.writeCopyTo(exportRealm);
    }
}
