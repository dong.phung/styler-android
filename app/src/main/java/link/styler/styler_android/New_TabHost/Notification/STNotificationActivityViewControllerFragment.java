package link.styler.styler_android.New_TabHost.Notification;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.models.STNotification;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterNotification;
import link.styler.styler_android.R;


public class STNotificationActivityViewControllerFragment extends Fragment {

    //model
    private RealmResults<STNotification> stNotifications;
    private RealmList<STNotification> notifications;
    private Integer lastObjectID;

    //view
    private LinearLayout linearLayoutFragment;
    private PullRefreshLayout pullRefreshLayout;
    private ListView listViewNotification;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterNotification adapter = null;

    private void createAdapter() {
        stNotifications = DataLoader.getNotificationActivities();
        notifications = new RealmList<>();
        notifications.addAll(stNotifications);
        adapter = new MyAdapterNotification(getActivity(), notifications);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapter == null) {
            createAdapter();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_notification_activity_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_activity);
        listViewNotification = (ListView) view.findViewById(R.id.list_activity);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);

        listViewNotification.setAdapter(adapter);
        //if (adapter.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        //}
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        listViewNotification.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STNotiActivityVCF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STNotiActivityVCF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STNotiActivityVCF", "totalItemCount: " + totalItemCount);
                    Log.i("STNotiActivityVCF", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveNotificationActivity(null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stNotifications = DataLoader.getNotificationActivities();
                    notifications = new RealmList<STNotification>();
                    notifications.addAll(stNotifications);
                    if (STNotificationActivityViewControllerFragment.this.adapter == null)
                        STNotificationActivityViewControllerFragment.this.createAdapter();
                    STNotificationActivityViewControllerFragment.this.adapter.setNotifications(notifications);
                    STNotificationActivityViewControllerFragment.this.adapter.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STNotificationActivityViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveNotificationActivity(lastObjectID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stNotifications = DataLoader.getNotificationActivities();
                    notifications = new RealmList<STNotification>();
                    notifications.addAll(stNotifications);
                    if (STNotificationActivityViewControllerFragment.this.adapter == null)
                        STNotificationActivityViewControllerFragment.this.createAdapter();
                    STNotificationActivityViewControllerFragment.this.adapter.setNotifications(notifications);
                    STNotificationActivityViewControllerFragment.this.adapter.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STNotificationActivityViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STNotiActivityVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STNotiActivityVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STNotiActivityVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STNotiActivityVCF", "onDestroy");
    }
}
