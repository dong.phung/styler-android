package link.styler.styler_android.New_TabHost.MyPage.Account;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.WaitActivity;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

import static android.app.Activity.RESULT_OK;
import static link.styler.Utils.Prefectures.Prefectures;

public class STProfileInfoViewControllerFragment extends Fragment {

    //model
    private STUser user = new STUser();
    private int locationID = 0;

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController stBaseNavigationController;
    private ImageView userImageView;
    private TextView imageUploadButton;
    private EditText nameField, locationField, birthDayField;
    private RadioGroup radioGroup;
    private RadioButton manView, womanView;
    private EditText introductionPlaceHolder;
    private LinearLayout layoutLocation;
    private TextView txtDone;
    private ListView listLocation;

    //helper
    String viewType = "edit";
    String picturePath = "";
    String birthday;

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            user = LoginManager.getsIntstance().me() != null ? LoginManager.getsIntstance().me() : new STUser();
        } catch (Exception e) {
            user = new STUser();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_profile_info_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        stBaseNavigationController = new STBaseNavigationController(view);
        userImageView = (ImageView) view.findViewById(R.id.userImageView);
        imageUploadButton = (TextView) view.findViewById(R.id.imageUploadButton);
        nameField = (EditText) view.findViewById(R.id.nameField);
        locationField = (EditText) view.findViewById(R.id.locationField);
        birthDayField = (EditText) view.findViewById(R.id.birthDayField);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        manView = (RadioButton) view.findViewById(R.id.manView);
        womanView = (RadioButton) view.findViewById(R.id.womanView);
        introductionPlaceHolder = (EditText) view.findViewById(R.id.introductionPlaceHolder);

        layoutLocation = (LinearLayout) view.findViewById(R.id.layoutLocation);
        txtDone = (TextView) view.findViewById(R.id.txtDone);
        listLocation = (ListView) view.findViewById(R.id.listLocation);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        stBaseNavigationController.getTitleNavigation().setText("プロフィール情報");
        stBaseNavigationController.getSubTileNavigation().setVisibility(View.GONE);
        stBaseNavigationController.getBackButton().setImageResource(R.drawable.button_previous);
        stBaseNavigationController.getUpdateButton().setText("更新");
        stBaseNavigationController.getUpdateButton().setEnabled(false);

        Picasso.with(getActivity())
                .load(user.imageURL)
                .transform(new TransformationUtils().new CircleTransform())
                .placeholder(R.drawable.default_avatar)
                .into(userImageView);
        nameField.setText(user.name);
        locationField.setInputType(InputType.TYPE_NULL);
        locationField.setText(Prefectures[user.locationID]);
        locationID = user.locationID;
        birthDayField.setInputType(InputType.TYPE_NULL);
        String date = new STDateFormat().DateFormat(user.birthday);
        birthDayField.setText(date);
        if (user.gender.equals("male")) {
            manView.setChecked(true);
        } else if (user.gender.equals("female")) {
            womanView.setChecked(true);
        }
        introductionPlaceHolder.setText(user.profile);
        layoutLocation.setVisibility(View.GONE);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, Prefectures);
        listLocation.setAdapter(adapter);

        addAction();
    }

    private void addAction() {
        stBaseNavigationController.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        stBaseNavigationController.getUpdateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                upDateProfile();
            }
        });
        imageUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
                changeUpdateButtonEnabled();
            }
        });

        nameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        locationField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard();
                layoutLocation.setVisibility(View.VISIBLE);
            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                birthday = sdf.format(myCalendar.getTime());
                String date = new STDateFormat().DateFormat(birthday);
                birthDayField.setText(date);
            }
        };

        birthDayField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                if (hasFocus) {
                    new DatePickerDialog(getActivity()
                            , android.R.style.Widget_DatePicker
                            , date
                            , myCalendar.get(Calendar.YEAR)
                            , myCalendar.get(Calendar.MONTH)
                            , myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                } else {
                }
            }
        });
        birthDayField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                new DatePickerDialog(getActivity()
                        , android.R.style.Widget_DatePicker
                        , date
                        , myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH)
                        , myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        introductionPlaceHolder.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        listLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                locationID = position;
                locationField.setText(Prefectures[position]);
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        checkEditText();
    }

    private void upDateProfile() {
        File photo;
        if (!picturePath.isEmpty())
            photo = new File(picturePath.toString());
        else
            photo = null;
        String name = nameField.getText().toString();
        String gender = "";
        if (manView.isChecked())
            gender = "male";
        else if (womanView.isChecked())
            gender = "female";
        String profile = introductionPlaceHolder.getText().toString();
        stBaseNavigationController.getUpdateButton().setEnabled(false);
        stBaseNavigationController.getUpdateButton().setTextColor(0xFF9B9B9B);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        DataSender.updateUser(photo, name, locationID, birthday, gender, profile, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                stBaseNavigationController.getUpdateButton().setEnabled(true);
                stBaseNavigationController.getUpdateButton().setTextColor(0xFF28ABEC);
                progressDialog.dismiss();
                if (isSuccess) {
                    if (viewType.equals("edit")) {
                        Toast.makeText(getActivity(), "プロフィール情報を更新しました", Toast.LENGTH_SHORT).show();
                        STMyPagePageViewControllerFragment.setResumeType(STEnums.resumeType.editUser);
                        Intent intent = new Intent(getActivity(), WaitActivity.class);
                        startActivity(intent);
                        getFragmentManager().popBackStackImmediate();
                    } else if (viewType.equals("firstSetting")) {
                        Toast.makeText(getActivity(), "プロフィール情報を更新しました", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    private void chooseImage() {
        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);
    }

    private void checkEditText() {
        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        locationField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        birthDayField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                changeUpdateButtonEnabled();
            }
        });
        introductionPlaceHolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeUpdateButtonEnabled() {
        if (checkValidationForParameter()) {
            stBaseNavigationController.getUpdateButton().setEnabled(true);
            stBaseNavigationController.getUpdateButton().setTextColor(0xFF28ABEC);
        } else {
            stBaseNavigationController.getUpdateButton().setEnabled(false);
            stBaseNavigationController.getUpdateButton().setTextColor(0xFF9B9B9B);
        }
    }

    private boolean checkValidationForParameter() {
        boolean valid = true;
        if (viewType.equals("edit")) {
            if (user.imageURL.isEmpty()) {
                if (picturePath.isEmpty())
                    valid = false;
            }
        } else {
            if (picturePath.isEmpty())
                valid = false;
        }
        if (nameField.getText().length() == 0)
            valid = false;
        if (locationField.getText().length() == 0)
            valid = false;
        if (birthDayField.getText().length() == 0)
            valid = false;
        if (!manView.isChecked() && !womanView.isChecked())
            valid = false;
        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.i("STProfileInfoVCF", "picturePath " + picturePath);
            cursor.close();
            try {
                Picasso.with(getActivity())
                        .load(new File(picturePath))
                        .transform(new TransformationUtils().new CircleTransform())
                        .into(userImageView);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STProfileInfoVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STProfileInfoVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STProfileInfoVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STProfileInfoVCF", "onDestroy");
    }
}
