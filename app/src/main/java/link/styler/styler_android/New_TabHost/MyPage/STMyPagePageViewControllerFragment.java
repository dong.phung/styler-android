package link.styler.styler_android.New_TabHost.MyPage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.InfiniteViewPagerHandler;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Common.STBlankFragment;
import link.styler.styler_android.R;


public class STMyPagePageViewControllerFragment extends Fragment {

    //model
    private int userID = 0;
    private int shopID = 0;
    private STUser user = new STUser();
    private STShop shop = new STShop();
    private STEnums.MyPageType pageType = STEnums.MyPageType.NotLogin;
    private ViewPagerAdapter adapter;

    // View
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private STMyPageHeaderView stHeaderView;
    private ViewPager viewPagerMyPage;

    // Helper
    private boolean shouldReloadPageViewController = true;
    private boolean isInitNaviTitleView = false;
    private int meID = 0;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public static void setResumeType(STEnums.resumeType resumeType) {
        STMyPagePageViewControllerFragment.resumeType = resumeType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("STMyPagePageVCF", "onCreate");

        if (userID == 0 && shopID == 0) {
            userID = LoginManager.getsIntstance().getUserID() != null ? LoginManager.getsIntstance().getUserID() : 0;
        }
        if (userID != 0) {
            user = DataLoader.getUser(userID);
        } else if (shopID != 0) {
            shop = DataLoader.getShop(shopID);
        } else {
            user = new STUser();
            shop = new STShop();
        }
        meID = LoginManager.getsIntstance().getUserID() != null ? LoginManager.getsIntstance().getUserID() : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("STMyPagePageVCF", "onCreateView");
        View view = inflater.inflate(R.layout.fragment_st_my_page_page_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        stHeaderView = new STMyPageHeaderView(view, getActivity());
        viewPagerMyPage = (ViewPager) view.findViewById(R.id.viewPagerMyPage);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("STMyPagePageVCF", "onActivityCreated");
        Log.i("STMyPagePageVCF", "userID: " + userID);
        Log.i("STMyPagePageVCF", "shopID: " + shopID);

        if (userID != 0) {
            if (user != null) {
                fillData();
            } else {
                DataPreserver.saveUser(userID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        user = DataLoader.getUser(userID) != null ? DataLoader.getUser(userID) : new STUser();
                        fillData();
                    }
                });
            }
        } else if (shopID != 0) {
            if (shop != null) {
                fillData();
            } else {
                DataPreserver.saveShop(shopID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        shop = DataLoader.getShop(shopID) != null ? DataLoader.getShop(shopID) : new STShop();
                        fillData();
                    }
                });
            }
        } else {
            user = new STUser();
            shop = new STShop();
            fillData();
        }
        addAction();
    }

    private void fillData() {
        setMyPageType();
        setRightNaviButton();

        if (user.userID != 0) {
            stHeaderView.configDataWithUser(user, pageType);
        } else if (shop.shopID != 0)
            stHeaderView.configDataWithShop(shop, pageType);
        else
            stHeaderView.configDataWithUser(user, pageType);

        setPageViewController();
    }

    private void setMyPageType() {
        if (userID != 0) {
            if (user.userID == meID) {
                if (user.status.equals(STEnums.UserStatus.Customer.getUserStatus()))
                    pageType = STEnums.MyPageType.MyCustomerPage;
                else if (user.status.equals(STEnums.UserStatus.Staff.getUserStatus()))
                    pageType = STEnums.MyPageType.MyStaffPage;
            } else if (user.userID != meID) {
                if (user.status.equals(STEnums.UserStatus.Customer.getUserStatus()))
                    pageType = STEnums.MyPageType.OtherCustomerPage;
                else if (user.status.equals(STEnums.UserStatus.Staff.getUserStatus()))
                    pageType = STEnums.MyPageType.OtherStaffPage;
            }
        } else if (shopID != 0) {
            int myShopID = 0;
            try {
                myShopID = LoginManager.getsIntstance().me().shopID;
            } catch (Exception e) {
            }
            Log.i("STMyPagePageVC02", "me().shopID: " + shopID);
            Log.i("STMyPagePageVC02", "myShopID " + myShopID);

            if (shop.shopID == myShopID) {
                pageType = STEnums.MyPageType.MyShopPage;
            } else {
                pageType = STEnums.MyPageType.OtherShopPage;
            }
        } else if (meID == 0) {
            pageType = STEnums.MyPageType.NotLogin;
        }
    }

    private void setRightNaviButton() {
        String myPageTitle = "マイページ";
        if (pageType == STEnums.MyPageType.OtherCustomerPage)
            myPageTitle = "ユーザー";
        else if (pageType == STEnums.MyPageType.OtherStaffPage)
            myPageTitle = "スタッフ";
        else if (pageType == STEnums.MyPageType.MyShopPage || pageType == STEnums.MyPageType.OtherShopPage)
            myPageTitle = "ショップページ";

        navigation.getTitleNavigation().setText(myPageTitle);
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        if (pageType == STEnums.MyPageType.NotLogin || pageType == STEnums.MyPageType.MyCustomerPage || pageType == STEnums.MyPageType.MyStaffPage) {
            navigation.getBackButton().setVisibility(View.INVISIBLE);
        } else {
            navigation.getBackButton().setVisibility(View.VISIBLE);
        }
    }

    private void setPageViewController() {
        switch (pageType) {
            case None:
                none();
                break;
            case NotLogin:
                notLogin();
                break;
            case MyCustomerPage:
                myCustomerPage();
                break;
            case OtherCustomerPage:
                otherCustomerPage();
                break;
            case MyStaffPage:
                myStaffPage();
                break;
            case OtherStaffPage:
                otherStaffPage();
                break;
            case MyShopPage:
                myShopPage();
                break;
            case OtherShopPage:
                otherShopPage();
                break;
            default:
                break;
        }
    }

    private void notLogin() {
        myCustomerPage();
    }

    private void none() {
        myCustomerPage();
    }

    private void otherCustomerPage() {
        myCustomerPage();
    }

    private void myCustomerPage() {
        Log.i("STMyPagePageVC98", "user: " + user);

        String[] titles = {"Posts", "Watches", "Likes", "Events", "Following"};
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFrag(new STBlankFragment(), titles[1]);                                  // 0

        STMyPageLikeViewControllerFragment likesVC1 = new STMyPageLikeViewControllerFragment();
        likesVC1.setUserID(userID);
        adapter.addFrag(likesVC1, titles[2]);                                               // 1

        STMyPageEventViewControllerFragment eventVC1 = new STMyPageEventViewControllerFragment();
        eventVC1.setUserID(userID);
        adapter.addFrag(eventVC1, titles[3]);                                               // 2

        STMyPageFollowingViewControllerFragment followingVC1 = new STMyPageFollowingViewControllerFragment();
        followingVC1.setUserID(userID);
        adapter.addFrag(followingVC1, titles[4]);                                           // 3

        STMyPagePostViewControllerFragment pagePost = new STMyPagePostViewControllerFragment();
        pagePost.setUserID(userID);
        adapter.addFrag(pagePost, titles[0]);                                               // 4        1

        STMyPageWatchViewControllerFragment watchesVC = new STMyPageWatchViewControllerFragment();
        watchesVC.setUserID(userID);
        adapter.addFrag(watchesVC, titles[1]);                                              // 5        2

        STMyPageLikeViewControllerFragment likesVC = new STMyPageLikeViewControllerFragment();
        likesVC.setUserID(userID);
        adapter.addFrag(likesVC, titles[2]);                                                // 6        3

        STMyPageEventViewControllerFragment eventVC = new STMyPageEventViewControllerFragment();
        eventVC.setUserID(userID);
        adapter.addFrag(eventVC, titles[3]);                                                // 7        4

        adapter.addFrag(new STBlankFragment(), titles[4]);                                  // 8

        viewPagerMyPage.setAdapter(adapter);
        viewPagerMyPage.setCurrentItem(4);
        viewPagerMyPage.setOffscreenPageLimit(9);
        viewPagerMyPage.addOnPageChangeListener(new InfiniteViewPagerHandler(viewPagerMyPage, 5) {
            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);

            }
        });
    }

    private void otherShopPage() {
        myShopPage();
    }

    private void myShopPage() {

        String[] titles = {"Items", "Events", "Staffs", "About"};
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFrag(new STBlankFragment(), titles[2]);                                  // 0

        STMyPageAboutViewControllerFragment aboutVC1 = new STMyPageAboutViewControllerFragment();
        aboutVC1.setShopID(shopID);
        adapter.addFrag(aboutVC1, titles[3]);                                               // 1

        STMyPageItemViewControllerFragment itemVC = new STMyPageItemViewControllerFragment();
        itemVC.setShopID(shopID);
        adapter.addFrag(itemVC, titles[0]);                                                 // 2        1

        STMyPageEventOfShopPageViewControllerFragment eventVC = new STMyPageEventOfShopPageViewControllerFragment();
        eventVC.setShopID(shopID);
        adapter.addFrag(eventVC, titles[1]);                                                // 3        2

        STMyPageStaffViewControllerFragment staffVC = new STMyPageStaffViewControllerFragment();
        staffVC.setShopID(shopID);
        adapter.addFrag(staffVC, titles[2]);                                                // 4        3

        STMyPageAboutViewControllerFragment aboutVC = new STMyPageAboutViewControllerFragment();
        aboutVC.setShopID(shopID);
        adapter.addFrag(aboutVC, titles[3]);                                                // 5        4

        STMyPageItemViewControllerFragment itemVC1 = new STMyPageItemViewControllerFragment();
        itemVC1.setShopID(shopID);
        adapter.addFrag(itemVC1, titles[0]);                                                // 6

        adapter.addFrag(new STBlankFragment(), titles[1]);                                  // 7

        viewPagerMyPage.setAdapter(adapter);
        viewPagerMyPage.setCurrentItem(2);
        viewPagerMyPage.setOffscreenPageLimit(8);
        viewPagerMyPage.addOnPageChangeListener(new InfiniteViewPagerHandler(viewPagerMyPage, 4) {
            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);

            }
        });
    }

    private void otherStaffPage() {
        myStaffPage();
    }

    private void myStaffPage() {

        String[] titles = {"Replies", "Likes", "Events", "Followers"};
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFrag(new STBlankFragment(), titles[2]);                                  // 0

        STMyPageFollowerViewControllerFragment followerVC1 = new STMyPageFollowerViewControllerFragment();
        followerVC1.setUserID(userID);
        adapter.addFrag(followerVC1, titles[3]);                                            // 1

        STMyPageStaffReplyViewControllerFragment replyVC = new STMyPageStaffReplyViewControllerFragment();
        replyVC.setUserID(userID);
        adapter.addFrag(replyVC, titles[0]);                                                // 2        1

        STMyPageLikeViewControllerFragment likeVC = new STMyPageLikeViewControllerFragment();
        likeVC.setUserID(userID);
        adapter.addFrag(likeVC, titles[1]);                                                 //  3       2

        STMyPageEventViewControllerFragment eventVC = new STMyPageEventViewControllerFragment();
        eventVC.setUserID(userID);
        adapter.addFrag(eventVC, titles[2]);                                                // 4        3

        STMyPageFollowerViewControllerFragment followerVC = new STMyPageFollowerViewControllerFragment();
        followerVC.setUserID(userID);
        adapter.addFrag(followerVC, titles[3]);                                             // 5        4

        STMyPageStaffReplyViewControllerFragment replyVC1 = new STMyPageStaffReplyViewControllerFragment();
        replyVC1.setUserID(userID);
        adapter.addFrag(replyVC1, titles[0]);                                               // 6

        adapter.addFrag(new STBlankFragment(), titles[1]);                                  //7

        viewPagerMyPage.setAdapter(adapter);
        viewPagerMyPage.setCurrentItem(2);
        viewPagerMyPage.setOffscreenPageLimit(8);
        viewPagerMyPage.addOnPageChangeListener(new InfiniteViewPagerHandler(viewPagerMyPage, 4) {
            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);

            }
        });
    }

    private void addAction() {
        stHeaderView.getBtnShopInfo().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setShopID(user.shopID);
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STMyPagePageVCF", "onResume");
        int meID = LoginManager.getsIntstance().getUserID() != null ? LoginManager.getsIntstance().getUserID() : 0;
        if (this.meID != meID) {

            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.putExtra("userID", meID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().finish();
        }

        if (resumeType == STEnums.resumeType.editUser) {
            STMyPagePageViewControllerFragment.this.user = DataLoader.getUser(userID);
            STMyPagePageViewControllerFragment.this.stHeaderView.configDataWithUser(user, pageType);
            resumeType = STEnums.resumeType.none;
        } else if (resumeType == STEnums.resumeType.editShop) {
            Log.i("STMyPagePageVCF", "editShop");
            DataPreserver.saveShop(shopID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    STMyPagePageViewControllerFragment.this.shop = DataLoader.getShop(shopID);
                    STMyPagePageViewControllerFragment.this.stHeaderView.configDataWithShop(shop, pageType);
                    resumeType = STEnums.resumeType.none;
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STMyPagePageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STMyPagePageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STMyPagePageVCF", "onDestroy");
    }
}
