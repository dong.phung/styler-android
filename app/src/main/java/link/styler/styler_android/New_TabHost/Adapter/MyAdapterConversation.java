package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STConversation;
import link.styler.styler_android.New_TabHost.Message.STMessageViewControllerFragment;
import link.styler.styler_android.R;


/**
 * Created by admin on 3/15/17.
 */

public class MyAdapterConversation extends BaseAdapter {

    private FragmentActivity fragmentActivity;
    private RealmList<STConversation> conversations;

    public MyAdapterConversation(FragmentActivity fragmentActivity, RealmList<STConversation> conversations) {
        this.fragmentActivity = fragmentActivity;
        this.conversations = conversations;
    }

    public void setConversations(RealmList<STConversation> conversations) {
        this.conversations = conversations;
    }

    @Override
    public int getCount() {
        return conversations.size();
    }

    @Override
    public Object getItem(int position) {
        return conversations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        STConversation conversation = conversations.get(position);
        ViewHolderMessageCell viewHolderMessageCell = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View) inflater.inflate(R.layout.st_notification_message_cell, null);

            viewHolderMessageCell = new ViewHolderMessageCell();
            viewHolderMessageCell.initMessageCell(convertView);
            convertView.setTag(viewHolderMessageCell);
        } else {
            viewHolderMessageCell = (ViewHolderMessageCell) convertView.getTag();
        }

        viewHolderMessageCell.setUpMessageCell(fragmentActivity, conversation);

        return convertView;
    }

    private class ViewHolderMessageCell {
        private ImageView avatarImageView;
        private TextView nameLabel, descriptionLabel, newLabel, timeLabel;
        private LinearLayout linearLayout;

        public void initMessageCell(View view) {
            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
            avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);
            nameLabel = (TextView) view.findViewById(R.id.nameLabel);
            nameLabel.setTextColor(Color.BLACK);
            descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
            descriptionLabel.setTextColor(Color.BLACK);

            newLabel = (TextView) view.findViewById(R.id.newLabel);
            newLabel.setText("New");
            newLabel.setTextColor(Color.WHITE);

            timeLabel = (TextView) view.findViewById(R.id.timeLabel);
            timeLabel.setTextColor(Color.BLACK);
        }

        public void setUpMessageCell(final Context context, final STConversation conversation) {
            String imageURL = conversation.destinationImageURL;
            if (imageURL.isEmpty())
                avatarImageView.setImageResource(R.drawable.icon_default_avatar);
            else
                Picasso.with(context).load(imageURL)
                        .transform(new TransformationUtils().new CircleTransform()).resize(220, 220)
                        .into(avatarImageView);

            nameLabel.setText(conversation.destinationName);
            descriptionLabel.setText(conversation.newestText);

            timeLabel.setText(conversation.newestTime);

            if (conversation.unreadFlag)
                newLabel.setVisibility(View.VISIBLE);
            else
                newLabel.setVisibility(View.GONE);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCell(conversation);
                }
            });
        }

        private void clickCell(STConversation conversation) {
            STMessageViewControllerFragment fragment = new STMessageViewControllerFragment();
            fragment.setDestinationUserID(conversation.destinationID);
            fragment.setDestinationUserName(conversation.destinationName);
            fragmentActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.tabcontent, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }
}
