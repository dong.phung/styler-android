package link.styler.styler_android.New_TabHost.MyPage.Account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.Utils.LoginManager;
import link.styler.models.STShop;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.WaitActivity;
import link.styler.styler_android.R;

import static android.app.Activity.RESULT_OK;
import static link.styler.Utils.Prefectures.Prefectures;


public class STShopPageSettingViewControllerFragment extends Fragment {

    //model
    private STShop shop = new STShop();
    private Integer shopID = null;
    private int locationID = 0;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private ImageView shopImageView;
    private TextView imageUploadButton;
    private EditText shopNameField;
    private EditText shopDescriptionTextView;
    private EditText prefectureField;
    private EditText locationField;
    private EditText nearestStationField;
    private EditText homepageField;
    private EditText telField;
    private EditText businessHourField;

    private LinearLayout layoutLocation;
    private TextView txtDone;
    private ListView listLocation;

    //helper
    private String picturePath = "";
    private boolean isLoading = false;

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (shopID == null) {
            try {
                shopID = LoginManager.getsIntstance().me().shopID;
                if (shopID == null)
                    shopID = 0;
            } catch (Exception e) {
                shopID = 0;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_shop_page_setting_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        shopImageView = (ImageView) view.findViewById(R.id.shopImageView);
        imageUploadButton = (TextView) view.findViewById(R.id.imageUploadButton);
        shopNameField = (EditText) view.findViewById(R.id.shopNameField);
        shopDescriptionTextView = (EditText) view.findViewById(R.id.shopDescriptionTextView);
        prefectureField = (EditText) view.findViewById(R.id.prefectureField);
        locationField = (EditText) view.findViewById(R.id.locationField);
        nearestStationField = (EditText) view.findViewById(R.id.nearestStationField);
        homepageField = (EditText) view.findViewById(R.id.homepageField);
        telField = (EditText) view.findViewById(R.id.telField);
        businessHourField = (EditText) view.findViewById(R.id.businessHourField);

        layoutLocation = (LinearLayout) view.findViewById(R.id.layoutLocation);
        txtDone = (TextView) view.findViewById(R.id.txtDone);
        listLocation = (ListView) view.findViewById(R.id.listLocation);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.titleNavigation.setText("プロフィール情報");
        navigation.subTileNavigation.setVisibility(View.GONE);
        navigation.backButton.setImageResource(R.drawable.button_previous);
        navigation.updateButton.setText("更新");
        navigation.updateButton.setTextColor(0xFF9B9B9B);
        navigation.updateButton.setEnabled(false);
        layoutLocation.setVisibility(View.GONE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, Prefectures);
        listLocation.setAdapter(adapter);

        if (shopID != null && shopID != 0) {
            DataPreserver.saveShop(shopID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    shop = DataLoader.getShop(shopID) != null ? DataLoader.getShop(shopID) : new STShop();
                    configData(shop);
                }
            });
        } else {
            configData(shop);
        }
        addAction();
    }

    private void configData(STShop shop) {
        locationID = shop.locationID;
        /*if (!shop.imageURL.isEmpty())
            Picasso.with(this)
                    .load(shop.imageURL)
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(shopImageView);*/

        if (!shop.name.isEmpty() && !shop.name.equals("null"))
            shopNameField.setText(shop.name);

        if (!shop.profile.isEmpty() && !shop.profile.equals("null"))
            shopDescriptionTextView.setText(shop.profile);

        prefectureField.setInputType(InputType.TYPE_NULL);
        prefectureField.setText(Prefectures[shop.locationID]);

        if (!shop.website.isEmpty() && !shop.website.equals("null"))
            homepageField.setText(shop.website);

        if (!shop.address.isEmpty() && !shop.address.equals("null"))
            locationField.setText(shop.address);

        if (!shop.station.isEmpty() && !shop.station.equals("null"))
            nearestStationField.setText(shop.station);

        if (!shop.tel.isEmpty() && !shop.tel.equals("null"))
            telField.setText(shop.tel);

        if (!shop.businessHour.isEmpty() && !shop.businessHour.equals("null"))
            businessHourField.setText(shop.businessHour);
    }

    private void addAction() {

        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        navigation.getUpdateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                updateShop();
            }
        });

        imageUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                chooseImage();
                checkEditText();
            }
        });

        shopNameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        shopDescriptionTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        prefectureField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard();
                layoutLocation.setVisibility(View.VISIBLE);
            }
        });

        homepageField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        locationField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        nearestStationField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        telField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        businessHourField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
            }
        });

        listLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                locationID = position;
                prefectureField.setText(Prefectures[position]);
            }
        });

        checkEditText();
    }

    private void updateShop() {
        STStructs.StylerShopStruct param = new STStructs().new StylerShopStruct();
        param.name = shopNameField.getText().toString();
        param.profile = shopDescriptionTextView.getText().toString();
        param.address = locationField.getText().toString();
        param.tel = telField.getText().toString();
        param.station = nearestStationField.getText().toString();
        param.location = locationID;
        param.website = homepageField.getText().toString();
        param.business_hours = businessHourField.getText().toString();
        File photo = new File(picturePath.toString());

        navigation.getUpdateButton().setEnabled(false);
        navigation.getUpdateButton().setTextColor(0xFF9B9B9B);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        DataSender.updateShop(photo, param, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                navigation.getUpdateButton().setEnabled(true);
                navigation.getUpdateButton().setTextColor(0xFF28ABEC);
                progressDialog.dismiss();
                if (err != null)
                    return;
                if (isSuccess) {
                    Toast.makeText(getActivity(), "ショップ情報を更新しました", Toast.LENGTH_SHORT).show();
                    STMyPagePageViewControllerFragment.setResumeType(STEnums.resumeType.editShop);
                    Intent intent = new Intent(getActivity(), WaitActivity.class);
                    startActivity(intent);
                    getFragmentManager().popBackStackImmediate();
                } else {
                    Toast.makeText(getActivity(), "ショップ情報の更新に失敗しました", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void chooseImage() {
        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);
    }

    private void checkEditText() {
        shopNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        shopDescriptionTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        prefectureField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        locationField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        nearestStationField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeUpdateButtonEnabled() {
        if (checkRequiredParamValidation()) {
            navigation.getUpdateButton().setEnabled(true);
            navigation.getUpdateButton().setTextColor(0xFF28ABEC);
        } else {
            navigation.getUpdateButton().setEnabled(false);
            navigation.getUpdateButton().setTextColor(0xFF9B9B9B);
        }
    }

    private boolean checkRequiredParamValidation() {
        boolean valid = true;

        if (picturePath == "")
            valid = false;
        if (shopNameField.getText().length() == 0)
            valid = false;
        if (shopDescriptionTextView.getText().length() == 0)
            valid = false;
        if (prefectureField.getText().length() == 0)
            valid = false;
        if (locationField.getText().length() == 0)
            valid = false;
        if (nearestStationField.getText().length() == 0)
            valid = false;
        return valid;
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.i("STCreateReplyVC", "picturePath " + picturePath);
            cursor.close();
            try {
                Picasso.with(getActivity())
                        .load(new File(picturePath))
                        //.transform(new TransformationUtils().new RoundedCornersTransform())
                        .into(shopImageView);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkEditText();
        Log.i("STShopPageSettingVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STShopPageSettingVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STShopPageSettingVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STShopPageSettingVCF", "onDestroy");
    }
}
