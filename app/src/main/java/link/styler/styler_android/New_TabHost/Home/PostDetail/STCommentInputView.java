package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import link.styler.styler_android.R;

/**
 * Created by macOS on 4/4/17.
 */

public class STCommentInputView {
    private EditText editText;
    private Button sendButton;
    private ImageButton selectLikeItemButton;

    public STCommentInputView(Activity activity) {
        init(activity);
        addControls();
    }


    private void init(Activity activity) {
        this.editText = (EditText) activity.findViewById(R.id.editText);
        this.sendButton = (Button) activity.findViewById(R.id.sendButton);
        if (editText.getText().length() == 0) {
            sendButton.setEnabled(false);
            sendButton.setVisibility(View.GONE);
        }
        this.selectLikeItemButton = (ImageButton) activity.findViewById(R.id.selectLikeItemButton);
    }

    public STCommentInputView(View view) {
        init(view);
        addControls();
    }

    private void init(View view) {
        this.editText = (EditText) view.findViewById(R.id.editText);
        this.sendButton = (Button) view.findViewById(R.id.sendButton);
        if (editText.getText().length() == 0) {
            sendButton.setEnabled(false);
            sendButton.setVisibility(View.GONE);
        }
        this.selectLikeItemButton = (ImageButton) view.findViewById(R.id.selectLikeItemButton);
    }

    private void addControls() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().length() != 0) {
                    sendButton.setEnabled(true);
                    sendButton.setVisibility(View.VISIBLE);
                } else {
                    sendButton.setEnabled(false);
                    sendButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public EditText getEditText() {
        return editText;
    }

    public Button getSendButton() {
        return sendButton;
    }

    public ImageButton getSelectLikeItemButton() {
        return selectLikeItemButton;
    }
}
