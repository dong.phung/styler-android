package link.styler.styler_android.New_TabHost.Home.Event;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import io.realm.RealmList;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.models.STEvent;
import link.styler.models.STUser;
import link.styler.models.STUserSort;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterEventGoing;
import link.styler.styler_android.R;

public class STEventGoingListViewControllerFragment extends Fragment {

    //model
    private int eventID;
    private STEvent event;
    private RealmList<STUser> users = new RealmList<>();
    private MyAdapterEventGoing adapterEventGoing = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private GridView tableView;

    //helper
    private void createAdapterEventGoing() {
        event = DataLoader.getEvent(eventID) != null ? DataLoader.getEvent(eventID) : new STEvent();
        users = new RealmList<>();
        if (event.interestUsers.size() != 0)
            for (STUserSort userSort : event.interestUsers) {
                users.add(userSort.user);
            }
        adapterEventGoing = new MyAdapterEventGoing(getActivity(), users);
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterEventGoing == null)
            createAdapterEventGoing();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_event_going_list_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        tableView = (GridView) view.findViewById(R.id.tableView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText("参加者の一覧");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        tableView.setAdapter(adapterEventGoing);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STEventGoingListVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STEventGoingListVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STEventGoingListVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STEventGoingListVCF", "onDestroy");
    }
}
