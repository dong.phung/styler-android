package link.styler.styler_android.New_TabHost.Article.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import link.styler.STStructs.STEnums;
import link.styler.styler_android.New_TabHost.Article.STArticleNewListViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 1/24/17.
 */

public class KeywordAdapter extends RecyclerView.Adapter<KeywordAdapter.ViewHolder> {

    private List<String> listKeyword;
    private FragmentActivity fragmentActivity;

    public KeywordAdapter(List<String> listKeyword, FragmentActivity fragmentActivity) {
        this.listKeyword = listKeyword;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articles_btn_keyword, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String keyword = listKeyword.get(position);
        holder.btnKeyword.setText(keyword);

        //Action
        holder.btnKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STArticleNewListViewControllerFragment fragment = new STArticleNewListViewControllerFragment();
                fragment.setTypeList(STEnums.STArticleListType.tag);
                fragment.setNameList(keyword);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listKeyword.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button btnKeyword;

        public ViewHolder(View itemView) {
            super(itemView);
            btnKeyword = (Button) itemView.findViewById(R.id.btnKeyword);

        }
    }

}