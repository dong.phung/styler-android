package link.styler.styler_android.New_TabHost.Adapter;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.styler_android.New_TabHost.Home.Event.STEventDetailViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/19/17.
 */

public class MyAdapterEvent extends BaseAdapter {

    //model
    private RealmList<STEventSort> events = new RealmList<>();
    private STEvent event;

    //view
    private ImageView eventImageView;
    private TextView numOfGoingLabel;
    private TextView eventDateLabel;
    private TextView eventTitleLabel;
    private ImageView shopImageView;
    private TextView shopNameLabel;
    private LinearLayout linearLayoutEventCell;

    //hepler
    private FragmentActivity fragmentActivity;
    private Context context;

    public MyAdapterEvent(FragmentActivity fragmentActivity, RealmResults<STEventSort> events) {
        this.fragmentActivity = fragmentActivity;
        this.context = fragmentActivity.getApplicationContext();
        if (events != null)
            this.events.addAll(events);
    }

    public void setEvents(RealmResults<STEventSort> events) {
        if (events != null) {
            this.events = new RealmList<>();
            this.events.addAll(events);
        }
    }

    @Override
    public int getCount() {
        return events == null ? 0 : events.size();
    }

    @Override
    public Object getItem(int position) {
        return event == null ? 0 : events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return event == null ? 0 : events.get(position).eventID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_event_held_cell, null);
        intView(convertView);
        //}
        STEvent event = events.get(position).event;
        setupView(event);
        return convertView;
    }

    private void intView(View view) {
        eventImageView = (ImageView) view.findViewById(R.id.eventImageView);
        numOfGoingLabel = (TextView) view.findViewById(R.id.numOfGoingLabel);
        eventDateLabel = (TextView) view.findViewById(R.id.eventDateLabel);
        eventTitleLabel = (TextView) view.findViewById(R.id.eventTitleLabel);
        shopImageView = (ImageView) view.findViewById(R.id.shopImageView);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
        linearLayoutEventCell = (LinearLayout) view.findViewById(R.id.linearLayoutEventCell);
    }

    private void setupView(final STEvent event) {
        String urlImage = event.imageURL;
        if (urlImage != null) {
            Picasso.with(context)
                    .load(urlImage)
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(eventImageView);
        }
        if (!event.title.isEmpty())
            eventTitleLabel.setText(event.title);
        else
            eventTitleLabel.setText("No Title");

        STDateFormat dateFormat = new STDateFormat();
        String dateText = dateFormat.DateFormat(event.startDate) + "~";
        if (!event.finishDate.equals(null)) {
            dateText = dateText + dateFormat.DateFormat(event.finishDate);
        }
        eventDateLabel.setText(dateText);

        numOfGoingLabel.setText(event.interestsCount + "");

        String urlShop = event.shop.imageURL;
        if (urlShop != null) {
            try {
                Picasso.with(context)
                        .load(urlShop)
                        .placeholder(R.drawable.default_avatar)
                        .transform(new TransformationUtils().new RoundedCornersTransform())
                        .into(shopImageView);
                shopNameLabel.setText(event.shop.name);
            } catch (Exception e) {

            }
        }

        //Action
        linearLayoutEventCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                STEventDetailViewControllerFragment fragment = new STEventDetailViewControllerFragment();
                fragment.setEventID(event.eventID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

}
