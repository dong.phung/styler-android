package link.styler.styler_android.New_TabHost.Create;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STPost;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.New_TabHost.Home.STNewArrivalsPostFragment;
import link.styler.styler_android.New_TabHost.Home.STHomePageViewControllerFragment;
import link.styler.styler_android.R;

public class STCreatePostViewControllerActivity extends AppCompatActivity {
    //data
    String arr[] = {"ジャケット・アウター",
            "スーツ・セットアップ",
            "シャツ・ブラウス",
            "Tシャツ・カットソー",
            "ニット・スウェット",
            "パンツ",
            "ドレス・ワンピース・スカート",
            "シューズ",
            "バッグ",
            "帽子",
            "アクセサリー・時計",
            "メガネ・サングラス",
            "財布・小物・雑貨",
            "その他"};

    String arr2[] = {".Outer", //2
            ".Suit", // 3
            ".Shirts",//4
            ".Cutsew",//5
            ".Knit",//6
            ".Pants",//7
            ".Dress",//8
            ".Shoes",//9
            ".Bag",//10
            ".Hat",//11
            ".Accessory",//12
            ".Glasses",//13
            ".Wallet",//14
            ".NoCategory"};      //1
    int ID[] = {2,  // 2
            3,  // 3
            4,  // 4
            5,  // 5
            6,  // 6
            7,  // 7
            8,  // 8
            9,  // 9
            10, //10
            11, //11
            12, //12
            13, //13
            14, //14
            1}; // 1
    Integer categoryID = null;
    Boolean isLoading = false;

    //controls
    ImageView imaClose;
    ImageView imaCategory;
    TextView txtCategory;
    TextView txtCount;
    TextView txtDone;
    EditText txtPlaceHolder;
    LinearLayout ll1;
    LinearLayout ll2;
    ListView lvCategory;
    Button btnSend;

    public static STPost post = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_create_post_view_controller);

        addControls();
        addEvents();

        hideListCategory();
        EditText myEditText = (EditText) findViewById(R.id.txtPlaceHolder);
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);

        if (post != null) {
            txtPlaceHolder.setText(post.text);
            txtCategory.setText(post.categoryName);
            int i = 100 - txtPlaceHolder.getText().length();
            txtCount.setText("残り" + i + "文字");
            categoryID = post.categoryID;
        }
    }

    private void addControls() {
        imaClose = (ImageView) findViewById(R.id.imaClose);
        imaCategory = (ImageView) findViewById(R.id.imaCategory);
        txtCategory = (TextView) findViewById(R.id.txtCategory);
        txtCount = (TextView) findViewById(R.id.txtCount);
        txtPlaceHolder = (EditText) findViewById(R.id.txtPlaceHolder);
        txtPlaceHolder.setImeOptions(EditorInfo.IME_ACTION_DONE);
        ll1 = (LinearLayout) findViewById(R.id.llCreatePost1);
        ll2 = (LinearLayout) findViewById(R.id.llCreatePost2);
        txtDone = (TextView) findViewById(R.id.txtDone);
        lvCategory = (ListView) findViewById(R.id.lvCategory);
        addDatalvCategory(); // <=============== add data list view

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setEnabled(false);
        categoryID = null;
    }

    private void addEvents() {

        imaClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(STCreatePostViewControllerActivity.this);
                builder.setTitle("入力を終了してよろしいですか？");
                builder.setMessage("現在の編集内容は保存されません").setPositiveButton("OK", dialogClickListener)
                        .setNegativeButton("キャンセル", dialogClickListener).show();
            }
        });

        imaCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListCategory();
            }
        });
        txtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListCategory();
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideListCategory();
            }
        });

        txtPlaceHolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int i = 100 - txtPlaceHolder.getText().length();
                txtCount.setText("残り" + i + "文字");
                if (i < 0) {
                    txtCount.setTextColor(0xFFF0405A);

                } else {
                    txtCount.setTextColor(0xFF007AFF);

                }
                if (i == 100 || i < 0) {
                    btnSend.setBackgroundColor(0xFF787878);
                    btnSend.setEnabled(false);
                } else {
                    btnSend.setBackgroundColor(0xFF007AFF);
                    btnSend.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideListCategory();
            }
        });

        lvCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = (String) lvCategory.getAdapter().getItem(position);
                txtCategory.setText(s);
                categoryID = ID[position];
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoading) {
                    return;
                }
                if (txtPlaceHolder.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Post内容を入力してください", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (categoryID == null) {
                    Toast.makeText(getApplicationContext(), "カテゴリーを選択してください", Toast.LENGTH_SHORT).show();
                    return;
                }
                isLoading = true;
                if (post == null) {
                    //create
                    Log.i("Tan001", "Text " + String.valueOf(txtPlaceHolder.getText()));
                    Log.i("Tan001", "categoryID " + categoryID);
                    Log.i("Tan001", "token " + StylerApi.getAccessTokenBlock);
                    DataSender.createPost(String.valueOf(txtPlaceHolder.getText()), categoryID, new STCompletion.WithNullableInt() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                            Log.i("Tan001", " " + isSuccess);
                            if (isSuccess) {
                                hideKeyBoard();
                                Log.i("Tan001", "ID " + nullableInt);
                                isLoading = false;
                                finish();
                                MainActivity.setTabHostID(0);
                                MainActivity.setResumeType(STEnums.resumeType.createPost);
                                MainActivity.setResumeCreatePostID(nullableInt);
                                STHomePageViewControllerFragment.setHomeViewPagerID(2);
                                STHomePageViewControllerFragment.setResumeType(STEnums.resumeType.createPost);
                                STNewArrivalsPostFragment.setResumeType(STEnums.resumeType.createPost);
                            } else {
                                Toast.makeText(getApplicationContext(), "Postの作成に失敗しました", Toast.LENGTH_SHORT).show();
                                isLoading = false;
                            }
                        }
                    });
                } else {
                    //edit
                    Log.i("Tan001", "postID " + post.postID);
                    Log.i("Tan001", "text " + String.valueOf(txtPlaceHolder.getText()));
                    Log.i("Tan001", "categoryID " + categoryID);
                    Log.i("Tan001", "token " + StylerApi.getAccessTokenBlock);
                    DataSender.editPost(post.postID, String.valueOf(txtPlaceHolder.getText()), categoryID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            Log.i("Tan001", " " + isSuccess);
                            if (isSuccess) {
                                hideKeyBoard();
                                Log.i("Tan001", "ID " + post.postID);
                                isLoading = false;
                                finish();
                                MainActivity.setResumeType(STEnums.resumeType.createPost);
                                MainActivity.setResumeCreatePostID(post.postID);
                            } else {
                                Toast.makeText(getApplicationContext(), "Postの更新に失敗しました", Toast.LENGTH_SHORT).show();
                                isLoading = false;
                            }
                        }
                    });
                }
            }
        });
    }

    private void addDatalvCategory() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, arr);
        lvCategory.setAdapter(adapter);
    }

    private void hideListCategory() {
        ll2.setVisibility(View.GONE);
    }

    private void showListCategory() {
        ll2.setVisibility(View.VISIBLE);
        hideKeyBoard();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        post = null;
    }
}
