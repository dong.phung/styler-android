package link.styler.styler_android.New_TabHost.Article.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import io.realm.RealmList;
import link.styler.models.STTag;
import link.styler.styler_android.R;

/**
 * Created by macOS on 6/6/17.
 */

public class MyAdapterTag extends BaseAdapter {

    //model
    private RealmList<STTag> tags;
    private STTag tag;

    //view
    private TextView txtKeyword;
    //hepler
    private Context context;

    public MyAdapterTag(Context context, RealmList<STTag> tags) {
        this.context = context;
        this.tags = tags;
    }

    public void setTags(RealmList<STTag> tags) {
        this.tags = tags;
    }

    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return tags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tags.get(position).tagID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.articles_keyword_list_item, null);
        intView(convertView);
        //}
        STTag tag = tags.get(position);
        setupView(tag);
        addControl(tag);
        return convertView;
    }

    private void intView(View view) {
        txtKeyword = (TextView) view.findViewById(R.id.txtKeyword);
    }

    private void setupView(STTag tag) {
        txtKeyword.setText(tag.name);
    }

    private void addControl(final STTag tag) {

    }
}
