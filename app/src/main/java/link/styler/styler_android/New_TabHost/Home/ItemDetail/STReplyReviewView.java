package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import link.styler.styler_android.R;

/**
 * Created by macOS on 6/13/17.
 */

class STReplyReviewView {

    private View view;
    private TextView titleLabel;
    private ImageView starView1, starView2, starView3, starView4, starView5;
    private TextView commentLabel;

    private String[] reviewComments = {"ありがとう！", "希望通りです！", "気に入りました！", "すごく好みです！", "今すぐ欲しいです！"};

    public STReplyReviewView(View view) {
        this.view = view;
        initSTReplyReviewView(this.view);
    }

    private void initSTReplyReviewView(View view) {
        this.titleLabel = (TextView) view.findViewById(R.id.titleLabel);
        this.starView1 = (ImageView) view.findViewById(R.id.starView1);
        this.starView2 = (ImageView) view.findViewById(R.id.starView2);
        this.starView3 = (ImageView) view.findViewById(R.id.starView3);
        this.starView4 = (ImageView) view.findViewById(R.id.starView4);
        this.starView5 = (ImageView) view.findViewById(R.id.starView5);
        this.commentLabel = (TextView) view.findViewById(R.id.commentLabel);

        //addAction();
    }

    public void clickStar1() {
        starView1.setImageResource(R.drawable.icon_star_active);
        starView2.setImageResource(R.drawable.icon_star);
        starView3.setImageResource(R.drawable.icon_star);
        starView4.setImageResource(R.drawable.icon_star);
        starView5.setImageResource(R.drawable.icon_star);
        commentLabel.setText(reviewComments[0]);
        commentLabel.setTextColor(0xFFF5A623);
        commentLabel.setTextSize(14);
    }
    public void clickStar2() {
        starView1.setImageResource(R.drawable.icon_star_active);
        starView2.setImageResource(R.drawable.icon_star_active);
        starView3.setImageResource(R.drawable.icon_star);
        starView4.setImageResource(R.drawable.icon_star);
        starView5.setImageResource(R.drawable.icon_star);
        commentLabel.setText(reviewComments[1]);
        commentLabel.setTextColor(0xFFF5A623);
        commentLabel.setTextSize(14);
    }
    public void clickStar3() {
        starView1.setImageResource(R.drawable.icon_star_active);
        starView2.setImageResource(R.drawable.icon_star_active);
        starView3.setImageResource(R.drawable.icon_star_active);
        starView4.setImageResource(R.drawable.icon_star);
        starView5.setImageResource(R.drawable.icon_star);
        commentLabel.setText(reviewComments[2]);
        commentLabel.setTextColor(0xFFF5A623);
        commentLabel.setTextSize(14);
    }
    public void clickStar4() {
        starView1.setImageResource(R.drawable.icon_star_active);
        starView2.setImageResource(R.drawable.icon_star_active);
        starView3.setImageResource(R.drawable.icon_star_active);
        starView4.setImageResource(R.drawable.icon_star_active);
        starView5.setImageResource(R.drawable.icon_star);
        commentLabel.setText(reviewComments[3]);
        commentLabel.setTextColor(0xFFF5A623);
        commentLabel.setTextSize(14);
    }
    public void clickStar5() {
        starView1.setImageResource(R.drawable.icon_star_active);
        starView2.setImageResource(R.drawable.icon_star_active);
        starView3.setImageResource(R.drawable.icon_star_active);
        starView4.setImageResource(R.drawable.icon_star_active);
        starView5.setImageResource(R.drawable.icon_star_active);
        commentLabel.setText(reviewComments[4]);
        commentLabel.setTextColor(0xFFF5A623);
        commentLabel.setTextSize(14);
    }

    public ImageView getStarView1() {
        return starView1;
    }

    public ImageView getStarView2() {
        return starView2;
    }

    public ImageView getStarView3() {
        return starView3;
    }

    public ImageView getStarView4() {
        return starView4;
    }

    public ImageView getStarView5() {
        return starView5;
    }

    public TextView getCommentLabel() {
        return commentLabel;
    }
}
