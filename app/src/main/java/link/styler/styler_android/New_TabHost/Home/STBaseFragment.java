package link.styler.styler_android.New_TabHost.Home;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import java.lang.ref.WeakReference;

/**
 * Created by dongphung on 4/28/17.
 */

public class STBaseFragment extends Fragment {
    WeakReference<STBaseFragment> weakSelf;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void referenceMyself(STBaseFragment self) {
        weakSelf = new WeakReference<STBaseFragment>(self);
    }
}
