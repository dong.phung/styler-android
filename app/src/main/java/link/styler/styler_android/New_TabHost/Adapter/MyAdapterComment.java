package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmResults;
import link.styler.STStructs.STEnums;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STComment;
import link.styler.models.STItem;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/4/17.
 */

public class MyAdapterComment extends BaseAdapter {

    private FragmentActivity fragmentActivity;
    private RealmResults<STComment> comments;

    public MyAdapterComment(FragmentActivity fragmentActivity, RealmResults<STComment> comments) {
        this.fragmentActivity = fragmentActivity;
        this.comments = comments;
    }

    public void setComments(RealmResults<STComment> comments) {
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments != null ? comments.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return comments != null ? comments.get(position) : 0;
    }

    @Override
    public long getItemId(int position) {
        return comments != null ? comments.get(position).commentID : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        STComment comment = comments.get(position);
        STItem item = new STItem();
        try {
            item = comment.item;
        } catch (Exception e) {
        }
        if (item.itemID != 0) {
            return createSTCommentItemCell(comment, convertView);
        } else {
            return createSTCommentCell(comment, convertView);
        }
    }

    private View createSTCommentItemCell(final STComment comment, View convertView) {

        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_comment_item_cell, null);

        //intView();
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);
        ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatarImageView);
        ImageView itemImageView = (ImageView) convertView.findViewById(R.id.itemImageView);
        TextView brandLabel = (TextView) convertView.findViewById(R.id.brandLabel);
        TextView nameLabel = (TextView) convertView.findViewById(R.id.nameLabel);
        TextView priceLabel = (TextView) convertView.findViewById(R.id.priceLabel);
        TextView dateLabel = (TextView) convertView.findViewById(R.id.dateLabel);
        Button reportButton = (Button) convertView.findViewById(R.id.reportButton);
        ImageView avatarImageViewMe = (ImageView) convertView.findViewById(R.id.avatarImageViewMe);

        //setupView();
        if (comment == null || comment.commentID == 0)
            return null;
        STItem item = comment.item;
        if (item == null || item.itemID == 0)
            return null;
        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {
        }
        if (meID == comment.user.userID) {
            linearLayout.setGravity(Gravity.END | Gravity.BOTTOM);
            reportButton.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.GONE);
            avatarImageViewMe.setVisibility(View.VISIBLE);
            Picasso.with(fragmentActivity)
                    .load(LoginManager.getsIntstance().me().imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    //.placeholder(R.drawable.default_avatar)
                    .into(avatarImageViewMe);
        } else {
            linearLayout.setGravity(Gravity.START | Gravity.BOTTOM);
            reportButton.setVisibility(View.VISIBLE);
            avatarImageViewMe.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.VISIBLE);
            if (comment.user.imageURL.length() != 0) {
                Picasso.with(fragmentActivity)
                        .load(comment.user.imageURL)
                        .transform(new TransformationUtils().new CircleTransform())
                        //.placeholder(R.drawable.default_avatar)
                        .into(avatarImageView);
            }
        }
        try {
            Picasso.with(fragmentActivity)
                    .load(item.mainImageURL)
                    .resize(400, 400)
                    .centerCrop()
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(itemImageView);
        } catch (Exception e) {
        }

        brandLabel.setText(item.brand);
        nameLabel.setText(item.name);
        if (!item.brand.isEmpty()) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("￥" + decimalFormat.format(Integer.parseInt(item.price)) + "（税込）");
        } else {
            priceLabel.setText("");
        }

        String stringDate = "";

        if (!comment.createdAt.isEmpty()) {
            stringDate = comment.createdAt;
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(stringDate);
                format = new SimpleDateFormat("yyyy年MM月dd日");
                stringDate = format.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        dateLabel.setText(stringDate);

        //addcontrol()
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int meID = 0;
                try {
                    meID = LoginManager.getsIntstance().me().userID;
                } catch (Exception e) {
                    return;
                }
                if (comment.commentID == 0) {
                    return;
                }
            }
        });

        itemImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.item == null || comment.item.itemID == 0)
                    return;
                STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                fragment.setItemID(comment.item.itemID);
                if (comment.postID != 0) {
                    fragment.setPostID(comment.postID);
                    fragment.setViewType(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType());
                }
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return convertView;
    }

    private View createSTCommentCell(final STComment comment, View convertView) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_comment_cell, null);

        //intView();
        ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatarImageView);
        TextView commentLabel = (TextView) convertView.findViewById(R.id.commentLabel);
        TextView dateLabel = (TextView) convertView.findViewById(R.id.dateLabel);
        Button reportButton = (Button) convertView.findViewById(R.id.reportButton);
        ImageView avatarImageViewMe = (ImageView) convertView.findViewById(R.id.avatarImageViewMe);

        //setupView();
        if (comment == null || comment.commentID == 0)
            return null;

        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {

        }
        if (meID == comment.user.userID && comment.commentID != 0) {
            commentLabel.setTextColor(0xFFFFFFFF);
            commentLabel.setBackgroundColor(0xFF4A4A4A);
            reportButton.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.INVISIBLE);
            avatarImageViewMe.setVisibility(View.VISIBLE);
            Picasso.with(fragmentActivity)
                    .load(LoginManager.getsIntstance().me().imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    //.placeholder(R.drawable.default_avatar)
                    .into(avatarImageViewMe);
        } else {
            commentLabel.setTextColor(0xFF000000);
            commentLabel.setBackgroundColor(0xFFEFEFEF);
            reportButton.setVisibility(View.VISIBLE);
            avatarImageView.setVisibility(View.VISIBLE);
            avatarImageViewMe.setVisibility(View.INVISIBLE);

            Picasso.with(fragmentActivity)
                    .load(comment.user.imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    //.placeholder(R.drawable.default_avatar)
                    .into(avatarImageView);
        }

        commentLabel.setText(comment.text);
        String date = new STDateFormat().DateFormat(comment.createdAt);
        dateLabel.setText(date);

        //addcontrol()
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int meID = 0;
                try {
                    meID = LoginManager.getsIntstance().me().userID;
                } catch (Exception e) {
                    return;
                }
                if (comment.commentID == 0) {
                    return;
                }
                //// TODO: 3/30/17  report
            }
        });
        return convertView;
    }

}
