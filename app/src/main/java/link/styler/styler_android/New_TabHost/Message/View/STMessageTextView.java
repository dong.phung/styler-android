package link.styler.styler_android.New_TabHost.Message.View;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import link.styler.styler_android.R;

/**
 * Created by macOS on 3/29/17.
 */

public class STMessageTextView {
    private EditText editText;
    private Button sendButton;

    public EditText getEditText() {
        return editText;
    }

    public Button getSendButton() {
        return sendButton;
    }

    //user activity
    public STMessageTextView(Activity activity) {
        init(activity);
        addControls();
    }

    private void init(Activity activity) {
        this.editText = (EditText) activity.findViewById(R.id.editText);
        this.sendButton = (Button) activity.findViewById(R.id.sendButton);
    }

    //use view
    public STMessageTextView(View view) {
        init(view);
        addControls();
    }

    private void init(View view) {
        this.editText = (EditText) view.findViewById(R.id.editText);
        this.sendButton = (Button) view.findViewById(R.id.sendButton);
    }

    private void addControls() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().length() > 0) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
