package link.styler.styler_android.New_TabHost.Walkthrough;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import link.styler.Utils.DataSharing;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.R;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_LOGS;
import static android.Manifest.permission.WAKE_LOCK;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class STTutorialViewController extends Activity {

    private static int currentIndex = 0;
    private ImageButton button;

    private TransitionDrawable transitionDrawable;
    private Drawable background[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_tutorial_view_controller);

        background = new Drawable[5];
        background[0] = ResourcesCompat.getDrawable(getResources(),R.drawable.sttutorial_0,null);
        background[1] = ResourcesCompat.getDrawable(getResources(),R.drawable.sttutorial_1,null);
        background[2] = ResourcesCompat.getDrawable(getResources(),R.drawable.sttutorial_2,null);
        background[3] = ResourcesCompat.getDrawable(getResources(),R.drawable.sttutorial_3,null);
        background[4] = ResourcesCompat.getDrawable(getResources(),R.drawable.sttutorial_4,null);

        button = (ImageButton) findViewById(R.id.imagebutton_tutorial);

        transitionDrawable = new TransitionDrawable(new Drawable[]{background[0],background[1]});
        transitionDrawable.setCrossFadeEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(transitionDrawable);
        }
        else
        {
            button.setBackgroundDrawable(transitionDrawable);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nexAction();
            }
        });

    }


    private void nexAction(){

        currentIndex+=1;

        if(currentIndex >= 5){
            DataSharing.getsIntstance().setIsNotFirstLauch();
            //startActivity(new Intent(STTutorialViewController.this, HomeActivity.class));
            ActivityCompat.requestPermissions(this,
                    new String[]{INTERNET,ACCESS_NETWORK_STATE,WRITE_EXTERNAL_STORAGE,WAKE_LOCK,READ_EXTERNAL_STORAGE,READ_LOGS},
                    1);
            finish();
            return;
        }

        transitionDrawable = new TransitionDrawable(new Drawable[]{background[currentIndex-1],background[currentIndex]});
        transitionDrawable.setCrossFadeEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            button.setBackground(transitionDrawable);
        }
        else
        {
            button.setBackgroundDrawable(transitionDrawable);
        }

        transitionDrawable.startTransition(2500);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        background[0] = null;
        background[1] = null;
        background[2] = null;
        background[3] = null;
        background[4] = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
