package link.styler.styler_android.New_TabHost.Create;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STCategory;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterPosts;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;


public class STReplySelectPostViewControllerFragment extends Fragment {

    //model
    private int categoryID = 0;
    private STCategory category = new STCategory();
    private RealmResults<STPostSort> postSorts;
    private RealmList<STPost> posts;
    private Integer lastObjectID = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private PullRefreshLayout pullRefreshLayout;
    private ListView listView;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterPosts adapterPosts = null;

    private void createAdapterPosts() {
        posts = new RealmList<>();
        DataLoader.deleteUnrepliedPosts(categoryID);
        postSorts = DataLoader.getCategorizedPostsWithoutReply(categoryID);
        for (STPostSort postSort : postSorts) {
            posts.add(postSort.post);
        }
        adapterPosts = new MyAdapterPosts(getActivity(), posts);
        adapterPosts.mode = STEnums.MyAdapterViewMode.createReply;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (categoryID != 0) {
            category = DataLoader.getCategory(categoryID);
            if (category == null) {
                DataPreserver.saveCategoris(new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {

                    }
                });
            }
            if (adapterPosts == null) {
                createAdapterPosts();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_reply_select_post_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_selectPost);
        listView = (ListView) view.findViewById(R.id.listView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);

        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText(category.name);
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        listView.setAdapter(adapterPosts);
        if (adapterPosts.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (categoryID == 0 || isLoading)
            return;
        isLoading = true;
        DataPreserver.saveUnrepliedPosts(categoryID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    //DataLoader.deleteUnrepliedPosts(categoryID);
                    postSorts = DataLoader.getCategorizedPostsWithoutReply(categoryID);
                    posts = new RealmList<STPost>();
                    for (STPostSort stPostSort : postSorts) {
                        posts.add(stPostSort.post);
                    }
                    adapterPosts.setPosts(posts);
                    adapterPosts.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STReplySelectPostViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (categoryID == 0 || isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveUnrepliedPosts(categoryID, lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    //DataLoader.deleteUnrepliedPosts(categoryID);
                    postSorts = DataLoader.getCategorizedPostsWithoutReply(categoryID);
                    posts = new RealmList<STPost>();
                    for (STPostSort stPostSort : postSorts) {
                        posts.add(stPostSort.post);
                    }
                    adapterPosts.setPosts(posts);
                    adapterPosts.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STReplySelectPostViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STReplySelectPostVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STReplySelectPostVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STReplySelectPostVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STReplySelectPostVCF", "onDestroy");
    }
}
