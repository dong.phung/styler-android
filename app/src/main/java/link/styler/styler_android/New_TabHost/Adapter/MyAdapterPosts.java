package link.styler.styler_android.New_TabHost.Adapter;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STPost;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Create.STReplySelectItemPageViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.PostDetail.STPostCommentViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.PostDetail.STPostDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.New_TabHost.PopUpMoreActionPost;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/18/17.
 */

public class MyAdapterPosts extends BaseAdapter {

    //model
    private RealmList<STPost> posts;
    private FragmentActivity activity;


    //helper
    public STEnums.MyAdapterViewMode mode = STEnums.MyAdapterViewMode.normal;

    public void setPosts(RealmList<STPost> posts) {
        this.posts = posts;
    }

    public MyAdapterPosts(FragmentActivity activity, RealmList<STPost> posts) {
        this.activity = activity;
        this.posts = posts;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int position) {
        return posts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolderItem viewHolderItem;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View) inflater.inflate(R.layout.list_post_layout_2, null);

            viewHolderItem = new ViewHolderItem();

            viewHolderItem.txtName = (TextView) convertView.findViewById(R.id.txtNamePost);
            viewHolderItem.txtAge = (TextView) convertView.findViewById(R.id.txtAgePost);
            viewHolderItem.txtSex = (TextView) convertView.findViewById(R.id.txtSexPost);
            viewHolderItem.txtTile = (TextView) convertView.findViewById(R.id.txtTitlePost);
            viewHolderItem.txtTextPost = (TextView) convertView.findViewById(R.id.txtTextPost);

            viewHolderItem.imaAvatar = (ImageView) convertView.findViewById(R.id.imaAvatar);
            viewHolderItem.txtCategory = (TextView) convertView.findViewById(R.id.txtCategory);
            viewHolderItem.txtCountReply = (TextView) convertView.findViewById(R.id.txtCountReply);

            viewHolderItem.linearLayout4Ima = (LinearLayout) convertView.findViewById(R.id.LinearLayout4Ima);
            viewHolderItem.ima41 = (ImageView) convertView.findViewById(R.id.ima41);
            viewHolderItem.ima42 = (ImageView) convertView.findViewById(R.id.ima42);
            viewHolderItem.ima43 = (ImageView) convertView.findViewById(R.id.ima43);
            viewHolderItem.ima44 = (ImageView) convertView.findViewById(R.id.ima44);

            viewHolderItem.ima31 = (ImageView) convertView.findViewById(R.id.ima31);

            viewHolderItem.ima21 = (ImageView) convertView.findViewById(R.id.ima21);
            viewHolderItem.ima22 = (ImageView) convertView.findViewById(R.id.ima22);

            viewHolderItem.ima11 = (ImageView) convertView.findViewById(R.id.ima11);
            viewHolderItem.image4MaskView = (TextView) convertView.findViewById(R.id.image4MaskView);

            viewHolderItem.txtWatchCount = (TextView) convertView.findViewById(R.id.txtWatchCount);
            viewHolderItem.txtCommentCount = (TextView) convertView.findViewById(R.id.txtCommentCount);

            viewHolderItem.imaUserComment = (ImageView) convertView.findViewById(R.id.imaUserComment);
            viewHolderItem.llComment2 = (LinearLayout) convertView.findViewById(R.id.llComment2);
            viewHolderItem.txtUserNameComment = (TextView) convertView.findViewById(R.id.txtUserNameComment);
            viewHolderItem.txtComment = (TextView) convertView.findViewById(R.id.txtComment);
            viewHolderItem.llPostItem = (LinearLayout) convertView.findViewById(R.id.llPostItem);
            viewHolderItem.imaWatchIcon = (ImageView) convertView.findViewById(R.id.imaWatchIcon);
            viewHolderItem.txtWatchStatus = (TextView) convertView.findViewById(R.id.txtWatchStatus);
            viewHolderItem.llWatch = (LinearLayout) convertView.findViewById(R.id.llWatch);
            viewHolderItem.llComment = (LinearLayout) convertView.findViewById(R.id.llComment);
            viewHolderItem.btnMore = (ImageButton) convertView.findViewById(R.id.btnMore);
            viewHolderItem.postMenu = (LinearLayout) convertView.findViewById(R.id.postMenu);

            convertView.setTag(viewHolderItem);
        } else {
            viewHolderItem = (ViewHolderItem) convertView.getTag();
        }

        final STPost post = posts.get(position);
        viewHolderItem.setUpViewHolderItem(post, activity);

        //action
        if (mode == STEnums.MyAdapterViewMode.normal) {
            viewHolderItem.llPostItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    STPostDetailViewControllerFragment fragment = new STPostDetailViewControllerFragment();
                    fragment.setPostID(post.postID);
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

            viewHolderItem.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    STPostCommentViewControllerFragment fragment = new STPostCommentViewControllerFragment();
                    fragment.setPostID(post.postID);
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

            boolean b = post.watchID == 0 ? true : false;

            final boolean[] dowatch = {b};

            viewHolderItem.llWatch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (post == null)
                        return;
                    STUser me = LoginManager.getsIntstance().me();
                    if (me != null) {
                        String status = me.status;
                        if (status.equals(STEnums.UserStatus.Customer.getUserStatus())) {
                            Integer postedUserID = post.user.userID;
                            if (postedUserID != null) {
                                if (me.userID == postedUserID) {
                                    Toast.makeText(activity, "自分のPostはWatch出来ません", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    if (dowatch[0]) {
                                        dowatch[0] = !dowatch[0];
                                        viewHolderItem.txtWatchStatus.setText("Watch");
                                        viewHolderItem.txtWatchStatus.setTextColor(0xFF007AFF);
                                        viewHolderItem.imaWatchIcon.setColorFilter(0xFF007AFF);
                                        DataSender.createWatch(post.postID, new STCompletion.Base() {
                                            @Override
                                            public void onRequestComplete(boolean isSuccess, STError err) {
                                                if (!isSuccess) {
                                                    dowatch[0] = true;
                                                    viewHolderItem.txtWatchStatus.setText("Watchする");
                                                    viewHolderItem.txtWatchStatus.setTextColor(0xFFB2B2B2);
                                                    viewHolderItem.imaWatchIcon.setColorFilter(0xFFB2B2B2);
                                                }
                                            }
                                        });
                                    } else {
                                        dowatch[0] = !dowatch[0];
                                        viewHolderItem.txtWatchStatus.setText("Watchする");
                                        viewHolderItem.txtWatchStatus.setTextColor(0xFFB2B2B2);
                                        viewHolderItem.imaWatchIcon.setColorFilter(0xFFB2B2B2);
                                        DataSender.deleteWatch(post.postID, post.watchID, new STCompletion.Base() {
                                            @Override
                                            public void onRequestComplete(boolean isSuccess, STError err) {
                                                if (!isSuccess) {
                                                    dowatch[0] = false;
                                                    viewHolderItem.txtWatchStatus.setText("Watch");
                                                    viewHolderItem.txtWatchStatus.setTextColor(0xFF007AFF);
                                                    viewHolderItem.imaWatchIcon.setColorFilter(0xFF007AFF);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        } else if (status.equals(STEnums.UserStatus.Staff.getUserStatus())) {

                            STReplySelectItemPageViewControllerFragment fragment = new STReplySelectItemPageViewControllerFragment();
                            fragment.setPostID(post.postID);
                            fragment.setCategoryID(post.categoryID);
                            Log.i("aaaaaaaaaaaaaaa", "post.categoryID: " + post.categoryID);
                            activity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .add(android.R.id.tabcontent, fragment)
                                    .addToBackStack(null)
                                    .commit();

                        } else {
                            PopUpLogin popUpLogin = new PopUpLogin(activity);
                            popUpLogin.show();
                        }
                    } else {
                        PopUpLogin popUpLogin = new PopUpLogin(activity);
                        popUpLogin.show();
                    }
                }
            });

            viewHolderItem.btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String status = "";
                    try {
                        status = LoginManager.getsIntstance().me().status;
                    } catch (Exception e) {
                        status = "";
                    }
                    if (status == "" || status.equals("none")) {
                        PopUpLogin login = new PopUpLogin(activity);
                        login.show();
                    } else {
                        PopUpMoreActionPost moreAction = new PopUpMoreActionPost(activity, post);
                        moreAction.show();
                    }
                }
            });
        } else if (mode == STEnums.MyAdapterViewMode.createReply) {
            viewHolderItem.postMenu.setVisibility(View.GONE);
            viewHolderItem.llPostItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    STReplySelectItemPageViewControllerFragment fragment = new STReplySelectItemPageViewControllerFragment();
                    fragment.setPostID(post.postID);
                    fragment.setCategoryID(post.categoryID);
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
        //end action

        return convertView;
    }

    static class ViewHolderItem {

        private ImageView imaAvatar;

        private TextView txtName;
        private TextView txtAge;
        private TextView txtSex;
        private TextView txtTile;
        private TextView txtTextPost;
        private TextView txtCountReply;
        private TextView txtCategory;

        private LinearLayout linearLayout4Ima;

        private ImageView ima41;
        private ImageView ima42;
        private ImageView ima43;
        private ImageView ima44;
        private ImageView ima31;
        private ImageView ima21;
        private ImageView ima22;
        private ImageView ima11;
        private TextView image4MaskView;

        private TextView txtWatchCount;
        private TextView txtCommentCount;
        private ImageView imaUserComment;
        private LinearLayout llComment2;
        private TextView txtUserNameComment;
        private TextView txtComment;
        private LinearLayout llPostItem;
        private ImageView imaWatchIcon;
        private TextView txtWatchStatus;
        private LinearLayout llWatch;
        private LinearLayout llComment;
        private ImageButton btnMore;
        private LinearLayout postMenu;

        private void setUpViewHolderItem(final STPost stPost, final Context context) {

            txtName.setText(stPost.user.name);
            String s = new String(stPost.user.birthday);
            txtAge.setText(stPost.user.apiDetail);
            txtTextPost.setText(stPost.text);
            Picasso.with(context)
                    .load(stPost.user.imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    .placeholder(R.drawable.default_avatar)
                    .into(imaAvatar);

            if (stPost.categoryName.equals("カテゴリーを選択しない")) {
                txtCategory.setText("その他");
            } else {
                txtCategory.setText(stPost.categoryName);
            }

            txtCountReply.setText("PostへのReply (" + stPost.repliesCount + ")");

            clearImageView();

            if (stPost.itemSorts.size() == 1) {
                ima11.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(stPost.itemSorts.get(0).item.mainImageURL)
                        .into(ima11);
            } else if (stPost.itemSorts.size() == 2) {
                ima21.setVisibility(View.VISIBLE);
                ima22.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(stPost.itemSorts.get(0).item.mainImageURL)
                        .into(ima21);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(1).item.mainImageURL)
                        .into(ima22);
            } else if (stPost.itemSorts.size() == 3) {


                ima31.setVisibility(View.VISIBLE);
                ima43.setVisibility(View.VISIBLE);
                ima44.setVisibility(View.VISIBLE);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(0).item.mainImageURL)
                        .into(ima31);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(1).item.mainImageURL)
                        .into(ima43);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(2).item.mainImageURL)
                        .into(ima44);
            } else if (stPost.itemSorts.size() >= 4) {

                ima41.setVisibility(View.VISIBLE);
                ima42.setVisibility(View.VISIBLE);
                ima43.setVisibility(View.VISIBLE);
                ima44.setVisibility(View.VISIBLE);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(0).item.mainImageURL)
                        .into(ima41);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(1).item.mainImageURL)
                        .into(ima42);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(2).item.mainImageURL)
                        .into(ima43);

                Picasso.with(context)
                        .load(stPost.itemSorts.get(3).item.mainImageURL)
                        .into(ima44);
            } else if (stPost.itemSorts.size() == 0) {
                linearLayout4Ima.setVisibility(View.GONE);
            }
            if (stPost.repliesCount > 4) {
                image4MaskView.setText("+" + (stPost.repliesCount - 4) + " ");
                image4MaskView.setVisibility(View.VISIBLE);
            } else {
                image4MaskView.setVisibility(View.GONE);
            }

            txtWatchCount.setText(stPost.watchesCount + "");
            txtCommentCount.setText(stPost.commentsCount + " コメント");

            try {
                Picasso.with(context)
                        .load(stPost.comment.user.imageURL)
                        .transform(new TransformationUtils().new CircleTransform())
                        .into(imaUserComment);


                txtUserNameComment.setText(stPost.comment.user.name);
                txtComment.setText(stPost.comment.text);
            } catch (Exception e) {
                llComment2.setVisibility(View.GONE);
            }
            String status = "";
            try {
                status = LoginManager.status;
                if (status == null) {
                    status = "";
                }
            } catch (Exception e) {
            }
            if (status.equals(STEnums.UserStatus.Staff.getUserStatus())) {
                if (stPost.replyID == 0) {
                    txtWatchStatus.setText("Replyする");
                    txtWatchStatus.setTextColor(0xFFB2B2B2);
                    imaWatchIcon.setImageResource(R.drawable.icon_create);
                    imaWatchIcon.setColorFilter(0xFFB2B2B2);
                    llWatch.setEnabled(true);
                } else {
                    txtWatchStatus.setText("Reply済み");
                    txtWatchStatus.setTextColor(0xFF007AFF);
                    imaWatchIcon.setImageResource(R.drawable.icon_create_selected);
                    imaWatchIcon.setColorFilter(0xFF007AFF);
                    llWatch.setEnabled(false);
                }
            } else {
                if (stPost.watchID == 0) {
                    txtWatchStatus.setText("Watchする");
                    txtWatchStatus.setTextColor(0xFFB2B2B2);
                    imaWatchIcon.setColorFilter(0xFFB2B2B2);
                    llWatch.setEnabled(true);
                } else {
                    txtWatchStatus.setText("Watch");
                    txtWatchStatus.setTextColor(0xFF007AFF);
                    imaWatchIcon.setColorFilter(0xFF007AFF);
                    llWatch.setEnabled(true);
                }
            }
        }

        private void clearImageView() {
            ima11.setVisibility(View.GONE);
            ima21.setVisibility(View.GONE);
            ima22.setVisibility(View.GONE);
            ima31.setVisibility(View.GONE);
            ima41.setVisibility(View.GONE);
            ima42.setVisibility(View.GONE);
            ima43.setVisibility(View.GONE);
            ima44.setVisibility(View.GONE);
            image4MaskView.setVisibility(View.GONE);
            linearLayout4Ima.setVisibility(View.VISIBLE);
        }
    }
}
