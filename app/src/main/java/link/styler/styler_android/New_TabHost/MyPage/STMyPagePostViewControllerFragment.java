package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterPosts;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/20/17.
 */

public class STMyPagePostViewControllerFragment extends Fragment {

    //model
    private RealmResults<STPostSort> postSorts;
    private RealmList<STPost> posts = new RealmList<STPost>();
    private Integer lastObjectID;
    private int userID;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterPosts adapterPosts = null;

    public void createAdapterPosts() {
        this.postSorts = DataLoader.getUserPosts(userID);
        this.posts = new RealmList<STPost>();
        for (STPostSort stPostSort : this.postSorts) {
            this.posts.add(stPostSort.post);
        }
        this.adapterPosts = new MyAdapterPosts(getActivity(), this.posts);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterPosts == null)
            createAdapterPosts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_post_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_post);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        gridView.setAdapter(adapterPosts);
        if (adapterPosts.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        STMyPagePostViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPagePostVCF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPagePostVCF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPagePostVCF", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPagePostVCF", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPagePostVCF", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPagePostVCF", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.savePostsOfUser(userID, null, STEnums.PostSortType.User.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPagePostViewControllerFragment.this.postSorts = DataLoader.getUserPosts(userID);
                    STMyPagePostViewControllerFragment.this.posts = new RealmList<STPost>();
                    for (STPostSort stPostSort : STMyPagePostViewControllerFragment.this.postSorts) {
                        STMyPagePostViewControllerFragment.this.posts.add(stPostSort.post);
                    }
                    if (STMyPagePostViewControllerFragment.this.adapterPosts == null)
                        STMyPagePostViewControllerFragment.this.createAdapterPosts();

                    STMyPagePostViewControllerFragment.this.adapterPosts = (MyAdapterPosts) STMyPagePostViewControllerFragment.this.gridView.getAdapter();
                    STMyPagePostViewControllerFragment.this.adapterPosts.setPosts(STMyPagePostViewControllerFragment.this.posts);
                    STMyPagePostViewControllerFragment.this.adapterPosts.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPagePostViewControllerFragment.this.lastObjectID = lastObjectID;
                STMyPagePostViewControllerFragment.this.isLoading = false;
                STMyPagePostViewControllerFragment.this.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.savePostsOfUser(userID, lastObjectID, STEnums.PostSortType.User.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPagePostViewControllerFragment.this.postSorts = DataLoader.getUserPosts(userID);
                    STMyPagePostViewControllerFragment.this.posts = new RealmList<STPost>();
                    for (STPostSort stPostSort : STMyPagePostViewControllerFragment.this.postSorts) {
                        STMyPagePostViewControllerFragment.this.posts.add(stPostSort.post);
                    }
                    if (STMyPagePostViewControllerFragment.this.adapterPosts == null)
                        STMyPagePostViewControllerFragment.this.createAdapterPosts();

                    STMyPagePostViewControllerFragment.this.adapterPosts = (MyAdapterPosts) STMyPagePostViewControllerFragment.this.gridView.getAdapter();
                    STMyPagePostViewControllerFragment.this.adapterPosts.setPosts(STMyPagePostViewControllerFragment.this.posts);
                    STMyPagePostViewControllerFragment.this.adapterPosts.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPagePostViewControllerFragment.this.lastObjectID = lastObjectID;
                STMyPagePostViewControllerFragment.this.isLoading = false;
                STMyPagePostViewControllerFragment.this.progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STMyPagePostVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STMyPagePostVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STMyPagePostVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STMyPagePostVCF", "onDestroy");
    }
}
