package link.styler.styler_android.New_TabHost.Common;


import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import link.styler.Utils.Logger;
import link.styler.styler_android.R;

/**
 * Created by admin on 3/6/17.
 */

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private double lng;
    private double lnt;

    public static MapsFragment newInstance(double lng, double lnt){
        MapsFragment mapsFragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putDouble("lng",lng);
        args.putDouble("lnt",lnt);
        mapsFragment.setArguments(args);
        return mapsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lng = getArguments().getDouble("lng");
        lnt = getArguments().getDouble("lnt");
        Logger.print("lng "+lng+" lnt "+lnt);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.activity_maps, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Logger.print("getActivity "+getActivity().getSupportFragmentManager());

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFragment == null){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map,mapFragment).commit();
        }

        if(mapFragment != null){
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng location = new LatLng(lnt,lng);
        mMap.addMarker(new MarkerOptions().position(location).title(""));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,15.0f));
    }
}
