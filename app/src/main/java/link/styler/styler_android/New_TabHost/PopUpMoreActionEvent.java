package link.styler.styler_android.New_TabHost;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import link.styler.models.STEvent;
import link.styler.styler_android.New_TabHost.Create.STCreateEventViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 6/8/17.
 */

public class PopUpMoreActionEvent extends Dialog {
    //model
    private STEvent event = new STEvent();
    private FragmentActivity activity;
    private String type;

    //view
    public Button editButton, reporButton, cancelButton;
    public LinearLayout lineLayout;

    //helper
    int myShopID = 0;
    int shopID = 0;

    public PopUpMoreActionEvent(FragmentActivity activity, STEvent event) {
        super(activity);
        this.activity = activity;
        this.event = event;
        this.shopID = event.shopID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_more_action_item);
        addControls();
        if (event.eventID == 0)
            dismiss();

        initView();
        addEvents();

    }

    private void addControls() {
        editButton = (Button) findViewById(R.id.editButton);
        reporButton = (Button) findViewById(R.id.reportButtonDialog);
        reporButton.setVisibility(View.GONE);
        cancelButton = (Button) findViewById(R.id.cancelButton);
    }

    private void initView() {
        editButton.setText("Eventを編集する");
    }

    private void addEvents() {
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAction();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void editAction() {
        STCreateEventViewControllerFragment fragment = new STCreateEventViewControllerFragment();
        fragment.setEventID(event.eventID);
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
        dismiss();
    }

    private void reportAction() {
        Toast.makeText(activity, "報告されたPostは運営で調査致します。", Toast.LENGTH_LONG).show();
        dismiss();
    }

}