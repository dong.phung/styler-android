package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import io.realm.RealmList;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STItem;
import link.styler.models.STMessage;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/30/17.
 */

public class MyAdapterMessage extends BaseAdapter {

    private FragmentActivity fragmentActivity;
    private RealmList<STMessage> messages;

    public MyAdapterMessage(FragmentActivity fragmentActivity, RealmList<STMessage> messages) {
        this.fragmentActivity = fragmentActivity;
        this.messages = messages;
    }

    public void setMessages(RealmList<STMessage> messages) {
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return messages.get(position).messageID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        STMessage message = messages.get(position);
        Log.i("MyAdapterMessage001", "position" + position);
        Log.i("MyAdapterMessage001", "message" + message);
        switch (message.type) {
            case "user":
                Log.i("MyAdapterMessage002", "message.type " + message.type);
                return createSTMessageCell(message, convertView);
            case "item":
                Log.i("MyAdapterMessage003", "message.type " + message.type);
                return createSTMessageItemCell(message, convertView);
            case "notification":
                Log.i("MyAdapterMessage004", "message.type " + message.type);
                return createSTMessageNotificationCell(message, convertView);
            case "link":
                Log.i("MyAdapterMessage005", "message.type " + message.type);
                return createSTMessageLinkCell(message, convertView);
            default:
                Log.i("MyAdapterMessage006", "message.type " + message.type);
                return convertView;
        }
    }

    private View createSTMessageCell(final STMessage message, View convertView) {

        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_message_cell, null);

        //intView();
        final ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatarImageView);
        TextView commentLabel = (TextView) convertView.findViewById(R.id.commentLabel);
        TextView dateLabel = (TextView) convertView.findViewById(R.id.dateLabel);
        Button reportButton = (Button) convertView.findViewById(R.id.reportButton);
        ImageView avatarImageViewMe = (ImageView) convertView.findViewById(R.id.avatarImageViewMe);

        //setupView();
        if (message == null || message.messageID == 0)
            return null;

        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {

        }
        if (meID == message.userID) {
            commentLabel.setTextColor(0xFFFFFFFF);
            commentLabel.setBackgroundColor(0xFF4A4A4A);
            reportButton.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.INVISIBLE);
            avatarImageViewMe.setVisibility(View.VISIBLE);
            Picasso.with(fragmentActivity)
                    .load(LoginManager.getsIntstance().me().imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageViewMe);
        } else {
            commentLabel.setTextColor(0xFF000000);
            commentLabel.setBackgroundColor(0xFFBEBEBE);
            reportButton.setVisibility(View.VISIBLE);
            avatarImageView.setVisibility(View.VISIBLE);
            avatarImageViewMe.setVisibility(View.INVISIBLE);
            DataPreserver.saveUser(message.userID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (isSuccess) {
                        STUser user = DataLoader.getUser(message.userID);
                        Picasso.with(fragmentActivity)
                                .load(user.imageURL)
                                .transform(new TransformationUtils().new CircleTransform())
                                .into(avatarImageView);
                    }
                }
            });
        }

        commentLabel.setText(message.text);
        String date = new STDateFormat().DateFormat(message.createdAt);
        dateLabel.setText(date);

        //action
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int meID = 0;
                try {
                    meID = LoginManager.getsIntstance().me().userID;
                } catch (Exception e) {
                    return;
                }
                if (message.messageID == 0) {
                    return;
                }
                //// TODO: 3/30/17  report
            }
        });

        return convertView;
    }

    private View createSTMessageItemCell(final STMessage message, View convertView) {

        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_message_item_cell, null);

        //intView();
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);
        final ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatarImageView);
        ImageView itemImageView = (ImageView) convertView.findViewById(R.id.itemImageView);
        TextView brandLabel = (TextView) convertView.findViewById(R.id.brandLabel);
        TextView nameLabel = (TextView) convertView.findViewById(R.id.nameLabel);
        TextView priceLabel = (TextView) convertView.findViewById(R.id.priceLabel);
        TextView dateLabel = (TextView) convertView.findViewById(R.id.dateLabel);
        Button reportButton = (Button) convertView.findViewById(R.id.reportButton);
        ImageView avatarImageViewMe = (ImageView) convertView.findViewById(R.id.avatarImageViewMe);

        //setupView();
        if (message == null || message.messageID == 0)
            return null;
        final STItem item = message.item;
        if (item == null || message.item.itemID == 0)
            return null;
        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {
        }
        if (meID == message.userID) {
            linearLayout.setGravity(Gravity.END | Gravity.BOTTOM);
            reportButton.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.GONE);
            avatarImageViewMe.setVisibility(View.VISIBLE);
            Picasso.with(fragmentActivity)
                    .load(LoginManager.getsIntstance().me().imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    //.placeholder(R.drawable.default_avatar)
                    .into(avatarImageViewMe);
        } else {
            linearLayout.setGravity(Gravity.START | Gravity.BOTTOM);
            reportButton.setVisibility(View.VISIBLE);
            avatarImageViewMe.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.VISIBLE);

            DataPreserver.saveUser(message.userID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (isSuccess) {
                        STUser user = DataLoader.getUser(message.userID);
                        Picasso.with(fragmentActivity)
                                .load(user.imageURL)
                                .transform(new TransformationUtils().new CircleTransform())
                                .into(avatarImageView);
                    }
                }
            });
        }
        Log.i("MyAdapterMessage011", "item.mainImageURL: " + item.mainImageURL);
        try {
            Picasso.with(fragmentActivity)
                    .load(item.mainImageURL)
                    .resize(400, 400)
                    .centerCrop()
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(itemImageView);
        } catch (Exception e) {
        }

        brandLabel.setText(item.brand);
        nameLabel.setText(item.name);
        if (!item.brand.isEmpty()) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("￥" + decimalFormat.format(Integer.parseInt(item.price)) + "（税込）");
        } else {
            priceLabel.setText("");
        }

        String date = new STDateFormat().DateFormat(message.createdAt);
        dateLabel.setText(date);

        //addcontrol()
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int meID = 0;
                try {
                    meID = LoginManager.getsIntstance().me().userID;
                } catch (Exception e) {
                    return;
                }
                if (message.messageID == 0) {
                    return;
                }
                //// TODO: 3/30/17  report
            }
        });

        itemImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                fragment.setItemID(item.itemID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        return convertView;
    }

    private View createSTMessageNotificationCell(STMessage message, View convertView) {
        {
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View) inflater.inflate(R.layout.st_message_notification_cell, null);

            //intView();
            TextView notificationLabel = (TextView) convertView.findViewById(R.id.notificationLabel);

            //setupView();
            notificationLabel.setText(message.text);

            //addcontrol()

            return convertView;
        }
    }

    private View createSTMessageLinkCell(STMessage message, View convertView) {

        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_message_link_cell, null);

        //intView();
        ImageView itemImageView = (ImageView) convertView.findViewById(R.id.itemImageView);
        TextView titleLabel = (TextView) convertView.findViewById(R.id.titleLabel);
        TextView descriptionLabel = (TextView) convertView.findViewById(R.id.descriptionLabel);
        //setupView();

        Picasso.with(fragmentActivity)
                .load(message.imageURL)
                //.placeholder(R.drawable.default_avatar)
                .into(itemImageView);
        titleLabel.setText(message.title);
        descriptionLabel.setText(message.linkDescription);

        //addcontrol()

        return convertView;
    }
}
