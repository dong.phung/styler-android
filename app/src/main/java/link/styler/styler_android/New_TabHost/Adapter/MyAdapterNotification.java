package link.styler.styler_android.New_TabHost.Adapter;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import io.realm.RealmList;
import link.styler.STStructs.STEnums;
import link.styler.Utils.Logger;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STNotification;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.PostDetail.STPostCommentViewControllerFragment;
import link.styler.styler_android.New_TabHost.Message.STMessageViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;


/**
 * Created by admin on 1/16/17.
 */

public class MyAdapterNotification extends BaseAdapter {

    private FragmentActivity fragmentActivity;
    private RealmList<STNotification> notifications;

    private final int Comment = 1;
    private final int Event = 2;
    private final int Feedback = 3;
    private final int Follow = 4;
    private final int Interest = 5;
    private final int Like = 6;
    private final int Reply = 7;
    private final int Watch = 8;


    public MyAdapterNotification(FragmentActivity fragmentActivity, RealmList<STNotification> notifications) {
        Log.i("Quang", "My Adapter .....");
        this.fragmentActivity = fragmentActivity;
        this.notifications = notifications;
    }

    public void setNotifications(RealmList<STNotification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Object getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int postion, View converView, ViewGroup parent) {


        STNotification notification = notifications.get(postion);
        ViewHolderReplyCell viewHolderReplyCell = null;
        ViewHolderCommentCell viewHolderCommentCell = null;
        ViewHolderFeedbackCell viewHolderFeedbackCell = null;
        ViewHolderFollowCell viewHolderFollowCell = null;
        Logger.print("ABCD " + converView);
        if (converView == null ||
                converView.getTag(R.id.reply_cell) == null ||
                converView.getTag(R.id.comment_cell) == null ||
                converView.getTag(R.id.feedback_cell) == null ||
                converView.getTag(R.id.follow_cell) == null)

        {
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            switch (notifications.get(postion).type) {
                case Reply:
                case Like:
                case Event:
                case Interest:
                    Logger.print("ABCD Reply");
                    converView = (View) inflater.inflate(R.layout.reply_cell_layout, null);
                    viewHolderReplyCell = new ViewHolderReplyCell();
                    viewHolderReplyCell.initReplyCell(converView);
                    converView.setTag(R.id.reply_cell, viewHolderReplyCell);
                    break;
                case Comment:
                case Watch:
                    Logger.print("ABCD Comment");
                    converView = (View) inflater.inflate(R.layout.comment_cell_layout, null);
                    viewHolderCommentCell = new ViewHolderCommentCell();
                    viewHolderCommentCell.initCommentCell(converView);
                    converView.setTag(R.id.comment_cell, viewHolderCommentCell);
                    break;
                case Feedback:
                    Logger.print("ABCD Comment");
                    converView = (View) inflater.inflate(R.layout.feedback_cell_layout, null);
                    viewHolderFeedbackCell = new ViewHolderFeedbackCell();
                    viewHolderFeedbackCell.initFeedbackCell(converView);
                    converView.setTag(R.id.feedback_cell, viewHolderFeedbackCell);
                    break;
                case Follow:
                    Logger.print("ABCD Follow");
                    converView = (View) inflater.inflate(R.layout.follow_cell_layout, null);
                    viewHolderFollowCell = new ViewHolderFollowCell();
                    viewHolderFollowCell.initFollowCell(converView);
                    converView.setTag(R.id.follow_cell, viewHolderFollowCell);
                    break;
                default:
                    break;
            }
        } else {
            Logger.print("ABCD " + notification.type);
            if (notification.type == Reply || notification.type == Like || notification.type == Event || notification.type == Interest) {
                viewHolderReplyCell = (ViewHolderReplyCell) converView.getTag(R.id.reply_cell);
                Logger.print("ABCD " + viewHolderReplyCell);
            } else if (notification.type == Comment || notification.type == Watch) {
                viewHolderCommentCell = (ViewHolderCommentCell) converView.getTag(R.id.comment_cell);
            } else if (notification.type == Feedback) {
                viewHolderFeedbackCell = (ViewHolderFeedbackCell) converView.getTag(R.id.feedback_cell);
            } else if (notification.type == Follow) {
                viewHolderFollowCell = (ViewHolderFollowCell) converView.getTag(R.id.follow_cell);
            }
        }

        if (notification.type == Reply || notification.type == Like || notification.type == Event || notification.type == Interest) {
            viewHolderReplyCell.setUpReplyCell(notification, fragmentActivity);
        } else if (notification.type == Comment || notification.type == Watch) {
            viewHolderCommentCell.setUpCommentCell(notification, fragmentActivity);
        } else if (notification.type == Feedback) {
            viewHolderFeedbackCell.setUpFeedbackCell(notification, fragmentActivity);
        } else if (notification.type == Follow) {
            viewHolderFollowCell.setUpFollowCell(notification, fragmentActivity);
        }
        return converView;
    }

    private class ViewHolderReplyCell {

        private ImageView avatarImageView;
        private TextView titleLabel;
        private TextView brandLabel;
        private TextView productNameLabel;
        private TextView priceLabel;
        private ImageView itemImageView;
        private LinearLayout reply_cell_layout;

        public void initReplyCell(final View view) {
            avatarImageView = (ImageView) view.findViewById(R.id.avatar_reply_cell);
            titleLabel = (TextView) view.findViewById(R.id.titlelable_reply_cell);
            brandLabel = (TextView) view.findViewById(R.id.brandLabel_reply_cell);
            priceLabel = (TextView) view.findViewById(R.id.priceLabel_reply_cell);
            productNameLabel = (TextView) view.findViewById(R.id.productNameLabel_reply_cell);
            itemImageView = (ImageView) view.findViewById(R.id.item_reply_cell);
            reply_cell_layout = (LinearLayout) view.findViewById(R.id.reply_cell_layout);
        }

        public void setUpReplyCell(final STNotification notification, final Context context) {
            String authorImageURL = notification.authorImageURL;
            if (authorImageURL.isEmpty()) {
                avatarImageView.setImageResource(R.drawable.default_avatar);
            } else {
                Picasso.with(context).load(authorImageURL).transform(new TransformationUtils().new CircleTransform()).placeholder(R.drawable.default_avatar).into(avatarImageView);
            }

            titleLabel.setText(notification.notificationText);

            switch (notification.type) {
                case 7:
                case 6:
                    brandLabel.setText(notification.itemBrand);
                    productNameLabel.setText(notification.itemName);
                    DecimalFormat decimalFormat = new DecimalFormat("#,###");
                    priceLabel.setText("¥" + decimalFormat.format(notification.itemPrice) + "(税込)");
                    Picasso.with(context).load(notification.itemImageURL).error(R.drawable.default_avatar).transform(new TransformationUtils().new RoundedCornersTransform()).into(itemImageView);
                    break;
                case 2:
                case 5:
                    brandLabel.setText(notification.eventTitle);
                    productNameLabel.setText(notification.eventStartDate + "〜");
                    priceLabel.setText("");
                    Picasso.with(context).load(notification.eventImageURL).error(R.drawable.default_avatar).transform(new TransformationUtils().new RoundedCornersTransform()).into(itemImageView);
                    break;
            }

            reply_cell_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                    fragment.setItemID(notification.itemID);
                    if (notification.postID != 0) {
                        fragment.setPostID(notification.postID);
                        fragment.setViewType(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType());
                    }
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }

    }

    private class ViewHolderCommentCell {
        private ImageView avatarImageView;
        private TextView titleLabel, descriptionLabel;
        private LinearLayout comment_cell_layout;

        public void initCommentCell(View view) {
            avatarImageView = (ImageView) view.findViewById(R.id.avatar_comment_cell);
            titleLabel = (TextView) view.findViewById(R.id.titleLabel_comment_cell);
            descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel_comment_cell);
            comment_cell_layout = (LinearLayout) view.findViewById(R.id.comment_cell_layout);

        }

        public void setUpCommentCell(final STNotification notification, final Context context) {
            String authorIamgeURL = notification.authorImageURL;
            if (authorIamgeURL.isEmpty()) {
                avatarImageView.setImageResource(R.drawable.default_avatar);
            } else {
                Picasso.with(context).load(authorIamgeURL).placeholder(R.drawable.default_avatar).transform(new TransformationUtils().new CircleTransform()).into(avatarImageView);
            }

            titleLabel.setText(notification.notificationText);

            if (notification.type == 1)
                if (!notification.commentText.equals("null"))
                    descriptionLabel.setText(notification.commentText);
                else
                    descriptionLabel.setText(notification.postText);
            comment_cell_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    STPostCommentViewControllerFragment fragment = new STPostCommentViewControllerFragment();
                    fragment.setPostID(notification.postID);
                    fragmentActivity
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    private class ViewHolderFeedbackCell {
        private ImageView authorImageView;
        private TextView notificationLabel;
        private TextView brandLabel;
        private TextView productNameLabel;
        private TextView priceLabel;
        private ImageView itemImageView;
        private ImageView feedStarView1;
        private ImageView feedStarView2;
        private ImageView feedStarView3;
        private ImageView feedStarView4;
        private ImageView feedStarView5;
        private TextView feedLabel;

        private LinearLayout feedback_cell_layout;

        public void initFeedbackCell(View view) {
            authorImageView = (ImageView) view.findViewById(R.id.author_feedback_cell);
            notificationLabel = (TextView) view.findViewById(R.id.notification_feedback_cell);
            brandLabel = (TextView) view.findViewById(R.id.brandLabel_feedback_cell);
            productNameLabel = (TextView) view.findViewById(R.id.productNameLabel_feedback_cell);
            priceLabel = (TextView) view.findViewById(R.id.priceLabel_feedback_cell);
            itemImageView = (ImageView) view.findViewById(R.id.item_feedback_cell);
            feedStarView1 = (ImageView) view.findViewById(R.id.feedStarView1_feedback_cell);
            feedStarView2 = (ImageView) view.findViewById(R.id.feedStarView2_feedback_cell);
            feedStarView3 = (ImageView) view.findViewById(R.id.feedStarView3_feedback_cell);
            feedStarView4 = (ImageView) view.findViewById(R.id.feedStarView4_feedback_cell);
            feedStarView5 = (ImageView) view.findViewById(R.id.feedStarView5_feedback_cell);
            feedLabel = (TextView) view.findViewById(R.id.feedLabel_feedback_cell);

            feedback_cell_layout = (LinearLayout) view.findViewById(R.id.feedback_cell_layout);

        }

        public void setUpFeedbackCell(final STNotification notification, final Context context) {
            String authorImageURL = notification.authorImageURL;
            if (authorImageURL.isEmpty()) {
                authorImageView.setImageResource(R.drawable.default_avatar);
            } else {
                Picasso.with(context).load(authorImageURL).transform(new TransformationUtils().new CircleTransform()).into(authorImageView);
            }

            brandLabel.setText(notification.itemBrand);
            productNameLabel.setText(notification.itemName);
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("¥" + decimalFormat.format(notification.itemPrice) + "(税込)");

            String itemImageURL = notification.itemImageURL;
            if (!itemImageURL.isEmpty()) {
                Picasso.with(context).load(itemImageURL).transform(new TransformationUtils().new RoundedCornersTransform()).into(itemImageView);
            }

            switch (notification.feedbackRate) {
                case 1:
                    feedStarView1.setImageResource(R.drawable.icon_star_active);
                    feedStarView2.setImageResource(R.drawable.icon_star);
                    feedStarView3.setImageResource(R.drawable.icon_star);
                    feedStarView4.setImageResource(R.drawable.icon_star);
                    feedStarView5.setImageResource(R.drawable.icon_star);
                    feedLabel.setText("ありがとう！");
                    break;
                case 2:
                    feedStarView1.setImageResource(R.drawable.icon_star_active);
                    feedStarView2.setImageResource(R.drawable.icon_star_active);
                    feedStarView3.setImageResource(R.drawable.icon_star);
                    feedStarView4.setImageResource(R.drawable.icon_star);
                    feedStarView5.setImageResource(R.drawable.icon_star);
                    feedLabel.setText("希望通りです！");
                    break;
                case 3:
                    feedStarView1.setImageResource(R.drawable.icon_star_active);
                    feedStarView2.setImageResource(R.drawable.icon_star_active);
                    feedStarView3.setImageResource(R.drawable.icon_star_active);
                    feedStarView4.setImageResource(R.drawable.icon_star);
                    feedStarView5.setImageResource(R.drawable.icon_star);
                    feedLabel.setText("気に入りました！");
                    break;
                case 4:
                    feedStarView1.setImageResource(R.drawable.icon_star_active);
                    feedStarView2.setImageResource(R.drawable.icon_star_active);
                    feedStarView3.setImageResource(R.drawable.icon_star_active);
                    feedStarView4.setImageResource(R.drawable.icon_star_active);
                    feedStarView5.setImageResource(R.drawable.icon_star);
                    feedLabel.setText("すごく好みです！");
                    break;
                case 5:
                    feedStarView1.setImageResource(R.drawable.icon_star_active);
                    feedStarView2.setImageResource(R.drawable.icon_star_active);
                    feedStarView3.setImageResource(R.drawable.icon_star_active);
                    feedStarView4.setImageResource(R.drawable.icon_star_active);
                    feedStarView5.setImageResource(R.drawable.icon_star_active);
                    feedLabel.setText("今すぐ欲しいです！");
                    break;
                default:
                    break;
            }
            feedback_cell_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                    fragment.setItemID(notification.itemID);
                    if (notification.postID != 0) {
                        fragment.setPostID(notification.postID);
                        fragment.setViewType(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType());
                    }
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    private class ViewHolderFollowCell {

        private ImageView userImageView;
        private TextView notificationLabel;
        private Button followButton;

        private LinearLayout follow_cell_layout;

        public void initFollowCell(View view) {
            userImageView = (ImageView) view.findViewById(R.id.user_follow_cell);
            notificationLabel = (TextView) view.findViewById(R.id.notification_follow_cell);
            followButton = (Button) view.findViewById(R.id.followbtn_follow_cell);
            followButton.setBackgroundColor(Color.rgb(239, 239, 239));
            followButton.setText("フォローのお礼をメッセージする");
            follow_cell_layout = (LinearLayout) view.findViewById(R.id.follow_cell_layout);

        }

        public void setUpFollowCell(final STNotification notification, final Context context) {
            String imageURL = notification.authorImageURL;
            if (imageURL.isEmpty()) {
                userImageView.setImageResource(R.drawable.icon_default_avatar);
            } else {
                Picasso.with(context).load(imageURL).transform(new TransformationUtils().new CircleTransform()).into(userImageView);
            }
            notificationLabel.setText(notification.notificationText);

            followButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    STMessageViewControllerFragment fragment = new STMessageViewControllerFragment();
                    fragment.setDestinationUserID(notification.authorID);
                    fragment.setDestinationUserName(notification.authorName);
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

            follow_cell_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                    fragment.setUserID(notification.authorID);
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

}
