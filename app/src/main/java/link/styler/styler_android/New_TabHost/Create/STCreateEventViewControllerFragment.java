package link.styler.styler_android.New_TabHost.Create;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.models.STEvent;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.New_TabHost.WaitActivity;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Home.Event.STEventDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.Home.STHomePageViewControllerFragment;
import link.styler.styler_android.R;

import static android.app.Activity.RESULT_OK;

public class STCreateEventViewControllerFragment extends Fragment {

    //model
    private STEvent event = null;
    private Integer eventID = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private ImageView photoImageView;
    private Button uploadButton;
    private EditText eventTextFieldView, startTextFieldView, endTextFieldView;
    private ImageButton deleteDateButton;
    private EditText locationTextFieldView, descriptionTextView;
    private Button actionButton;

    //helper
    private String picturePath = "";
    private STEnums.resumeType resumeType = STEnums.resumeType.none;

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (eventID != null) {
            event = DataLoader.getEvent(eventID);
            if (event == null) {
                DataPreserver.saveEvent(eventID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        event = DataLoader.getEvent(eventID) != null ? DataLoader.getEvent(eventID) : new STEvent();
                    }
                });
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_create_event_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        photoImageView = (ImageView) view.findViewById(R.id.photoImageView);
        uploadButton = (Button) view.findViewById(R.id.uploadButton);
        eventTextFieldView = (EditText) view.findViewById(R.id.eventTextFieldView);
        startTextFieldView = (EditText) view.findViewById(R.id.startTextFieldView);
        endTextFieldView = (EditText) view.findViewById(R.id.endTextFieldView);
        deleteDateButton = (ImageButton) view.findViewById(R.id.deleteDateButton);
        locationTextFieldView = (EditText) view.findViewById(R.id.locationTextFieldView);
        descriptionTextView = (EditText) view.findViewById(R.id.descriptionTextView);
        actionButton = (Button) view.findViewById(R.id.actionButton);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText("Event 作成");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        startTextFieldView.setInputType(InputType.TYPE_NULL);
        endTextFieldView.setInputType(InputType.TYPE_NULL);

        setEventData();
        addAction();
    }

    private void setEventData() {
        if (event != null) {
            picturePath = "link";
            photoImageView.setVisibility(View.VISIBLE);
            Picasso.with(getActivity())
                    .load(event.imageURL)
                    .into(photoImageView);
            uploadButton.setText("画像の変更は出来ません");
            uploadButton.setEnabled(false);
            eventTextFieldView.setText(event.title);
            startTextFieldView.setText(event.startDate);
            endTextFieldView.setText(event.finishDate);
            locationTextFieldView.setText(event.location);
            descriptionTextView.setText(event.text);
            actionButton.setBackgroundColor(0xFFEFEFEF);
            actionButton.setTextColor(0xFF9B9B9B);
            actionButton.setEnabled(false);
        }
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                getFragmentManager().popBackStackImmediate();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("入力を終了してよろしいですか？");
                builder.setMessage("現在の編集内容は保存されません").setPositiveButton("OK", dialogClickListener)
                        .setNegativeButton("キャンセル", dialogClickListener).show();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });

        eventTextFieldView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        startTextFieldView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        setStartDate();

        endTextFieldView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
                if (endTextFieldView.getText().length() > 0)
                    deleteDateButton.setVisibility(View.VISIBLE);
                else
                    deleteDateButton.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        setEndDate();

        deleteDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTextFieldView.setText("");
                changePostButtonEnabled();
            }
        });

        locationTextFieldView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        descriptionTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                actionButtonAction();
            }
        });
    }

    private void loadImage() {
        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);
        changePostButtonEnabled();
    }

    private void setStartDate() {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                startTextFieldView.setText(sdf.format(myCalendar.getTime()));
            }
        };


        startTextFieldView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(getActivity()
                            , android.R.style.Widget_DatePicker
                            , date
                            , myCalendar.get(Calendar.YEAR)
                            , myCalendar.get(Calendar.MONTH)
                            , myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                } else {

                }
            }
        });

        startTextFieldView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity()
                        , android.R.style.Widget_DatePicker
                        , date
                        , myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH)
                        , myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void setEndDate() {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                endTextFieldView.setText(sdf.format(myCalendar.getTime()));
            }
        };


        endTextFieldView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(getActivity()
                            , android.R.style.Widget_DatePicker
                            , date
                            , myCalendar.get(Calendar.YEAR)
                            , myCalendar.get(Calendar.MONTH)
                            , myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                } else {

                }
            }
        });
        endTextFieldView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity()
                        , android.R.style.Widget_DatePicker
                        , date
                        , myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH)
                        , myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    private void actionButtonAction() {

        if (!validationForParam()) {
            Toast.makeText(getActivity(), "必須項目を全て入力してください", Toast.LENGTH_SHORT).show();
            return;
        }
        actionButton.setEnabled(false);
        if (event != null) {
            // Patch
            STStructs.StylerPatchEventParam param = createPatchEventParam();
            if (param != null) {
                final ProgressDialog progress;
                progress = ProgressDialog.show(getActivity(), null, null);
                DataSender.patchEvent(param, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        actionButton.setEnabled(true);
                        progress.dismiss();
                        if (err != null) {
                            Log.i("STCreateEventVCF", "err" + err);
                        }
                        if (isSuccess) {
                            resumeType = STEnums.resumeType.patchEvent;
                            STEventDetailViewControllerFragment.setResumeType(STEnums.resumeType.patchEvent);
                            Intent intent = new Intent(getActivity(), WaitActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "データの更新に失敗しました", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                actionButton.setEnabled(true);
                Toast.makeText(getActivity(), "画像のアップロードに失敗しました\\nもう一度お試しください", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            // Create
            STStructs.StylerCreateEventParam param = createParamFromViewValue();
            if (param != null) {
                final ProgressDialog progress;
                progress = ProgressDialog.show(getActivity(), null, null);
                DataSender.createEvent(param, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        actionButton.setEnabled(true);
                        progress.dismiss();
                        if (err != null) {
                            Log.i("STCreateEventVCF", "err" + err);
                        }
                        if (isSuccess) {
                            MainActivity.setResumeType(STEnums.resumeType.createEvent);
                            MainActivity.setTabHostID(0);
                            MainActivity.setResumeCreateEventID(lastObjectID);
                            STHomePageViewControllerFragment.setResumeType(STEnums.resumeType.createEvent);
                            STHomePageViewControllerFragment.setHomeViewPagerID(5);
                            Intent intent = new Intent(getActivity(), WaitActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "Eventの作成に失敗しました\\nもう一度お試しください", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                actionButton.setEnabled(true);
                Toast.makeText(getActivity(), "画像のアップロードに失敗しました\\nもう一度お試しください", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    private STStructs.StylerPatchEventParam createPatchEventParam() {
        if (event == null)
            return null;
        Bitmap bitmap = photoImageView.getDrawingCache();
        File file = new File("image");
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file != null) {
            STStructs.StylerPatchEventParam param = new STStructs().new StylerPatchEventParam();
            param.eventID = event.eventID;
            param.photoData = file;
            param.title = eventTextFieldView.getText().toString();
            param.text = descriptionTextView.getText().toString();
            param.start_date = startTextFieldView.getText().toString();
            param.finish_date = endTextFieldView.getText().toString();
            param.location = locationTextFieldView.getText().toString();

            Log.i("aaaaaaaxxx","aaaaxxx: "+ param.location);

            return param;
        }
        return null;
    }

    private STStructs.StylerCreateEventParam createParamFromViewValue() {
        STStructs.StylerCreateEventParam param = new STStructs().new StylerCreateEventParam();
        File file = new File(picturePath.toString());
        if (file != null) {
            param.photoData = file;
            param.title = eventTextFieldView.getText().toString();
            param.text = descriptionTextView.getText().toString();
            param.start_date = startTextFieldView.getText().toString();
            param.finish_date = endTextFieldView.getText().toString();
            param.location = locationTextFieldView.getText().toString();

            return param;
        }
        return null;
    }

    private void changePostButtonEnabled() {
        if (validationForParam()) {
            actionButton.setEnabled(true);
            actionButton.setBackgroundColor(0xFF28ABEC);
            actionButton.setTextColor(0xFFFFFFFF);
        } else {
            actionButton.setBackgroundColor(0xFFEFEFEF);
            actionButton.setTextColor(0xFF9B9B9B);
            actionButton.setEnabled(false);
        }
    }

    private boolean validationForParam() {
        boolean valid = true;
        if (picturePath == "")
            valid = false;
        if (eventTextFieldView.getText().length() == 0)
            valid = false;
        if (startTextFieldView.getText().length() == 0)
            valid = false;
        if (locationTextFieldView.getText().length() == 0)
            valid = false;
        if (descriptionTextView.getText().length() == 0)
            valid = false;
        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.i("STCreateReplyVC00", "picturePath " + picturePath);
            cursor.close();
            try {
                Picasso.with(getActivity())
                        .load(new File(picturePath))
                        .into(photoImageView);
                changePostButtonEnabled();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STCreateEventVCF", "onResume");
        if (resumeType == STEnums.resumeType.patchEvent) {
            getFragmentManager().popBackStackImmediate();
            resumeType = STEnums.resumeType.none;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STCreateEventVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STCreateEventVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STCreateEventVCF", "onDestroy");
    }
}
