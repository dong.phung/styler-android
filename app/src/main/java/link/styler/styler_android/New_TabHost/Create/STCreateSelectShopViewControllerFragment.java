package link.styler.styler_android.New_TabHost.Create;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.STStructs.STEnums;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.R;

public class STCreateSelectShopViewControllerFragment extends Fragment {

    //model
    String status = STEnums.UserStatus.None.getUserStatus();

    //view
    private LinearLayout linearLayoutFragment;
    private TextView txtCreateReply, txtCreateEvent;
    private LinearLayout createReply, createEvent, layout;
    //helper


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_create_select_shop_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        txtCreateReply = (TextView) view.findViewById(R.id.txtCreateReply);
        txtCreateEvent = (TextView) view.findViewById(R.id.txtCreateEvent);
        createReply = (LinearLayout) view.findViewById(R.id.createReply);
        createEvent = (LinearLayout) view.findViewById(R.id.createEvent);
        layout = (LinearLayout) view.findViewById(R.id.layout);
        if (status.equals("customer")) {
            layout.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addAction();
    }

    private void addAction() {
        createReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STSelectCategoryReplyViewControllerFragment fragment = new STSelectCategoryReplyViewControllerFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STCreateEventViewControllerFragment fragment = new STCreateEventViewControllerFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STCreateSelectShopVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STCreateSelectShopVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STCreateSelectShopVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STCreateSelectShopVCF", "onDestroy");
    }
}
