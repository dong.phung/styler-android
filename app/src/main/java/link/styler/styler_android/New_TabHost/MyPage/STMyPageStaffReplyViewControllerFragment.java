package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageStaffReplyViewControllerFragment extends Fragment {

    //model,API
    private int userID = 0;
    private RealmResults<STItemSort> replies;
    private RealmList<STItem> items = new RealmList<STItem>();
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterItem adapterItem = null;

    public void createAdapterItem() {
        this.replies = DataLoader.getRepliesOfUser(userID);

        this.adapterItem = new MyAdapterItem(getActivity(), replies, STEnums.STItemListCollectionViewType.defaultType);
        adapterItem.setUserID(userID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_staff_reply_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Item);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        STMyPageStaffReplyViewControllerFragment.this.gridView.setAdapter(adapterItem);
        if (adapterItem.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        STMyPageStaffReplyViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageStaffReplyVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageStaffReplyVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageStaffReplyVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageStaffReplyVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageStaffReplyVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageStaffReplyVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveRepliesOfUser(userID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageStaffReplyViewControllerFragment.this.replies = DataLoader.getRepliesOfUser(userID);
                    STMyPageStaffReplyViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STItemSort reply : STMyPageStaffReplyViewControllerFragment.this.replies) {
                        STMyPageStaffReplyViewControllerFragment.this.items.add(reply.item);
                    }

                    if (STMyPageStaffReplyViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageStaffReplyViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageStaffReplyViewControllerFragment.this.gridView.getAdapter();
                    STMyPageStaffReplyViewControllerFragment.this.adapterItem.setItems(STMyPageStaffReplyViewControllerFragment.this.items);
                    STMyPageStaffReplyViewControllerFragment.this.adapterItem.notifyDataSetChanged();

                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageStaffReplyViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveRepliesOfUser(userID, STMyPageStaffReplyViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageStaffReplyViewControllerFragment.this.replies = DataLoader.getRepliesOfUser(userID);
                    STMyPageStaffReplyViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STItemSort reply : STMyPageStaffReplyViewControllerFragment.this.replies) {
                        STMyPageStaffReplyViewControllerFragment.this.items.add(reply.item);
                    }

                    if (STMyPageStaffReplyViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageStaffReplyViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageStaffReplyViewControllerFragment.this.gridView.getAdapter();
                    STMyPageStaffReplyViewControllerFragment.this.adapterItem.setItems(STMyPageStaffReplyViewControllerFragment.this.items);
                    STMyPageStaffReplyViewControllerFragment.this.adapterItem.notifyDataSetChanged();

                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageStaffReplyViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}