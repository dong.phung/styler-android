package link.styler.styler_android.New_TabHost.Message.View;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.styler_android.R;

/**
 * Created by macOS on 3/29/17.
 */

public class STMessageInitialView {
    private LinearLayout layout;
    private TextView titleLabel;
    private TextView descriptionLabels;
    private Activity activity;
    private View view;

    //use Activity
    public STMessageInitialView(Activity activity) {
        this.activity = activity;
        init(this.activity);
    }

    private void init(Activity activity) {
        layout = (LinearLayout) activity.findViewById(R.id.viewSTMessageInitialView);
        titleLabel = (TextView) activity.findViewById(R.id.titleLabel);
        descriptionLabels = (TextView) activity.findViewById(R.id.descriptionLabels);
    }

    //use View
    public STMessageInitialView(View view) {
        this.view = view;
        init(this.view);
    }

    private void init(View view) {
        layout = (LinearLayout) view.findViewById(R.id.viewSTMessageInitialView);
        titleLabel = (TextView) view.findViewById(R.id.titleLabel);
        descriptionLabels = (TextView) view.findViewById(R.id.descriptionLabels);
    }

    public void initView(String title, String[] descriptions) {
        titleLabel.setText(title);
        String s = "";
        for (int i = 0; i < descriptions.length; i++) {
            s = s + descriptions[i] + "\n";
        }
        descriptionLabels.setText(s);
    }
}
