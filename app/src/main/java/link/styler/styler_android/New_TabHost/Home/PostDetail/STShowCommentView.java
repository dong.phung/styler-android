package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.models.STPost;
import link.styler.styler_android.New_TabHost.Home.PostDetail.STPostCommentViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/10/17.
 */

public class STShowCommentView {

    //model
    STPost post;

    //view
    ImageView commentImageView;
    TextView titleLabel;
    TextView countLabel;
    ImageView rightArrorImageView;
    LinearLayout linComment;

    //helper
    private Context context;
    private Activity activity;
    private FragmentActivity fragmentActivity;
    private View view;

    //use activity
    public STShowCommentView(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        initView(activity);
        addControls();
    }

    private void initView(Activity activity) {
        commentImageView = (ImageView) activity.findViewById(R.id.commentImageView);
        titleLabel = (TextView) activity.findViewById(R.id.titleLabel);
        countLabel = (TextView) activity.findViewById(R.id.countLabel);
        rightArrorImageView = (ImageView) activity.findViewById(R.id.rightArrorImageView);
        linComment = (LinearLayout) activity.findViewById(R.id.linComment);
    }

    //use view
    public STShowCommentView(View view, FragmentActivity fragmentActivity) {
        this.view = view;
        this.context = view.getContext();
        this.fragmentActivity = fragmentActivity;
        initView(view);
        addControls();
    }

    private void initView(View view) {
        commentImageView = (ImageView) view.findViewById(R.id.commentImageView);
        titleLabel = (TextView) view.findViewById(R.id.titleLabel);
        countLabel = (TextView) view.findViewById(R.id.countLabel);
        rightArrorImageView = (ImageView) view.findViewById(R.id.rightArrorImageView);
        linComment = (LinearLayout) view.findViewById(R.id.linComment);
    }

    private void addControls() {
        linComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STPostCommentViewControllerFragment fragment = new STPostCommentViewControllerFragment();
                fragment.setPostID(post.postID);
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    public void configData(STPost post) {
        if (post == null || post.postID == 0)
            return;
        this.post = post;
        countLabel.setText(post.commentsCount + " 件");
    }
}
