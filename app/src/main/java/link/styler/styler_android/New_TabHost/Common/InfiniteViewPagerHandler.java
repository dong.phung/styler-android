package link.styler.styler_android.New_TabHost.Common;

import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;

import link.styler.Utils.Logger;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;

/**
 * Created by dongphung on 5/3/17.
 */

public class InfiniteViewPagerHandler implements ViewPager.OnPageChangeListener {
    private int previousState = ViewPager.SCROLL_STATE_IDLE;

    protected ViewPager mViewPager; // TODO: create weak reference for it
    protected int mNumOfPages;
    private  int previousPage = -1;

    public InfiniteViewPagerHandler(final ViewPager viewPager, int numOfPages) {
        mViewPager = viewPager;
        mNumOfPages = numOfPages;
        previousPage = viewPager.getCurrentItem();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        /*
        Log.d("GOND", "onPageSrolled() : position: " + position + ", posOffset: " + positionOffset + ", posOffsetPixels: " + positionOffsetPixels);
        ViewPagerAdapter pagerAdapter = ((ViewPagerAdapter)mViewPager.getAdapter());
        FragmentManager fragMgr = ((ViewPagerAdapter)mViewPager.getAdapter()).getItem(position).getFragmentManager();
        if (pagerAdapter == null)
            return;

        //if (positionOffsetPixels == 0) {
            if (position == 2) {
                //Bundle saveState = new Bundle();
                SparseArray<Parcelable> container = new SparseArray<>();
                pagerAdapter.getItem(position).getView().saveHierarchyState(container); //onSaveInstanceState(saveState);
                pagerAdapter.getItem(mNumOfPages + 2).getView().restoreHierarchyState(container); //restoreViewState(saveState);
                //fragMgr.
                Log.d("GOND", "onPageScrolled > sync from 2 to " + (mNumOfPages + 2));
            } else if (position == mNumOfPages + 1) {
                //Bundle saveState = new Bundle();
                SparseArray<Parcelable> container = new SparseArray<>();
                pagerAdapter.getItem(position).getView().saveHierarchyState(container); //onSaveInstanceState(saveState);
                pagerAdapter.getItem(1).getView().restoreHierarchyState(container); //onViewStateRestored(saveState);
                Log.d("GOND", "onPageScrolled > sync from " + (mNumOfPages + 1) + " to 1");
            }
        //}
        */
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("GOND", "onPageSelected() : position: " + position);
        if(mViewPager == null || mViewPager.getAdapter() == null)
            return;
        ViewPagerAdapter pagerAdapter = ((ViewPagerAdapter)mViewPager.getAdapter());

        if (previousPage == 2) {
            //Bundle saveState = new Bundle();
            SparseArray<Parcelable> container = new SparseArray<>();
            pagerAdapter.getItem(2).getView().saveHierarchyState(container);
            pagerAdapter.getItem(mNumOfPages + 2).getView().restoreHierarchyState(container);
            //fragMgr.
            Log.d("GOND", "onPageScrolled > sync from 2 to " + (mNumOfPages + 2));
        } else if (previousPage == mNumOfPages + 1) {
            //Bundle saveState = new Bundle();
            SparseArray<Parcelable> container = new SparseArray<>();
            pagerAdapter.getItem(mNumOfPages + 1).getView().saveHierarchyState(container); //onSaveInstanceState(saveState);
            pagerAdapter.getItem(1).getView().restoreHierarchyState(container); //onViewStateRestored(saveState);
            Log.d("GOND", "onPageScrolled > sync from " + (mNumOfPages + 1) + " to 1");
        }
        previousPage = position;
    }


    @Override
    public void onPageScrollStateChanged(int state) {

//                if(Looper.myLooper() == Looper.getMainLooper()){
//                    Logger.print("Main Thread onPageScrollStateChanged ... .... ...");
//                }

        if(mViewPager == null || mViewPager.getAdapter() == null)
            return;


        int currentPage = mViewPager.getCurrentItem();
        Logger.print("onPageScrollStateChanged: current state: " + state + ", prev state: " + previousState + ", current page: " + currentPage);

        if (state == ViewPager.SCROLL_STATE_IDLE) { // && previousState == ViewPager.SCROLL_STATE_DRAGGING) {
            if (currentPage <= 1) {
                mViewPager.setCurrentItem(currentPage + mNumOfPages, false);
            }else if (currentPage >= (mNumOfPages + 2)) {
                mViewPager.setCurrentItem(currentPage - mNumOfPages, false);
            }
        }
        previousState = state;
        //Log.i("Tan012", "homeViewPagerID " + homeViewPagerID);
    }
}
