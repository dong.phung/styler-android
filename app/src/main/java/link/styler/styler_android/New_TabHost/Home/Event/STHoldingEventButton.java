package link.styler.styler_android.New_TabHost.Home.Event;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.styler_android.R;

/**
 * Created by macOS on 4/18/17.
 */

public class STHoldingEventButton {

    //model

    //view
    public TextView titleLabel;
    public TextView countLabel;
    public ImageView rightArrorImageView;
    public LinearLayout holdingEventButton;

    //helper
    private Context context;
    private Activity activity;
    private View view;

    public STHoldingEventButton(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        initView(activity);
        addControls();
    }

    private void initView(Activity activity) {
        titleLabel = (TextView) activity.findViewById(R.id.titleLabel1);
        countLabel = (TextView) activity.findViewById(R.id.countLabel1);
        rightArrorImageView = (ImageView) activity.findViewById(R.id.buttonRight1);
        holdingEventButton = (LinearLayout) activity.findViewById(R.id.holdingEventButton);
    }

    public STHoldingEventButton(View view) {
        this.view = view;
        this.context = view.getContext();
        initView(view);
        addControls();
    }

    private void initView(View view) {
        titleLabel = (TextView) view.findViewById(R.id.titleLabel1);
        countLabel = (TextView) view.findViewById(R.id.countLabel1);
        rightArrorImageView = (ImageView) view.findViewById(R.id.buttonRight1);
        holdingEventButton = (LinearLayout) view.findViewById(R.id.holdingEventButton);
    }

    private void addControls() {

    }

    public void configData(int count, String unit) {
        if (unit.isEmpty())
            unit = "件";
        countLabel.setText(count + " " + unit);
    }
}