package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STItem;
import link.styler.models.STReply;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/25/17.
 */

public class STReplyItemDetailView {

    private STItem item = new STItem();
    private STReply reply = new STReply();

    private ImageView mainImageView;
    private List<String> replyImageViews;
    private ImageView replyImageView;
    private TextView brandLabel;
    private ImageButton likeButton;
    private TextView productionNameLabel;
    private TextView likeCountLabel;
    private TextView likeLabel;
    private TextView priceLabel;
    private TextView tagLabel;
    private ImageButton shareButton;

    private Context context;
    private Activity activity = null;
    private View view;
    private FragmentActivity fragmentActivity;
    private boolean likeOn;

    //use Activity
    public STReplyItemDetailView(Activity activity) {
        initSTReplyItemDetailView(activity);
        this.activity = activity;
        this.context = activity.getApplicationContext();
        addcontrols();
    }

    private void initSTReplyItemDetailView(Activity activity) {
        mainImageView = (ImageView) activity.findViewById(R.id.mainImageView);
        replyImageView = (ImageView) activity.findViewById(R.id.replyImageView);

        brandLabel = (TextView) activity.findViewById(R.id.brandLabel);
        likeButton = (ImageButton) activity.findViewById(R.id.likeButton);
        productionNameLabel = (TextView) activity.findViewById(R.id.productionReplyItemLabel);
        likeCountLabel = (TextView) activity.findViewById(R.id.likeCountLabel);
        likeLabel = (TextView) activity.findViewById(R.id.likeLabel);
        priceLabel = (TextView) activity.findViewById(R.id.priceRepleItemLable);
        tagLabel = (TextView) activity.findViewById(R.id.tagLabel);
        shareButton = (ImageButton) activity.findViewById(R.id.shareButton);
    }

    //use View
    public STReplyItemDetailView(View view, FragmentActivity fragmentActivity) {
        initSTReplyItemDetailView(view);
        this.view = view;
        this.fragmentActivity = fragmentActivity;
        this.context = view.getContext();
        addcontrols();
    }

    private void initSTReplyItemDetailView(View view) {
        mainImageView = (ImageView) view.findViewById(R.id.mainImageView);
        replyImageView = (ImageView) view.findViewById(R.id.replyImageView);

        brandLabel = (TextView) view.findViewById(R.id.brandLabel);
        likeButton = (ImageButton) view.findViewById(R.id.likeButton);
        productionNameLabel = (TextView) view.findViewById(R.id.productionReplyItemLabel);
        likeCountLabel = (TextView) view.findViewById(R.id.likeCountLabel);
        likeLabel = (TextView) view.findViewById(R.id.likeLabel);
        priceLabel = (TextView) view.findViewById(R.id.priceRepleItemLable);
        tagLabel = (TextView) view.findViewById(R.id.tagLabel);
        shareButton = (ImageButton) view.findViewById(R.id.shareButton);
    }

    //

    private void addcontrols() {
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("STReplyItemDetailView", "click like");
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    if (activity != null) {
                        PopUpLogin login = new PopUpLogin(activity);
                        login.show();
                    } else {
                        PopUpLogin login = new PopUpLogin(context);
                        login.show();
                    }
                } else {
                    likeAction();
                }
            }
        });
        mainImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STReplyImageDetailViewControllerFragment fragment = new STReplyImageDetailViewControllerFragment();
                fragment.setUrl(item.mainImageURL);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        replyImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STReplyImageDetailViewControllerFragment fragment = new STReplyImageDetailViewControllerFragment();
                fragment.setUrl(item.mainImageURL);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAction();
                Log.i("STReplyItemDetailView", "click share");
            }
        });
    }

    private void likeAction() {
        if (likeOn) {
            Integer replyID = null;
            if (reply != null)
                replyID = reply.replyID == 0 ? null : reply.replyID;
            likeButton.setEnabled(false);
            DataSender.createLike(item.itemID, replyID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    likeButton.setEnabled(true);
                    if (err != null) {
                        return;
                    }
                    if (isSuccess) {
                        likeOn = !likeOn;
                        likeButton.setBackgroundResource(R.drawable.button_liked2x);
                        int lc = Integer.parseInt(likeCountLabel.getText().toString());
                        likeCountLabel.setText((lc + 1) + "");
                    }
                }
            });
        } else {
            likeButton.setEnabled(false);
            DataSender.deleteLike(item.itemID, item.likeID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    likeButton.setEnabled(true);
                    if (err != null) {
                        return;
                    }
                    if (isSuccess) {
                        likeOn = !likeOn;
                        likeButton.setBackgroundResource(R.drawable.button_like2x);
                        int lc = Integer.parseInt(likeCountLabel.getText().toString());
                        likeCountLabel.setText((lc - 1) + "");
                    }
                }
            });
        }
    }

    private void shareAction() {

    }

    public void setUpSTReplyItemDetailView(STItem item, STReply reply) {
        this.item = item;
        this.reply = reply;

        Log.i("STReplyItemDetailView", "item " + this.item.itemID);
        if (reply != null)
            Log.i("STReplyItemDetailView", "reply " + this.reply.replyID);
        else
            Log.i("STReplyItemDetailView", "reply null");

        likeOn = item.likeID == 0 ? true : false;
        String mainImageURL = this.item.mainImageURL;

        if (!mainImageURL.isEmpty()) {
            Picasso.with(context)
                    .load(mainImageURL)
                    //.transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(mainImageView);
            Picasso.with(context)
                    .load(mainImageURL)
                    //.transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(replyImageView);
        }

        brandLabel.setText(this.item.brand);
        if (this.item.likeID != 0) {
            likeButton.setBackgroundResource(R.drawable.button_liked2x);
        } else {
            likeButton.setBackgroundResource(R.drawable.button_like2x);
        }
        int myShopID = 0;
        try {
            myShopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
        }
        if (item.shopID == myShopID) {
            likeButton.setVisibility(View.INVISIBLE);
        } else {
            likeButton.setVisibility(View.VISIBLE);
        }

        productionNameLabel.setText(this.item.name);
        likeCountLabel.setText("" + this.item.likeCount);

        if (!this.item.price.isEmpty()) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("¥ " + decimalFormat.format(Integer.parseInt(item.price)) + "(税込)");
        } else
            priceLabel.setText("");

        if (this.item.category != null) {
            tagLabel.setVisibility(View.VISIBLE);
            if (this.item.category.name.contains("カテゴリーを選択しない")) {
                tagLabel.setText("その他");
            } else
                tagLabel.setText(this.item.category.name);
        } else
            tagLabel.setVisibility(View.GONE);
    }

    public ImageButton getShareButton() {
        return shareButton;
    }
}
