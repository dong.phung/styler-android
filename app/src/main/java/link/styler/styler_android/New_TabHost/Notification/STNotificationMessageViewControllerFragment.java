package link.styler.styler_android.New_TabHost.Notification;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.models.STConversation;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterConversation;
import link.styler.styler_android.R;


public class STNotificationMessageViewControllerFragment extends Fragment {

    //model
    private RealmResults<STConversation> stConversations;
    private RealmList<STConversation> conversations;
    private Integer lastObjectID;

    //view
    private LinearLayout linearLayoutFragment;
    private PullRefreshLayout pullRefreshLayout;
    private ListView listConversation;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterConversation adapter = null;

    private void createAdapter() {
        stConversations = DataLoader.getConversations();
        conversations = new RealmList<>();
        conversations.addAll(stConversations);
        adapter = new MyAdapterConversation(getActivity(), conversations);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapter == null) {
            createAdapter();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_notification_message_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_message);
        listConversation = (ListView) view.findViewById(R.id.list_conversation);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        listConversation.setAdapter(adapter);
        //if (adapter.getCount() == 0) {
        progressBar.setVisibility(View.VISIBLE);
        getNewData();
        //}

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        listConversation.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STNotiActivityVCF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STNotiActivityVCF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STNotiActivityVCF", "totalItemCount: " + totalItemCount);
                    Log.i("STNotiActivityVCF", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveConversation(null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stConversations = DataLoader.getConversations();
                    conversations = new RealmList<STConversation>();
                    conversations.addAll(stConversations);
                    if (STNotificationMessageViewControllerFragment.this.adapter == null)
                        STNotificationMessageViewControllerFragment.this.createAdapter();
                    STNotificationMessageViewControllerFragment.this.adapter.setConversations(conversations);
                    STNotificationMessageViewControllerFragment.this.adapter.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STNotificationMessageViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveConversation(lastObjectID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stConversations = DataLoader.getConversations();
                    conversations = new RealmList<STConversation>();
                    conversations.addAll(stConversations);
                    if (STNotificationMessageViewControllerFragment.this.adapter == null)
                        STNotificationMessageViewControllerFragment.this.createAdapter();
                    STNotificationMessageViewControllerFragment.this.adapter.setConversations(conversations);
                    STNotificationMessageViewControllerFragment.this.adapter.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STNotificationMessageViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STNotiMessageVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STNotiMessageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STNotiMessageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STNotiMessageVCF", "onDestroy");
    }
}

