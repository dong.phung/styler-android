package link.styler.styler_android.New_TabHost.Home;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STPost;
import link.styler.models.STPostSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterPosts;
import link.styler.styler_android.R;


public class STPopularPostFragment extends STBaseFragment {

    //model,api
    private RealmResults<STPostSort> postSorts;
    private RealmList<STPost> posts = new RealmList<>();
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private MyAdapterPosts adapterPosts = null;
    private boolean gotData = false;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;

    public static void setResumeType(STEnums.resumeType resumeType) {
        STPopularPostFragment.resumeType = resumeType;
    }

    public void createAdapterPosts() {
        this.postSorts = DataLoader.getPopularPosts();
        this.posts = new RealmList<STPost>();
        for (STPostSort postSort : postSorts) {
            this.posts.add(postSort.post);
        }
        this.adapterPosts = new MyAdapterPosts(getActivity(), this.posts);
    }
    //end

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        referenceMyself(this);
        if (adapterPosts == null)
            createAdapterPosts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_popular_post_layout, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_post);
        gridView = (GridView) view.findViewById(R.id.gridView);

        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        gridView.setAdapter(adapterPosts);

        if (adapterPosts.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        final STPopularPostFragment self = (STPopularPostFragment) weakSelf.get();
        DataPreserver.savePosts(null, STEnums.PostSortType.Popular.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.postSorts = DataLoader.getPopularPosts();

                    self.posts = new RealmList<STPost>();
                    for (STPostSort postSort : self.postSorts) {
                        self.posts.add(postSort.post);
                    }

                    if (self.adapterPosts == null)
                        createAdapterPosts();

                    self.adapterPosts = (MyAdapterPosts) self.gridView.getAdapter();
                    self.adapterPosts.setPosts(self.posts);
                    self.adapterPosts.notifyDataSetChanged();
                }

                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        final STPopularPostFragment self = (STPopularPostFragment) weakSelf.get();
        DataPreserver.savePosts(self.lastObjectID, STEnums.PostSortType.Popular.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.postSorts = DataLoader.getPopularPosts();

                    self.posts = new RealmList<STPost>();
                    for (STPostSort postSort : self.postSorts) {
                        self.posts.add(postSort.post);
                    }

                    if (self.adapterPosts == null)
                        createAdapterPosts();

                    self.adapterPosts = (MyAdapterPosts) self.gridView.getAdapter();
                    self.adapterPosts.setPosts(self.posts);
                    self.adapterPosts.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (resumeType == STEnums.resumeType.createReply) {
            getNewData();
            resumeType = STEnums.resumeType.none;
        }
        Log.i("STPopularPostF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STPopularPostF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STPopularPostF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STPopularPostF", "onDestroy");
    }
}
