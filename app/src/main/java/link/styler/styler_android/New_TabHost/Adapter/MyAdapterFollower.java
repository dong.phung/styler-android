package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.Message.STMessageViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/13/17.
 */

public class MyAdapterFollower extends BaseAdapter {

    //model
    private RealmList<STUser> followers;
    private STUser follower;

    //view
    private ImageView avatarImageView;
    private STShopStatusView statusView;
    private TextView nameLabel;
    private TextView userStatusLabel;
    private LinearLayout linearLayout;


    //hepler
    private FragmentActivity fragmentActivity;

    public MyAdapterFollower(FragmentActivity fragmentActivity, RealmList<STUser> followers) {
        this.fragmentActivity = fragmentActivity;
        this.followers = followers;
    }
    public void setFollowers(RealmList<STUser> followers) {
        this.followers = followers;
    }

    @Override
    public int getCount() {
        return followers.size();
    }

    @Override
    public Object getItem(int position) {
        return followers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return followers.get(position).userID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_follower_cell, null);
        intView(convertView);
        //}
        STUser follower = followers.get(position);
        setupView(follower);
        addControl(follower);
        return convertView;
    }

    private void intView(View view) {
        avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);
        statusView = new STShopStatusView(view);
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        userStatusLabel = (TextView) view.findViewById(R.id.userStatusLabel);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
    }

    private void setupView(STUser follower) {
        String urlImage = follower.imageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }
        if (!status.isEmpty()) {
            if (status.equals("staff")) {
                statusView.getActionButton().setText("メッセージする");
                statusView.getActionButton().setTextColor(0xFF28ABEC);
                statusView.getActionButton().setEnabled(true);
            } else {
                statusView.getActionButton().setText("詳細を見る");
                statusView.getActionButton().setEnabled(false);
            }
        } else {
            statusView.getActionButton().setText("詳細を見る");
            statusView.getActionButton().setEnabled(false);
        }

        statusView.configData(follower);
        nameLabel.setText(follower.name);
        userStatusLabel.setText(follower.apiDetail);
    }

    private void addControl(final STUser follower) {
        statusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                STMessageViewControllerFragment fragment = new STMessageViewControllerFragment();
                fragment.setDestinationUserID(follower.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(follower.userID);
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
