package link.styler.styler_android.New_TabHost.Article;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STTag;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Article.Adapter.MyAdapterTag;
import link.styler.styler_android.R;

public class STArticleTagListViewControllerFragment extends Fragment {

    //model
    private RealmList<STTag> tags = new RealmList<>();
    private Integer lastObjectID = null;

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController navigation;
    private PullRefreshLayout pullRefreshLayout;
    private ListView lvKeyword;
    private ProgressBar progressBar;

    //helper
    private MyAdapterTag adapterTag = null;
    private boolean isLoading = false;
    private boolean gotData = false;

    private void createTagAdapter() {
        tags = new RealmList<>();
        tags.addAll(DataLoader.getTags());
        adapterTag = new MyAdapterTag(getActivity(), tags);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterTag == null) {
            createTagAdapter();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_article_tag_list_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefresh_TagList);
        lvKeyword = (ListView) view.findViewById(R.id.lvKeyword);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText("キーワード一覧");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        if (adapterTag == null) {
            createTagAdapter();
        }
        lvKeyword.setAdapter(adapterTag);
        if (tags.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        lvKeyword.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });

        lvKeyword.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                STArticleNewListViewControllerFragment fragment = new STArticleNewListViewControllerFragment();
                fragment.setTypeList(STEnums.STArticleListType.tag);
                fragment.setNameList(tags.get(position).name);
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveTags(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    tags = new RealmList<STTag>();
                    tags.addAll(DataLoader.getTags());

                    adapterTag.setTags(tags);
                    adapterTag.notifyDataSetChanged();
                }
                isLoading = false;
                if (lastObjectID != null && lastObjectID != 0)
                    STArticleTagListViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveTags(lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    tags = new RealmList<STTag>();
                    tags.addAll(DataLoader.getTags());

                    adapterTag.setTags(tags);
                    adapterTag.notifyDataSetChanged();
                }
                isLoading = false;
                if (lastObjectID != null && lastObjectID != 0)
                    STArticleTagListViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STArticleTagListVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STArticleTagListVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STArticleTagListVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STArticleTagListVCF", "onDestroy");
    }
}
