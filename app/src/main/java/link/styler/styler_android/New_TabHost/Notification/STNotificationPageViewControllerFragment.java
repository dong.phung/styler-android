package link.styler.styler_android.New_TabHost.Notification;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STNotificationPageViewControllerFragment extends Fragment {

    //model


    //view
    private LinearLayout linearLayoutFragment;
    private ViewPager viewPager;
    private STBaseNavigationController navigation;

    //helper


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_notification_page_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getBackButton().setEnabled(false);
        navigation.getTitleNavigation().setText("お知らせ");
        navigation.getSubTileNavigation().setVisibility(View.GONE);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        STNotificationActivityViewControllerFragment activityFragment = new STNotificationActivityViewControllerFragment();
        adapter.addFrag(activityFragment, "アクティビティ");

        STNotificationMessageViewControllerFragment messageFragment = new STNotificationMessageViewControllerFragment();
        adapter.addFrag(messageFragment, "メッセージ");

        viewPager.setAdapter(adapter);

        addAction();
    }

    private void addAction() {

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STNotiPageVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STNotiPageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STNotiPageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STNotiPageVCF", "onDestroy");
    }
}
