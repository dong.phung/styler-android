package link.styler.styler_android.New_TabHost.Common;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.styler_android.R;

/**
 * Created by admin on 2/25/17.
 */

public class STBaseNavigationController {

    public LinearLayout view;
    public TextView titleNavigation;
    public TextView subTileNavigation;
    public ImageButton backButton;
    public TextView updateButton;
    public ImageButton moreButton;

    private LinearLayout linearLayout;

    public STBaseNavigationController(final Activity activity) {
        initSTBaseNavigatoinController(activity);
    }

    private void initSTBaseNavigatoinController(Activity activity) {
        this.view = (LinearLayout) activity.findViewById(R.id.view);
        this.titleNavigation = (TextView) activity.findViewById(R.id.titleNavigation);
        this.subTileNavigation = (TextView) activity.findViewById(R.id.subTitleNavigation);
        this.backButton = (ImageButton) activity.findViewById(R.id.backButton);
        this.updateButton = (TextView) activity.findViewById(R.id.updateButton);
        this.moreButton = (ImageButton) activity.findViewById(R.id.moreButton);
    }

    public STBaseNavigationController(final View view) {
        Log.i("Quang", "STBaseNavigationController view");
        initSTBaseNavigatoinController(view);
    }

    private void initSTBaseNavigatoinController(View view) {
        this.view = (LinearLayout) view.findViewById(R.id.view);
        this.titleNavigation = (TextView) view.findViewById(R.id.titleNavigation);
        this.subTileNavigation = (TextView) view.findViewById(R.id.subTitleNavigation);
        this.backButton = (ImageButton) view.findViewById(R.id.backButton);
        this.updateButton = (TextView) view.findViewById(R.id.updateButton);
        this.moreButton = (ImageButton) view.findViewById(R.id.moreButton);
        this.linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
    }

    public LinearLayout getView() {
        return view;
    }

    public TextView getTitleNavigation() {
        return titleNavigation;
    }

    public TextView getSubTileNavigation() {
        return subTileNavigation;
    }

    public ImageButton getBackButton() {
        return backButton;
    }

    public TextView getUpdateButton() {
        return updateButton;
    }

    public ImageButton getMoreButton() {
        return moreButton;
    }
}
