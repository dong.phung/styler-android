package link.styler.styler_android.New_TabHost.MyPage.Account;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STLoginInfoViewControllerFragment extends Fragment {

    //model

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController navigation;
    private EditText emailTextField, currentPasswordTextField, newPasswordTextField, confirmPasswordTextField;

    //helper
    private String email;
    private boolean isloading = false;

    private enum mode {
        email,
        password;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            email = LoginManager.getsIntstance().getUserEmail();
        } catch (Exception e) {
            email = "";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_login_info_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        emailTextField = (EditText) view.findViewById(R.id.emailTextField);
        currentPasswordTextField = (EditText) view.findViewById(R.id.currentPasswordTextField);
        newPasswordTextField = (EditText) view.findViewById(R.id.newPasswordTextField);
        confirmPasswordTextField = (EditText) view.findViewById(R.id.confirmPasswordTextField);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText("ログイン情報");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getUpdateButton().setText("更新");
        navigation.getUpdateButton().setTextColor(0xFF28ABEC);

        emailTextField.setText(email);

        addAction();
    }

    private void addAction() {
        navigation.getUpdateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (checkMode()) {
                    case email:
                        changeEmailAPI();
                        break;
                    case password:
                        changePasswordAPI();
                        break;
                    default:
                        break;
                }
                hideKeyboard();
            }
        });
    }

    private mode checkMode() {
        String newEmail = emailTextField.getText().toString();
        String currentPassword = currentPasswordTextField.getText().toString();
        String newPassword = newPasswordTextField.getText().toString();
        String confirmPassword = confirmPasswordTextField.getText().toString();
        if (!newEmail.equals(email)) {
            return mode.email;
        } else if (currentPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()) {
            return mode.email;
        } else {
            return mode.password;
        }
    }

    private void changeEmailAPI() {
        Log.i("STLoginInfoVCF", "changeEmailAPI");
        final String newEmail = emailTextField.getText().toString();
        if (newEmail.isEmpty()) {
            Toast.makeText(getActivity(), "メールアドレスを入力してください", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isEmailValid(newEmail)) {
            if (isloading)
                return;
            isloading = true;
            DataSender.sendEmail(newEmail, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (isSuccess) {
                        email = newEmail;
                        Toast.makeText(getActivity(), "メールアドレスを変更しました", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "メールアドレスの形式が不正です", Toast.LENGTH_SHORT).show();
                    }
                    isloading = false;
                }
            });
        } else {
            Toast.makeText(getActivity(), "メールアドレスの形式が不正です", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void changePasswordAPI() {
        Log.i("STLoginInfoVCF", "changePasswordAPI");
        String currentPassword = currentPasswordTextField.getText().toString();
        String newPassword = newPasswordTextField.getText().toString();
        String confirmPassword = confirmPasswordTextField.getText().toString();
        if (currentPassword.length() < 8 || newPassword.length() < 8 || confirmPassword.length() < 8) {
            Toast.makeText(getActivity(), "パスワードは8文字以上入力してください", Toast.LENGTH_SHORT).show();
        } else {
            if (isloading)
                return;
            isloading = true;
            DataSender.sendPassWord(currentPassword, newPassword, confirmPassword, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (isSuccess) {
                        Toast.makeText(getActivity(), "パスワードを更新しました", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "パスワードの更新に失敗しました", Toast.LENGTH_SHORT).show();
                    }
                    isloading = false;
                }
            });
        }
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STLoginInfoVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STLoginInfoVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STLoginInfoVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STLoginInfoVCF", "onDestroy");
    }
}
