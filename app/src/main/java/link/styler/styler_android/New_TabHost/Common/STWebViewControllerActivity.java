package link.styler.styler_android.New_TabHost.Common;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import link.styler.styler_android.R;

public class STWebViewControllerActivity extends AppCompatActivity {

    private String defaultURL = "";
    private String defaultTitle = "";
    private STBaseNavigationController navigation;
    private WebView webView;
    private boolean shouldSetNavi = true;
    private boolean shouldSetBackItem = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_web_view_controller);
        try {
            Intent intent = getIntent();
            defaultTitle = intent.getStringExtra("title");
            defaultURL = intent.getStringExtra("url");
        } catch (Exception e) {

        }
        viewDidLoad();
        addControls();
    }

    private void addControls() {
        navigation.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void viewDidLoad() {
        if (shouldSetNavi) {
            // TODO: 3/23/17
        } else {
            //// TODO: 3/23/17
        }
        setupView();
        loadWebView();
    }

    private void setupView() {
        navigation = new STBaseNavigationController(this);
        navigation.backButton.setImageResource(R.drawable.button_previous);
        navigation.titleNavigation.setText(defaultTitle);
        navigation.subTileNavigation.setVisibility(View.GONE);
        webView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void loadWebView() {
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(defaultURL);
    }
}
