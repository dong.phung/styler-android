package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 5/11/17.
 */

public class MyAdapterEventGoing extends BaseAdapter {

    //model
    private RealmList<STUser> users;
    private STUser user;

    //view
    private ImageView userImageView;
    private STShopStatusView statusView;
    private TextView nameLabel;
    private TextView userInfoLabel;
    private TextView descriptionLabel;
    private LinearLayout linearLayout;


    //hepler
    private Context context;
    private FragmentActivity fragmentActivity;

    public MyAdapterEventGoing(FragmentActivity fragmentActivity, RealmList<STUser> followers) {
        this.fragmentActivity = fragmentActivity;
        this.context = fragmentActivity.getApplicationContext();
        this.users = followers;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).userID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_event_going_cell, null);
        intView(convertView);

        STUser user = users.get(position);
        setupView(user);
        addControl(user);
        return convertView;
    }

    private void intView(View view) {
        userImageView = (ImageView) view.findViewById(R.id.userImageView);
        statusView = new STShopStatusView(view);
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) view.findViewById(R.id.userInfoLabel);
        descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
    }

    private void setupView(STUser user) {
        if (user == null)
            return;
        if (user.userID == 0)
            return;
        String urlImage = user.imageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(context)
                    .load(urlImage)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(userImageView);
        }
        statusView.getActionButton().setText("ユーザー詳細を見る");
        statusView.configData(user);
        nameLabel.setText(user.name);
        userInfoLabel.setText(user.apiDetail);
        descriptionLabel.setText(user.profile);
    }

    private void addControl(final STUser user) {
        if (user == null)
            return;
        if (user.userID == 0)
            return;
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(user.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        statusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(user.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
