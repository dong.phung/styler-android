package link.styler.styler_android.New_TabHost;

import java.lang.Package;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;

import link.styler.styler_android.BuildConfig;

/**
 * Created by dongphung on 1/4/17.
 */

public class Constants {
    /* For DEBUG build */

    private static String kServerProtocol  = "https";
    //private static String kServerHost      = "styler:676d1df9@staging.styler.link";
    private static String kServerHost      = "staging.styler.link";

    public static String kusername = "styler";
    public static String kpassword = "676d1df9";

    String kPushServerRegistURL     = "https://push.styler.link/push/regist_pro.php";

    private static boolean kUseApiLog      = false;
    private static boolean kUseDbLog       = false;
    private static boolean kUseDebugLog    = false;
    private static boolean kUseFatalLog    = false;
    private static boolean kUseFMDBLog     = false;
    private static String kDeviceTokenKey  = "deviceToken";
    private static String kTrackingId      = "UA-60235835-3";
    private static String kOpenWindowKey   = "openWindow";
    private static String kOpenTabKey      = "openTab";
    private static String kOpenPageKey     = "openPage";

    private static String kAdjustAppToken    = "5exasi7loww0";
    private static String kAdjustEnvironment = AdjustConfig.ENVIRONMENT_SANDBOX;

    static String kReproAppToken  = "4a48063d-1ee4-483d-8eaa-a6def2eb54a1";

    /* End For DEBUG build */

    /* For RELEASE build */
/*
    private static String kServerProtocol  = "https";
    private static String kServerHost      = "styler.link";

    static String kPushServerRegistURL     = "https://push.styler.link/push/regist_pro.php";

    private static boolean kUseApiLog      = false;
    private static boolean kUseDbLog       = false;
    private static boolean kUseDebugLog    = false;
    private static boolean kUseFatalLog    = false;
    private static boolean kUseFMDBLog     = false;
    private static String kDeviceTokenKey  = "deviceToken";
    private static String kTrackingId      = "UA-60235835-3";
    private static String kOpenWindowKey   = "openWindow";
    private static String kOpenTabKey      = "openTab";
    private static String kOpenPageKey     = "openPage";

    private static String kAdjustAppToken    = "5exasi7loww0";
    private static String kAdjustEnvironment = AdjustConfig.ENVIRONMENT_PRODUCTION;
    private static String kIsNotFirstLaunch  = "isNotFirstLaunch";

    static String kReproAppToken  = "4a48063d-1ee4-483d-8eaa-a6def2eb54a1";
*/
    /* End For RELEASE build */

    static String kNotificationTimerObserver = "kNotificationTimerObserver";
    static String kCFBundleShortVersionString = "kCFBundleShortVersionString";
    static String kCFBundleVersion = "kCFBundleVersion";
    static String kPushNotification = "kPushNotification";
    String kPushMailNotification = "kPushMailNotification";
    String kMailMagazine = "kMailMagazine";

    /* Implementation */
    // MARK: App Version

    public static String appVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public static String buildVersion() {
        return BuildConfig.VERSION_NAME;
    }

    // MARK: Device Token Key
    public static String deviceTokenKey() {
        return kDeviceTokenKey;
    }

    // MARK: App Token

    public static String reproAppToken() {
        return kReproAppToken;
    }

    public static String adjustAppToken() {
        return kAdjustAppToken;
    }

    public static String adjustEnvironment() {
        return kAdjustEnvironment;
    }

    // MARK: NSUserDefaultKey
    public static String openWindow() {
        return kOpenWindowKey;
    }

    public static String openTab() {
        return kOpenTabKey;
    }

    public static String openPage() {
        return kOpenPageKey;
    }

    // MARK: Service

    public static String serverProtocol() {
        return kServerProtocol;
    }

    public static String serverHost() {
        return kServerHost;
    }

    public static String serverUrl() {
        return kServerProtocol + "://" + kServerHost;
    }

    public static String serverUrl(String path) {
        if (path.startsWith("/")) {
            return (kServerProtocol + "://" + kServerHost + path);
        }
        return (kServerProtocol + "://" + kServerHost + "/" + path);
    }

    public static String apiUrl(String path) {
        if (path != null && path.startsWith("/")) {
            return (kServerProtocol + "://" + kServerHost + "/api" + path);
        }
        return (kServerProtocol + "://" + kServerHost  + "/api/" + path);
    }

    // MARK: Notification Name

    public static String bundleShortVersionKey() {
        return kCFBundleShortVersionString;
    }

    public static String bundleVersionKey() {
        return kCFBundleVersion;
    }

    // MARK: Notification Name

    public static String notificationTimerObserver() {
        return kNotificationTimerObserver;
    }

    // MARK: Logger

    public static boolean useApiLog() {
        return kUseApiLog;
    }

    public static boolean useDbLog() {
        return kUseDbLog;
    }

    public static boolean useDebugLog() {
        return kUseDebugLog;
    }

    public static boolean useFatalLog() {
        return kUseFatalLog;
    }

    public static boolean useFMDBLog() {
        return kUseFMDBLog;
    }

    public static String gaTrackingId() {
        return kTrackingId;
    }
}
