package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Message.STMessageViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/28/17.
 */

public class STStaffCommentView {

    private ImageView staffImageView;
    private Button messageButton;
    private TextView staffCommentView;


    private STShopStatusView stShopStatusView;

    private TextView staffNameLabel, shopNameLabel;

    private View shopStatus;

    private Context context;

    private Activity activity = null;

    private View view;

    private FragmentActivity fragmentActivity;

    private boolean doFollow;

    //use Activity
    public STStaffCommentView(Activity activity) {
        initSTStaffCommentView(activity);
        this.activity = activity;
        this.context = activity.getApplicationContext();

    }

    private void initSTStaffCommentView(Activity activity) {
        staffCommentView = (TextView) activity.findViewById(R.id.staffCommentView);

        staffImageView = (ImageView) activity.findViewById(R.id.staffImageView);
        messageButton = (Button) activity.findViewById(R.id.messageButton);


        shopStatus = activity.findViewById(R.id.stShopStatus_StaffComment);

        stShopStatusView = new STShopStatusView(shopStatus);
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
            status = "";
        }
        if (status.equals("staff")) {
            stShopStatusView.getActionButton().setVisibility(View.INVISIBLE);
            messageButton.setVisibility(View.INVISIBLE);
        }

        staffNameLabel = (TextView) activity.findViewById(R.id.staffNameLabel);
        shopNameLabel = (TextView) activity.findViewById(R.id.shopNameLabel);
    }

    //use View
    public STStaffCommentView(View view, FragmentActivity fragmentActivity) {
        initSTStaffCommentView(view);
        this.view = view;
        this.fragmentActivity = fragmentActivity;
        this.context = view.getContext();
    }

    private void initSTStaffCommentView(View view) {
        staffCommentView = (TextView) view.findViewById(R.id.staffCommentView);

        staffImageView = (ImageView) view.findViewById(R.id.staffImageView);
        messageButton = (Button) view.findViewById(R.id.messageButton);


        shopStatus = view.findViewById(R.id.stShopStatus_StaffComment);

        stShopStatusView = new STShopStatusView(shopStatus);
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
            status = "";
        }
        if (status.equals("staff")) {
            stShopStatusView.getActionButton().setVisibility(View.INVISIBLE);
            messageButton.setVisibility(View.INVISIBLE);
        }

        staffNameLabel = (TextView) view.findViewById(R.id.staffNameLabel);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
    }

    //

    public void setUpSTStaffCommentView(STReply stReply) {

        Logger.print("setUpSTStaffCommentView " + stReply);
        if (stReply.user != null) {

            staffCommentView.setText(stReply.comment);

            String url = stReply.user.imageURL;
            if (!url.isEmpty())
                Picasso.with(context).load(url).resize(200, 200).transform(new TransformationUtils().new CircleTransform()).into(staffImageView);
            else
                staffImageView.setImageResource(R.drawable.default_avatar);

            List status = new ArrayList();

            if (stReply.user.status.equals("staff")) {
                Log.i("Tan018", "if stReply.user.status = staff ");
                status.add("" + stReply.user.replyCount);
                status.add("" + stReply.user.followerCount);
                status.add("" + stReply.user.likeCount);

                status.add("Replies");
                status.add("Followers");
                status.add("Likes");
            } else {
                Log.i("Tan018", "if stReply.user.status orther ");
                status.add("" + stReply.user.postCount);
                status.add("" + stReply.user.watchCount);
                status.add("" + stReply.user.likeCount);

                status.add("Posts");
                status.add("Watches");
                status.add("Likes");
            }
            status.add("フォローする");
            for (int i = 0; i < status.size(); i++) {
                Logger.print("status " + status.get(i));
            }
            stShopStatusView.setUpSTShopStatusView(status);
            if (stReply.user.followID != 0) {
                stShopStatusView.getActionButton().setText("フォロー中");
                stShopStatusView.getActionButton().setTextColor(0xFFFFFFFF);
                stShopStatusView.getActionButton().setBackgroundColor(0xFF28ABEC);
            } else {
                stShopStatusView.getActionButton().setText("フォローする");
                stShopStatusView.getActionButton().setTextColor(0xFF9B9B9B);
                stShopStatusView.getActionButton().setBackgroundColor(0xFFEFEFEF);
            }

            staffNameLabel.setText(stReply.user.name);
            STShop shop = DataLoader.getShop(stReply.user.shopID);
            shopNameLabel.setText(shop.name);
            Log.i("STStaffCommentView05", "shop.name: " + shop.name);
            Log.i("STStaffCommentView05", "stReply.replyID: " + stReply.replyID);

        }
        doFollow = stReply.user.followID == 0 ? true : false;
        action(stReply);
    }

    private void action(final STReply stReply) {
        staffImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(stReply.user.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    if (activity != null) {
                        PopUpLogin login = new PopUpLogin(activity);
                        login.show();
                    } else {
                        PopUpLogin login = new PopUpLogin(context);
                        login.show();
                    }
                } else {
                    messageAction(stReply.user, stReply);
                }
            }
        });
        stShopStatusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    if (activity != null) {
                        PopUpLogin login = new PopUpLogin(activity);
                        login.show();
                    } else {
                        PopUpLogin login = new PopUpLogin(context);
                        login.show();
                    }
                } else {
                    Log.i("STStaffCommentView01", "doFollow: " + doFollow);
                    if (doFollow) {
                        stShopStatusView.getActionButton().setEnabled(false);
                        DataSender.createFollowStaff(stReply.user.userID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                stShopStatusView.getActionButton().setEnabled(true);
                                if (err != null)
                                    return;
                                if (isSuccess) {
                                    doFollow = !doFollow;
                                    stShopStatusView.getActionButton().setText("フォロー中");
                                    stShopStatusView.getActionButton().setTextColor(0xFFFFFFFF);
                                    stShopStatusView.getActionButton().setBackgroundColor(0xFF28ABEC);
                                }
                            }
                        });
                    } else {
                        stShopStatusView.getActionButton().setEnabled(false);
                        Log.i("STStaffCommentView01", "stReply.user.userID: " + stReply.user.userID);
                        Log.i("STStaffCommentView01", "stReply.user.followID: " + stReply.user.followID);
                        DataSender.deleteFollowStaff(stReply.user.userID, stReply.user.followID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                stShopStatusView.getActionButton().setEnabled(true);
                                if (err != null)
                                    return;
                                if (isSuccess) {
                                    doFollow = !doFollow;
                                    stShopStatusView.getActionButton().setText("フォローする");
                                    stShopStatusView.getActionButton().setTextColor(0xFF9B9B9B);
                                    stShopStatusView.getActionButton().setBackgroundColor(0xFFEFEFEF);
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void messageAction(STUser user, STReply stReply) {

        STMessageViewControllerFragment fragment = new STMessageViewControllerFragment();
        fragment.setDestinationUserID(user.userID);
        fragment.setItemID(stReply.itemID);
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }
}
