package link.styler.styler_android.New_TabHost.Adapter;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.realm.RealmList;
import link.styler.models.STLocation;
import link.styler.styler_android.New_TabHost.Home.ShopArea.STShopAreaPageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/19/17.
 */

public class MyAdapterShopArea extends BaseAdapter {

    private FragmentActivity fragmentActivity;
    private Context context;
    private RealmList<STLocation> stLocations;

    private TextView titleLabel;
    private TextView subTitleLabel;
    private TextView newLabel;
    private LinearLayout linearLayout_shop_area;

    public MyAdapterShopArea(FragmentActivity fragmentActivity, RealmList<STLocation> stLocations) {
        this.fragmentActivity = fragmentActivity;
        this.context = fragmentActivity.getApplicationContext();
        this.stLocations = stLocations;
    }

    public void setStLocations(RealmList<STLocation> stLocations) {
        this.stLocations = stLocations;
    }

    private void initView(View view) {
        titleLabel = (TextView) view.findViewById(R.id.titleLabel);
        titleLabel.setTextColor(Color.BLACK);
        subTitleLabel = (TextView) view.findViewById(R.id.subTitleLabel);
        newLabel = (TextView) view.findViewById(R.id.newLabel);
        newLabel.setText("新規あり");
        linearLayout_shop_area = (LinearLayout) view.findViewById(R.id.linearLayout_shop_area);
    }

    private void setUpView(STLocation stLocation) {

        titleLabel.setText(stLocation.name);
        subTitleLabel.setText(stLocation.count + "件");
        if (stLocation.newShopFlag)
            newLabel.setVisibility(View.VISIBLE);
        else
            newLabel.setVisibility(View.GONE);

    }

    @Override
    public int getCount() {
        return stLocations.size();
    }

    @Override
    public Object getItem(int position) {
        return stLocations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_shop_area_cell, null);
        initView(convertView);
        STLocation stLocation = stLocations.get(position);
        setUpView(stLocation);
        addControl(stLocation);
        return convertView;
    }

    private void addControl(final STLocation stLocation) {
        linearLayout_shop_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STShopAreaPageViewControllerFragment fragment = new STShopAreaPageViewControllerFragment();
                fragment.setLocationID(stLocation.locationID);
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
