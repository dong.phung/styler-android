package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.models.STEventSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterEvent;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/20/17.
 */
public class STMyPageEventViewControllerFragment extends Fragment {

    //model
    private int userID;
    private RealmResults<STEventSort> events;
    //private RealmList<STEvent>events =new RealmList<STEvent>();
    Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterEvent adapterEvent = null;

    public void createAdapterEvent() {
        this.events = DataLoader.getInterestEventsOfUser(userID);
        this.adapterEvent = new MyAdapterEvent(getActivity(), events);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (STMyPageEventViewControllerFragment.this.adapterEvent == null)
            createAdapterEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_event_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefresh_event);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        STMyPageEventViewControllerFragment.this.gridView.setAdapter(adapterEvent);
        if (adapterEvent.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });
        STMyPageEventViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageEventVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageEventVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageEventVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageEventVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageEventVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageEventVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });

    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveInterestsOfUser(userID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageEventViewControllerFragment.this.events = DataLoader.getInterestEventsOfUser(userID);

                    if (STMyPageEventViewControllerFragment.this.adapterEvent == null)
                        createAdapterEvent();

                    STMyPageEventViewControllerFragment.this.adapterEvent = (MyAdapterEvent) STMyPageEventViewControllerFragment.this.gridView.getAdapter();
                    STMyPageEventViewControllerFragment.this.adapterEvent.setEvents(STMyPageEventViewControllerFragment.this.events);
                    STMyPageEventViewControllerFragment.this.adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageEventViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveInterestsOfUser(userID, STMyPageEventViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageEventViewControllerFragment.this.events = DataLoader.getInterestEventsOfUser(userID);

                    if (STMyPageEventViewControllerFragment.this.adapterEvent == null)
                        createAdapterEvent();

                    STMyPageEventViewControllerFragment.this.adapterEvent = (MyAdapterEvent) STMyPageEventViewControllerFragment.this.gridView.getAdapter();
                    STMyPageEventViewControllerFragment.this.adapterEvent.setEvents(STMyPageEventViewControllerFragment.this.events);
                    STMyPageEventViewControllerFragment.this.adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageEventViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}