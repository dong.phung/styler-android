package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STReply;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Create.STCreateReplyViewControllerActivity;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/18/17.
 */

public class MyAdapterItem extends BaseAdapter {

    //mode
    private Integer postID = 0;
    private Integer userID = 0;
    private Integer shopID = 0;
    private STReply reply = new STReply();
    private RealmList<STItem> items = new RealmList<>();
    private RealmList<STItemSort> itemSorts = new RealmList<>();


    //helper
    // private Context context;
    private FragmentActivity fragmentActivity;
    private STEnums.STItemListCollectionViewType type = STEnums.STItemListCollectionViewType.defaultType;
    private STEnums.MyAdapterViewMode mode = STEnums.MyAdapterViewMode.normal;

    public static int selectItemID = 0;
    private int selectedPosition = -1;

    public MyAdapterItem(FragmentActivity fragmentActivity, RealmList<STItem> items, STEnums.STItemListCollectionViewType type) {
        this.fragmentActivity = fragmentActivity;
        //this.context = fragmentActivity.getApplicationContext();
        this.items = items;
        this.type = type;
    }

    public MyAdapterItem(FragmentActivity fragmentActivity, RealmResults<STItemSort> itemSorts, STEnums.STItemListCollectionViewType type) {
        this.fragmentActivity = fragmentActivity;
        this.itemSorts.addAll(itemSorts);
        this.items = new RealmList<>();
        for (STItemSort itemSort : itemSorts) {
            this.items.add(itemSort.item);
        }
        this.type = type;
    }

    public void setItems(RealmList<STItem> items) {
        this.items = items;
    }

    public void setPostID(Integer postID) {
        this.postID = postID;
    }

    public void setShopID(Integer shopID) {
        this.shopID = shopID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
    }

    public void setMode(STEnums.MyAdapterViewMode mode) {
        this.mode = mode;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).itemID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        switch (type) {
            case defaultType:
                return createSTReplyItemCell(items, convertView, position);
            case noAvatar:
                return createSTItemNoAvatarCell(items, convertView, position);
            case selectLike:
                return createSTItemSelectLikeCell(items, convertView, position);
            default:
                return convertView;
        }
    }

    private View createSTReplyItemCell(final RealmList<STItem> items, View convertView, final int position) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_reply_item_cell, null);

        //init
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        TextView userNameLabel = (TextView) convertView.findViewById(R.id.userNameLabel);
        ImageView userImageView = (ImageView) convertView.findViewById(R.id.userImageView);
        TextView brandLabel = (TextView) convertView.findViewById(R.id.brandLabel);
        TextView productNameLabel = (TextView) convertView.findViewById(R.id.productNameLabel);
        TextView priceLabel = (TextView) convertView.findViewById(R.id.priceLabel);
        final TextView likeCountLabel = (TextView) convertView.findViewById(R.id.likeCountLabel);
        final ImageButton likeButton = (ImageButton) convertView.findViewById(R.id.likeButton);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);

        //getdata
        final STItem item = items.get(position);
        String urlImage = item.mainImageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .resize(400, 400)
                    .centerCrop()
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(imageView);
        }

        if (postID != 0) {
            for (STReply reply : item.replies) {
                if (reply.postID == postID) {
                    this.reply = reply;
                    String userImageURL = reply.user.imageURL;
                    if (!userImageURL.isEmpty())
                        Picasso.with(fragmentActivity)
                                .load(userImageURL)
                                .transform(new TransformationUtils().new CircleTransform())
                                .into(userImageView);
                    userNameLabel.setText(reply.user.name);
                }
            }
        } else if (userID != 0) {
            for (STReply reply : item.replies) {
                if (reply.user.userID == userID) {
                    this.reply = reply;
                    String userImageURL = reply.user.imageURL;
                    if (!userImageURL.isEmpty())
                        Picasso.with(fragmentActivity)
                                .load(userImageURL)
                                .transform(new TransformationUtils().new CircleTransform())
                                .into(userImageView);
                    userNameLabel.setText(reply.user.name);
                }
            }

            if (itemSorts.size() == items.size()) {
                this.reply = DataLoader.getReply(itemSorts.get(position).replyID) != null ? DataLoader.getReply(itemSorts.get(position).replyID) : new STReply();
                String userImageURL = reply.user.imageURL;
                if (!userImageURL.isEmpty())
                    Picasso.with(fragmentActivity)
                            .load(userImageURL)
                            .transform(new TransformationUtils().new CircleTransform())
                            .into(userImageView);
                userNameLabel.setText(reply.user.name);
            }
        }


        brandLabel.setText(item.brand);
        productNameLabel.setText(item.name);
        if (item.price != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("¥ " + decimalFormat.format(Integer.parseInt(item.price)) + "(税込)");
        } else {
            priceLabel.setText("未設定");
        }
        likeCountLabel.setText(item.likeCount + "");

        likeButton.setVisibility(View.INVISIBLE);

        int meShopID = 0;
        try {
            meShopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
            meShopID = 0;
        }
        if (meShopID != item.shopID && meShopID == 0) {
            likeButton.setVisibility(View.VISIBLE);
        }

        if (item.likeID != 0) {
            likeButton.setImageResource(R.drawable.button_liked2x);
        } else {
            likeButton.setImageResource(R.drawable.button_like2x);
        }


        //action
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                fragment.setItemID(item.itemID);
                fragment.setPostID(postID);
                if (itemSorts.size() == items.size())
                    fragment.setReplyID(itemSorts.get(position).replyID);
                fragment.setUserID(userID);
                fragment.setViewType(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType());
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        final boolean[] likeOn = {item.likeID == 0 ? true : false};
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    PopUpLogin login = new PopUpLogin(fragmentActivity);
                    login.show();
                } else {
                    if (likeOn[0]) {
                        Integer replyID = null;
                        if (reply != null)
                            replyID = reply.replyID == 0 ? null : reply.replyID;
                        likeButton.setEnabled(false);
                        DataSender.createLike(item.itemID, replyID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                likeButton.setEnabled(true);
                                if (err != null) {
                                    return;
                                }
                                if (isSuccess) {
                                    likeOn[0] = !likeOn[0];
                                    likeButton.setImageResource(R.drawable.button_liked2x);
                                    int lc = Integer.parseInt(likeCountLabel.getText().toString());
                                    likeCountLabel.setText((lc + 1) + "");
                                }
                            }
                        });
                    } else {
                        likeButton.setEnabled(false);
                        DataSender.deleteLike(item.itemID, item.likeID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                likeButton.setEnabled(true);
                                if (err != null) {
                                    return;
                                }
                                if (isSuccess) {
                                    likeOn[0] = !likeOn[0];
                                    likeButton.setImageResource(R.drawable.button_like2x);
                                    int lc = Integer.parseInt(likeCountLabel.getText().toString());
                                    likeCountLabel.setText((lc - 1) + "");
                                }
                            }
                        });
                    }
                }
            }
        });
        return convertView;
    }

    private View createSTItemNoAvatarCell(RealmList<STItem> items, View convertView, int position) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_item_no_avatar_cell, null);

        //init
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        TextView brandLabel = (TextView) convertView.findViewById(R.id.brandLabel);
        TextView productNameLabel = (TextView) convertView.findViewById(R.id.productNameLabel);
        TextView priceLabel = (TextView) convertView.findViewById(R.id.priceLabel);
        final TextView likeCountLabel = (TextView) convertView.findViewById(R.id.likeCountLabel);
        TextView likeLabel = (TextView) convertView.findViewById(R.id.likeLabel);
        final Button likeButton = (Button) convertView.findViewById(R.id.likeButton);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);

        //getdata
        final STItem item = items.get(position);
        String urlImage = item.mainImageURL;
        if (urlImage != null) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .resize(400, 400)
                    .centerCrop()
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(imageView);
        }

        brandLabel.setText(item.brand);
        productNameLabel.setText(item.name);

        if (item.price != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("¥ " + decimalFormat.format(Integer.parseInt(item.price)) + "(税込)");
        } else {
            priceLabel.setText("未設定");
        }

        likeCountLabel.setText(item.likeCount + "");

        if (item.likeID != 0) {
            likeCountLabel.setTextColor(Color.rgb(155, 155, 155));
            likeLabel.setTextColor(Color.rgb(155, 155, 155));
            likeButton.setBackgroundResource(R.drawable.button_liked2x);
        } else {
            likeCountLabel.setTextColor(Color.rgb(155, 155, 155));
            likeLabel.setTextColor(Color.rgb(155, 155, 155));
            likeButton.setBackgroundResource(R.drawable.button_like2x);
        }

        if (LoginManager.getsIntstance().me() != null)
            if (item.shopID == LoginManager.getsIntstance().me().shopID && LoginManager.getsIntstance().me().shopID != 0) {
                likeButton.setVisibility(View.GONE);
            }

        //action
        if (mode == STEnums.MyAdapterViewMode.normal)
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                    fragment.setItemID(item.itemID);
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        else if (mode == STEnums.MyAdapterViewMode.createReply)
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // don't convert
                    Intent intent = new Intent(fragmentActivity, STCreateReplyViewControllerActivity.class);
                    intent.putExtra("itemID", item.itemID);
                    intent.putExtra("postID", postID);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    fragmentActivity.startActivity(intent);
                }
            });
        final boolean[] likeOn = {item.likeID == 0 ? true : false};
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    PopUpLogin login = new PopUpLogin(fragmentActivity);
                    login.show();
                } else {
                    if (likeOn[0]) {
                        Integer replyID = null;
                        if (reply != null)
                            replyID = reply.replyID == 0 ? null : reply.replyID;
                        likeButton.setEnabled(false);
                        DataSender.createLike(item.itemID, replyID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                likeButton.setEnabled(true);
                                if (err != null) {
                                    return;
                                }
                                if (isSuccess) {
                                    likeOn[0] = !likeOn[0];
                                    likeButton.setBackgroundResource(R.drawable.button_liked2x);
                                    int lc = Integer.parseInt(likeCountLabel.getText().toString());
                                    likeCountLabel.setText((lc + 1) + "");
                                }
                            }
                        });
                    } else {
                        likeButton.setEnabled(false);
                        DataSender.deleteLike(item.itemID, item.likeID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                likeButton.setEnabled(true);
                                if (err != null) {
                                    return;
                                }
                                if (isSuccess) {
                                    likeOn[0] = !likeOn[0];
                                    likeButton.setBackgroundResource(R.drawable.button_like2x);
                                    int lc = Integer.parseInt(likeCountLabel.getText().toString());
                                    likeCountLabel.setText((lc - 1) + "");
                                }
                            }
                        });
                    }
                }
            }
        });
        return convertView;
    }

    private View createSTItemSelectLikeCell(RealmList<STItem> items, View convertView, final int position) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_item_select_like_cell, null);

        //init
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        ImageView selectedMaskView = (ImageView) convertView.findViewById(R.id.selectedMaskView);
        ImageView selectedImageView = (ImageView) convertView.findViewById(R.id.selectedImageView);
        TextView brandLabel = (TextView) convertView.findViewById(R.id.brandLabel);
        TextView productNameLabel = (TextView) convertView.findViewById(R.id.productNameLabel);
        TextView priceLabel = (TextView) convertView.findViewById(R.id.priceLabel);

        //getdata
        final STItem item = items.get(position);
        String urlImage = item.mainImageURL;
        if (urlImage != null) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .resize(400, 400)
                    .centerCrop()
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(imageView);
        }
        brandLabel.setText(item.brand);
        productNameLabel.setText(item.name);

        if (item.price != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            priceLabel.setText("¥ " + decimalFormat.format(Integer.parseInt(item.price)) + "(税込)");
        } else {
            priceLabel.setText("未設定");
        }

        if (position == selectedPosition) {
            selectedMaskView.setVisibility(View.VISIBLE);
            selectedImageView.setVisibility(View.VISIBLE);
        } else {
            selectedMaskView.setVisibility(View.GONE);
            selectedImageView.setVisibility(View.GONE);
        }

        //action

        return convertView;
    }

}
