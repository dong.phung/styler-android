package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STCategory;
import link.styler.models.STPost;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;


/**
 * Created by macOS on 3/28/17.
 */

public class STSinglePostHeaderView {

    //model
    private STPost post = new STPost();

    //view
    public ImageView avatarImageView;           //action
    public TextView categoryLabel;
    public STShopStatusView statusView;         //action
    public TextView nameLabel, userInfoLabel, descriptionLabel;
    public ImageView watchImageView;
    public TextView watchCountLabel;

    //Helper
    private Context context;
    private Activity activity;
    private View view;
    private FragmentActivity fragmentActivity;

    //use activity + post
    public STSinglePostHeaderView(Activity activity, STPost post) {
        this.post = post;
        this.activity = activity;
        this.context = activity.getApplicationContext();
        initView(activity);
        configData(post);
        addAction();
    }

    //use activity
    public STSinglePostHeaderView(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        initView(activity);
        addAction();
    }

    private void initView(Activity activity) {
        avatarImageView = (ImageView) activity.findViewById(R.id.avatarImageView);
        categoryLabel = (TextView) activity.findViewById(R.id.categoryLabel);
        View view = activity.findViewById(R.id.statusView);
        statusView = new STShopStatusView(view);
        nameLabel = (TextView) activity.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) activity.findViewById(R.id.userInfoLabel);
        descriptionLabel = (TextView) activity.findViewById(R.id.descriptionLabel);
        watchImageView = (ImageView) activity.findViewById(R.id.watchImageView);
        watchCountLabel = (TextView) activity.findViewById(R.id.watchCountLabel);
    }

    //use view
    public STSinglePostHeaderView(View view, FragmentActivity fragmentActivity) {
        this.view = view;
        this.fragmentActivity = fragmentActivity;
        this.context = view.getContext();
        initView(view);
        addAction();
    }

    private void initView(View view) {
        avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);
        categoryLabel = (TextView) view.findViewById(R.id.categoryLabel);

        statusView = new STShopStatusView(view.findViewById(R.id.statusView));

        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) view.findViewById(R.id.userInfoLabel);
        descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
        watchImageView = (ImageView) view.findViewById(R.id.watchImageView);
        watchCountLabel = (TextView) view.findViewById(R.id.watchCountLabel);
    }


    //config view
    public void configData(STPost post) {
        this.post = post;
        if (post == null || post.postID == 0)
            return;

        String status = null;
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }
        if (status != null) {
            if (status.equals("customer")) {
                int userID = post.user.userID;
                int meID;
                try {
                    meID = LoginManager.getsIntstance().me().userID;
                } catch (Exception e) {
                    meID = 0;
                }
                if (userID == meID) {
                    statusView.getActionButton().setVisibility(View.INVISIBLE);
                } else if (post.watchID != 0) {
                    statusView.getActionButton().setText("このPostをWatch中");
                    statusView.getActionButton().setTextColor(0xFF28ABEC);
                    Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
                    img.setColorFilter(0xFF28ABEC, PorterDuff.Mode.MULTIPLY);
                    statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                } else {
                    statusView.getActionButton().setText("このPostをWatchする");
                    statusView.getActionButton().setTextColor(0xFF9B9B9B);
                    Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
                    img.setColorFilter(0xFF9B9B9B, PorterDuff.Mode.MULTIPLY);
                    statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                }

            } else {
                statusView.getActionButton().setText("ユーザー詳細を見る");
                statusView.getActionButton().setTextColor(0xFF9B9B9B);
                Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
                img.setColorFilter(0xFF9B9B9B, PorterDuff.Mode.MULTIPLY);
                statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }
        } else {
            statusView.getActionButton().setText("このPostをWatchする");
            statusView.getActionButton().setTextColor(0xFF9B9B9B);
            Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
            img.setColorFilter(0xFF9B9B9B, PorterDuff.Mode.MULTIPLY);
            statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }

        STCategory category = post.category;
        Log.i("STSinglePostHeaderView", "post: " + post);
        Log.i("STSinglePostHeaderView", "post.category: " + post.category);
        Log.i("STSinglePostHeaderView", "category: " + category);
        if (category != null && category.categoryID != 0) {
            categoryLabel.setVisibility(View.VISIBLE);
            if (category.name.equals("カテゴリーを選択しない")) {
                categoryLabel.setText("その他");
            } else {
                categoryLabel.setText(category.name);
            }
        } else if (!post.categoryName.isEmpty()) {
            categoryLabel.setVisibility(View.VISIBLE);
            if (post.categoryName.equals("カテゴリーを選択しない")) {
                categoryLabel.setText("その他");
            } else {
                categoryLabel.setText(post.categoryName);
            }
        } else {

            categoryLabel.setVisibility(View.GONE);
        }

        String userImageURL = post.user.imageURL;
        if (userImageURL != "") {
            Picasso.with(context)
                    .load(userImageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    .placeholder(R.drawable.default_avatar)
                    .into(avatarImageView);
        } else {
            avatarImageView.setImageResource(R.drawable.default_avatar);
        }

        STUser user = post.user;
        if (user != null && user.userID != 0) {
            statusView.configData(user);
            userInfoLabel.setText(user.apiDetail);
        }
        if (post.user.name != null && !post.user.name.isEmpty())
            nameLabel.setText(post.user.name);
        else
            nameLabel.setText("未設定");
        descriptionLabel.setText(post.text);
        watchCountLabel.setText("" + post.watchesCount);
    }

    private void addAction() {

        avatarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapAvatarAction();
            }
        });

        statusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusViewAction();
            }
        });
    }

    private void tapAvatarAction() {
        Log.i("STSinglePostHeaderV001", "click avatar");
        int userID = 0;
        try {
            userID = post.user.userID;
        } catch (Exception e) {
        }

        if (userID == 0)
            return;

        STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
        fragment.setUserID(userID);
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void statusViewAction() {
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }

        if (status != "") {
            if (status.equals(STEnums.UserStatus.Customer.getUserStatus())) {
                final STPost post = this.post;
                if (post == null || post.postID == 0) {
                    return;
                }
                statusView.getActionButton().setEnabled(false);
                int watchID = post.watchID;
                boolean doWatch = watchID == 0 ? true : false;
                onWatchButton(doWatch);
                if (doWatch) {
                    DataSender.createWatch(post.postID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            if (isSuccess) {
                                watchCountLabel.setText("" + (post.watchesCount + 1));

                            }
                            onWatchButton(isSuccess);
                            statusView.getActionButton().setEnabled(true);
                        }
                    });
                } else {
                    DataSender.deleteWatch(post.postID, watchID, new STCompletion.Base() {
                        @Override
                        public void onRequestComplete(boolean isSuccess, STError err) {
                            if (isSuccess) {
                                watchCountLabel.setText("" + (post.watchesCount - 1));
                            }
                            onWatchButton(!isSuccess);
                            statusView.getActionButton().setEnabled(true);
                        }
                    });
                }
            } else if (status.equals(STEnums.UserStatus.Staff.getUserStatus())) {
                if (post.user.userID == 0)
                    return;

                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(post.user.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            } else {
                showLoginModal();
            }
        } else {
            showLoginModal();
        }
    }

    private void onWatchButton(boolean doWatch) {
        if (doWatch) {
            statusView.getActionButton().setText("このPostをWatch中");
            statusView.getActionButton().setTextColor(0xFF28ABEC);                           //st_azureColor
            Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
            img.setColorFilter(0xFF28ABEC, PorterDuff.Mode.MULTIPLY);
            statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {
            statusView.getActionButton().setText("このPostをWatchする");
            statusView.getActionButton().setTextColor(0xFF9B9B9B);                           //st_lightGrayColor
            Drawable img = ContextCompat.getDrawable(context, R.drawable.icon_watch);
            img.setColorFilter(0xFF9B9B9B, PorterDuff.Mode.MULTIPLY);
            statusView.getActionButton().setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }
    }

    private void showLoginModal() {
        if (activity != null) {
            PopUpLogin popUpLogin = new PopUpLogin(activity);
            popUpLogin.show();
        } else {
            PopUpLogin popUpLogin = new PopUpLogin(view.getContext());
            popUpLogin.show();
        }
    }
}
