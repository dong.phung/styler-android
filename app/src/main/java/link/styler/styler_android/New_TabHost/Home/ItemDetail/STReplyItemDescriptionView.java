package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import link.styler.models.STItem;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/28/17.
 */

public class STReplyItemDescriptionView {

    private TextView titleLable;
    private TextView itemDescriptionLabel;
    private TextView recommendLabel;

    //use Activity
    public STReplyItemDescriptionView(Activity activity){
        initSTReplyItemDescriptionView(activity);
    }

    private void initSTReplyItemDescriptionView(Activity activity){
        titleLable = (TextView)activity.findViewById(R.id.titleLableDescription);
        titleLable.setText("アイテム説明：");
        titleLable.setTextColor(Color.BLACK);

        itemDescriptionLabel = (TextView)activity.findViewById(R.id.itemDescription);

        recommendLabel = (TextView)activity.findViewById(R.id.recommendDescription);
        recommendLabel.setText("おすすめポイント:");
    }

    //use View
    public STReplyItemDescriptionView(View view) {
        initSTReplyItemDescriptionView(view);
    }

    private void initSTReplyItemDescriptionView(View view) {
        titleLable = (TextView)view.findViewById(R.id.titleLableDescription);
        titleLable.setText("アイテム説明：");
        titleLable.setTextColor(Color.BLACK);

        itemDescriptionLabel = (TextView)view.findViewById(R.id.itemDescription);

        recommendLabel = (TextView)view.findViewById(R.id.recommendDescription);
        recommendLabel.setText("おすすめポイント:");
    }

    public void setUpSTReplyItemDescriptionView(STItem item){
        itemDescriptionLabel.setText(item.text);
    }
}
