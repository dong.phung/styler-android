package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/17/17.
 */

public class MyAdapterStaff extends BaseAdapter {

    //model
    private RealmList<STUser> users;
    private STUser user;

    //view
    private ImageView userImageView;
    private STShopStatusView statusView;
    private TextView nameLabel;
    private TextView shopNameLabel;
    private LinearLayout linearLayout;

    //hepler
    private FragmentActivity fragmentActivity;

    public MyAdapterStaff(FragmentActivity fragmentActivity, RealmList<STUser> users) {
        this.fragmentActivity = fragmentActivity;
        this.users = users;
    }

    public void setUsers(RealmList<STUser> users) {
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).userID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_staff_cell, null);
        intView(convertView);
        //}
        STUser user = users.get(position);
        setupView(user);
        addControl(user);
        return convertView;
    }

    private void intView(View view) {
        userImageView = (ImageView) view.findViewById(R.id.userImageView);
        statusView = new STShopStatusView(view);
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
    }

    private void setupView(STUser user) {
        String urlImage = user.imageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(userImageView);
        }
        statusView.getActionButton().setText("詳細を見る");
        statusView.getActionButton().setEnabled(false);
        statusView.configData(user);
        nameLabel.setText(user.name);
        STShop stShop = DataLoader.getShop(user.shopID);
        if (stShop != null)
            shopNameLabel.setText(stShop.name);
    }

    private void addControl(final STUser user) {
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setUserID(user.userID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

}
