package link.styler.styler_android.New_TabHost.Home.Event;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.models.STShop;
import link.styler.styler_android.R;

/**
 * Created by macOS on 5/9/17.
 */

public class STEventHeaderView {

    //model
    private STEventSort eventSort = new STEventSort();
    private int eventID;
    private View view;
    private Context context;
    private FragmentActivity fragmentActivity;

    //view
    private TextView titleLabel;
    private ImageView eventImageView;
    private TextView numOfGoingLabel;
    private TextView goingLabel;
    private TextView eventDateLabel;
    private TextView eventTitleLabel;
    private ImageView shopImageView;
    private TextView shopNameLabel;
    private STHoldingEventButton holdingEventButton;

    private LinearLayout viewEvent;
    private LinearLayout viewHoldingEventButton;

    //Helper


    public STEventHeaderView(View view, FragmentActivity fragmentActivity) {
        this.view = view;
        this.context = view.getContext();
        this.fragmentActivity = fragmentActivity;
        initView(this.view);
        //      configData();
        addControls();
    }

    private void initView(View view) {
        eventImageView = (ImageView) view.findViewById(R.id.eventImageView);
        numOfGoingLabel = (TextView) view.findViewById(R.id.numOfGoingLabel);
        eventDateLabel = (TextView) view.findViewById(R.id.eventDateLabel);
        eventTitleLabel = (TextView) view.findViewById(R.id.eventTitleLabel);
        shopImageView = (ImageView) view.findViewById(R.id.shopImageView);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
        holdingEventButton = new STHoldingEventButton(view);
        holdingEventButton.titleLabel.setText("開催中のイベント一覧 >");

        viewEvent = (LinearLayout) view.findViewById(R.id.viewEvent);
        viewHoldingEventButton = (LinearLayout) view.findViewById(R.id.viewHoldingEventButton);
    }

    public void configData() {
        eventSort = DataLoader.getMostPopularOngoingEvent();

        eventID = eventSort.eventID;
        if (eventSort == null)
            return;
        final STEvent event = eventSort.event;
        if (event == null)
            return;
        String imageURL = event.imageURL;
        Log.i("STEventHeaderView", "imageURL: " + imageURL);
        if (!imageURL.isEmpty())
            Picasso.with(context)
                    .load(imageURL)
                    .placeholder(R.color.st_lightGrayColor)
                    .transform(new TransformationUtils().new RoundedCornersTransform())
                    .into(eventImageView);

        String eventTitle = event.title;
        if (!eventTitle.equals(null)) {
            eventTitleLabel.setText(eventTitle);
        }

        STDateFormat dateFormat = new STDateFormat();
        String dateText = dateFormat.DateFormat(event.startDate) + "~";
        if (!event.finishDate.equals(null)) {
            dateText = dateText + dateFormat.DateFormat(event.finishDate);
        }

        eventDateLabel.setText(dateText);
        numOfGoingLabel.setText(event.interestsCount + "");
        DataPreserver.saveShop(event.shopID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                STShop shop = DataLoader.getShop(event.shopID);
                if (shop != null) {
                    String url = "";
                    if (!shop.imageURL.isEmpty())
                        Picasso.with(context)
                                .load(shop.imageURL)
                                .placeholder(R.color.st_lightGrayColor)
                                .transform(new TransformationUtils().new RoundedCornersTransform())
                                .into(shopImageView);
                    shopNameLabel.setText(shop.name);
                }
            }
        });
        holdingEventButton.configData(DataLoader.getOngoingEvents().size(), "");

    }

    private void addControls() {
        viewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STEventDetailViewControllerFragment fragment = new STEventDetailViewControllerFragment();
                fragment.setEventID(eventID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        viewHoldingEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STEventHeldViewControllerFragment fragment = new STEventHeldViewControllerFragment();
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
