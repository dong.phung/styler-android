package link.styler.styler_android.New_TabHost.SDKFacebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;

import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.Logger;
import link.styler.styler_android.R;

public class FacebookLoginActivity extends Activity {

    private CallbackManager callbackManager;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_facebook_login);

        loginButton = (LoginButton) findViewById(R.id.login_facebook);
        loginButton.performClick();
        loginButton.setVisibility(View.INVISIBLE);
        loginButton.setReadPermissions("public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Logger.print("facebook onSuccess");
                String token = loginResult.getAccessToken().getToken();
                Logger.print("facebook onSuccess "+loginResult.getAccessToken().getToken()+ " "+loginResult.getAccessToken().getSource());
                StylerApi.facebookLogin(token, new STCompletion.WithJsonToken() {
                    @Override
                    public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, String token) {
                        Logger.print("facebookLogin json "+json);
                        Logger.print("facebookLogin isSuccess "+isSuccess);
                        Logger.print("facebookLogin token "+token);

                        JSONObject users = json.optJSONObject("user");
                        Logger.print("facebookLogin userid "+users.optInt("id"));

                        link.styler.Utils.LoginManager.isLogin = true;
                        link.styler.Utils.LoginManager.getsIntstance().setUserID(users.optInt("id"));
                        link.styler.Utils.LoginManager.getsIntstance().setAccessToken(token);


                    }
                });
            }

            @Override
            public void onCancel() {
                Logger.print("facebook onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Logger.print("facebook onError error "+error.toString());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
