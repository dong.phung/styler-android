package link.styler.styler_android.New_TabHost;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import link.styler.Utils.LoginManager;
import link.styler.models.STItem;
import link.styler.styler_android.New_TabHost.Create.STCreateReplyViewControllerActivity;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/21/17.
 */

public class PopUpMoreActionItem extends Dialog {
    //model
    private STItem item = new STItem();
    private Context context;
    private String type;
    private int replyID = 0;

    //view
    public Button editButton, reporButton, cancelButton;
    public LinearLayout lineLayout;

    //helper
    int myShopID = 0;
    int shopID = 0;

    public PopUpMoreActionItem(Context context, STItem item, String type) {
        super(context);
        this.context = context;
        this.item = item;
        this.shopID = item.shopID;
        this.type = type;
    }

    public void setReplyID(int replyID) {
        this.replyID = replyID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_more_action_item);
        addControls();
        if (item.itemID == 0)
            dismiss();

        initView();
        addEvents();

    }

    private void addControls() {
        editButton = (Button) findViewById(R.id.editButton);
        reporButton = (Button) findViewById(R.id.reportButtonDialog);
        cancelButton = (Button) findViewById(R.id.cancelButton);
    }

    private void initView() {
        try {
            myShopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
        }
        if (myShopID != item.shopID) {
            editButton.setVisibility(View.GONE);
        } else {
            editButton.setVisibility(View.VISIBLE);
        }
        if (type.equals("item")) {
            editButton.setText("Itemを編集する");
        } else if (type.equals("reply")) {
            editButton.setText("Replyを編集する");
        }
    }

    private void addEvents() {

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAction();
            }
        });

        reporButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportAction();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void editAction() {
        Intent intent = new Intent(context, STCreateReplyViewControllerActivity.class);
        intent.putExtra("itemID", item.itemID);
        if (type.equals("reply"))
            intent.putExtra("replyID", replyID);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        dismiss();
    }

    private void reportAction() {
        Toast.makeText(context, "報告されたPostは運営で調査致します。", Toast.LENGTH_LONG).show();
        dismiss();
    }

}
