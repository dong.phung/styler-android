package link.styler.styler_android.New_TabHost.MyPage;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.models.STShop;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/17/17.
 */

public class STMyPageShopButton {

    STShop shop;

    private Context context;
    private ImageView shopImageView;
    private TextView shopNameLabel;
    private ImageView rightArrowView;
    private LinearLayout st_my_page_shop_button;

    public STMyPageShopButton(Activity activity) {
        initSTMyPageShopButton(activity);
        this.context = activity.getApplicationContext();
        addControls();
    }

    private void initSTMyPageShopButton(Activity activity) {
        shopImageView = (ImageView) activity.findViewById(R.id.shopImageView);
        shopNameLabel = (TextView) activity.findViewById(R.id.shopNameLabel);
        rightArrowView = (ImageView) activity.findViewById(R.id.rightArrowView);
        st_my_page_shop_button = (LinearLayout) activity.findViewById(R.id.st_my_page_shop_button);
    }

    public STMyPageShopButton(View view) {
        initSTMyPageShopButton(view);
        this.context = view.getContext();
        addControls();
    }

    private void initSTMyPageShopButton(View view) {
        shopImageView = (ImageView) view.findViewById(R.id.shopImageView);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
        rightArrowView = (ImageView) view.findViewById(R.id.rightArrowView);
        st_my_page_shop_button = (LinearLayout) view.findViewById(R.id.st_my_page_shop_button);
    }

    private void addControls() {

    }

    public void setUpSTMyPageShopButton(STShop shop) {
        this.shop = shop;
        Picasso.with(context)
                .load(shop.imageURL)
                .error(R.drawable.icon_user)
                .into(shopImageView);
        shopNameLabel.setText(shop.name);
    }

    public void configData(STShop shop) {
        try {
            this.shop = shop;
            Picasso.with(context)
                    .load(shop.imageURL)
                    .error(R.drawable.icon_user)
                    .into(shopImageView);
            shopNameLabel.setText(shop.name);
        } catch (Exception e) {

        }
    }
}
