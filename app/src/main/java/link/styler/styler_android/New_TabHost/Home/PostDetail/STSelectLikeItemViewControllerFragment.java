package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STItem;

import link.styler.models.STLike;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.WaitActivity;
import link.styler.styler_android.R;

public class STSelectLikeItemViewControllerFragment extends Fragment {

    //model
    private RealmResults<STLike> items;
    private RealmList<STItem> stItems;
    private Integer lastObjectID = null;
    private MyAdapterItem adapterLikes = null;
    private int meID;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigationView;
    private GridView collectionView;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    int firstVsb = 0;
    private String notificationToken; //// TODO: 4/6/17 notificationToken

    private STItem selectedItem;
    private int itemID = 0;
    private Integer postID;
    private static int selectedItemID = 0;

    private void createAdapterItem() {
        items = DataLoader.getLikesOfUser(meID);
        stItems = new RealmList<STItem>();
        for (STLike stLike : items) {
            stItems.add(stLike.item);
        }
        adapterLikes = new MyAdapterItem(getActivity(), stItems, STEnums.STItemListCollectionViewType.selectLike);
    }

    public void setPostID(Integer postID) {
        this.postID = postID;
    }

    public static int getSelectedItemID() {
        return selectedItemID;
    }

    public static void setSelectedItemID(int selectedItemID) {
        STSelectLikeItemViewControllerFragment.selectedItemID = selectedItemID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedItemID = 0;
        try {
            meID = LoginManager.getsIntstance().getUserID();
        } catch (Exception e) {
            meID = 0;
            return;
        }
        if (adapterLikes == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_select_like_item_view_controller, container, false);

        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigationView = new STBaseNavigationController(view);
        collectionView = (GridView) view.findViewById(R.id.collectionView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigationView.getTitleNavigation().setText("おすすめを選択");
        navigationView.getSubTileNavigation().setVisibility(View.GONE);
        navigationView.getBackButton().setImageResource(R.drawable.button_close);
        navigationView.getBackButton().setPadding(0, 35, 0, 35);
        navigationView.getUpdateButton().setText("投稿する");
        navigationView.getUpdateButton().setTextColor(0xFF9B9B9B);
        navigationView.getUpdateButton().setEnabled(false);
        progressBar.setVisibility(View.GONE);

        collectionView.setAdapter(adapterLikes);
        if (adapterLikes.getCount() == 0)
            getData();
        addAction();
    }


    private void addAction() {
        navigationView.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        navigationView.getUpdateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItemID = itemID;
                if (selectedItemID != 0) {
                    Intent intent = new Intent(getActivity(), WaitActivity.class);
                    startActivity(intent);
                    getFragmentManager().popBackStackImmediate();
                }
            }
        });

        collectionView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int i = 0;
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STSelectLikeItemVCF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STSelectLikeItemVCF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STSelectLikeItemVCF", "totalItemCount: " + totalItemCount);
                    Log.i("STSelectLikeItemVCF", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                        firstVsb = firstVisibleItem;
                    }
                } else {
                    gotData = false;
                }
                //if (firstVsb != firstVisibleItem)
                //    gotData = false;
            }
        });

        collectionView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                adapterLikes.setSelectedPosition(position);
                adapterLikes.notifyDataSetChanged();
                itemID = stItems.get(position).itemID;
                navigationView.getUpdateButton().setTextColor(0xFF28ABEC);
                navigationView.getUpdateButton().setEnabled(true);
            }
        });
    }

    private void getData() {
        if (meID == 0 || isLoading)
            return;
        isLoading = true;

        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveLikesOfUser(meID, lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    items = DataLoader.getLikesOfUser(meID);
                    stItems = new RealmList<STItem>();
                    for (STLike stLike : items) {
                        stItems.add(stLike.item);
                    }
                    STSelectLikeItemViewControllerFragment.this.adapterLikes.setItems(stItems);
                    STSelectLikeItemViewControllerFragment.this.adapterLikes.notifyDataSetChanged();

                    if (lastObjectID != null && lastObjectID != 0)
                        STSelectLikeItemViewControllerFragment.this.lastObjectID = lastObjectID;
                }
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
