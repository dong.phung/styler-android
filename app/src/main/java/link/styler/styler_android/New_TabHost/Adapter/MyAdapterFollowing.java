package link.styler.styler_android.New_TabHost.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STFollow;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.Message.STMessageViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPageFollowingViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 5/6/17.
 */

public class MyAdapterFollowing extends BaseAdapter {

    //model
    private RealmList<STUser> staffs;
    private int userID;

    //view
    private ImageView userImageView;
    private ImageButton messageButton;
    private STShopStatusView statusView;
    private TextView nameLabel;
    private TextView userInfoLabel;
    private LinearLayout linearLayout;


    //hepler
    private FragmentActivity fragmentActivity;

    public MyAdapterFollowing(FragmentActivity fragmentActivity, RealmList<STUser> users) {
        this.fragmentActivity = fragmentActivity;
        this.staffs = users;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setStaffs(RealmList<STUser> staffs) {
        this.staffs = staffs;
    }

    @Override
    public int getCount() {
        return staffs.size();
    }

    @Override
    public Object getItem(int position) {
        return staffs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return staffs.get(position).userID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_following_cell, null);
        intView(convertView);
        //}

        if (staffs.size() == 0)
            return convertView;

        setupView(staffs.get(position));
        addControl(staffs.get(position));

        statusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog =new ProgressDialog(fragmentActivity);
                progressDialog.show();
                DataSender.deleteFollowStaff(staffs.get(position).userID, staffs.get(position).followID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        if (isSuccess) {
                            updateView(null);
                        }else {
                            progressDialog.dismiss();
                        }
                    }

                    private void updateView(Integer lastObjectID) {
                        DataPreserver.saveFollowingOfUser(userID, lastObjectID, new STCompletion.WithLastObjectID() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                                if (isSuccess) {
                                    if (lastObjectID != null && lastObjectID > staffs.get(position).followID) {
                                        updateView(lastObjectID);
                                    } else {
                                        RealmResults<STFollow> followings = DataLoader.getFollowsOfUser(userID);
                                        MyAdapterFollowing.this.staffs = new RealmList<STUser>();
                                        for (STFollow follow : followings) {
                                            MyAdapterFollowing.this.staffs.add(follow.staff);
                                        }
                                        notifyDataSetChanged();
                                        progressDialog.dismiss();
                                    }
                                }else {
                                    progressDialog.dismiss();
                                }
                            }
                        });
                    }
                });
            }
        });


        return convertView;
    }

    private void intView(View view) {
        userImageView = (ImageView) view.findViewById(R.id.userImageView);
        messageButton = (ImageButton) view.findViewById(R.id.messageButton);
        statusView = new STShopStatusView(view);
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) view.findViewById(R.id.userInfoLabel);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
    }

    private void setupView(STUser user) {
        String urlImage = user.imageURL;
        if (!urlImage.isEmpty()) {
            Picasso.with(fragmentActivity)
                    .load(urlImage)
                    .error(R.drawable.icon_default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(userImageView);
        }
        String status = "";
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
        }
        statusView.getActionButton().setEnabled(true);
        if (!status.isEmpty() && !status.equals("none")) {
            if (user.followID != 0) {
                statusView.getActionButton().setBackgroundColor(0xFF28ABEC);
                statusView.getActionButton().setTextColor(0xFFFFFFFF);
                statusView.getActionButton().setText("フォロー中");
            } else {
                statusView.getActionButton().setBackgroundColor(0xFFEFEFEF);
                statusView.getActionButton().setTextColor(0xFF9B9B9B);
                statusView.getActionButton().setText("＋ フォローする");
            }
        } else {
            statusView.getActionButton().setEnabled(false);
            statusView.getActionButton().setBackgroundColor(0xFFEFEFEF);
            statusView.getActionButton().setTextColor(0xFF9B9B9B);
            statusView.getActionButton().setText("詳細を見る");
        }

        statusView.configData(user);
        nameLabel.setText(user.name);
        try {
            STShop shop = DataLoader.getShop(user.shopID);
            if (shop != null) ;
            userInfoLabel.setText(shop.name);
        } catch (Exception e) {

        }
    }

    private void addControl(final STUser user) {
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoUserDetail(user);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMessage(user);
            }
        });
    }

    private void gotoUserDetail(STUser user) {
        STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
        fragment.setUserID(user.userID);
        fragmentActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void gotoMessage(STUser user) {
        STMessageViewControllerFragment fragment = new STMessageViewControllerFragment();
        fragment.setDestinationUserID(user.userID);
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

}
