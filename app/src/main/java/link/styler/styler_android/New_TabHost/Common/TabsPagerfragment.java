package link.styler.styler_android.New_TabHost.Common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import link.styler.Utils.Logger;

/**
 * Created by admin on 3/6/17.
 */

public class TabsPagerfragment extends FragmentPagerAdapter {
    private double lng, lnt;

    public TabsPagerfragment(FragmentManager fm,double lng,double lnt) {
        super(fm);
        this.lng = lng;
        this.lnt = lnt;
    }

    @Override
    public Fragment getItem(int position) {
        Logger.print("x "+lng+" y "+lnt);
        return new MapsFragment().newInstance(lng,lnt);
    }

    @Override
    public int getCount() {
        return 1;
    }
}
