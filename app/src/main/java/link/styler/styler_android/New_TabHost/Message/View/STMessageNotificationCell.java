package link.styler.styler_android.New_TabHost.Message.View;

import android.app.Activity;
import android.widget.TextView;

import link.styler.models.STMessage;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/29/17.
 */

public class STMessageNotificationCell {
    //model
    STMessage message = new STMessage();
    //view
    public TextView notificationLabel;

    public STMessageNotificationCell(Activity activity) {
        init(activity);
        addControls();
    }

    private void init(Activity activity) {
        this.notificationLabel = (TextView) activity.findViewById(R.id.notificationLabel);
    }

    private void addControls() {
    }

    public void configData(STMessage message){
        this.message = message;
        notificationLabel.setText(message.text);
    }
}
