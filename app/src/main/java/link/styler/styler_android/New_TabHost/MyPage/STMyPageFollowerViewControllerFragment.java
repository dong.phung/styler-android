package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STFollow;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterFollower;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageFollowerViewControllerFragment extends Fragment {

    //model
    private int userID;
    private RealmResults<STFollow> follows;
    private RealmList<STUser> users = new RealmList<>();
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterFollower adapterFollower = null;

    public void createAdapterFollower() {
        this.follows = DataLoader.getFollowersOfStaff(userID);
        this.users = new RealmList<STUser>();
        for (STFollow follow : STMyPageFollowerViewControllerFragment.this.follows) {
            STMyPageFollowerViewControllerFragment.this.users.add(follow.user);
        }
        this.adapterFollower = new MyAdapterFollower(getActivity(), users);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (STMyPageFollowerViewControllerFragment.this.adapterFollower == null)
            createAdapterFollower();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_follower_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_followers);
        gridView = (GridView) view.findViewById(R.id.gridViewFollower);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        STMyPageFollowerViewControllerFragment.this.gridView.setAdapter(adapterFollower);
        if (adapterFollower.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        STMyPageFollowerViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageFollowerVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageFollowerVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageFollowerVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageFollowerVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageFollowerVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageFollowerVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveFollowerOfUser(userID, null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageFollowerViewControllerFragment.this.follows = DataLoader.getFollowersOfStaff(userID);
                    STMyPageFollowerViewControllerFragment.this.users = new RealmList<STUser>();
                    for (STFollow follow : STMyPageFollowerViewControllerFragment.this.follows) {
                        STMyPageFollowerViewControllerFragment.this.users.add(follow.user);
                    }

                    if (STMyPageFollowerViewControllerFragment.this.adapterFollower == null)
                        createAdapterFollower();

                    STMyPageFollowerViewControllerFragment.this.adapterFollower = (MyAdapterFollower) STMyPageFollowerViewControllerFragment.this.gridView.getAdapter();
                    STMyPageFollowerViewControllerFragment.this.adapterFollower.setFollowers(STMyPageFollowerViewControllerFragment.this.users);
                    STMyPageFollowerViewControllerFragment.this.adapterFollower.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageFollowerViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveFollowerOfUser(userID, STMyPageFollowerViewControllerFragment.this.lastObjectID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageFollowerViewControllerFragment.this.follows = DataLoader.getFollowersOfStaff(userID);
                    STMyPageFollowerViewControllerFragment.this.users = new RealmList<STUser>();
                    for (STFollow follow : STMyPageFollowerViewControllerFragment.this.follows) {
                        STMyPageFollowerViewControllerFragment.this.users.add(follow.user);
                    }

                    if (STMyPageFollowerViewControllerFragment.this.adapterFollower == null)
                        createAdapterFollower();

                    STMyPageFollowerViewControllerFragment.this.adapterFollower = (MyAdapterFollower) STMyPageFollowerViewControllerFragment.this.gridView.getAdapter();
                    STMyPageFollowerViewControllerFragment.this.adapterFollower.setFollowers(STMyPageFollowerViewControllerFragment.this.users);
                    STMyPageFollowerViewControllerFragment.this.adapterFollower.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageFollowerViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}