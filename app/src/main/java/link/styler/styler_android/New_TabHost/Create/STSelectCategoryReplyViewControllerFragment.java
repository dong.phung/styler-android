package link.styler.styler_android.New_TabHost.Create;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import link.styler.Utils.STWearCategory;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterCategoryName;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STSelectCategoryReplyViewControllerFragment extends Fragment {

    //model
    private ArrayList<String> categoryNames;

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController navigation;
    private ListView listView;

    //helper
    private MyAdapterCategoryName adapterCategoryName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryNames = new ArrayList<String>(Arrays.asList(STWearCategory.japName));
        adapterCategoryName = new MyAdapterCategoryName(getActivity(),
                R.layout.st_category_name_cell,
                categoryNames);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_select_category_reply_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        listView = (ListView) view.findViewById(R.id.listView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText("Replyを作成する");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        listView.setAdapter(adapterCategoryName);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                STReplySelectPostViewControllerFragment fragment = new STReplySelectPostViewControllerFragment();
                fragment.setCategoryID(STWearCategory.ID[position]);
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STSelectCReplyVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STSelectCReplyVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STSelectCReplyVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STSelectCReplyVCF", "onDestroy");
    }
}
