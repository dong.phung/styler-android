package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import link.styler.Utils.Logger;
import link.styler.Utils.SUtils;
import link.styler.styler_android.R;

/**
 * Created by admin on 3/7/17.
 */

public class ListImageRecyclerViewAdapter extends RecyclerView.Adapter<ListImageRecyclerViewAdapter.MyViewHolder> {

    private Context context;
    private List<String> listURLImage;

    public ListImageRecyclerViewAdapter(Context context, List<String> urlImage){
        Logger.print("ListImageRecyclerViewAdapter urlImage "+urlImage);
        this.context = context;
        this.listURLImage = urlImage;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Logger.print("onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_row_layout,null);
        MyViewHolder  viewHolder = new MyViewHolder(view);
        Logger.print("onCreateViewHolder viewHolder "+viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Logger.print("onBindViewHolder holder " +holder+" position "+position );
        SUtils.LoadImageIntoViewImagAndWHCircleTransform(context,listURLImage.get(position),holder.imageView,160,160);
    }


    @Override
    public int getItemCount() {
        return listURLImage.size();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{

        public ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imagerow);
            Logger.print("MyViewHolder "+imageView);
        }
    }
}
