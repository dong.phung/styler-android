package link.styler.styler_android.New_TabHost.Home;

import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STLocation;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterShopArea;
import link.styler.styler_android.R;


public class STShopAreaFragment extends STBaseListFragment {

    //model
    private RealmResults<STLocation> stLocations;
    private RealmList<STLocation> locations = new RealmList<>();

    //view
    private PullRefreshLayout pullRefreshLayout;

    //helper
    private boolean isLoading = false;
    private MyAdapterShopArea adapterShopArea = null;

    public void createAdapterShopArea() {
        this.stLocations = DataLoader.getLocationAll();
        this.stLocations = stLocations.where().greaterThan("locationID", 0).findAll();
        this.locations = new RealmList<>();
        this.locations.addAll(stLocations);

        adapterShopArea = new MyAdapterShopArea(getActivity(), locations);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        referenceMyself(this);
        if (adapterShopArea == null)
            createAdapterShopArea();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_st_shop_area_layout, container, false);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefresh_following);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        return view;
    }

    private void onRefreshData() {
        refreshData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setAdapter(adapterShopArea);
        refreshData();
    }

    private void refreshData() {
        if (isLoading)
            return;
        isLoading = true;

        if (adapterShopArea == null)
            createAdapterShopArea();

        final STShopAreaFragment self = (STShopAreaFragment) weakSelf.get();
        if (self == null) {
            return;
        }

        DataPreserver.saveLocations(new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if (isSuccess) {
                    self.stLocations = DataLoader.getLocationAll();
                    self.stLocations = stLocations.where().greaterThan("locationID", 0).findAll();
                    self.locations = new RealmList<STLocation>();
                    self.locations.addAll(self.stLocations);

                    self.adapterShopArea.setStLocations(self.locations);
                    self.adapterShopArea.notifyDataSetChanged();
                }
                isLoading = true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STShopAreaF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STShopAreaF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STShopAreaF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STShopAreaF", "onDestroy");
    }
}
