package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import link.styler.Utils.Logger;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STShop;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/25/17.
 */

public class STReplyShopDetailView {

    private ImageView shopImageView;
    private TextView shopNameLabel;
    private TextView addressLabel;

    private STShopStatusView stShopStatusView;

    private Context context;

    //use Activity
    public STReplyShopDetailView(Activity activity) {
        Logger.print("STReplyShopDetailView");
        initSTReplyShopDetailView(activity);
        this.context = activity.getApplicationContext();
    }

    private void initSTReplyShopDetailView(Activity activity) {
        Logger.print("STReplyShopDetailView initSTReplyShopDetailView");
        shopImageView = (ImageView) activity.findViewById(R.id.replyShopImageView);
        shopNameLabel = (TextView) activity.findViewById(R.id.replyShopNameLable);
        addressLabel = (TextView) activity.findViewById(R.id.replyShopAddressLabel);

        stShopStatusView = new STShopStatusView((View) activity.findViewById(R.id.stShopStatus_ShopDetail));
    }

    //use View
    public STReplyShopDetailView(View view) {
        initSTReplyShopDetailView(view);
        this.context = view.getContext();
    }

    private void initSTReplyShopDetailView(View view) {
        Logger.print("STReplyShopDetailView initSTReplyShopDetailView");
        shopImageView = (ImageView) view.findViewById(R.id.replyShopImageView);
        shopNameLabel = (TextView) view.findViewById(R.id.replyShopNameLable);
        addressLabel = (TextView) view.findViewById(R.id.replyShopAddressLabel);

        stShopStatusView = new STShopStatusView(view.findViewById(R.id.stShopStatus_ShopDetail));
    }

    public void setUpSTReplyShopDetailView(STShop stShop) {
        Logger.print("STReplyShopDetailView setUpSTReplyShopDetailView stShop " + stShop);
        String shopURL = stShop.imageURL;
        if (!shopURL.isEmpty())
            Picasso.with(context).load(shopURL).transform(new TransformationUtils().new RoundedCornersTransform()).into(shopImageView);

        shopNameLabel.setText(stShop.name);

        if (!stShop.address.isEmpty())
            addressLabel.setText(stShop.address);
        Log.i("Tan017", "address: " + stShop.address);

        List textStatusShop = new ArrayList();
        textStatusShop.add("" + stShop.itemCount);
        textStatusShop.add("" + stShop.eventCount);
        textStatusShop.add("" + stShop.staffCount);
        textStatusShop.add("Replies");
        textStatusShop.add("Events");
        textStatusShop.add("Staffs");
        textStatusShop.add("ショップ詳細をみる");

        stShopStatusView.setUpSTShopStatusView(textStatusShop);
    }

    public STShopStatusView getStShopStatusView() {
        return stShopStatusView;
    }
}
