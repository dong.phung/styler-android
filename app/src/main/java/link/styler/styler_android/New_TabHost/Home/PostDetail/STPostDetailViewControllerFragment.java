package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STPost;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.New_TabHost.PopUpMoreActionPost;
import link.styler.styler_android.R;

public class STPostDetailViewControllerFragment extends Fragment {

    ///model,API
    private RealmResults<STItemSort> itemSorts;
    private RealmList<STItem> items = new RealmList<>();
    private STPost post = new STPost();
    private int postID = 0;
    private Integer lastObjectID = null;
    private MyAdapterItem adapterItem = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private STSinglePostHeaderView headerView;
    private STShowCommentView showCommentView;
    private TextView replyCountLabel;
    private GridView collectionView;
    private ScrollView scrollView;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private STEnums.resumeType resumeType = STEnums.resumeType.none;
    private int resumeCreateReplyID;

    public void setResumeType(STEnums.resumeType resumeType) {
        this.resumeType = resumeType;
    }

    public void setResumeCreateReplyID(int resumeCreateReplyID) {
        this.resumeCreateReplyID = resumeCreateReplyID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    private void createAdapterItem() {
        itemSorts = DataLoader.getItemsWithPostID(postID);
        items = new RealmList<>();
        for (STItemSort itemSort : itemSorts) {
            items.add(itemSort.item);
        }
        adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.defaultType);
        adapterItem.setPostID(postID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        post = DataLoader.getPost(postID);
        if (adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_post_detail_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        // Create view: new include, findViewById
        // in ViewDidLoad, InitView

        navigation = new STBaseNavigationController(view);
        headerView = new STSinglePostHeaderView(view, getActivity());
        showCommentView = new STShowCommentView(view, getActivity());
        replyCountLabel = (TextView) view.findViewById(R.id.replyCountLabelPostDetail);
        collectionView = (GridView) view.findViewById(R.id.collectionView);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //fill data to view
        navigation.getTitleNavigation().setText("Post");
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getUpdateButton().setVisibility(View.GONE);
        navigation.getMoreButton().setImageResource(R.drawable.icon_more2x);

        if (postID == 0)
            return;

        post = DataLoader.getPost(postID);
        if (post != null) {
            fillDataPostToView();
        }

        DataPreserver.savePost(postID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                post = DataLoader.getPost(postID);
                if (post != null) {
                    fillDataPostToView();
                } else {
                    post = new STPost();
                    fillDataPostToView();
                }
                progressBar.setVisibility(View.GONE);
            }
        });

        collectionView.setAdapter(adapterItem);
        //set height gridView
        setGridViewHeightBasedOnChildren(collectionView, 2);

        progressBar.setVisibility(View.VISIBLE);
        refreshAdapter();

        addAction();
    }

    private void fillDataPostToView() {
        STDateFormat dateFormat = new STDateFormat();
        String date = dateFormat.DateFormat(post.createdAt);
        navigation.getSubTileNavigation().setText(date);

        headerView.configData(post);

        showCommentView.configData(post);

        replyCountLabel.setText("スタッフからのおすすめ (" + post.repliesCount + ")");
    }

    private void addAction() {
        // click more button
        navigation.getMoreButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                //chest login;
                if (status == "" || status.equals("none")) {
                    PopUpLogin login = new PopUpLogin(getActivity());
                    login.show();
                } else {
                    PopUpMoreActionPost moreAction = new PopUpMoreActionPost(getActivity(), post);
                    moreAction.show();
                }
            }
        });

        //action previous in Fragment (không có trong các activity)
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        //check scroll to bottom (scrollview != gridView )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                    if (diff <= 0 && !gotData) {
                        gotData = true;
                        getData();

                    } else if (diff > 145 && gotData) {
                        gotData = false;
                    }
                }
            });
        }
    }

    private void refreshAdapter() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveReplies(postID, null, STEnums.ItemSortType.None.getItemSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STPostDetailViewControllerFragment.this.itemSorts = DataLoader.getItemsWithPostID(STPostDetailViewControllerFragment.this.postID);
                    STPostDetailViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STItemSort itemSort : STPostDetailViewControllerFragment.this.itemSorts) {
                        STPostDetailViewControllerFragment.this.items.add(itemSort.item);
                    }
                    STPostDetailViewControllerFragment.this.adapterItem.setItems(STPostDetailViewControllerFragment.this.items);
                    adapterItem.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(collectionView, 2);
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STPostDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                STPostDetailViewControllerFragment.this.isLoading = false;
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveReplies(postID, lastObjectID, STEnums.ItemSortType.None.getItemSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STPostDetailViewControllerFragment.this.itemSorts = DataLoader.getItemsWithPostID(STPostDetailViewControllerFragment.this.postID);
                    STPostDetailViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STItemSort itemSort : STPostDetailViewControllerFragment.this.itemSorts) {
                        STPostDetailViewControllerFragment.this.items.add(itemSort.item);
                    }
                    STPostDetailViewControllerFragment.this.adapterItem.setItems(STPostDetailViewControllerFragment.this.items);
                    adapterItem.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(collectionView, 2);
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STPostDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                STPostDetailViewControllerFragment.this.isLoading = false;
                STPostDetailViewControllerFragment.this.progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        try {
            ListAdapter listAdapter = gridView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int items = listAdapter.getCount();
            float rows = 0;

            View listItem = listAdapter.getView(0, null, gridView);
            listItem.measure(0, 0);
            totalHeight = listItem.getMeasuredHeight();

            float x = 1;
            if (items > columns) {
                x = Math.round((items + 0.5) / columns);
                Log.i("1234567aaaa", "x: " + x);
                rows = (float) (x + 0.5);
                totalHeight *= rows;
            }

            ViewGroup.LayoutParams params = gridView.getLayoutParams();
            params.height = totalHeight;
            gridView.setLayoutParams(params);
        } catch (Exception e) {
        }
    }

    public void onResume() {
        super.onResume();
        if (this.resumeType == STEnums.resumeType.createReply) {
            STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
            fragment.setItemID(this.resumeCreateReplyID);
            fragment.setPostID(this.postID);
            fragment.setViewType(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType());
            getFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.tabcontent, fragment)
                    .addToBackStack(null)
                    .commit();
            resumeType = STEnums.resumeType.none;
        }
        Log.i("STPostDetailVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i("STPostDetailVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STPostDetailVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STPostDetailVCF", "onDestroy");
    }
}
