package link.styler.styler_android.New_TabHost.Article.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.Utils.STDateFormat;
import link.styler.models.STArticle;
import link.styler.styler_android.R;

/**
 * Created by macOS on 6/6/17.
 */

public class CoordinateAdapter extends BaseAdapter {

    //model
    private RealmList<STArticle> articles;

    //view
    private ImageView imgCell;
    private TextView txtCellTitle;
    private TextView txtCellDate;
    private ProgressBar progressBar;

    //hepler
    private Context context;

    public CoordinateAdapter(Context context, RealmList<STArticle> articles) {
        this.context = context;
        this.articles = articles;
    }

    public void setArticles(RealmList<STArticle> articles) {
        this.articles = articles;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return articles.get(position).articleID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_coordinate_collection_cell, null);
        intView(convertView);

        setupView(articles.get(position));
        addControl(articles.get(position));
        return convertView;
    }

    private void intView(View view) {
        imgCell = (ImageView) view.findViewById(R.id.imgArticlesCell);
        txtCellTitle = (TextView) view.findViewById(R.id.txtArticlesCellTitle);
        txtCellDate = (TextView) view.findViewById(R.id.txtArticlesCellDate);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    }

    private void setupView(STArticle article) {
        if (article != null) {
            if (!article.imageURL.isEmpty())
                Picasso.with(context)
                        .load(article.imageURL)
                        //.placeholder(R.drawable.default_avatar)
                        .into(imgCell);

            txtCellTitle.setText(article.title);
            STDateFormat dateFormat = new STDateFormat();
            String date = dateFormat.DateFormat(article.publishedAt);
            txtCellDate.setText(date);
        }
    }

    private void addControl(final STArticle article) {

    }
}
