package link.styler.styler_android.New_TabHost.Home;

import android.support.v4.app.ListFragment;
import android.os.Bundle;

import java.lang.ref.WeakReference;

/**
 * Created by dongphung on 4/28/17.
 */

public class STBaseListFragment extends ListFragment {
    WeakReference<STBaseListFragment> weakSelf;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void referenceMyself(STBaseListFragment self) {
        weakSelf = new WeakReference<STBaseListFragment>(self);
    }
}
