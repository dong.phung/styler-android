package link.styler.styler_android.New_TabHost.Create;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STItem;
import link.styler.models.STItemSort;

import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/13/17.
 */

public class STReplySelectPastItemViewControllerFragment extends Fragment {

    //model
    private RealmResults<STItemSort> itemSorts;
    private RealmList<STItem> items = new RealmList<>();
    private Integer shopID = null;
    private Integer postID = null;
    private Integer categoryID = null;
    private Integer lastObjectID = null;

    //view
    private PullRefreshLayout pullRefreshLayout;
    private GridView gridView;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterItem adapterItem = null;

    private void createAdapterItem() {
        itemSorts = DataLoader.getItemsOfShopWithCategory(shopID, categoryID);
        items = new RealmList<STItem>();
        for (STItemSort stItemSort : itemSorts) {
            items.add(stItemSort.item);
        }
        adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.noAvatar);
        adapterItem.setMode(STEnums.MyAdapterViewMode.createReply);
        adapterItem.setPostID(postID);
    }

    public void ItemFindByCategoryFragment(Integer postID, Integer categoryID) {
        this.postID = postID;
        this.categoryID = categoryID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            shopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
            shopID = null;
        }

        if (shopID == null || postID == null || categoryID == null)
            return;
        if (adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_popular_item_layout, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Item);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        if (shopID == null || postID == null || categoryID == null)
            return;
        gridView.setAdapter(adapterItem);
        if (adapterItem.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveItemsOfShopWithCategory(shopID, categoryID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    itemSorts = DataLoader.getItemsOfShopWithCategory(shopID, categoryID);
                    items = new RealmList<STItem>();
                    for (STItemSort stItemSort : itemSorts) {
                        items.add(stItemSort.item);
                    }
                    adapterItem.setItems(items);
                    adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STReplySelectPastItemViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveItemsOfShopWithCategory(shopID, categoryID, lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    itemSorts = DataLoader.getItemsOfShopWithCategory(shopID, categoryID);
                    items = new RealmList<STItem>();
                    for (STItemSort stItemSort : itemSorts) {
                        items.add(stItemSort.item);
                    }
                    adapterItem.setItems(items);
                    adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STReplySelectPastItemViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STReplySelectPItemVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STReplySelectPItemVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STReplySelectPItemVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STReplySelectPItemVCF", "onDestroy");
    }
}
