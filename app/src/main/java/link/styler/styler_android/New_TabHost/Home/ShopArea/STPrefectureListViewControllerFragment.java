package link.styler.styler_android.New_TabHost.Home.ShopArea;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.Sort;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STShop;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterSTPrefectureDetail;
import link.styler.styler_android.R;

/**
 * Created by macOS on 5/9/17.
 */

public class STPrefectureListViewControllerFragment extends Fragment {

    //model
    private int locationID;
    private RealmList<STShop> shops = new RealmList<>();
    private Integer lastObjectID = null;

    //view
    private PullRefreshLayout pullRefreshLayout;
    private GridView gridViewPrefecture;
    private ProgressBar progressBar;

    //helper
    boolean isLoading = false;
    private MyAdapterSTPrefectureDetail adapterSTPrefectureDetail = null;
    private boolean gotData = false;

    private void createAdapterSTPrefectureDetail() {
        shops = new RealmList<>();
        shops.addAll(DataLoader.getShopList(locationID).sort("name", Sort.ASCENDING));
        adapterSTPrefectureDetail = new MyAdapterSTPrefectureDetail(getActivity(), shops);
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public STPrefectureListViewControllerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterSTPrefectureDetail == null) {
            createAdapterSTPrefectureDetail();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_prefecture_list_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Prefecture);
        gridViewPrefecture = (GridView) view.findViewById(R.id.gridViewPrefecture);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        gridViewPrefecture.setAdapter(adapterSTPrefectureDetail);

        refreshData();
        addControl();

    }

    private void addControl() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        gridViewPrefecture.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        refreshData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void refreshData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveShops(locationID, null, null, STEnums.ShopSortType.New.getShopSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    shops = new RealmList<STShop>();
                    shops.addAll(DataLoader.getShopList(locationID).sort("name", Sort.ASCENDING));

                    if (adapterSTPrefectureDetail == null)
                        createAdapterSTPrefectureDetail();
                    adapterSTPrefectureDetail.setShops(shops);
                    adapterSTPrefectureDetail.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STPrefectureListViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.saveShops(locationID, null, lastObjectID, STEnums.ShopSortType.New.getShopSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    shops = new RealmList<STShop>();
                    shops.addAll(DataLoader.getShopList(locationID).sort("name", Sort.ASCENDING));

                    if (adapterSTPrefectureDetail == null)
                        createAdapterSTPrefectureDetail();
                    adapterSTPrefectureDetail.setShops(shops);
                    adapterSTPrefectureDetail.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STPrefectureListViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("STPrefectureListVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STPrefectureListVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STPrefectureListVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STPrefectureListVCF", "onDestroy");
    }
}