package link.styler.styler_android.New_TabHost.Article;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import link.styler.Utils.STShareAction;
import link.styler.styler_android.R;

public class STArticleWebViewControllerActivity extends AppCompatActivity {
    //don't convert Activity to Fragment

    private ImageButton btnDone;
    private TextView txtUrl;
    private ImageButton btnReload;
    private WebView webViewArticlesSingle;
    private ImageButton btnBack;
    private ImageButton btnForward;
    private ImageButton btnCombined;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_article_web_view_controller);
        Intent intent = getIntent();
        try {
            url = intent.getStringExtra("URL");
        } catch (Exception e) {
        }

        addControls();
        addEvents();
    }

    private void addControls() {
        btnDone = (ImageButton) findViewById(R.id.btnDone);
        txtUrl = (TextView) findViewById(R.id.txtUrl);
        btnReload = (ImageButton) findViewById(R.id.btnReload);
        webViewArticlesSingle = (WebView) findViewById(R.id.webViewArticlesSingle);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        btnCombined = (ImageButton) findViewById(R.id.btnCombined);

        txtUrl.setText(url);
        webViewArticlesSingle.setWebViewClient(new WebViewClient());
        webViewArticlesSingle.loadUrl(url);
        WebSettings webSettings = webViewArticlesSingle.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void addEvents() {
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewArticlesSingle.reload();
                txtUrl.setText(webViewArticlesSingle.getUrl());
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webViewArticlesSingle.canGoBack()) {
                    webViewArticlesSingle.goBack();
                    txtUrl.setText(webViewArticlesSingle.getUrl());
                }
            }
        });
        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webViewArticlesSingle.canGoForward()) {
                    webViewArticlesSingle.goForward();
                    txtUrl.setText(webViewArticlesSingle.getUrl());
                }
            }
        });
        btnCombined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openShareMode();
            }
        });
    }

    private void openShareMode() {
        STShareAction shareAction = new STShareAction();
        shareAction.ShareAction(STArticleWebViewControllerActivity.this, url);
    }

}
