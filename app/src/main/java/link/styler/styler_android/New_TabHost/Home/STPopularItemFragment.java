package link.styler.styler_android.New_TabHost.Home;

import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STItem;
import link.styler.models.STPopularItem;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.R;

public class STPopularItemFragment extends STBaseFragment {

    //model,api
    private RealmResults<STPopularItem> popularItems;
    private RealmList<STItem> items = new RealmList<STItem>();
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private MyAdapterItem adapterItem = null;
    private boolean gotData = false;

    public void createAdapterItem() {
        this.popularItems = DataLoader.getPopularItems();

        this.items = new RealmList<STItem>();
        for (STPopularItem popularItem : popularItems) {
            this.items.add(popularItem.item);
        }
        this.adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.noAvatar);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        referenceMyself(this);
        if (adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_popular_item_layout, container, false);

        gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setAdapter(adapterItem);
        if (adapterItem.getCount() == 0)
            refreshData();

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Item);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        return view;
    }

    private void onRefreshData() {
        refreshData();
        pullRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void refreshData() {
        if (isLoading)
            return;
        isLoading = true;
        final STPopularItemFragment self = (STPopularItemFragment) weakSelf.get();
        DataPreserver.savePopularItems(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.popularItems = DataLoader.getPopularItems();

                    self.items = new RealmList<STItem>();
                    for (STPopularItem popularItem : self.popularItems) {
                        self.items.add(popularItem.item);
                    }

                    if (self.adapterItem == null)
                        createAdapterItem();

                    self.adapterItem = (MyAdapterItem) self.gridView.getAdapter();
                    self.adapterItem.setItems(self.items);
                    self.adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        final STPopularItemFragment self = (STPopularItemFragment) weakSelf.get();
        DataPreserver.savePopularItems(self.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.popularItems = DataLoader.getPopularItems();

                    self.items = new RealmList<STItem>();
                    for (STPopularItem popularItem : self.popularItems) {
                        self.items.add(popularItem.item);
                    }

                    if (self.adapterItem == null)
                        createAdapterItem();

                    self.adapterItem = (MyAdapterItem) self.gridView.getAdapter();
                    self.adapterItem.setItems(self.items);
                    self.adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STPopularItemF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STPopularItemF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STPopularItemF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STPopularItemF", "onDestroy");
    }
}
