package link.styler.styler_android.New_TabHost.MyPage;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STItem;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageItemViewControllerFragment extends Fragment {

    //model,api
    private RealmResults<STItem> stItems;
    private RealmList<STItem> items = new RealmList<STItem>();
    private int shopID = 0;
    private Integer lastObjectID = null;

    //view
    private GridView collectionView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterItem adapterItem = null;

    public void createAdapterItem() {
        this.stItems = DataLoader.getItemsOfShopWithSTItem(shopID).sort("createdAt", Sort.DESCENDING);
        this.items = new RealmList<STItem>();
        this.items.addAll(STMyPageItemViewControllerFragment.this.stItems);
        this.adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.noAvatar);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_item_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_item);
        collectionView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        return view;
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        STMyPageItemViewControllerFragment.this.collectionView.setAdapter(adapterItem);
        if (adapterItem.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        STMyPageItemViewControllerFragment.this.collectionView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STNewArrivalsPostF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STNewArrivalsPostF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STNewArrivalsPostF", "totalItemCount: " + totalItemCount);
                    Log.i("STNewArrivalsPostF", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STNewArrivalsPostF", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STNewArrivalsPostF", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveItemsOfShop(shopID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageItemViewControllerFragment.this.stItems = DataLoader.getItemsOfShopWithSTItem(shopID).sort("createdAt", Sort.DESCENDING);
                    STMyPageItemViewControllerFragment.this.items = new RealmList<STItem>();
                    STMyPageItemViewControllerFragment.this.items.addAll(STMyPageItemViewControllerFragment.this.stItems);

                    if (STMyPageItemViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageItemViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageItemViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageItemViewControllerFragment.this.adapterItem.setItems(STMyPageItemViewControllerFragment.this.items);
                    STMyPageItemViewControllerFragment.this.adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageItemViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveItemsOfShop(shopID, STMyPageItemViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageItemViewControllerFragment.this.lastObjectID = lastObjectID;
                    STMyPageItemViewControllerFragment.this.stItems = DataLoader.getItemsOfShopWithSTItem(shopID).sort("createdAt", Sort.DESCENDING);
                    STMyPageItemViewControllerFragment.this.items = new RealmList<STItem>();
                    STMyPageItemViewControllerFragment.this.items.addAll(STMyPageItemViewControllerFragment.this.stItems);

                    if (STMyPageItemViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageItemViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageItemViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageItemViewControllerFragment.this.adapterItem.setItems(STMyPageItemViewControllerFragment.this.items);
                    STMyPageItemViewControllerFragment.this.adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageItemViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }
}