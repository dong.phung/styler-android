package link.styler.styler_android.New_TabHost.Home.Common;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;

import link.styler.STStructs.STEnums;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.R;

import static link.styler.STStructs.STEnums.MyPageType.MyShopPage;
import static link.styler.STStructs.STEnums.MyPageType.OtherShopPage;

/**
 * Created by macOS on 3/17/17.
 */

public class STNavigationTabView {

    //view
    private ViewPager viewPager;

    //
    private String[] titles;
    private int countTitles;
    private STEnums.MyPageType pageType;
    private Integer userID = null;
    private Integer shopID = null;
    private ViewPagerAdapter adapter;
    Context context;

    public STNavigationTabView(Activity activity) {
        initSTNavigationTabView(activity);
        this.context = activity.getApplicationContext();
        //addControls();
        //setDataToView(this.adapter);
    }

    private void setDataToView(ViewPagerAdapter adapter) {
        switch (pageType) {
            case MyCustomerPage:
                /*adapter.addFrag(new STMyPageWatchViewControllerFragment(),titles[1]);       //0
                adapter.addFrag(new STMyPageLikeViewControllerFragment(),titles[2]);        //1

                adapter.addFrag(new STMyPageEventViewControllerFragment(),titles[3]);       //2
                adapter.addFrag(new STMyPageFollowingViewControllerFragment(),titles[4]);   //3
                adapter.addFrag(new setUserID(),titles[0]);        //4 <=
                adapter.addFrag(new STMyPageWatchViewControllerFragment(),titles[1]);       //5
                adapter.addFrag(new STMyPageLikeViewControllerFragment(),titles[2]);        //6

                adapter.addFrag(new STMyPageEventViewControllerFragment(),titles[3]);       //7
                adapter.addFrag(new STMyPageFollowingViewControllerFragment(),titles[4]);   //8

                Log.i("STNavigationTabView", "case MyCustomerPage");
                adapter.addFrag(new STNewArrivalsPostFragment(),"新着Post (New Post)");
                adapter.addFrag(new STPopularItemFragment(),"注目アイテム (Featured item)");
                adapter.addFrag(new STShopAreaFragment(),"ショップ (Shop)");
                adapter.addFrag(new STEventFragment(),"イベント (Event)");
                adapter.addFrag(new STPopularPostFragment(),"人気Post (Popular Post)");
                adapter.addFrag(new STNewArrivalsPostFragment(),"新着Post (New Post)");
                adapter.addFrag(new STPopularItemFragment(),"注目アイテム (Featured item)");
                adapter.addFrag(new STShopAreaFragment(),"ショップ (Shop)");
                adapter.addFrag(new STEventFragment(),"イベント (Event)");


                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(4);
                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        int currentPage = viewPager.getCurrentItem();

                        if(viewPager.getCurrentItem() == 7 && state == 0){
                            viewPager.setCurrentItem(2,false);
                        }

                        if(currentPage == 1 && state == 0){
                            viewPager.setCurrentItem(6,false);
                        }
                    }
                });*/
                break;
            default:
                break;
        }

    }


    private void addControls() {

    }

    private void initSTNavigationTabView(Activity activity) {
        viewPager = (ViewPager) activity.findViewById(R.id.viewPager);
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
        this.countTitles = titles.length;
    }

    public void setTab(STEnums.MyPageType pageType, int userID, ViewPagerAdapter adapter) {
        this.pageType = pageType;
        this.adapter = adapter;
        if (pageType == OtherShopPage || pageType == MyShopPage)
            this.shopID = userID;
        else
            this.userID = userID;
    }
}
