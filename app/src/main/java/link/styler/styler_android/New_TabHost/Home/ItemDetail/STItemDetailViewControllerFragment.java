package link.styler.styler_android.New_TabHost.Home.ItemDetail;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.STShareAction;
import link.styler.models.STItem;
import link.styler.models.STItemSort;
import link.styler.models.STPost;
import link.styler.models.STReply;
import link.styler.models.STShop;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.New_TabHost.PopUpMoreActionItem;
import link.styler.styler_android.R;

public class STItemDetailViewControllerFragment extends Fragment {

    //model
    private STItem item;
    private STReply reply = new STReply();
    private STPost post;

    private int itemID = 0;
    private int replyID = 0;
    private int postID = 0;
    private int userID = 0;
    private Integer lastObjectID = null;


    RealmResults<STItemSort> itemSorts;
    RealmList<STItem> items = new RealmList<STItem>();

    private MyAdapterItem adapterItem = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private STReplyItemDetailView replyItemView;
    private STReplyShopDetailView shopView;
    private LinearLayout shopDetailView;
    private STReplyItemDescriptionView itemDescriptionView;
    private STStaffCommentView staffCommentViews;
    private LinearLayout layoutReviewView;
    private STReplyReviewView reviewView;
    private TextView relationLabel;
    private GridView relationItemCollectionView;
    private ScrollView scrollView;
    private ProgressBar progressBar;

    //helper
    private String viewType = STEnums.STItemDetailViewType.Item.getSTItemDetailViewType();
    private boolean isLoading = false;
    private boolean gotData = false;

    private boolean checkPost = false;
    private boolean checkItem = false;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;

    public void createAdapterItem() {
        items = new RealmList<>();
        if (viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType())) {
            itemSorts = DataLoader.getItemsOfShopWithSTItemSort(item.shopID).sort("itemID", Sort.DESCENDING);
            for (STItemSort itemSort : itemSorts) {
                items.add(itemSort.item);
            }
            adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.noAvatar);
        } else {
            itemSorts = DataLoader.getItemsWithPostID(postID).sort("itemID", Sort.DESCENDING);
            for (STItemSort itemSort : itemSorts) {
                items.add(itemSort.item);
            }
            adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.defaultType);
            adapterItem.setPostID(postID);
        }
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public void setReplyID(int replyID) {
        this.replyID = replyID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
        Log.i("11111111", "replyID: " + replyID);
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public static void setResumeType(STEnums.resumeType resumeType) {
        STItemDetailViewControllerFragment.resumeType = resumeType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (postID != 0) {
            post = DataLoader.getPost(postID);
        }
        item = DataLoader.getItemWithItemID(itemID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_item_detail_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        replyItemView = new STReplyItemDetailView(view, getActivity());
        shopView = new STReplyShopDetailView(view);
        shopDetailView = (LinearLayout) view.findViewById(R.id.shopDetailView);
        itemDescriptionView = new STReplyItemDescriptionView(view);
        staffCommentViews = new STStaffCommentView(view, getActivity());
        layoutReviewView = (LinearLayout) view.findViewById(R.id.layoutReviewView);
        reviewView = new STReplyReviewView(view);
        relationLabel = (TextView) view.findViewById(R.id.relationLabel);
        relationItemCollectionView = (GridView) view.findViewById(R.id.gridView);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fillDataToView();

        addAction();
    }

    private void fillDataToView() {
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getUpdateButton().setVisibility(View.GONE);
        navigation.getMoreButton().setImageResource(R.drawable.icon_more2x);

        if (postID != 0) {
            if (post == null) {
                progressBar.setVisibility(View.VISIBLE);
                DataPreserver.savePost(postID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        progressBar.setVisibility(View.GONE);
                        post = DataLoader.getPost(postID) != null ? DataLoader.getPost(postID) : new STPost();

                        checkPost = true;
                        if (checkPost && checkItem)
                            fillMainItem();
                    }
                });
            } else {
                checkPost = true;
                if (checkPost && checkItem)
                    fillMainItem();
            }
        } else {
            checkPost = true;
            if (checkPost && checkItem)
                fillMainItem();
        }

        if (item != null)
            fillMainItem();

        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveItem(itemID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                progressBar.setVisibility(View.GONE);
                item = DataLoader.getItemWithItemID(itemID) != null ? DataLoader.getItemWithItemID(itemID) : new STItem();
                checkItem = true;
                if (checkPost && checkItem)
                    fillMainItem();
            }
        });

    }

    private void fillMainItem() {
        Log.i("STItemDetailVCF", "fillMainItem");
        if (item == null) {
            progressBar.setVisibility(View.GONE);
            return;
        }

        if (viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType()))
            navigation.getTitleNavigation().setText("Item");
        else
            navigation.getTitleNavigation().setText("Reply");

        String date = !item.createdAt.isEmpty() ? item.createdAt : "";
        STDateFormat dateFormat = new STDateFormat();
        date = dateFormat.DateFormat(date);
        navigation.getSubTileNavigation().setText(date);

        replyItemView.setUpSTReplyItemDetailView(item, reply);
        itemDescriptionView.setUpSTReplyItemDescriptionView(item);


        STShop shop = DataLoader.getShop(item.shopID);
        if (shop == null) {
            DataPreserver.saveShop(item.shopID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (DataLoader.getShop(item.shopID) != null)
                        shopView.setUpSTReplyShopDetailView(DataLoader.getShop(item.shopID));
                    else
                        shopView.setUpSTReplyShopDetailView(new STShop());
                }
            });
        } else {
            shopView.setUpSTReplyShopDetailView(shop);
        }

        DataPreserver.saveItem(itemID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                item = DataLoader.getItemWithItemID(itemID);
                STReply reply = new STReply();
                if (!item.replies.isEmpty()) {
                    reply = item.replies.first();
                    if (postID != 0) {
                        for (STReply reply1 : item.replies) {
                            if (reply1.postID == postID) {
                                reply = reply1;
                                STItemDetailViewControllerFragment.this.replyID = reply1.replyID;
                            }
                        }
                    }
                    if (reply != null) {
                        staffCommentViews.setUpSTStaffCommentView(reply);
                    }
                }
                setupReviewView(reply);
                progressBar.setVisibility(View.GONE);
            }
        });
        loadListItem();
    }

    private void setupReviewView(STReply reply) {
        layoutReviewView.setVisibility(View.GONE);

        STPost post = DataLoader.getPost(postID) != null ? DataLoader.getPost(postID) : null;
        int meID;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {
            meID = 0;
        }
        if (viewType.equals(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType())) {
            if (!reply.feedbackFlag) {
                if (post.user != null && post.user.userID == meID && meID != 0)
                    layoutReviewView.setVisibility(View.VISIBLE);
                this.reply = reply;
            }
        }
    }

    private void loadListItem() {
        if (adapterItem == null) {
            createAdapterItem();
        }

        String relation = viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType()) ? (DataLoader.getShop(item.shopID).name + "のアイテム") : ("他のReply");
        relationLabel.setText(relation);
        relationItemCollectionView.setAdapter(adapterItem);
        setGridViewHeightBasedOnChildren(relationItemCollectionView, 2);
        //if (adapterItem.getCount() == 0) {
        progressBar.setVisibility(View.VISIBLE);
        refreshItemList();
        //}
    }

    private void refreshItemList() {
        if (isLoading)
            return;
        isLoading = true;
        if (viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType())) {
            DataPreserver.saveItemsOfShop(item.shopID, null, false, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    if (isSuccess) {
                        itemSorts = DataLoader.getItemsOfShopWithSTItemSort(STItemDetailViewControllerFragment.this.item.shopID).sort("itemID", Sort.DESCENDING);
                        items = new RealmList<STItem>();
                        for (STItemSort itemSort : itemSorts) {
                            items.add(itemSort.item);
                        }
                        if (adapterItem == null)
                            createAdapterItem();
                        adapterItem.setItems(items);
                        adapterItem.notifyDataSetChanged();
                        setGridViewHeightBasedOnChildren(relationItemCollectionView, 2);
                    }
                    if (lastObjectID != null && lastObjectID != 0)
                        STItemDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            DataPreserver.saveReplies(postID, null, STEnums.ItemSortType.None.getItemSortType(), false, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    if (isSuccess) {
                        itemSorts = DataLoader.getItemsWithPostID(postID).sort("itemID", Sort.DESCENDING);
                        items = new RealmList<STItem>();
                        for (STItemSort itemSort : itemSorts) {
                            items.add(itemSort.item);
                        }
                        if (adapterItem == null)
                            createAdapterItem();
                        adapterItem.setPostID(postID);
                        adapterItem.notifyDataSetChanged();
                        setGridViewHeightBasedOnChildren(relationItemCollectionView, 2);
                    }
                    if (lastObjectID != null && lastObjectID != 0)
                        STItemDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    private void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        try {
            ListAdapter listAdapter = gridView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int items = listAdapter.getCount();
            float rows = 0;

            View listItem = listAdapter.getView(0, null, gridView);
            listItem.measure(0, 0);
            totalHeight = listItem.getMeasuredHeight();

            float x = 1;
            if (items > columns) {
                x = Math.round((items + 0.5) / columns);
                rows = (float) (1.04 * x + 0.5);
                totalHeight *= rows;
            }

            ViewGroup.LayoutParams params = gridView.getLayoutParams();
            params.height = totalHeight;
            gridView.setLayoutParams(params);
        } catch (Exception e) {
        }
    }

    private void addAction() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                    if (diff <= 0 && !gotData) {
                        gotData = true;
                        getData();

                    } else if (diff > 145 && gotData) {
                        gotData = false;
                    }
                }
            });
        }
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        navigation.getMoreButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    PopUpLogin login = new PopUpLogin(getContext());
                    login.show();
                } else {
                    PopUpMoreActionItem moreAction = new PopUpMoreActionItem(getContext(), item, viewType);
                    if (viewType.equals(STEnums.STItemDetailViewType.Reply.getSTItemDetailViewType()))
                        Log.i("asdasdasdas", "replyID: " + replyID);
                    moreAction.setReplyID(replyID);
                    moreAction.show();
                }
            }
        });

        shopDetailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setShopID(item.shopID);
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        shopView.getStShopStatusView().getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setShopID(item.shopID);
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        replyItemView.getShareButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType())) {
                    String path = "items/" + itemID;
                    STShareAction shareAction = new STShareAction();
                    shareAction.ShareAction(getActivity(), path);
                } else {
                    for (STReply reply : item.replies) {
                        if (reply.postID == postID) {
                            replyID = reply.replyID;
                        }
                    }
                    String path = "replies/" + replyID;
                    STShareAction shareAction = new STShareAction();
                    shareAction.ShareAction(getActivity(), path);
                }
            }
        });

        reviewView.getStarView1().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewView.clickStar1();
                createFeedBack(1);
            }
        });
        reviewView.getStarView2().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewView.clickStar2();
                createFeedBack(2);
            }
        });
        reviewView.getStarView3().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewView.clickStar3();
                createFeedBack(3);
            }
        });
        reviewView.getStarView4().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewView.clickStar4();
                createFeedBack(4);
            }
        });
        reviewView.getStarView5().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewView.clickStar5();
                createFeedBack(5);
            }
        });
    }

    private void createFeedBack(final int num) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        DataSender.createFeedBack(num, reply.replyID, new STCompletion.Base() {
                            @Override
                            public void onRequestComplete(boolean isSuccess, STError err) {
                                if (isSuccess) {
                                    layoutReviewView.setVisibility(View.GONE);
                                    scrollView.scrollTo(0, 0);
                                }
                            }
                        });
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("確認");
        builder.setMessage("下記の内容で評価してもよろしいですか？\n" + num + "⭐️" + reviewView.getCommentLabel().getText().toString())
                .setPositiveButton("OK", dialogClickListener)
                .setNegativeButton("キャンセル", dialogClickListener).show();

    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        if (viewType.equals(STEnums.STItemDetailViewType.Item.getSTItemDetailViewType())) {
            relationLabel.setText(DataLoader.getShop(item.shopID).name + "のアイテム");
            DataPreserver.saveItemsOfShop(item.shopID, lastObjectID, false, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    if (isSuccess) {
                        itemSorts = DataLoader.getItemsOfShopWithSTItemSort(STItemDetailViewControllerFragment.this.item.shopID).sort("itemID", Sort.DESCENDING);
                        items = new RealmList<STItem>();
                        for (STItemSort itemSort : itemSorts) {
                            items.add(itemSort.item);
                        }
                        if (adapterItem == null)
                            createAdapterItem();
                        adapterItem.setItems(items);
                        adapterItem.notifyDataSetChanged();
                        setGridViewHeightBasedOnChildren(relationItemCollectionView, 2);
                    }
                    if (lastObjectID != null && lastObjectID != 0)
                        STItemDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            relationLabel.setText("他のReply");
            DataPreserver.saveReplies(postID, lastObjectID, STEnums.ItemSortType.None.getItemSortType(), false, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    if (isSuccess) {
                        itemSorts = DataLoader.getItemsWithPostID(postID).sort("itemID", Sort.DESCENDING);
                        items = new RealmList<STItem>();
                        for (STItemSort itemSort : itemSorts) {
                            items.add(itemSort.item);
                        }
                        if (adapterItem == null)
                            createAdapterItem();
                        adapterItem.setPostID(postID);
                        adapterItem.notifyDataSetChanged();
                        setGridViewHeightBasedOnChildren(relationItemCollectionView, 2);
                    }
                    if (lastObjectID != null && lastObjectID != 0)
                        STItemDetailViewControllerFragment.this.lastObjectID = lastObjectID;
                    isLoading = false;
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STItemDetailViewCF", "onResume");
        if (resumeType == STEnums.resumeType.editItem) {
            fillMainItem();
            resumeType = STEnums.resumeType.none;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STItemDetailViewCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STItemDetailViewCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STItemDetailViewCF", "onDestroy");
    }
}
