package link.styler.styler_android.New_TabHost;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.Utils.DataSharing;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.R;

public class LoginActivity extends AppCompatActivity {

    TextView txtCancel;
    EditText txtEmail, txtPassword;
    Button btnLogin;
    TextView txtForgotPassword;
    TextView txtSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addControls();
        addEvents();
    }

    private void addControls() {
        txtCancel = (TextView) findViewById(R.id.txtCancel);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        txtForgotPassword.setPaintFlags(txtForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtSignUp = (TextView) findViewById(R.id.txtSignUp);
        txtSignUp.setPaintFlags(txtSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void addEvents() {
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLogin.setEnabled(false);
                login();
            }
        });
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login() {
        Log.i("Tan006 ", txtEmail.getText().toString() + "");
        Log.i("Tan006 ", txtPassword.getText().toString() + "");
        LoginManager.getsIntstance().login(txtEmail.getText().toString(), txtPassword.getText().toString(), new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                btnLogin.setEnabled(true);
                if (err != null) {
                    Toast.makeText(LoginActivity.this, "Eメールまたパスワードが不正です", Toast.LENGTH_SHORT).show();
                    Log.i("Tan006 ", "err  " + err);
                    return;
                }
                if (isSuccess) {
                    Log.i("Tan006 ", "StylerApi.getAccessTokenBlock " + StylerApi.getAccessTokenBlock);
                    Log.i("Tan006 ", "StylerApi.getAccessTokenBlock " + DataSharing.getsIntstance().getAccessToken());
                    Log.i("Tan999 ", "me().userID) " + LoginManager.getsIntstance().me().userID);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Eメールまたパスワードが不正です", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
