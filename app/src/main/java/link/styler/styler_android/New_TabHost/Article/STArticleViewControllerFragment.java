package link.styler.styler_android.New_TabHost.Article;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STArticle;
import link.styler.models.STArticleSort;
import link.styler.models.STCategory;
import link.styler.models.STTag;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.InfiniteViewPagerHandler;
import link.styler.styler_android.New_TabHost.Common.STBlankFragment;
import link.styler.styler_android.R;
import link.styler.styler_android.New_TabHost.Article.Adapter.ArticlesAdapter;
import link.styler.styler_android.New_TabHost.Article.Adapter.CategoryAdapter;
import link.styler.styler_android.New_TabHost.Article.Adapter.KeywordAdapter;

public class STArticleViewControllerFragment extends Fragment {

    //model
    RealmResults<STArticleSort> featureArticles;
    RealmResults<STArticleSort> latestArticles;
    RealmList<STArticle> articles = new RealmList<STArticle>();

    RealmResults<STCategory> categories;

    Boolean isLoading = false;
    Integer lastObjID;

    //view
    private ScrollView sv1;
    private ProgressBar progressBar;
    private ViewPager vpArticlesHead;
    private ViewPagerAdapter viewPagerAdapter = null;
    private int homeViewPagerID = 1;

    private TextView txtNewArticle;
    private ListView lvNewArticles;
    private ArticlesAdapter articlesAdapter = null;
    private Button btnOpenNewArticlesList;

    private TextView txtCategory;
    private RecyclerView rvCategory;
    private CategoryAdapter categoryAdapter;
    private ArrayList<String> arrayListCategory;

    private TextView txtKeyWord;
    private RecyclerView rvKeyword;
    private KeywordAdapter keywordAdapter;
    private ArrayList<String> arrayListKeyword;

    private Button btnOpenKeywordList;

    //helper

    private void createViewPagerAdapter() {
        featureArticles = DataLoader.getFeatureArticles();
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        if (featureArticles != null && featureArticles.size() == 4) {

            viewPagerAdapter.addFrag(new STBlankFragment(), "");

            ArticlesTopFragment fragment3B = new ArticlesTopFragment();
            fragment3B.setArticlesItem(featureArticles.get(3).article);
            viewPagerAdapter.addFrag(fragment3B, "");

            ArticlesTopFragment fragment0 = new ArticlesTopFragment();
            fragment0.setArticlesItem(featureArticles.get(0).article);
            viewPagerAdapter.addFrag(fragment0, "");

            ArticlesTopFragment fragment1 = new ArticlesTopFragment();
            fragment1.setArticlesItem(featureArticles.get(1).article);
            viewPagerAdapter.addFrag(fragment1, "");

            ArticlesTopFragment fragment2 = new ArticlesTopFragment();
            fragment2.setArticlesItem(featureArticles.get(2).article);
            viewPagerAdapter.addFrag(fragment2, "");

            ArticlesTopFragment fragment3 = new ArticlesTopFragment();
            fragment3.setArticlesItem(featureArticles.get(3).article);
            viewPagerAdapter.addFrag(fragment3, "");

            ArticlesTopFragment fragment0B = new ArticlesTopFragment();
            fragment0B.setArticlesItem(featureArticles.get(0).article);
            viewPagerAdapter.addFrag(fragment0B, "");

            viewPagerAdapter.addFrag(new STBlankFragment(), "");
        } else {
            for (int i = 0; i < 9; i++) {
                ArticlesTopFragment fragment = new ArticlesTopFragment();
                fragment.setArticlesItem(new STArticle());
                viewPagerAdapter.addFrag(fragment, "");
            }
        }
    }

    private void createArticlesAdapter() {
        latestArticles = DataLoader.getArticles();
        if (latestArticles != null && latestArticles.size() >= 4) {
            articles = new RealmList<>();
            for (int i = 0; i < 4; i++) {
                articles.add(latestArticles.get(i).article);
            }
            articlesAdapter = new ArticlesAdapter(getActivity(), articles);
        } else {
            articles = new RealmList<>();
            for (int i = 0; i < 4; i++) {
                articles.add(new STArticle());
            }
            articlesAdapter = new ArticlesAdapter(getActivity(), articles);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("STArticleVCF", "onCreate");

        if (featureArticles != null)
            Log.i("STArticleVCF", "featureArticles: " + featureArticles.size());

        if (latestArticles != null)
            Log.i("STArticleVCF", "latestArticles: " + latestArticles.size());


        if (articlesAdapter == null) {
            createArticlesAdapter();
        }
        if (viewPagerAdapter == null) {
            createViewPagerAdapter();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_article_view_controller, container, false);
        Log.i("STArticleVCF", "onCreateView");

        sv1 = (ScrollView) view.findViewById(R.id.sv1);

        txtNewArticle = (TextView) view.findViewById(R.id.txtNewArticle);
        lvNewArticles = (ListView) view.findViewById(R.id.lvNewArticles);
        btnOpenNewArticlesList = (Button) view.findViewById(R.id.btnOpenNewArticlesList);

        vpArticlesHead = (ViewPager) view.findViewById(R.id.vpArticlesHead);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        txtCategory = (TextView) view.findViewById(R.id.txtCategory);
        rvCategory = (RecyclerView) view.findViewById(R.id.rvCategory);

        txtKeyWord = (TextView) view.findViewById(R.id.txtKeyWord);
        rvKeyword = (RecyclerView) view.findViewById(R.id.rvKeyword);
        btnOpenKeywordList = (Button) view.findViewById(R.id.btnViewKeywordList);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("STArticleVCF", "onActivityCreated");

        createListNewArticles();
        createViewPager();

        if (articles.get(0).articleID == 0) {
            DataPreserver.saveTopArticles(true, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (latestArticles == null || featureArticles == null) {
                        createArticlesAdapter();
                        createViewPager();
                        articlesAdapter.notifyDataSetChanged();
                        viewPagerAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        createListCategoryBtn();
        createListKeywordBtn();

        addEvents();
    }

    private void createListNewArticles() {
        txtNewArticle.setText("新着記事");
        this.lvNewArticles.setAdapter(articlesAdapter);
        setGridViewHeightBasedOnChildren(lvNewArticles, 1);
        btnOpenNewArticlesList.setText("新着記事の一覧を見る >");
    }

    private void setGridViewHeightBasedOnChildren(ListView listView, int columns) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int items = listAdapter.getCount();
            float rows = 0;

            View listItem = listAdapter.getView(0, null, listView);
            listItem.measure(0, 0);
            totalHeight = listItem.getMeasuredHeight();

            float x = 1;
            if (items > columns) {
                x = Math.round((items) / columns);
                rows = (float) (x + 0.1);
                totalHeight *= rows;
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
        } catch (Exception e) {
        }
    }

    private void createViewPager() {
        this.vpArticlesHead.setAdapter(viewPagerAdapter);
        this.vpArticlesHead.setCurrentItem(homeViewPagerID);
        this.vpArticlesHead.setOffscreenPageLimit(8);
        this.vpArticlesHead.addOnPageChangeListener(new InfiniteViewPagerHandler(this.vpArticlesHead, 4) {
            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                homeViewPagerID = mViewPager.getCurrentItem();
            }
        });
    }

    private void createListCategoryBtn() {
        txtCategory.setText("カテゴリーから探す");
        rvCategory.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvCategory.setLayoutManager(layoutManager);

        arrayListCategory = new ArrayList<>();
        categories = DataLoader.getArticleCategories();
        if (categories != null) {
            for (STCategory stCategory : categories) {
                String s = stCategory.name;
                arrayListCategory.add(s);
            }
            categoryAdapter = new CategoryAdapter(arrayListCategory, getActivity());
            rvCategory.setAdapter(categoryAdapter);
        } else {
            DataPreserver.saveCategoris(new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    categories = DataLoader.getArticleCategories();
                    for (STCategory stCategory : categories) {
                        String s = stCategory.name;
                        arrayListCategory.add(s);
                    }
                    categoryAdapter = new CategoryAdapter(arrayListCategory, getActivity());
                    rvCategory.setAdapter(categoryAdapter);
                }
            });
        }
    }

    private void createListKeywordBtn() {

        txtKeyWord.setText("キーワードから探す");

        rvKeyword.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvKeyword.setLayoutManager(layoutManager);

        DataPreserver.saveTags(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (err != null)
                    return;
                if (isSuccess) {
                    RealmResults<STTag> tags = DataLoader.getTags();
                    if (tags != null) {
                        arrayListKeyword = new ArrayList<>();
                        for (int i = 0; i < 10; i++) {
                            String s = (tags.get(i).name);
                            arrayListKeyword.add(s);
                        }
                        keywordAdapter = new KeywordAdapter(arrayListKeyword, getActivity());
                        rvKeyword.setAdapter(keywordAdapter);
                    }
                }
            }
        });
        btnOpenKeywordList.setText("キーワード一覧を見る ＞");
    }

    private void addEvents() {

        lvNewArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), STArticleWebViewControllerActivity.class);
                intent.putExtra("URL", latestArticles.get(position).article.articleURL);
                startActivity(intent);
            }
        });

        btnOpenNewArticlesList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                STArticleNewListViewControllerFragment fragment = new STArticleNewListViewControllerFragment();
                fragment.setTypeList(STEnums.STArticleListType.newList);
                fragment.setNameList("新着記事");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        btnOpenKeywordList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STArticleTagListViewControllerFragment fragment = new STArticleTagListViewControllerFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("STArticleVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STArticleVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STArticleVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STArticleVCF", "onDestroy");
    }
}