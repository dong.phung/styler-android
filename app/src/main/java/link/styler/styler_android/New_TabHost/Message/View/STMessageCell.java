package link.styler.styler_android.New_TabHost.Message.View;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STMessage;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/30/17.
 */

public class STMessageCell {

    //model
    private STMessage message = new STMessage();

    //view
    public ImageView avatarImageView, avatarImageViewMe;
    public TextView commentLabel;
    public TextView dateLabel;
    public Button reportButton;

    //helper
    Activity activity;
    Context context;

    public STMessageCell(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        init(activity);
        addControls();
    }

    private void init(Activity activity) {
        avatarImageView = (ImageView) activity.findViewById(R.id.avatarImageView);
        avatarImageViewMe = (ImageView) activity.findViewById(R.id.avatarImageViewMe);
        commentLabel = (TextView) activity.findViewById(R.id.commentLabel);
        dateLabel = (TextView) activity.findViewById(R.id.dateLabel);
        reportButton = (Button) activity.findViewById(R.id.reportButton);
    }

    private void addControls() {
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportAction();
            }
        });
    }

    private void reportAction() {
        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e){
            return;
        }
        if (message.messageID == 0){
            return;
        }
        //// TODO: 3/30/17  report
    }

    public void configData(STMessage message) {
        this.message = message;
        int meID = 0;
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {

        }
        if (meID == message.userID && message.userID != 0) {
            commentLabel.setTextColor(0xFFFFFFFF);
            commentLabel.setBackgroundColor(0xFF4A4A4A);
            reportButton.setVisibility(View.GONE);
            avatarImageView.setVisibility(View.GONE);
            avatarImageViewMe.setVisibility(View.VISIBLE);

            Picasso.with(context)
                    .load(LoginManager.getsIntstance().me().imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    //.placeholder(R.drawable.default_avatar)
                    .into(avatarImageViewMe);
        } else {
            commentLabel.setTextColor(0xFF000000);
            commentLabel.setBackgroundColor(0xFFEFEFEF);
            reportButton.setVisibility(View.VISIBLE);
            avatarImageView.setVisibility(View.VISIBLE);
            avatarImageViewMe.setVisibility(View.GONE);

            Bitmap bitmap = BitmapFactory.decodeByteArray(message.userImage, 0, message.userImage.length);
            avatarImageView.setImageBitmap(bitmap);

        }

        commentLabel.setText(message.text);

        dateLabel.setText(message.createdAt);
        // TODO: 3/30/17 dateFormat

    }
}
