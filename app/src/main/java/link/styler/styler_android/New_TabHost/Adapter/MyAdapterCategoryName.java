package link.styler.styler_android.New_TabHost.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import link.styler.styler_android.R;

/**
 * Created by macOS on 5/4/17.
 */

public class MyAdapterCategoryName extends ArrayAdapter<String> {
    Activity context;
    int resource;
    List<String> objects;

    public MyAdapterCategoryName(Activity context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View item = inflater.inflate(this.resource, null);
        String keywork = this.objects.get(position);
        TextView txtKeyword = (TextView) item.findViewById(R.id.txtCategoryName);

        txtKeyword.setText(keywork);

        return item;
    }
}
