package link.styler.styler_android.New_TabHost.Article;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.models.STArticle;
import link.styler.styler_android.R;

public class ArticlesTopFragment extends Fragment {

    //model
    private STArticle article;
    private String articleURL = null;

    private TextView txtDate;
    private TextView txtTitle;
    private ImageView imgFragment;
    private ProgressBar progressBar;

    //view
    private LinearLayout linearLayoutFragment;

    //helper

    public void setArticlesItem(STArticle article) {
        this.article = article;
        this.articleURL = article.articleURL;
    }

    public void setArticleURL(String articleURL) {
        this.articleURL = articleURL;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles_top, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        txtDate = (TextView) view.findViewById(R.id.txtDate);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        imgFragment = (ImageView) view.findViewById(R.id.imgFragment1);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (article != null) {
            if (!articleURL.isEmpty()) {
                Picasso.with(getContext())
                        .load(article.imageURL)
                        .into(imgFragment);
                //progressBar.setVisibility(View.GONE);
            } else {
                imgFragment.setVisibility(View.INVISIBLE);
            }
            txtDate.setText(article.publishedAt);
            txtTitle.setText(article.title);
        } else {

        }
        addAction();
    }

    private void addAction() {
        linearLayoutFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //don't convert Activity to Fragment
                Intent intent = new Intent(getActivity(), STArticleWebViewControllerActivity.class);
                intent.putExtra("URL", articleURL);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("ArticlesTopF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ArticlesTopF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ArticlesTopF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("ArticlesTopF", "onDestroy");
    }
}