package link.styler.styler_android.New_TabHost.MyPage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.GeocodeAddressString;
import link.styler.models.STShop;
import link.styler.styler_android.New_TabHost.Home.Event.STMapViewController;
import link.styler.styler_android.R;
import link.styler.styler_android.New_TabHost.Article.STArticleWebViewControllerActivity;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageAboutViewControllerFragment extends Fragment implements OnMapReadyCallback {

    //model
    private STShop shop = new STShop();
    private int shopID = 0;
    private boolean isLoading = false;
    private Integer lastObjID = null;

    //helper
    GoogleMap mGoogleMap;
    View mView;
    double lat = 0, lng = 0;

    //view
    private TextView titleLabel;
    private TextView locationLabel;
    private MapView mapView;

    private TextView titleLabel1;
    private LinearLayout holdingEventButton;

    private TextView titleLabelBusinessHour, titleLabelTel, titleLabelWebsite;
    private TextView descriptionLabelBusinessHour, descriptionLabelTel, descriptionLabelWebsite;
    private ImageView itemImageViewBusinessHour, itemImageViewTel, itemImageViewWebsite;
    private LinearLayout businessHour, tel, website;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("STMyPageAboutVC", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("STMyPageAboutVC", "onCreateView");
        mView = inflater.inflate(R.layout.fragment_st_my_page_about_view_controller, container, false);
        locationLabel = (TextView) mView.findViewById(R.id.locationLabel);
        titleLabel1 = (TextView) mView.findViewById(R.id.titleLabel1);
        holdingEventButton = (LinearLayout) mView.findViewById(R.id.holdingEventButton);

        descriptionLabelBusinessHour = (TextView) mView.findViewById(R.id.descriptionLabelBusinessHour);
        descriptionLabelTel = (TextView) mView.findViewById(R.id.descriptionLabelTel);
        descriptionLabelWebsite = (TextView) mView.findViewById(R.id.descriptionLabelWebsite);

        businessHour = (LinearLayout) mView.findViewById(R.id.businessHour);
        tel = (LinearLayout) mView.findViewById(R.id.tel);
        website = (LinearLayout) mView.findViewById(R.id.website);

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("STMyPageAboutVC", "onViewCreated");
        mapView = (MapView) mView.findViewById(R.id.mapView);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("STMyPageAboutVC", "onActivityCreated");

        if (shopID == 0)
            return;
        shop = DataLoader.getShop(shopID);
        if (shop != null && shop.shopID != 0) {
            reloadData();
        } else {
            DataPreserver.saveShop(shopID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (err != null)
                        return;
                    if (isSuccess) {
                        shop = DataLoader.getShop(shopID);
                        reloadData();
                    }
                }
            });
        }
        addControls();
    }

    private void reloadData() {
        Log.i("STMyPageAboutVC", "reloadData");
        locationLabel.setText(shop.address);
        titleLabel1.setText("大きい地図でみる");

        if (!shop.businessHour.isEmpty() && !shop.businessHour.equals("null"))
            descriptionLabelBusinessHour.setText(shop.businessHour);
        if (!shop.tel.isEmpty() && !shop.tel.equals("null"))
            descriptionLabelTel.setText(shop.tel);
        if (!shop.website.isEmpty() && !shop.website.equals("null"))
            descriptionLabelWebsite.setText(shop.website);
    }

    public void setShopID(int shopID) {
        Log.i("STMyPageAboutVC", "setShopID");
        this.shopID = shopID;
    }

    private void addControls() {
        holdingEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), STMapViewController.class);
                intent.putExtra("address", shop.address);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAction();
            }
        });
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), STArticleWebViewControllerActivity.class);
                intent.putExtra("URL", shop.website);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void callAction() {
        // TODO: 4/18/17 call action with tel number
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.i("STMyPageAboutVC", "onMapReady");
        Log.i("STMyPageAboutVC", "address " + shop.address);

        new GeocodeAddressString(new GeocodeAddressString.AsyncResponse() {
            @Override
            public void processFinish(double x, double y) {
                lat = y;
                lng = x;
                Log.i("STMyPageAboutVC", "lat: " + lat);
                Log.i("STMyPageAboutVC", "lng: " + lng);
                setMap();
            }

            private void setMap() {
                MapsInitializer.initialize(getContext());
                mGoogleMap = googleMap;
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));

                CameraPosition Liberty = CameraPosition.builder().target(new LatLng(lat, lng)).zoom(15).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Liberty));
            }
        }).execute(shop.address);
    }

}