package link.styler.styler_android.New_TabHost;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.DataSharing;
import link.styler.Utils.Logger;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.New_TabHost.Walkthrough.STTutorialViewController;
import link.styler.styler_android.R;

public class StylerActivity extends AppCompatActivity {

    private boolean popularPost = false;
    private boolean newPost = false;
    private boolean topEvent = false;
    private boolean featureEvent = false;
    private boolean popularItem = false;
    private boolean shopArea = false;

    private static boolean check = false;
    private String status = "";

    ImageView logo;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_styler);

        logo = (ImageView) findViewById(R.id.logo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        if (check) {
            logo.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            logo.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }

        status = LoginManager.getsIntstance().status != null ? LoginManager.getsIntstance().status : STEnums.UserStatus.None.getUserStatus();

        setUpData();

    }

    private void setUpData() {

        //setUpFirstData();

        if (DataSharing.getsIntstance().isNotFirstLaunch()) {
            setUpFirstData();
        } else {
            endSetUpHomePage1();
        }
    }

    private void setUpFirstData() {
        Logger.print("setUpFirstData");
        setUpHomePage1();
        setUpArticlePage();
        Logger.print("End setUpFirstData");
    }

    private void setUpHomePage1() {
        Logger.print("SetUp HomePage");

        Logger.print("SetUp popularPost");
        DataPreserver.savePosts(null, STEnums.PostSortType.Popular.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                Logger.print("setUpFirstData PopularPost savePosts: isSuceess " + isSuccess + "; err " + err + "; lasObjectID " + lastObjectID);
                endSetUpHomePage1();
            }
        });


        Logger.print("SetUp NewPost");
        DataPreserver.savePosts(null, STEnums.PostSortType.Latest.getPostSortType(), false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                Logger.print("setUpFirstData NewPost: isSuceess " + isSuccess + "; err " + err + "; lasObjectID " + lastObjectID);
            }
        });


        Logger.print("SetUp Event");
        DataPreserver.saveOngoingEvents(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                Logger.print("setUpFirstData Event saveOngoingEvents: isSuceess " + isSuccess + "; err " + err + "; lasObjectID " + lastObjectID);
            }
        });
        DataPreserver.saveFetureEvents(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                Logger.print("setUpFirstData Event saveFetureEvents: isSuceess " + isSuccess + "; err " + err + "; lasObjectID " + lastObjectID);
            }
        });

        Logger.print("SetUp PopularItem");
        DataPreserver.savePopularItems(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                Logger.print("setUpFirstData PopularItem savePopularItems: isSuceess " + isSuccess + "; err " + err + "; lasObjectID " + lastObjectID);
            }
        });


        Logger.print("SetUp ShopArea");
        /*DataPreserver.saveLocations(new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                Logger.print("setUpFirstData ShopArea saveLocations: isSuceess " + isSuccess + "; err " + err);
            }
        });*/

        Logger.print("End SetUp HomePage");
    }

    private void endSetUpHomePage1() {

        setUpNotificationPage();

        setUpMyPagePage();

        showWalkthrough();

    }

    private void setUpArticlePage() {
        DataPreserver.saveTopArticles(true, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {

            }
        });
        DataPreserver.saveCategoris(new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                Logger.print("setUpFirstData saveCategoris isSuceess " + isSuccess + " err " + err);
            }
        });
    }

    private void setUpNotificationPage() {
        if (!status.equals("none")) {
            DataPreserver.saveConversation(null, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    Logger.print("setUpFirstData saveConversation isSuccess " + isSuccess + " err " + err + " lastObjectID " + lastObjectID);
                    finish();
                }
            });

            DataPreserver.saveNotificationActivity(null, new STCompletion.WithLastObjectID() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                    Logger.print("setUpFirstData saveNotificationActivity isSuccess " + isSuccess + " err " + err + " lastObjectID " + lastObjectID);
                }
            });
        } else {
            finish();
        }
    }

    private void setUpMyPagePage() {
        if (status.equals(STEnums.UserStatus.Customer.getUserStatus())) {

        } else if (status.equals(STEnums.UserStatus.Staff.getUserStatus())) {

        } else {

        }
    }

    private void checkFinish() {
        //check = popularPost & newPost & topEvent & featureEvent & popularItem & shopArea;
        //check = true;
        //Logger.print("setUpFirstData  checkFinish: " + check);
        //if (check)
        finish();
    }

    private void showWalkthrough() {
        if (DataSharing.getsIntstance().isNotFirstLaunch()) {
            startActivity(new Intent(StylerActivity.this, STTutorialViewController.class));
        } else {
            DataSharing.getsIntstance().setIsNotFirstLauch();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressBar.setVisibility(View.GONE);
        check = true;
    }
}
