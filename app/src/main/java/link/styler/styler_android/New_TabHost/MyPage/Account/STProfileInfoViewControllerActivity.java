package link.styler.styler_android.New_TabHost.MyPage.Account;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.STDateFormat;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.R;

import static link.styler.Utils.Prefectures.Prefectures;

public class STProfileInfoViewControllerActivity extends AppCompatActivity {

    //model
    private STUser user = new STUser();
    private int locationID = 0;

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController stBaseNavigationController;
    private ImageView userImageView;
    private TextView imageUploadButton;
    private EditText nameField, locationField, birthDayField;
    private RadioGroup radioGroup;
    private RadioButton manView, womanView;
    private EditText introductionPlaceHolder;
    private LinearLayout layoutLocation;
    private TextView txtDone;
    private ListView listLocation;

    //helper
    String viewType = "firstSetting";
    String picturePath = "";
    String birthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_profile_info_view_controller);

        user = LoginManager.getsIntstance().me() != null ? LoginManager.getsIntstance().me() : new STUser();

        viewDidLoad();
        addControls();
    }

    private void viewDidLoad() {
        initView();

        stBaseNavigationController.getTitleNavigation().setText("プロフィール情報");
        stBaseNavigationController.getSubTileNavigation().setVisibility(View.GONE);
        stBaseNavigationController.getUpdateButton().setText("更新");
        stBaseNavigationController.getUpdateButton().setEnabled(false);

        /*if (!user.imageURL.isEmpty() || !user.imageURL.equals("null"))
            Picasso.with(this)
                    .load(user.imageURL)
                    .transform(new TransformationUtils().new CircleTransform())
                    .placeholder(R.drawable.default_avatar)
                    .into(userImageView);*/
        if (!user.name.equals("null"))
            nameField.setText(user.name);
        locationField.setInputType(InputType.TYPE_NULL);
        locationField.setText(Prefectures[user.locationID]);
        locationID = user.locationID;
        birthDayField.setInputType(InputType.TYPE_NULL);
        if (!user.birthday.equals("null")) {
            String date = new STDateFormat().DateFormat(user.birthday);
            birthDayField.setText(date);
        }
        if (user.gender.equals("male")) {
            manView.setChecked(true);
        } else if (user.gender.equals("female")) {
            womanView.setChecked(true);
        }
        if (!user.profile.equals("null"))
            introductionPlaceHolder.setText(user.profile);
        layoutLocation.setVisibility(View.GONE);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, Prefectures);
        listLocation.setAdapter(adapter);
    }

    private void initView() {
        stBaseNavigationController = new STBaseNavigationController(STProfileInfoViewControllerActivity.this);
        userImageView = (ImageView) findViewById(R.id.userImageView);
        imageUploadButton = (TextView) findViewById(R.id.imageUploadButton);
        nameField = (EditText) findViewById(R.id.nameField);
        locationField = (EditText) findViewById(R.id.locationField);
        birthDayField = (EditText) findViewById(R.id.birthDayField);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        manView = (RadioButton) findViewById(R.id.manView);
        womanView = (RadioButton) findViewById(R.id.womanView);
        introductionPlaceHolder = (EditText) findViewById(R.id.introductionPlaceHolder);

        layoutLocation = (LinearLayout) findViewById(R.id.layoutLocation);
        txtDone = (TextView) findViewById(R.id.txtDone);
        listLocation = (ListView) findViewById(R.id.listLocation);
    }

    private void addControls() {

        stBaseNavigationController.getUpdateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                upDateProfile();
            }
        });
        imageUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
                changeUpdateButtonEnabled();
            }
        });

        nameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        locationField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard();
                layoutLocation.setVisibility(View.VISIBLE);
            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                birthday = sdf.format(myCalendar.getTime());
                String date = new STDateFormat().DateFormat(birthday);
                birthDayField.setText(date);
            }
        };

        birthDayField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                if (hasFocus) {
                    new DatePickerDialog(STProfileInfoViewControllerActivity.this
                            , android.R.style.Widget_DatePicker
                            , date
                            , myCalendar.get(Calendar.YEAR)
                            , myCalendar.get(Calendar.MONTH)
                            , myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                } else {
                }
            }
        });
        birthDayField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                layoutLocation.setVisibility(View.GONE);
                new DatePickerDialog(STProfileInfoViewControllerActivity.this
                        , android.R.style.Widget_DatePicker
                        , date
                        , myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH)
                        , myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        introductionPlaceHolder.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        listLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                locationID = position;
                locationField.setText(Prefectures[position]);
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutLocation.setVisibility(View.GONE);
            }
        });

        checkEditText();
    }

    private void upDateProfile() {
        File photo;
        if (!picturePath.isEmpty())
            photo = new File(picturePath.toString());
        else
            photo = null;
        String name = nameField.getText().toString();
        String gender = "";
        if (manView.isChecked())
            gender = "male";
        else if (womanView.isChecked())
            gender = "female";
        String profile = introductionPlaceHolder.getText().toString();
        stBaseNavigationController.getUpdateButton().setEnabled(false);
        stBaseNavigationController.getUpdateButton().setTextColor(0xFF9B9B9B);
        final ProgressDialog progressDialog = new ProgressDialog(STProfileInfoViewControllerActivity.this);
        progressDialog.show();
        DataSender.updateUser(photo, name, locationID, birthday, gender, profile, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                stBaseNavigationController.getUpdateButton().setEnabled(true);
                stBaseNavigationController.getUpdateButton().setTextColor(0xFF28ABEC);
                progressDialog.dismiss();
                if (isSuccess) {
                    if (viewType.equals("edit")) {
                        //don't run
                    } else if (viewType.equals("firstSetting")) {
                        Toast.makeText(STProfileInfoViewControllerActivity.this, "プロフィール情報を更新しました", Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent = new Intent(STProfileInfoViewControllerActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                } else {

                }
            }
        });
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) STProfileInfoViewControllerActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(STProfileInfoViewControllerActivity.this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    private void chooseImage() {
        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);
    }

    private void checkEditText() {
        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        locationField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        birthDayField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                changeUpdateButtonEnabled();
            }
        });
        introductionPlaceHolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeUpdateButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeUpdateButtonEnabled() {
        if (checkValidationForParameter()) {
            stBaseNavigationController.getUpdateButton().setEnabled(true);
            stBaseNavigationController.getUpdateButton().setTextColor(0xFF28ABEC);
        } else {
            stBaseNavigationController.getUpdateButton().setEnabled(false);
            stBaseNavigationController.getUpdateButton().setTextColor(0xFF9B9B9B);
        }
    }

    private boolean checkValidationForParameter() {
        boolean valid = true;
        if (viewType.equals("edit")) {
            if (user.imageURL.isEmpty()) {
                if (picturePath.isEmpty())
                    valid = false;
            }
        } else {
            if (picturePath.isEmpty())
                valid = false;
        }
        if (nameField.getText().length() == 0)
            valid = false;
        if (locationField.getText().length() == 0)
            valid = false;
        if (birthDayField.getText().length() == 0)
            valid = false;
        if (!manView.isChecked() && !womanView.isChecked())
            valid = false;
        return valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.i("STCreateReplyVC", "picturePath " + picturePath);
            cursor.close();
            try {
                Picasso.with(this)
                        .load(new File(picturePath))
                        .transform(new TransformationUtils().new CircleTransform())
                        .into(userImageView);
            } catch (Exception e) {

            }
        }
    }
}
