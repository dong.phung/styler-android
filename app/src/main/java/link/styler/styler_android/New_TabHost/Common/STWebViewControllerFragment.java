package link.styler.styler_android.New_TabHost.Common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import link.styler.styler_android.R;

public class STWebViewControllerFragment extends Fragment {

    //model
    private String defaultURL = "";
    private String defaultTitle = "";

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController navigation;
    private WebView webView;

    //helper
    private boolean shouldSetNavi = true;
    private boolean shouldSetBackItem = false;


    public void setDefaultURL(String defaultURL) {
        this.defaultURL = defaultURL;
    }

    public void setDefaultTitle(String defaultTitle) {
        this.defaultTitle = defaultTitle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_web_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        webView = (WebView) view.findViewById(R.id.webView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText(defaultTitle);
        navigation.getSubTileNavigation().setVisibility(View.GONE);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(defaultURL);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STWebVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STWebVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STWebVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STWebVCF", "onDestroy");
    }
}
