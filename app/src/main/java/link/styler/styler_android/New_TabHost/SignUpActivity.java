package link.styler.styler_android.New_TabHost;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.New_TabHost.Common.STWebViewControllerActivity;
import link.styler.styler_android.New_TabHost.MyPage.Account.STProfileInfoViewControllerActivity;
import link.styler.styler_android.R;

public class SignUpActivity extends AppCompatActivity {

    TextView txtCancel;
    EditText txtEmail;
    EditText txtPassword;
    Button btnSignUp;
    TextView usePolicy;
    TextView privacyPolicy;
    TextView txtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        addControls();
        addEvents();
    }

    private void addControls() {
        txtCancel = (TextView) findViewById(R.id.txtCancel);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        usePolicy = (TextView) findViewById(R.id.usePolicy);
        usePolicy.setPaintFlags(usePolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        privacyPolicy = (TextView) findViewById(R.id.privacyPolicy);
        privacyPolicy.setPaintFlags(privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtLogin = (TextView) findViewById(R.id.txtLogin);
        txtLogin.setPaintFlags(txtLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void addEvents() {
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSignUp.setEnabled(false);
                signUp();
            }
        });
        usePolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, STWebViewControllerActivity.class);
                intent.putExtra("title", "利用規約");
                intent.putExtra("url", "http://styler.link/use_policy?app_webview");
                startActivity(intent);
            }
        });
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, STWebViewControllerActivity.class);
                intent.putExtra("title", "プライバシーポリシー");
                intent.putExtra("url", "http://styler.link/privacy_policy?app_webview");
                startActivity(intent);
            }
        });
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void signUp() {
        Log.i("SignUpActivity", "signUp");
        LoginManager.getsIntstance().signUp(txtEmail.getText().toString(), txtPassword.getText().toString(), new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if (isSuccess){
                    finishSighUp();
                }else {
                    Toast.makeText(SignUpActivity.this, "Eメールまたパスワードが不正です", Toast.LENGTH_SHORT).show();
                }
                btnSignUp.setEnabled(true);
            }
        });
    }

    private void finishSighUp() {
        MainActivity.setResumeType(STEnums.resumeType.createAccount);
        finish();
    }
}
