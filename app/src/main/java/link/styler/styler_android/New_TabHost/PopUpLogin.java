package link.styler.styler_android.New_TabHost;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import link.styler.styler_android.New_TabHost.SDKFacebook.FacebookLoginActivity;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/6/17.
 */

public class PopUpLogin extends Dialog {
    public Context context;
    public Button btnLogin, btnSignUp;

    private ImageButton facebookLogin;

    public PopUpLogin(Context context) {
        super(context);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_login);
        addControls();
        addEvents();



        facebookLogin = (ImageButton) findViewById(R.id.button_login);
        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FacebookLoginActivity.class);
                context.startActivity(intent);
            }
        });
    }


    private void addEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
                dismiss();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
                dismiss();
            }
        });

    }

    private void signUp() {
        Intent intent = new Intent(context, SignUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    private void login() {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void addControls() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
    }
}
