package link.styler.styler_android.New_TabHost.Message.View;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.models.STItem;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/29/17.
 */

public class STMessageItemInfoView {
    private ImageView imageView;
    private TextView brandLabel;
    private TextView itemNameLabel;
    private TextView priceLabel;
    private LinearLayout layout;
    private FragmentActivity fragmentActivity;
    private View view;
    private STItem item = new STItem();
    private String currentRequestURL;

    //use activity
    public STMessageItemInfoView(FragmentActivity activity, int itemID) {
        this.fragmentActivity = activity;
        init(activity, itemID);
        addControls();
    }

    private void init(Activity activity, final Integer itemID) {
        this.imageView = (ImageView) activity.findViewById(R.id.imageView);
        this.brandLabel = (TextView) activity.findViewById(R.id.brandLabel);
        this.itemNameLabel = (TextView) activity.findViewById(R.id.itemNameLabel);
        this.priceLabel = (TextView) activity.findViewById(R.id.priceLabel);
        this.layout = (LinearLayout) activity.findViewById(R.id.viewSTMessageItemInfoView);

        item = DataLoader.getItemWithItemID(itemID);
        if (item != null)
            configData(item);
        else {
            DataPreserver.saveItem(itemID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    item = DataLoader.getItemWithItemID(itemID) != null ? DataLoader.getItemWithItemID(itemID) : new STItem();
                    configData(item);
                }
            });
        }
    }

    //use view
    public STMessageItemInfoView(FragmentActivity fragmentActivity, View view, Integer itemID) {
        this.view = view;
        this.fragmentActivity = fragmentActivity;
        init(this.view, itemID);
        addControls();
    }

    private void init(View view, final Integer itemID) {
        this.imageView = (ImageView) view.findViewById(R.id.imageView);
        this.brandLabel = (TextView) view.findViewById(R.id.brandLabel);
        this.itemNameLabel = (TextView) view.findViewById(R.id.itemNameLabel);
        this.priceLabel = (TextView) view.findViewById(R.id.priceLabel);
        this.layout = (LinearLayout) view.findViewById(R.id.viewSTMessageItemInfoView);

        item = DataLoader.getItemWithItemID(itemID);
        if (item != null)
            configData(item);
        else {
            DataPreserver.saveItem(itemID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    item = DataLoader.getItemWithItemID(itemID) != null ? DataLoader.getItemWithItemID(itemID) : new STItem();
                    configData(item);
                }
            });
        }
    }

    private void configData(STItem item) {
        this.currentRequestURL = item.mainImageURL;
        if (!item.mainImageURL.isEmpty())
            Picasso.with(fragmentActivity)
                    .load(item.mainImageURL)
                    .into(imageView);
        itemNameLabel.setText(item.name);
        brandLabel.setText(item.brand);
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        if (!item.price.isEmpty())
            priceLabel.setText("￥ " + decimalFormat.format(Integer.parseInt(item.price)));
    }

    private void addControls() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STItemDetailViewControllerFragment fragment = new STItemDetailViewControllerFragment();
                fragment.setItemID(item.itemID);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
