package link.styler.styler_android.New_TabHost.Create;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STStructs;
import link.styler.Utils.STWearCategory;
import link.styler.models.STCategory;
import link.styler.models.STItem;
import link.styler.models.STReply;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STItemDetailViewControllerFragment;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Home.STHomePageViewControllerFragment;
import link.styler.styler_android.R;

public class STCreateReplyViewControllerActivity extends AppCompatActivity {

    //model
    private int postID = 0;
    private int categoryID = 0;
    private STCategory category = new STCategory();
    private int itemID = 0;
    private STItem item = new STItem();
    private int replyID = 0;
    private STReply reply = new STReply();

    //view
    private STBaseNavigationController navigation;
    private ImageView photoImageView1;
    private ImageView imaDelete;
    private Button uploadButton;
    private EditText edtBrand, edtName, edtPrice, edtDescription, edtMessage;
    private ImageView imaCategory;
    private TextView txtCategory;
    private Button btnSend;
    private LinearLayout llCreatePost2;
    private TextView txtDone;
    private ListView lvCategory;

    //helper
    private String viewType = "create";
    private String picturePath = "";
    private boolean isLoading = false;
    private File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("STCreateReplyVC", "onCreate ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_create_reply_view_controller);
        Intent intent = getIntent();
        initData(intent);
        viewDidLoad();
        addAction();

        changePostButtonEnabled();
        Log.i("STCreateReplyVC", "viewType " + viewType);
    }

    private void initData(Intent intent) {
        try {
            postID = intent.getIntExtra("postID", 0);
        } catch (Exception e) {
        }
        try {
            itemID = intent.getIntExtra("itemID", 0);
            item = DataLoader.getItemWithItemID(itemID);
        } catch (Exception e) {
        }
        try {
            replyID = intent.getIntExtra("replyID", 0);
            reply = DataLoader.getReply(replyID);
        } catch (Exception e) {
        }
        if (itemID != 0)
            viewType = "itemUpdate";
        if (itemID != 0 && replyID != 0)
            viewType = "itemAndReplyUpdate";
        if (postID != 0)
            viewType = "create";
        if (item == null) {
            DataPreserver.saveItem(itemID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    item = DataLoader.getItemWithItemID(itemID) != null ? DataLoader.getItemWithItemID(itemID) : new STItem();
                    categoryID = item.categoryID;
                    category = DataLoader.getCategory(categoryID);
                }
            });
        } else {
            categoryID = item.categoryID;
            category = DataLoader.getCategory(categoryID);
        }

        if (reply == null) {
            DataPreserver.saveReply(replyID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    reply = DataLoader.getReply(replyID) != null ? DataLoader.getReply(replyID) : new STReply();
                }
            });
        }
    }

    private void viewDidLoad() {
        initView();
        if (item.itemID != 0) {
            setItemData(item);
        }
        changePostButtonEnabled();
    }

    private void setItemData(STItem item) {
        Picasso.with(this)
                .load(item.mainImageURL)
                .into(photoImageView1);
        uploadButton.setVisibility(View.GONE);
        photoImageView1.setVisibility(View.VISIBLE);
        imaDelete.setVisibility(View.GONE);
        picturePath = "item";

        photoImageView1.buildDrawingCache();
        Bitmap bitmap = photoImageView1.getDrawingCache();

        file = new File("image.jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }

        edtBrand.setText(item.brand);
        edtName.setText(item.name);
        edtPrice.setText(item.price);
        edtDescription.setText(item.text);

        if (reply != null && reply.replyID != 0) {
            edtMessage.setText(reply.comment);
        }


        Log.i("STCreateReplyVC", "categoryID " + categoryID);
        if (category.name.equals("カテゴリーを選択しない"))
            txtCategory.setText("その他");
        else
            txtCategory.setText(category.name);
        changePostButtonEnabled();
    }

    private void initView() {
        navigation = new STBaseNavigationController(STCreateReplyViewControllerActivity.this);
        navigation.backButton.setImageResource(R.drawable.button_close);
        navigation.backButton.setPadding(0, 65, 0, 65);
        btnSend = (Button) findViewById(R.id.btnSend2);
        Log.i("STCreateReplyVC", "viewType " + viewType);
        if (viewType.equals("create")) {
            navigation.titleNavigation.setText("Reply 作成");
            btnSend.setText("投稿");
        } else if (viewType.equals("itemUpdate")) {
            navigation.titleNavigation.setText("Item 編集");
            btnSend.setText("更新");
        } else {
            navigation.titleNavigation.setText("Reply 編集");
            btnSend.setText("更新");
        }
        //navigation.subTileNavigation.setVisibility(View.GONE);
        photoImageView1 = (ImageView) findViewById(R.id.photoImageView1);
        imaDelete = (ImageView) findViewById(R.id.imaDelete);
        imaDelete.setVisibility(View.GONE);
        uploadButton = (Button) findViewById(R.id.uploadButton);
        edtBrand = (EditText) findViewById(R.id.edtBrand);
        edtName = (EditText) findViewById(R.id.edtName);
        edtPrice = (EditText) findViewById(R.id.edtPrice);
        edtDescription = (EditText) findViewById(R.id.edtDescription);
        edtMessage = (EditText) findViewById(R.id.edtMessage);
        imaCategory = (ImageView) findViewById(R.id.imaCategory);
        txtCategory = (TextView) findViewById(R.id.txtCategory);
        llCreatePost2 = (LinearLayout) findViewById(R.id.llCreatePost2);
        llCreatePost2.setVisibility(View.GONE);
        txtDone = (TextView) findViewById(R.id.txtDone);

        lvCategory = (ListView) findViewById(R.id.lvCategory);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, STWearCategory.japName);
        lvCategory.setAdapter(adapter);
    }

    private void addAction() {

        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(STCreateReplyViewControllerActivity.this);
                builder.setTitle("入力を終了してよろしいですか？");
                builder.setMessage("現在の編集内容は保存されません").setPositiveButton("OK", dialogClickListener)
                        .setNegativeButton("キャンセル", dialogClickListener).show();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(perms, permsRequestCode);
                }
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
                changePostButtonEnabled();
            }
        });

        imaDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoImageView1.setVisibility(View.GONE);
                imaDelete.setVisibility(View.GONE);
                uploadButton.setVisibility(View.VISIBLE);
                picturePath = "";
                changePostButtonEnabled();
                Log.i("STCreateReplyVC", "picturePath " + picturePath);
            }
        });

        edtBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });
        edtBrand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });
        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });
        edtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });
        edtDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });
        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changePostButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        imaCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.VISIBLE);
            }
        });
        txtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.VISIBLE);
                hideKeyBoard();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("clickSend", "c1");
                sendCreateOrEdit();
            }
        });

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCreatePost2.setVisibility(View.GONE);
            }
        });

        lvCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = (String) lvCategory.getAdapter().getItem(position);
                txtCategory.setText(s);
                categoryID = STWearCategory.ID[position];
                changePostButtonEnabled();
            }
        });
    }

    private void sendCreateOrEdit() {
        Log.i("clickSend", "c2");
        if (!checkRequiredParamValidation()) {
            Toast.makeText(this, "必須項目を全て入力してください", Toast.LENGTH_SHORT).show();
            return;
        }
        if (viewType.equals("create")) {
            Log.i("STCreateReplyVC", "create: ");
            if (postID == 0 || categoryID == 0 || isLoading == true)
                return;
            if (isLoading)
                return;
            isLoading = true;

            STStructs.StylerReplyAndItemStruct replyParam = new STStructs().new StylerReplyAndItemStruct();
            replyParam.postID = postID;
            replyParam.comment = edtMessage.getText().toString();
            replyParam.brand = edtBrand.getText().toString();
            replyParam.name = edtName.getText().toString();
            replyParam.price = edtPrice.getText().toString();
            replyParam.text = edtDescription.getText().toString();
            replyParam.categoryID = categoryID;
            replyParam.itemID = item.itemID;
            if (!picturePath.equals("item"))
                file = new File(picturePath.toString());

            if (file.exists()||picturePath.equals("item")) {
                final ProgressDialog progress;
                progress = ProgressDialog.show(STCreateReplyViewControllerActivity.this, null, null);
                DataSender.createReplyAndItem(replyParam, file, new STCompletion.WithNullableInt() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                        if (isSuccess) {
                            finish();
                            Log.i("nullableInt", "nullableInt: " + nullableInt);
                            MainActivity.setTabHostID(0);
                            MainActivity.setResumeType(STEnums.resumeType.createReply);
                            MainActivity.setResumeCreatePostID(postID);
                            MainActivity.setResumeCreateReplyID(nullableInt);
                            STHomePageViewControllerFragment.setHomeViewPagerID(6);
                            STHomePageViewControllerFragment.setResumeType(STEnums.resumeType.createReply);
                        } else {
                            Toast.makeText(STCreateReplyViewControllerActivity.this, "Replyの作成に失敗しました", Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                        isLoading = false;
                    }
                });
            }
        } else if (viewType.equals("itemUpdate")) {
            Log.i("STCreateReplyVC", "itemUpdate: ");
            if (itemID == 0 || categoryID == 0)
                return;
            if (isLoading)
                return;
            isLoading = true;

            STStructs.StylerUpdateItemStruct param = new STStructs().new StylerUpdateItemStruct();
            param.brand = edtBrand.getText().toString();
            param.name = edtName.getText().toString();
            param.price = edtPrice.getText().toString();
            param.text = edtDescription.getText().toString();
            param.categoryID = categoryID;

            final ProgressDialog progress;
            progress = ProgressDialog.show(STCreateReplyViewControllerActivity.this, null, null);
            DataSender.updateItem(itemID, param, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (isSuccess) {
                        STItemDetailViewControllerFragment.setResumeType(STEnums.resumeType.editItem);
                        finish();
                    } else {
                        Toast.makeText(STCreateReplyViewControllerActivity.this, "Replyの作成に失敗しました", Toast.LENGTH_SHORT).show();
                    }
                    progress.dismiss();
                    isLoading = false;
                }
            });
        } else {
            Log.i("STCreateReplyVC", "itemAndReplyUpdate: ");
            if (replyID == 0 || categoryID == 0)
                return;
            if (isLoading)
                return;
            isLoading = true;
            STStructs.StylerUpdateItemAndReplyStruct param = new STStructs().new StylerUpdateItemAndReplyStruct();
            param.replyID = replyID;
            param.comment = edtMessage.getText().toString();
            param.brand = edtBrand.getText().toString();
            param.name = edtName.getText().toString();
            param.price = edtPrice.getText().toString();
            param.text = edtDescription.getText().toString();
            param.categoryID = categoryID;

            final ProgressDialog progress;
            progress = ProgressDialog.show(STCreateReplyViewControllerActivity.this, null, null);
            DataSender.updateReply(param, file, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    Log.i("aaaaaasdsdsdww","11: "+isSuccess);
                    if (isSuccess) {
                        STItemDetailViewControllerFragment.setResumeType(STEnums.resumeType.editItem);
                        finish();
                    } else {
                        Toast.makeText(STCreateReplyViewControllerActivity.this, "Replyの作成に失敗しました", Toast.LENGTH_SHORT).show();
                    }
                    progress.dismiss();
                    isLoading = false;
                }
            });
        }
    }

    private void changePostButtonEnabled() {
        if (checkRequiredParamValidation()) {
            btnSend.setEnabled(true);
            btnSend.setBackgroundColor(0xFF28ABEC);
            btnSend.setTextColor(0xFFFFFFFF);
        } else {
            btnSend.setBackgroundColor(0xFFEFEFEF);
            btnSend.setTextColor(0xFF9B9B9B);
            btnSend.setEnabled(false);
        }
    }

    private boolean checkRequiredParamValidation() {
        boolean valid = true;
        if (viewType == "create") {
            if (postID == 0)
                valid = false;
        }
        if (picturePath == "")
            valid = false;
        if (viewType != "itemUpdate") {
            if (edtMessage.getText().length() == 0)
                valid = false;
        }
        if (edtBrand.getText().length() == 0)
            valid = false;
        if (edtName.getText().length() == 0)
            valid = false;
        if (edtPrice.getText().length() == 0)
            valid = false;
        if (edtDescription.getText().length() == 0)
            valid = false;
        if (categoryID == 0)
            valid = false;
        return valid;
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.i("STCreateReplyVC", "picturePath " + picturePath);
            cursor.close();
            try {
                //photoImageView1.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                Picasso.with(this)
                        .load(new File(picturePath))
                        //.transform(new TransformationUtils().new RoundedCornersTransform())
                        .into(photoImageView1);
                uploadButton.setVisibility(View.GONE);
                photoImageView1.setVisibility(View.VISIBLE);
                imaDelete.setVisibility(View.VISIBLE);
                changePostButtonEnabled();
            } catch (Exception e) {

            }
        }
    }
}
