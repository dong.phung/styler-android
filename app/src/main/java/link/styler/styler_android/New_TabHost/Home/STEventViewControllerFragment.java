package link.styler.styler_android.New_TabHost.Home;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.models.STEventSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterEvent2;
import link.styler.styler_android.New_TabHost.Home.Event.STEventHeaderView;
import link.styler.styler_android.R;

/**
 * Created by macOS on 5/9/17.
 */

public class STEventViewControllerFragment extends STBaseFragment {

    //model,API
    private RealmResults<STEventSort> events;
    private RealmList<STEventSort> onEvents = new RealmList<STEventSort>();
    private Integer lastObjectID = null;
    private boolean gotData = false;

    //view
    private STEventHeaderView headerView;
    private GridView tableView;
    private PullRefreshLayout pullRefreshLayout;
    private ScrollView scrollView;
    private ProgressBar progressBar;

    //helper
    boolean isLoading = false;
    private MyAdapterEvent2 adapterEvent2 = null;

    public void createAdapterEvent2() {
        this.events = DataLoader.getFetureEvents();
        this.onEvents = new RealmList<STEventSort>();
        this.onEvents.addAll(events);
        this.adapterEvent2 = new MyAdapterEvent2(getActivity(), this.onEvents);
    }
    //end

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        referenceMyself(this);
        if (adapterEvent2 == null)
            createAdapterEvent2();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_event_view_controller, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        headerView = new STEventHeaderView(view, getActivity());
        tableView = (GridView) view.findViewById(R.id.tableView);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Event);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar.setVisibility(View.GONE);
        headerView.configData();
        tableView.setAdapter(adapterEvent2);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);

        if (adapterEvent2.getCount() == 0)
            refreshData();

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                    Log.i("123456", "diff: " + diff);
                    Log.i("123456", "gotData: " + gotData);

                    if (diff <= 0 && !gotData) {
                        Log.i("123456aaaaaa", "diff: " + diff);
                        gotData = true;
                        getData();

                    } else if (diff > 145 && gotData) {
                        gotData = false;
                    }
                }
            });
        }
    }

    private void onRefreshData() {
        refreshData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void refreshData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveOngoingEvents(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    headerView.configData();
                }
            }
        });

        final STEventViewControllerFragment self = (STEventViewControllerFragment) weakSelf.get();
        DataPreserver.saveFetureEvents(null, true, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.events = DataLoader.getFetureEvents();
                    self.onEvents = new RealmList<STEventSort>();
                    self.onEvents.addAll(events);

                    if (self.adapterEvent2 == null)
                        createAdapterEvent2();

                    self.adapterEvent2 = (MyAdapterEvent2) self.tableView.getAdapter();
                    self.adapterEvent2.setEvents(self.onEvents);
                    self.adapterEvent2.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(self.tableView, 1);
                }
                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
                Log.i("saveFetureEvents", "lastObjectID: " + lastObjectID);
                Log.i("saveFetureEvents", "self.lastObjectID: " + self.lastObjectID);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        final STEventViewControllerFragment self = (STEventViewControllerFragment) weakSelf.get();
        DataPreserver.saveFetureEvents(self.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    self.events = DataLoader.getFetureEvents();
                    self.onEvents = new RealmList<STEventSort>();
                    self.onEvents.addAll(events);

                    if (self.adapterEvent2 == null)
                        createAdapterEvent2();

                    self.adapterEvent2 = (MyAdapterEvent2) self.tableView.getAdapter();
                    self.adapterEvent2.setEvents(self.onEvents);
                    self.adapterEvent2.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(self.tableView, 1);
                }
                if (lastObjectID != null && lastObjectID != 0)
                    self.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        try {
            ListAdapter listAdapter = gridView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int items = listAdapter.getCount();
            float rows = 0;

            View listItem = listAdapter.getView(0, null, gridView);
            listItem.measure(0, 0);
            totalHeight = listItem.getMeasuredHeight();

            float x = 1;
            if (items > columns) {
                x = ((items) / columns);
                rows = (float) (x + 0.5);
                totalHeight *= rows;
            }

            ViewGroup.LayoutParams params = gridView.getLayoutParams();
            params.height = totalHeight;
            gridView.setLayoutParams(params);
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STEventViewControllerF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STEventViewControllerF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STEventViewControllerF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STEventViewControllerF", "onDestroy");
    }
}
