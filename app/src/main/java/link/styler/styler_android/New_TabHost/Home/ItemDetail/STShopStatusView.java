package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import link.styler.STStructs.STEnums;
import link.styler.Utils.Logger;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/25/17.
 */

public class STShopStatusView {

    private TextView replyCountLabel;
    private TextView eventCountLabel;
    private TextView staffCountLabel;

    private TextView repliesLabel;
    private TextView eventsLabel;
    private TextView staffsLabel;

    private Button actionButton;

    private STUser user = new STUser();
    private STShop shop = new STShop();

    public STShopStatusView(View view) {
        initSTShopStatusView(view);
    }

    private void initSTShopStatusView(View view){
        replyCountLabel = (TextView)view.findViewById(R.id.replyCountLabel);
        eventCountLabel = (TextView)view.findViewById(R.id.eventCountLabel);
        staffCountLabel = (TextView)view.findViewById(R.id.staffCountLabel);

        repliesLabel = (TextView)view.findViewById(R.id.repliesLabel);
        eventsLabel = (TextView)view.findViewById(R.id.eventsLabel);
        staffsLabel = (TextView)view.findViewById(R.id.staffsLabel);
        actionButton = (Button)view.findViewById(R.id.actionButton);
    }

    public void setUpSTShopStatusView(List text){

        if(text.isEmpty())
        {
            clearStatus();
            return;
        }

        Logger.print("setUpSTShopStatusView "+text);
        replyCountLabel.setText(text.get(0).toString());
        eventCountLabel.setText(text.get(1).toString());
        staffCountLabel.setText(text.get(2).toString());

        repliesLabel.setText(text.get(3).toString());
        eventsLabel.setText(text.get(4).toString());
        staffsLabel.setText(text.get(5).toString());

        actionButton.setText(text.get(6).toString());
    }

    private void clearStatus(){
        repliesLabel.setVisibility(View.GONE);
        eventsLabel.setVisibility(View.GONE);
        staffsLabel.setVisibility(View.GONE);
        actionButton.setVisibility(View.GONE);
    }

    public void configData(STUser user) {
        this.user = user;
        if (user.status.equals(STEnums.UserStatus.Staff.getUserStatus())){
            replyCountLabel.setText(""+user.replyCount);
            repliesLabel.setText("Replies");
            eventCountLabel.setText(""+ user.followCount);
            eventsLabel.setText("Followers");
            staffCountLabel.setText(""+ user.likeCount);
            staffsLabel.setText("Likes");
        } else {
            replyCountLabel.setText(""+user.postCount);
            repliesLabel.setText("Posts");
            eventCountLabel.setText(""+ user.watchCount);
            eventsLabel.setText("Watches");
            staffCountLabel.setText(""+ user.likeCount);
            staffsLabel.setText("Likes");
        }
    }

    public void configDataWithShop(STShop shop) {
        this.shop = shop;
        replyCountLabel.setText(""+shop.itemCount);
        repliesLabel.setText("Items");
        eventCountLabel.setText(""+ shop.eventCount);
        eventsLabel.setText("Events");
        staffCountLabel.setText(""+ shop.staffCount);
        staffsLabel.setText("Staffs");
    }

    public Button getActionButton() {
        return actionButton;
    }

}
