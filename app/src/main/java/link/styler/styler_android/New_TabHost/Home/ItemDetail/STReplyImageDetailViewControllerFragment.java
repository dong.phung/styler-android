package link.styler.styler_android.New_TabHost.Home.ItemDetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;


public class STReplyImageDetailViewControllerFragment extends Fragment {

    //model
    private String url = "";

    //view
    private LinearLayout linearLayout;
    private STBaseNavigationController navigation;
    private ImageView imageView;

    //helper


    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_reply_image_detail_view_controller, container, false);

        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        navigation = new STBaseNavigationController(view);
        imageView = (ImageView) view.findViewById(R.id.imageView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getBackButton().setImageResource(R.drawable.button_close);
        navigation.getBackButton().setPadding(0, 65, 0, 65);
        navigation.getTitleNavigation().setText("Reply");
        navigation.getSubTileNavigation().setText("イメージギャラリー");

        Picasso.with(getActivity())
                .load(url)
                //.transform(new TransformationUtils().new RoundedCornersTransform())
                .into(imageView);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STReplyImageDetailVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STReplyImageDetailVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STReplyImageDetailVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STReplyImageDetailVCF", "onDestroy");
    }
}
