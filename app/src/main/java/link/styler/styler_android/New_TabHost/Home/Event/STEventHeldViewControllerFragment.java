package link.styler.styler_android.New_TabHost.Home.Event;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STEventSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterEvent;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STEventHeldViewControllerFragment extends Fragment {

    //model
    RealmResults<STEventSort> events;
    Integer lastObjectID = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterEvent adapterEvent = null;

    public void createAdapterEvent() {
        events = DataLoader.getOngoingEvents();
        this.adapterEvent = new MyAdapterEvent(getActivity(), events);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterEvent == null)
            createAdapterEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_event_held_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefresh_Event);
        gridView = (GridView) view.findViewById(R.id.gridViewEvent);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText("開催中のイベント");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        gridView.setAdapter(adapterEvent);
        progressBar.setVisibility(View.GONE);
        if (adapterEvent.getCount() == 0) {
            getNewData();
            progressBar.setVisibility(View.VISIBLE);
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STEventHeldVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STEventHeldVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STEventHeldVC", "totalItemCount: " + totalItemCount);
                    Log.i("STEventHeldVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STEventHeldVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STEventHeldVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveOngoingEvents(null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    events = DataLoader.getOngoingEvents();
                    if (adapterEvent == null)
                        createAdapterEvent();
                    adapterEvent.setEvents(events);
                    adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STEventHeldViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                Log.i("STEventHeldVC", "STEventHeldViewControllerFragment.this.lastObjectID: " + STEventHeldViewControllerFragment.this.lastObjectID);
                Log.i("STEventHeldVC", "lastObjectID: " + lastObjectID);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveOngoingEvents(lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    events = DataLoader.getOngoingEvents();

                    if (adapterEvent == null)
                        createAdapterEvent();

                    adapterEvent.setEvents(events);
                    adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STEventHeldViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                Log.i("STEventHeldVC", "STEventHeldViewController.this.lastObjectID: " + STEventHeldViewControllerFragment.this.lastObjectID);
                Log.i("STEventHeldVC", "lastObjectID: " + lastObjectID);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STEventHeldVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STEventHeldVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STEventHeldVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STEventHeldVCF", "onDestroy");
    }
}
