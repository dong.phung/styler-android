package link.styler.styler_android.New_TabHost.MyPage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STShop;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Home.Event.STMapViewController;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.Home.Common.STNavigationTabView;
import link.styler.styler_android.New_TabHost.MyPage.Account.STAccountSettingViewControllerFragment;
import link.styler.styler_android.New_TabHost.MyPage.Account.STShopPageSettingViewControllerFragment;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/17/17.
 */

public class STMyPageHeaderView {

    //model
    private Context context;
    private STUser user = new STUser();
    private STShop shop = new STShop();
    private STEnums.MyPageType pageType = STEnums.MyPageType.None;
    private Activity activity = null;
    private View view;
    private FragmentActivity fragmentActivity;

    //View
    private ImageView avatarImageView;
    private STShopStatusView statusView;
    private TextView nameLabel;
    private TextView userInfoLabel;
    private TextView descriptionLabel;
    private STMyPageShopButton shopInfoButton;

    private LinearLayout btnShopInfo;
    private STNavigationTabView tabView;

    //use Activity
    public STMyPageHeaderView(Activity activity) {
        initView(activity);
        statusView = new STShopStatusView((View) activity.findViewById(R.id.statusView));
        tabView = new STNavigationTabView(activity);
        this.activity = activity;
        this.context = activity.getApplicationContext();

        Log.i("STMyPageHeaderView11", "pageType: " + pageType);
        addControls();
    }

    private void initView(Activity activity) {
        avatarImageView = (ImageView) activity.findViewById(R.id.avatarImageView);
        nameLabel = (TextView) activity.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) activity.findViewById(R.id.userInfoLabel);
        descriptionLabel = (TextView) activity.findViewById(R.id.descriptionLabel);
        btnShopInfo = (LinearLayout) activity.findViewById(R.id.btnShopInfo);
    }

    //use View
    public STMyPageHeaderView(View view, FragmentActivity fragmentActivity) {
        initView(view);

        this.view = view;
        this.context = view.getContext();
        this.fragmentActivity = fragmentActivity;

        addControls();
    }

    private void initView(View view) {
        avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);
        statusView = new STShopStatusView(view.findViewById(R.id.statusView));
        nameLabel = (TextView) view.findViewById(R.id.nameLabel);
        userInfoLabel = (TextView) view.findViewById(R.id.userInfoLabel);
        descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
        btnShopInfo = (LinearLayout) view.findViewById(R.id.btnShopInfo);
    }

    //

    private void addControls() {
        statusView.getActionButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickActionButton();
            }
        });
    }

    private void clickActionButton() {
        switch (pageType) {
            case MyCustomerPage:
                STAccountSettingViewControllerFragment fragment = new STAccountSettingViewControllerFragment();
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case MyStaffPage:
                STAccountSettingViewControllerFragment fragment1 = new STAccountSettingViewControllerFragment();
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment1)
                        .addToBackStack(null)
                        .commit();
                break;
            case MyShopPage:
                STShopPageSettingViewControllerFragment fragment2 = new STShopPageSettingViewControllerFragment();
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment2)
                        .addToBackStack(null)
                        .commit();
                break;
            case OtherShopPage:
                //don't convert Activity to fragment
                Intent intent = new Intent(context, STMapViewController.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;
            case OtherStaffPage:
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                }
                if (status.equals("customer")) {
                    followAction();
                }
                break;
            case NotLogin:
                if (activity != null) {
                    PopUpLogin popUpLogin = new PopUpLogin(activity);
                    popUpLogin.show();
                } else {
                    PopUpLogin popUpLogin = new PopUpLogin(context);
                    popUpLogin.show();
                }
                break;
            case None:
                if (activity != null) {
                    PopUpLogin popUpLogin = new PopUpLogin(activity);
                    popUpLogin.show();
                } else {
                    PopUpLogin popUpLogin = new PopUpLogin(context);
                    popUpLogin.show();
                }
                break;
            default:
                break;
        }
    }

    public void configDataWithUser(STUser user, STEnums.MyPageType pageType) {
        this.user = user;
        this.pageType = pageType;
        setUserData();
    }

    public void configDataWithShop(STShop shop, STEnums.MyPageType pageType) {
        this.shop = shop;
        this.pageType = pageType;
        setUserData();
    }

    private void setUserData() {
        avatarImageView.setImageResource(R.drawable.icon_default_avatar);
        statusView.getActionButton().setVisibility(View.GONE);
        Log.i("STMyPageHeaderView12", "pageType: " + pageType);
        switch (pageType) {
            case MyCustomerPage:
                setMyCustomerPage();
                break;
            case MyStaffPage:
                setMyStaffPage();
                break;
            case MyShopPage:
                setMyShopPage();
                break;
            case OtherCustomerPage:
                setOtherCustomerPage();
                break;
            case OtherStaffPage:
                setOtherStaffPage();
                break;
            case OtherShopPage:
                setOtherShopPage();
                break;
            case NotLogin:
                setNotLogin();
                break;
            case None:
                statusView.getActionButton().setText("");
            default:
                break;
        }
        initShopButton();
    }

    private void setMyCustomerPage() {
        if (user.userID == 0)
            return;
        if (!user.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(user.imageURL)
                    .error(R.drawable.default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.configData(user);
        statusView.getActionButton().setText("アカウント設定");
        statusView.getActionButton().setTextColor(0xffFFffFF);
        statusView.getActionButton().setBackgroundColor(0xFF484A4A);
        statusView.getActionButton().setVisibility(View.VISIBLE);
        nameLabel.setText(user.name);
        if (!user.apiDetail.isEmpty())
            userInfoLabel.setText(user.apiDetail);
        else
            userInfoLabel.setText("未設定");
        if (!user.profile.isEmpty())
            descriptionLabel.setText(user.profile);
        else
            descriptionLabel.setText("未設定");
    }

    private void setMyStaffPage() {
        if (user.userID == 0)
            return;
        if (!user.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(user.imageURL)
                    .error(R.drawable.default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.configData(user);
        statusView.getActionButton().setText("アカウント設定");
        statusView.getActionButton().setTextColor(0xffFFffFF);
        statusView.getActionButton().setBackgroundColor(0xFF484A4A);
        statusView.getActionButton().setVisibility(View.VISIBLE);
        nameLabel.setText(user.name);
        STShop stShop = DataLoader.getShop(user.shopID);
        userInfoLabel.setText(stShop.name);
        descriptionLabel.setText(user.profile);
    }

    private void setMyShopPage() {
        if (shop.shopID == 0)
            return;
        if (!shop.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(shop.imageURL)
                    .error(R.drawable.icon_default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.getActionButton().setText("ショップ情報設定");
        statusView.getActionButton().setTextColor(0xffFFffFF);
        statusView.getActionButton().setBackgroundColor(0xFF484A4A);
        statusView.getActionButton().setVisibility(View.VISIBLE);
        statusView.configDataWithShop(shop);
        nameLabel.setText(shop.name);
        userInfoLabel.setText(shop.address);
        descriptionLabel.setText(shop.profile);
    }

    private void setOtherCustomerPage() {
        if (user.userID == 0)
            return;
        if (!user.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(user.imageURL)
                    .error(R.drawable.default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.configData(user);
        statusView.getActionButton().setVisibility(View.GONE);
        nameLabel.setText(user.name);
        if (!user.apiDetail.isEmpty())
            userInfoLabel.setText(user.apiDetail);
        else
            userInfoLabel.setText("未設定");
        if (!user.profile.isEmpty())
            descriptionLabel.setText(user.profile);
        else
            descriptionLabel.setText("未設定");
    }

    private void setOtherStaffPage() {
        if (user.userID == 0)
            return;
        if (!user.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(user.imageURL)
                    .error(R.drawable.icon_default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.configData(user);
        doFollowAction(user.followID != 0);
        nameLabel.setText(user.name);
        userInfoLabel.setText(user.shop.name);
        descriptionLabel.setText(user.profile);
    }

    private void setOtherShopPage() {
        if (shop.shopID == 0)
            return;
        if (!shop.imageURL.isEmpty()) {
            Picasso.with(context)
                    .load(shop.imageURL)
                    .error(R.drawable.icon_default_avatar)
                    .transform(new TransformationUtils().new CircleTransform())
                    .into(avatarImageView);
        }
        statusView.configData(user);
        statusView.getActionButton().setText("マップで所在地を見る");
        statusView.getActionButton().setTextColor(0xffFFffFF);
        statusView.getActionButton().setBackgroundColor(0xFF484A4A);
        statusView.getActionButton().setVisibility(View.VISIBLE);
        statusView.configDataWithShop(shop);
        nameLabel.setText(shop.name);
        userInfoLabel.setText(shop.address);
        descriptionLabel.setText(shop.profile);
    }

    private void setNotLogin() {
        user = new STUser();
        statusView.configData(user);
        statusView.getActionButton().setText("アカウントにログイン");
        statusView.getActionButton().setTextColor(0xffFFffFF);
        statusView.getActionButton().setBackgroundColor(0xFF28ABEC);
        statusView.getActionButton().setVisibility(View.VISIBLE);
        nameLabel.setText("未ログインユーザー");
        userInfoLabel.setText("年齢・性別・所在地");
        descriptionLabel.setText("アカウントにログインして、ファッションの新しい楽しみを発見しよう！");
    }

    private void doFollowAction(boolean doFollow) {
        if (doFollow) {
            statusView.getActionButton().setBackgroundColor(0xFF28ABEC);
            statusView.getActionButton().setTextColor(0xffffffff);
            statusView.getActionButton().setText("フォロー中");
            statusView.getActionButton().setVisibility(View.VISIBLE);
        } else {
            statusView.getActionButton().setBackgroundColor(0xFFEFEFEF);
            statusView.getActionButton().setTextColor(0xFF9B9B9B);
            statusView.getActionButton().setText("＋ フォローする");
            statusView.getActionButton().setVisibility(View.VISIBLE);
        }
        if (user == null)
            return;
        statusView.configData(user);
    }

    private void followAction() {
        if (user == null)
            return;
        if (user.userID == 0)
            return;
        boolean doFollow;
        doFollow = user.followID == 0 ? true : false;
        doFollowAction(doFollow);
        if (doFollow) {
            DataSender.createFollowStaff(user.userID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (err != null) {
                        Log.i("STMyPageHeaderView", "" + err);
                        return;
                    }
                    doFollowAction(isSuccess);
                }
            });
        } else {
            DataSender.deleteFollowStaff(user.userID, user.followID, new STCompletion.Base() {
                @Override
                public void onRequestComplete(boolean isSuccess, STError err) {
                    if (err != null) {
                        Log.i("STMyPageHeaderView", "" + err);
                        return;
                    }
                    doFollowAction(!isSuccess);
                }
            });
        }
    }

    private void initShopButton() {
        if (activity != null)
            shopInfoButton = new STMyPageShopButton(activity);
        else
            shopInfoButton = new STMyPageShopButton(view);
        Log.i("STMyPageHeaderView01", "user.shopID" + user.shopID);
        if (user.shopID != 0) {
            btnShopInfo.setVisibility(View.VISIBLE);
            STShop stShop = new STShop();
            try {
                stShop = DataLoader.getShop(user.shopID);
            } catch (Exception e) {
            }
            if (stShop == null)
                stShop = new STShop();
            if (stShop.shopID != 0)
                shopInfoButton.configData(stShop);
            else {
                DataPreserver.saveShop(user.shopID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        STShop stShop = DataLoader.getShop(user.shopID);
                        shopInfoButton.configData(stShop);
                    }
                });
            }
        } else {
            btnShopInfo.setVisibility(View.GONE);
        }
    }

    //
    public LinearLayout getBtnShopInfo() {
        return btnShopInfo;
    }
}
