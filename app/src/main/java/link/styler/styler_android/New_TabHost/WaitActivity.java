package link.styler.styler_android.New_TabHost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import link.styler.styler_android.R;

public class WaitActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait);
        finish();
    }
}
