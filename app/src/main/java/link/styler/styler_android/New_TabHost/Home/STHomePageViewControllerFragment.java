package link.styler.styler_android.New_TabHost.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import link.styler.STStructs.STEnums;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.InfiniteViewPagerHandler;
import link.styler.styler_android.New_TabHost.Common.STBlankFragment;
import link.styler.styler_android.R;

public class STHomePageViewControllerFragment extends Fragment {

    //model
    ViewPagerAdapter adapter = null;

    //view
    private LinearLayout linearLayoutFragment;
    private ViewPager viewPager;

    private static int homeViewPagerID;
    private static STEnums.resumeType resumeType = STEnums.resumeType.none;

    //helper

    private void createAdapter() {

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new STBlankFragment(), "イベント (Event)");
        adapter.addFrag(new STPopularPostFragment(), "人気Post (Popular Post)");
        adapter.addFrag(new STNewArrivalsPostFragment(), "新着Post (New Post)");
        adapter.addFrag(new STPopularItemFragment(), "注目アイテム (Featured item)");
        adapter.addFrag(new STShopAreaFragment(), "ショップ (Shop)");
        adapter.addFrag(new STEventViewControllerFragment(), "イベント (Event)");
        adapter.addFrag(new STPopularPostFragment(), "人気Post (Popular Post)");
        adapter.addFrag(new STNewArrivalsPostFragment(), "新着Post (New Post)");
        adapter.addFrag(new STBlankFragment(), "注目アイテム (Featured item)");
    }

    public static void setHomeViewPagerID(int homeViewPagerID) {
        STHomePageViewControllerFragment.homeViewPagerID = homeViewPagerID;
    }

    public static void setResumeType(STEnums.resumeType resumeType) {
        STHomePageViewControllerFragment.resumeType = resumeType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeViewPagerID = 6;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_home_page_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        viewPager = (ViewPager) view.findViewById(R.id.viewpagerHome);
        if (adapter == null)
            createAdapter();

        config(viewPager);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addAction();
    }

    private void config(ViewPager viewPager) {
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(homeViewPagerID);
        viewPager.setOffscreenPageLimit(9);
        viewPager.addOnPageChangeListener(new InfiniteViewPagerHandler(viewPager, 5) {
            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                homeViewPagerID = mViewPager.getCurrentItem();
            }
        });
    }

    private void addAction() {

    }

    @Override
    public void onResume() {
        super.onResume();
        switch (resumeType) {
            case createPost:
                viewPager.setCurrentItem(homeViewPagerID);
                resumeType = STEnums.resumeType.none;
                break;
            case createReply:
                viewPager.setCurrentItem(homeViewPagerID);
                resumeType = STEnums.resumeType.none;
                break;
            case createEvent:
                viewPager.setCurrentItem(homeViewPagerID);
                resumeType = STEnums.resumeType.none;
                break;
            case none:
                break;
            default:
                break;
        }
        Log.i("STHomePageVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STHomePageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STHomePageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STHomePageVCF", "onDestroy");
    }
}
