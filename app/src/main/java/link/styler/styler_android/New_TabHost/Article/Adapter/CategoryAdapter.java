package link.styler.styler_android.New_TabHost.Article.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import link.styler.STStructs.STEnums;
import link.styler.styler_android.New_TabHost.Article.STArticleNewListViewControllerFragment;
import link.styler.styler_android.New_TabHost.Article.STCoordinateListViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by macOS on 1/23/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private List<String> listCategory;
    private FragmentActivity fragmentActivity;

    public CategoryAdapter(List<String> listCategory, FragmentActivity fragmentActivity) {
        this.listCategory = listCategory;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articles_btn_category, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String categoryName = listCategory.get(position);
        holder.btnCategory.setText(categoryName);

        //Action
        holder.btnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryName.equals("COORDINATE")) {
                    Log.i("CategoryAdapter","COORDINATE");
                    STCoordinateListViewControllerFragment fragment = new STCoordinateListViewControllerFragment();
                    fragment.setNameList(categoryName);
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                } else {
                    STArticleNewListViewControllerFragment fragment = new STArticleNewListViewControllerFragment();
                    fragment.setTypeList(STEnums.STArticleListType.category);
                    fragment.setNameList(categoryName);
                    fragmentActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .add(android.R.id.tabcontent, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button btnCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            btnCategory = (Button) itemView.findViewById(R.id.btnCategory);

        }
    }
}