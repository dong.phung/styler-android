package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.models.STItem;
import link.styler.models.STLike;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterItem;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/20/17.
 */
public class STMyPageLikeViewControllerFragment extends Fragment {

    //model
    private RealmResults<STLike> likes;
    private RealmList<STItem> items = new RealmList<>();
    private int userID = 0;
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterItem adapterItem = null;

    public void createAdapterItem() {
        likes = DataLoader.getLikesOfUser(userID);
        items = new RealmList<STItem>();
        for (STLike like : likes) {
            items.add(like.item);
        }
        this.adapterItem = new MyAdapterItem(getActivity(), items, STEnums.STItemListCollectionViewType.noAvatar);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (STMyPageLikeViewControllerFragment.this.adapterItem == null)
            createAdapterItem();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_like_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_Item);
        gridView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        STMyPageLikeViewControllerFragment.this.gridView.setAdapter(adapterItem);
        if (adapterItem.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });
        STMyPageLikeViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageLikeVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageLikeVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageLikeVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageLikeVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageLikeVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageLikeVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveLikesOfUser(userID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageLikeViewControllerFragment.this.likes = DataLoader.getLikesOfUser(userID);
                    STMyPageLikeViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STLike like : STMyPageLikeViewControllerFragment.this.likes) {
                        STMyPageLikeViewControllerFragment.this.items.add(like.item);
                    }

                    if (STMyPageLikeViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageLikeViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageLikeViewControllerFragment.this.gridView.getAdapter();
                    STMyPageLikeViewControllerFragment.this.adapterItem.setItems(STMyPageLikeViewControllerFragment.this.items);
                    STMyPageLikeViewControllerFragment.this.adapterItem.notifyDataSetChanged();
                }

                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageLikeViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveLikesOfUser(userID, STMyPageLikeViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageLikeViewControllerFragment.this.likes = DataLoader.getLikesOfUser(userID);
                    STMyPageLikeViewControllerFragment.this.items = new RealmList<STItem>();
                    for (STLike like : STMyPageLikeViewControllerFragment.this.likes) {
                        STMyPageLikeViewControllerFragment.this.items.add(like.item);
                    }

                    if (STMyPageLikeViewControllerFragment.this.adapterItem == null)
                        createAdapterItem();

                    STMyPageLikeViewControllerFragment.this.adapterItem = (MyAdapterItem) STMyPageLikeViewControllerFragment.this.gridView.getAdapter();
                    STMyPageLikeViewControllerFragment.this.adapterItem.setItems(STMyPageLikeViewControllerFragment.this.items);
                    STMyPageLikeViewControllerFragment.this.adapterItem.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageLikeViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
