package link.styler.styler_android.New_TabHost.Home.PostDetail;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STComment;
import link.styler.models.STPost;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterComment;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.PopUpLogin;
import link.styler.styler_android.R;

public class STPostCommentViewControllerFragment extends Fragment {

    //model
    private STPost post = new STPost();
    private Integer postID = 0;
    private RealmResults<STComment> comments;
    private Integer lastObjectID;
    private MyAdapterComment adapterComment = null;

    //view
    LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private STSinglePostHeaderView headerView;
    private TextView commentCountLabel;

    private STCommentInputView commentInputView;
    private GridView tableView;

    private PullRefreshLayout pullRefreshLayout;
    private ScrollView scrollView;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    protected static int selectedItemID;


    String notificationToken; // TODO: 4/4/17 notificationToken
    boolean isNewCommentPosted;

    private void createAdapterComment() {
        comments = DataLoader.getComments(postID);
        adapterComment = new MyAdapterComment(getActivity(), comments);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.post = DataLoader.getPost(postID);
        if (adapterComment == null)
            createAdapterComment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_post_comment_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        // Create view: new include, findViewById
        // in ViewDidLoad, InitView
        navigation = new STBaseNavigationController(view);
        headerView = new STSinglePostHeaderView(view, getActivity());
        commentCountLabel = (TextView) view.findViewById(R.id.commentCountLabel);
        commentInputView = new STCommentInputView(view);
        tableView = (GridView) view.findViewById(R.id.gridView);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefeshComment);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        return view;

    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navigation.getTitleNavigation().setText("コメント");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        if (postID == 0)
            return;

        post = DataLoader.getPost(postID);
        if (post != null) {
            fillDataPostToView();
        }

        DataPreserver.savePost(postID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                post = DataLoader.getPost(postID);
                if (post != null) {
                    fillDataPostToView();
                } else {
                    post = new STPost();
                    fillDataPostToView();
                }
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });


        commentCountLabel.setText("コメント (" + comments.size() + ")");
        tableView.setAdapter(adapterComment);
        setGridViewHeightBasedOnChildren(tableView, 1);

        if (comments.size() <= 1) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void fillDataPostToView() {
        headerView.configData(post);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        DataPreserver.getComments(postID, null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STPostCommentViewControllerFragment.this.comments = DataLoader.getComments(STPostCommentViewControllerFragment.this.postID);
                    commentCountLabel.setText("コメント (" + comments.size() + ")");

                    STPostCommentViewControllerFragment.this.adapterComment.setComments(comments);
                    STPostCommentViewControllerFragment.this.adapterComment.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(tableView, 1);
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STPostCommentViewControllerFragment.this.lastObjectID = lastObjectID;
                STPostCommentViewControllerFragment.this.isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
        DataPreserver.savePost(postID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                post = DataLoader.getPost(postID);
                if (post != null) {
                    fillDataPostToView();
                } else {
                    post = new STPost();
                    fillDataPostToView();
                }
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.getComments(postID, lastObjectID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STPostCommentViewControllerFragment.this.comments = DataLoader.getComments(STPostCommentViewControllerFragment.this.postID);
                    commentCountLabel.setText("コメント (" + comments.size() + ")");
                    STPostCommentViewControllerFragment.this.adapterComment.setComments(comments);
                    STPostCommentViewControllerFragment.this.adapterComment.notifyDataSetChanged();
                    setGridViewHeightBasedOnChildren(tableView, 1);
                    hideKeyBoard();
                }
                progressBar.setVisibility(View.GONE);
                STPostCommentViewControllerFragment.this.isLoading = false;
                scrollBottom();
            }
        });
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        try {
            ListAdapter listAdapter = gridView.getAdapter();
            if (listAdapter == null) {
                return;
            }

            int totalHeight = 0;
            int items = listAdapter.getCount();

            for (int i = 0; i < items; i++) {
                View listItem = listAdapter.getView(i, null, gridView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            totalHeight = totalHeight + 50;

            ViewGroup.LayoutParams params = gridView.getLayoutParams();
            params.height = totalHeight;
            gridView.setLayoutParams(params);
        } catch (Exception e) {
        }
    }

    public void setPostID(Integer postID) {
        this.postID = postID;
    }

    private void addAction() {

        commentInputView.getSelectLikeItemButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLikeItem();
                hideKeyBoard();
            }
        });

        commentInputView.getSendButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "";
                try {
                    status = LoginManager.getsIntstance().me().status;
                } catch (Exception e) {
                    status = "";
                }
                if (status == "" || status.equals("none")) {
                    PopUpLogin login = new PopUpLogin(getActivity());
                    login.show();
                } else {
                    sendComment();
                }
            }
        });


        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity()
                        .getSupportFragmentManager()
                        .popBackStackImmediate();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                    Log.i("123456", "diff: " + diff);
                    Log.i("123456", "gotData: " + gotData);

                    if (diff <= 0 && !gotData) {
                        Log.i("123456aaaaaa", "diff: " + diff);
                        gotData = true;
                        getData();

                    } else if (diff > 145 && gotData) {
                        gotData = false;
                    }
                }
            });
        }
    }

    private void selectLikeItem() {
        String status;
        try {
            status = LoginManager.getsIntstance().me().status;
            if (status.equals("None")) {
                PopUpLogin login = new PopUpLogin(getActivity());
                login.show();
            }
        } catch (Exception e) {
            PopUpLogin login = new PopUpLogin(getActivity());
            login.show();
        }

        STSelectLikeItemViewControllerFragment fragment = new STSelectLikeItemViewControllerFragment();
        fragment.setPostID(postID);
        getFragmentManager()
                .beginTransaction()
                .add(android.R.id.tabcontent, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void sendComment() {
        Log.i("STPostCommentVC001", "sendComment");
        if (post.postID == 0 || post == null)
            return;
        Log.i("STPostCommentVC002", "postID: " + post.postID);
        if (isLoading)
            return;
        isLoading = true;
        commentInputView.getSendButton().setEnabled(false);
        commentInputView.getSendButton().setBackgroundColor(0xFF9B9B9B);
        DataSender.createComment(postID, commentInputView.getEditText().getText().toString(), null, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                isLoading = false;
                commentInputView.getSendButton().setEnabled(true);
                commentInputView.getSendButton().setBackgroundColor(0xFF4A4A4A);
                commentInputView.getEditText().setText("");
                if (err != null) {
                    Log.i("STPostCommentVC003", "err: " + err);
                    return;
                }
                Log.i("STPostCommentVC004", "createComment: " + isSuccess);
                if (isSuccess) {
                    getData();
                    isNewCommentPosted = true;
                }
            }
        });
    }

    private void sendItem(Integer selectedItemID) {
        if (isLoading)
            return;
        if (post == null || post.postID == 0)
            return;
        if (selectedItemID == null || selectedItemID == 0)
            return;
        progressBar.setVisibility(View.VISIBLE);
        commentInputView.getEditText().setEnabled(false);
        commentInputView.getSendButton().setBackgroundColor(0xFF9B9B9B);
        DataSender.createComment(postID, null, selectedItemID, new STCompletion.Base() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err) {
                if (isSuccess) {
                    getData();
                    isNewCommentPosted = true;
                }
                commentInputView.getSendButton().setEnabled(true);
                commentInputView.getSendButton().setBackgroundColor(0xFF4A4A4A);
                commentInputView.getEditText().setText("");
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void scrollBottom() {
        int y = scrollView.getHeight();
        scrollView.scrollTo(0, scrollView.getScrollY() + 2000);
    }

    private void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("STPostCommentVCF", "onResume");
        int itemID = 0;
        itemID = STSelectLikeItemViewControllerFragment.getSelectedItemID();
        if (itemID != 0) {
            STSelectLikeItemViewControllerFragment.setSelectedItemID(0);
            sendItem(itemID);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STPostCommentVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STPostCommentVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STPostCommentVCF", "onDestroy");
    }
}
