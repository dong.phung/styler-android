package link.styler.styler_android.New_TabHost.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.RealmList;
import link.styler.Utils.TransformationUtils;
import link.styler.models.STShop;
import link.styler.styler_android.New_TabHost.Home.ItemDetail.STShopStatusView;
import link.styler.styler_android.New_TabHost.MyPage.STMyPagePageViewControllerFragment;
import link.styler.styler_android.R;

/**
 * Created by admin on 2/22/17.
 */

public class MyAdapterSTPrefectureDetail extends BaseAdapter {
    //model
    private RealmList<STShop> shops = new RealmList<>();
    private STShop shop;

    //view
    private ImageView shopImageView;
    private STShopStatusView shopStatusView;
    private TextView shopNameLabel;
    private TextView addressLabel;
    private TextView nearestStationLabel;
    private TextView descriptionLabel;
    private LinearLayout linearLayout_PrefectureDetailCell;

    //hepler
    private FragmentActivity fragmentActivity;
    private Context context;

    public MyAdapterSTPrefectureDetail(FragmentActivity fragmentActivity, RealmList<STShop> shops) {
        this.fragmentActivity = fragmentActivity;
        this.context = fragmentActivity.getApplicationContext();
        this.shops = shops;
    }

    public void setShops(RealmList<STShop> shops) {
        this.shops = shops;
    }

    @Override
    public int getCount() {
        return shops.size();
    }

    @Override
    public Object getItem(int position) {
        return shops.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = (View) inflater.inflate(R.layout.st_prefecture_detail_cell, null);
        intView(convertView);

        STShop shop = shops.get(position);
        setUpView(shop);
        addControl(shop);
        return convertView;
    }

    private void intView(View view) {
        shopImageView = (ImageView) view.findViewById(R.id.shopImageView);
        shopStatusView = new STShopStatusView(view);
        shopNameLabel = (TextView) view.findViewById(R.id.shopNameLabel);
        addressLabel = (TextView) view.findViewById(R.id.addressLabel);
        nearestStationLabel = (TextView) view.findViewById(R.id.nearestStationLabel);
        descriptionLabel = (TextView) view.findViewById(R.id.descriptionLabel);
        linearLayout_PrefectureDetailCell = (LinearLayout) view.findViewById(R.id.linearLayout_PrefectureDetailCell);
    }

    private void setUpView(STShop shop) {
        Picasso.with(context)
                .load(shop.imageURL)
                .error(R.color.st_whiteGrayColor)
                .transform(new TransformationUtils().new RoundedCornersTransform())
                .into(shopImageView);
        shopStatusView.getActionButton().setText("ショップ詳細をみる");
        shopStatusView.getActionButton().setTextColor(0xFFFFFFFF);
        shopStatusView.getActionButton().setBackgroundColor(0xFF4A4A4A);
        shopStatusView.configDataWithShop(shop);
        if (!shop.name.equals(null))
            shopNameLabel.setText(shop.name);
        if (!shop.address.equals(null))
            addressLabel.setText(shop.address);
        if (!shop.profile.equals(null))
            descriptionLabel.setText(shop.profile);
    }

    private void addControl(final STShop shop) {
        linearLayout_PrefectureDetailCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STMyPagePageViewControllerFragment fragment = new STMyPagePageViewControllerFragment();
                fragment.setShopID(shop.shopID);
                fragmentActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
