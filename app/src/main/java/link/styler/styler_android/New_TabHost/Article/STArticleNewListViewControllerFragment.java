package link.styler.styler_android.New_TabHost.Article;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STArticle;
import link.styler.models.STArticleSort;
import link.styler.models.STCategory;
import link.styler.models.STTag;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;
import link.styler.styler_android.New_TabHost.Article.Adapter.ArticlesAdapter;

public class STArticleNewListViewControllerFragment extends Fragment {

    //model
    private RealmResults<STArticleSort> articleSorts;
    private RealmList<STArticle> articles = new RealmList<STArticle>();
    private Integer categoryID = null;
    private Integer tagID = null;
    private Integer lastObjectID = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private ListView lvArticles;
    private ProgressBar progressBar;

    private PullRefreshLayout pullRefreshLayout;

    //helper
    private ArticlesAdapter articlesAdapter = null;
    private boolean isLoading = false;
    private boolean gotData = false;

    private STEnums.STArticleListType typeList = STEnums.STArticleListType.newList;
    private String nameList = "新着記事";

    public void setTypeList(STEnums.STArticleListType typeList) {
        this.typeList = typeList;
    }

    public void setNameList(String nameList) {
        this.nameList = nameList;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public void setTagID(Integer tagID) {
        this.tagID = tagID;
    }

    private void createArticlesAdapter() {
        switch (typeList) {
            case newList:
                articles = new RealmList<>();
                articleSorts = DataLoader.getArticles();
                for (STArticleSort articleSort : articleSorts) {
                    articles.add(articleSort.article);
                }
                articlesAdapter = new ArticlesAdapter(getActivity(), articles);
                break;
            case category:
                if (categoryID != null) {
                    articles = new RealmList<>();
                    articleSorts = DataLoader.getCategoriezedArticles(categoryID);
                    for (STArticleSort articleSort : articleSorts) {
                        articles.add(articleSort.article);
                    }
                    articlesAdapter = new ArticlesAdapter(getActivity(), articles);
                }
                break;
            case tag:
                if (tagID != null) {
                    articles = new RealmList<>();
                    articleSorts = DataLoader.getRelateKeywordArticles(tagID);
                    for (STArticleSort articleSort : articleSorts) {
                        articles.add(articleSort.article);
                    }
                    articlesAdapter = new ArticlesAdapter(getActivity(), articles);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (typeList == STEnums.STArticleListType.category) {
            Realm realm = Realm.getDefaultInstance();
            categoryID = realm.where(STCategory.class).equalTo("name", nameList).findFirst().categoryID;
        } else if (typeList == STEnums.STArticleListType.tag) {
            Realm realm = Realm.getDefaultInstance();
            tagID = realm.where(STTag.class).equalTo("name", nameList).findFirst().tagID;
        }

        if (articlesAdapter == null)
            createArticlesAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_starticle_new_list_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefresh_articlesList);
        lvArticles = (ListView) view.findViewById(R.id.lvArticles);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText(nameList);
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        if (articlesAdapter == null)
            createArticlesAdapter();
        lvArticles.setAdapter(articlesAdapter);
        progressBar.setVisibility(View.GONE);
        if (articlesAdapter.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        lvArticles.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STPopularItemFragment", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STPopularItemFragment", "visibleItemCount: " + visibleItemCount);
                    Log.i("STPopularItemFragment", "totalItemCount: " + totalItemCount);
                    Log.i("STPopularItemFragment", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });

        lvArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), STArticleWebViewControllerActivity.class);
                intent.putExtra("URL", articles.get(position).articleURL);
                startActivity(intent);
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;
        switch (typeList) {
            case newList:
                DataPreserver.saveArticles(null, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getArticles();
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            case category:
                DataPreserver.saveCategorizedArticles(categoryID, null, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getCategoriezedArticles(categoryID);
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            case tag:
                DataPreserver.saveRelateKeywordArticles(tagID, null, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getRelateKeywordArticles(tagID);
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            default:
                break;
        }
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        switch (typeList) {
            case newList:
                DataPreserver.saveArticles(lastObjectID, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getArticles();
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            case category:
                DataPreserver.saveCategorizedArticles(categoryID, lastObjectID, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getCategoriezedArticles(categoryID);
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            case tag:
                DataPreserver.saveRelateKeywordArticles(tagID, lastObjectID, false, new STCompletion.WithLastObjectID() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                        if (isSuccess) {
                            articles = new RealmList<STArticle>();
                            articleSorts = DataLoader.getRelateKeywordArticles(tagID);
                            for (STArticleSort articleSort : articleSorts) {
                                articles.add(articleSort.article);
                            }
                            articlesAdapter.setArticles(articles);
                            articlesAdapter.notifyDataSetChanged();
                        }
                        if (lastObjectID != null && lastObjectID != 0)
                            STArticleNewListViewControllerFragment.this.lastObjectID = lastObjectID;
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                    }
                });
                break;
            default:
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("STArticleNewListVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STArticleNewListVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STArticleNewListVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STArticleNewListVCF", "onDestroy");
    }
}