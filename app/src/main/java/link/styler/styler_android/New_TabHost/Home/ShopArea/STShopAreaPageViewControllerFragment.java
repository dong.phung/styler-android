package link.styler.styler_android.New_TabHost.Home.ShopArea;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import link.styler.DataManager.Loader.DataLoader;
import link.styler.models.STLocation;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STShopAreaPageViewControllerFragment extends Fragment {

    //model
    private int locationID = -1;
    private STLocation location = new STLocation();

    //view
    private LinearLayout linearLayoutFragment;
    private ViewPager viewPagerShopAreaPage;
    private STBaseNavigationController navigation;

    //helper


    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        location = DataLoader.getLocation(locationID);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_shop_area_page_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        viewPagerShopAreaPage = (ViewPager) view.findViewById(R.id.viewPagerShopAreaPage);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        STPrefectureNewViewControllerFragment newList = new STPrefectureNewViewControllerFragment();
        newList.setLocationID(locationID);
        adapter.addFrag(newList, "新規");

        STPrefectureListViewControllerFragment allList = new STPrefectureListViewControllerFragment();
        allList.setLocationID(locationID);
        adapter.addFrag(allList, "A-Z");

        viewPagerShopAreaPage.setAdapter(adapter);

        navigation = new STBaseNavigationController(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText(location.name);
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STShopAreaPageVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STShopAreaPageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STShopAreaPageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STShopAreaPageVCF", "onDestroy");
    }
}
