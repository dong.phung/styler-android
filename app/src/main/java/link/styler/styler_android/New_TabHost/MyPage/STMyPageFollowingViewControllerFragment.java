package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STFollow;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterFollowing;
import link.styler.styler_android.R;

/**
 * Created by macOS on 3/20/17.
 */
public class STMyPageFollowingViewControllerFragment extends Fragment {

    //model
    private int userID;
    private RealmResults<STFollow> followings;
    private RealmList<STUser> staffs = new RealmList<STUser>();
    private Integer lastObjectID = null;

    //view
    private GridView gridView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterFollowing adapterFollowing = null;

    public void createAdapterFollower() {
        this.followings = DataLoader.getFollowsOfUser(userID);
        this.staffs = new RealmList<STUser>();
        for (STFollow follow : this.followings) {
            this.staffs.add(follow.staff);
        }
        this.adapterFollowing = new MyAdapterFollowing(getActivity(), this.staffs);
        adapterFollowing.setUserID(userID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (adapterFollowing == null)
            createAdapterFollower();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_my_page_following_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_following);
        gridView = (GridView) view.findViewById(R.id.gridViewFollowing);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        gridView.setAdapter(adapterFollowing);
        if (adapterFollowing.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }
        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                        Logger.print("onRefresh");
                    }
                }, 2000);
            }
        });

        STMyPageFollowingViewControllerFragment.this.gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMyPageFollowingVC", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMyPageFollowingVC", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMyPageFollowingVC", "totalItemCount: " + totalItemCount);
                    Log.i("STMyPageFollowingVC", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STMyPageFollowingVC", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STMyPageFollowingVC", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveFollowingOfUser(userID, null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageFollowingViewControllerFragment.this.followings = DataLoader.getFollowsOfUser(userID);
                    STMyPageFollowingViewControllerFragment.this.staffs = new RealmList<STUser>();
                    for (STFollow follow : STMyPageFollowingViewControllerFragment.this.followings) {
                        staffs.add(follow.staff);
                    }
                    if (STMyPageFollowingViewControllerFragment.this.adapterFollowing == null)
                        createAdapterFollower();

                    STMyPageFollowingViewControllerFragment.this.adapterFollowing = (MyAdapterFollowing) STMyPageFollowingViewControllerFragment.this.gridView.getAdapter();
                    STMyPageFollowingViewControllerFragment.this.adapterFollowing.setStaffs(STMyPageFollowingViewControllerFragment.this.staffs);
                    STMyPageFollowingViewControllerFragment.this.adapterFollowing.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageFollowingViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveFollowingOfUser(userID, STMyPageFollowingViewControllerFragment.this.lastObjectID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageFollowingViewControllerFragment.this.followings = DataLoader.getFollowsOfUser(userID);
                    STMyPageFollowingViewControllerFragment.this.staffs = new RealmList<STUser>();
                    for (STFollow follow : STMyPageFollowingViewControllerFragment.this.followings) {
                        staffs.add(follow.staff);
                    }
                    if (STMyPageFollowingViewControllerFragment.this.adapterFollowing == null)
                        createAdapterFollower();

                    STMyPageFollowingViewControllerFragment.this.adapterFollowing = (MyAdapterFollowing) STMyPageFollowingViewControllerFragment.this.gridView.getAdapter();
                    STMyPageFollowingViewControllerFragment.this.adapterFollowing.setStaffs(STMyPageFollowingViewControllerFragment.this.staffs);
                    STMyPageFollowingViewControllerFragment.this.adapterFollowing.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageFollowingViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}