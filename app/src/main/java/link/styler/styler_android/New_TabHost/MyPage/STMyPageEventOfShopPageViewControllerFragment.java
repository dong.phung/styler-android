package link.styler.styler_android.New_TabHost.MyPage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.STStructs.STError;
import link.styler.Utils.Logger;
import link.styler.models.STEvent;
import link.styler.models.STEventSort;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterEvent;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/12/17.
 */

public class STMyPageEventOfShopPageViewControllerFragment extends Fragment {

    //model,api
    private RealmResults<STEventSort> events;
    private RealmList<STEvent> stEvents = new RealmList<>();
    private int shopID = 0;
    private Integer lastObjectID = null;

    //view
    private GridView collectionView;
    private PullRefreshLayout pullRefreshLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterEvent adapterEvent = null;

    public void createAdapterEvent() {
        this.events = DataLoader.getEventsOfShop(shopID);
        this.adapterEvent = new MyAdapterEvent(getActivity(), events);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent == null)
            createAdapterEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.print("onCreateView");
        View view = inflater.inflate(R.layout.fragment_st_my_page_event_of_shop_page_view_controller, container, false);

        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_event);
        collectionView = (GridView) view.findViewById(R.id.gridView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        collectionView.setAdapter(adapterEvent);
        if (adapterEvent.getCount() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            getNewData();
        }

        addAction();
    }

    private void addAction() {
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        STMyPageEventOfShopPageViewControllerFragment.this.collectionView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STNewArrivalsPostF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STNewArrivalsPostF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STNewArrivalsPostF", "totalItemCount: " + totalItemCount);
                    Log.i("STNewArrivalsPostF", "gotData: " + gotData);
                    if (!gotData) {
                        Log.i("STNewArrivalsPostF", "getData");
                        gotData = true;
                        getData();
                    } else {
                        Log.i("STNewArrivalsPostF", "don't getData");
                    }
                } else {
                    gotData = false;
                }
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (isLoading)
            return;
        isLoading = true;

        DataPreserver.saveEventsOfShop(shopID, null, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageEventOfShopPageViewControllerFragment.this.events = DataLoader.getEventsOfShop(shopID);

                    if (STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent == null)
                        createAdapterEvent();

                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent = (MyAdapterEvent) STMyPageEventOfShopPageViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent.setEvents(STMyPageEventOfShopPageViewControllerFragment.this.events);
                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageEventOfShopPageViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        DataPreserver.saveEventsOfShop(shopID, STMyPageEventOfShopPageViewControllerFragment.this.lastObjectID, false, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    STMyPageEventOfShopPageViewControllerFragment.this.events = DataLoader.getEventsOfShop(shopID);

                    if (STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent == null)
                        createAdapterEvent();

                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent = (MyAdapterEvent) STMyPageEventOfShopPageViewControllerFragment.this.collectionView.getAdapter();
                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent.setEvents(STMyPageEventOfShopPageViewControllerFragment.this.events);
                    STMyPageEventOfShopPageViewControllerFragment.this.adapterEvent.notifyDataSetChanged();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMyPageEventOfShopPageViewControllerFragment.this.lastObjectID = lastObjectID;
                isLoading = false;
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }
}