package link.styler.styler_android.New_TabHost.Message;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;

import io.realm.RealmList;
import io.realm.RealmResults;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STMessage;
import link.styler.models.STUser;
import link.styler.styler_android.New_TabHost.Adapter.MyAdapterMessage;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Message.View.STMessageInitialView;
import link.styler.styler_android.New_TabHost.Message.View.STMessageItemInfoView;
import link.styler.styler_android.New_TabHost.Message.View.STMessageTextView;
import link.styler.styler_android.R;

public class STMessageViewControllerFragment extends Fragment {

    //model
    private RealmResults<STMessage> stMessages;
    private RealmList<STMessage> messages = new RealmList<>();
    private Integer destinationUserID = 0;
    private String destinationUserName = "";
    private Integer itemID = 0;
    private Integer lastObjectID = null;

    //view
    private LinearLayout linearLayoutFragment;

    private STBaseNavigationController navigationView;
    private PullRefreshLayout pullRefreshLayout;
    private GridView tableView;
    private STMessageInitialView initialView;
    private LinearLayout layoutInitialView;
    private STMessageTextView messageView;
    private STMessageItemInfoView itemInfoView;
    private LinearLayout itemInfoLayout;
    private ProgressBar progressBar;

    //helper
    private boolean isLoading = false;
    private boolean gotData = false;
    private MyAdapterMessage adapter = null;

    private void createAdapter() {
        stMessages = DataLoader.getMessagesFromDestination(destinationUserID);
        messages = new RealmList<>();
        messages.addAll(stMessages);
        adapter = new MyAdapterMessage(getActivity(), messages);
    }

    public void setDestinationUserID(Integer destinationUserID) {
        this.destinationUserID = destinationUserID;
    }

    public void setDestinationUserName(String destinationUserName) {
        this.destinationUserName = destinationUserName;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (destinationUserID == 0)
            return;
        if (destinationUserName.equals("")) {
            STUser user = DataLoader.getUser(destinationUserID);
            if (user == null) {
                DataPreserver.saveUser(destinationUserID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        STUser user = DataLoader.getUser(destinationUserID) != null ? DataLoader.getUser(destinationUserID) : new STUser();
                        destinationUserName = user.name;
                    }
                });
            } else {
                destinationUserName = user.name;
            }
        } else if (destinationUserName == null) {
            destinationUserName = "";
        }

        if (adapter == null)
            createAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_message_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigationView = new STBaseNavigationController(view);
        initialView = new STMessageInitialView(view);
        layoutInitialView = (LinearLayout) view.findViewById(R.id.layoutInitialView);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pullRefesh_message);
        tableView = (GridView) view.findViewById(R.id.gridView);
        messageView = new STMessageTextView(view);
        itemInfoView = new STMessageItemInfoView(getActivity(), view, itemID);
        itemInfoLayout = (LinearLayout) view.findViewById(R.id.itemInfoLayout);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.GONE);
        navigationView.getTitleNavigation().setText(!destinationUserName.equals("") ? destinationUserName : "メッセージ");
        navigationView.getBackButton().setImageResource(R.drawable.button_previous);
        navigationView.getUpdateButton().setVisibility(View.GONE);
        navigationView.getMoreButton().setImageResource(R.drawable.icon_more2x);
        layoutInitialView.setVisibility(View.VISIBLE);

        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        tableView.setVisibility(View.GONE);
        loadData();

        if (itemID == 0)
            itemInfoLayout.setVisibility(View.GONE);
        else
            itemInfoLayout.setVisibility(View.VISIBLE);

        addAction();
    }

    private void loadData() {
        String status;
        try {
            status = LoginManager.getsIntstance().me().status;
        } catch (Exception e) {
            status = "";
        }
        if (status.equals("Customer")) {
            String title = "下記についてよく聞かれます：";
            String[] descriptions = {"1. 商品を活用したコーディネイトについて", "2. 商品の店頭での在庫について", "3. 来店予約について"};
            initialView.initView(title, descriptions);
        } else {
            String title = "フォローのお礼と一緒によく聞かれること：";
            String[] descriptions = {"1. いま探しているアイテムについて", "2. 普段よく着る服のテイストについて", "3. 普段よく行くショップについて"};
            initialView.initView(title, descriptions);
        }
        tableView.setAdapter(adapter);
        setViewList();
        //if (adapter.getCount() == 0) {
        progressBar.setVisibility(View.VISIBLE);
        getNewData();
        //}
    }

    private void setViewList() {
        if (adapter.getCount() != 0) {
            tableView.setVisibility(View.VISIBLE);
            layoutInitialView.setVisibility(View.GONE);
        } else {
            tableView.setVisibility(View.GONE);
            layoutInitialView.setVisibility(View.VISIBLE);
        }
    }

    private void addAction() {
        navigationView.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onRefreshData();
                    }
                }, 2000);
            }
        });

        tableView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    Log.i("STMessageVCF", "firstVisibleItem: " + firstVisibleItem);
                    Log.i("STMessageVCF", "visibleItemCount: " + visibleItemCount);
                    Log.i("STMessageVCF", "totalItemCount: " + totalItemCount);
                    Log.i("STMessageVCF", "gotData: " + gotData);
                    if (!gotData) {
                        gotData = true;
                        getData();
                    }
                } else {
                    gotData = false;
                }
            }
        });

        messageView.getSendButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    private void onRefreshData() {
        getNewData();
        pullRefreshLayout.setRefreshing(false);
    }

    private void getNewData() {
        if (destinationUserID == 0)
            return;
        if (isLoading)
            return;
        isLoading = true;
        Integer lastestID = null;
        try {
            lastestID = messages.last().messageID != 0 ? messages.last().messageID : null;
        } catch (Exception e) {
        }
        DataPreserver.saveMessagesWithUserID(destinationUserID, null, null, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stMessages = DataLoader.getMessagesFromDestination(destinationUserID);
                    messages = new RealmList<STMessage>();
                    messages.addAll(stMessages);
                    if (STMessageViewControllerFragment.this.adapter == null)
                        STMessageViewControllerFragment.this.createAdapter();
                    STMessageViewControllerFragment.this.adapter.setMessages(messages);
                    STMessageViewControllerFragment.this.adapter.notifyDataSetChanged();
                    STMessageViewControllerFragment.this.setViewList();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMessageViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    private void getData() {
        if (destinationUserID == 0)
            return;
        if (isLoading)
            return;
        isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        Integer lastestID = null;
        try {
            lastestID = messages.last().messageID != 0 ? messages.last().messageID : null;
        } catch (Exception e) {
        }
        DataPreserver.saveMessagesWithUserID(destinationUserID, lastObjectID, lastestID, new STCompletion.WithLastObjectID() {
            @Override
            public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID) {
                if (isSuccess) {
                    stMessages = DataLoader.getMessagesFromDestination(destinationUserID);
                    messages = new RealmList<STMessage>();
                    messages.addAll(stMessages);
                    if (STMessageViewControllerFragment.this.adapter == null)
                        STMessageViewControllerFragment.this.createAdapter();
                    STMessageViewControllerFragment.this.adapter.setMessages(messages);
                    STMessageViewControllerFragment.this.adapter.notifyDataSetChanged();
                    STMessageViewControllerFragment.this.setViewList();
                }
                if (lastObjectID != null && lastObjectID != 0)
                    STMessageViewControllerFragment.this.lastObjectID = lastObjectID;
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    private void sendMessage() {
        if (destinationUserID == 0 || messageView.getEditText().getText().length() <= 0)
            return;
        if (isLoading)
            return;
        hideKeyBoard();
        progressBar.setVisibility(View.VISIBLE);
        isLoading = true;
        messageView.getSendButton().setEnabled(false);
        messageView.getSendButton().setBackgroundColor(0xFF9B9B9B);
        DataSender.createMesseage(destinationUserID, messageView.getEditText().getText().toString(), itemID, new STCompletion.WithNullableInt() {
            @Override
            public void onRequestComplete(boolean isSuccess, Integer nullableInt) {
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                messageView.getEditText().setText("");
                messageView.getSendButton().setBackgroundColor(0xFF4A4A4A);
                messageView.getSendButton().setEnabled(true);
                if (isSuccess) {
                    itemID = 0;
                    itemInfoLayout.setVisibility(View.GONE);
                    getNewData();
                } else {
                    Toast.makeText(getActivity(), "メッセージの送信に失敗しました", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STMessageVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STMessageVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STMessageVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STMessageVCF", "onDestroy");
    }
}