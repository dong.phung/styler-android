package link.styler.styler_android.New_TabHost.Create;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import link.styler.DataManager.Loader.DataLoader;
import link.styler.Utils.LoginManager;
import link.styler.styler_android.New_TabHost.Adapter.ViewPagerAdapter;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STReplySelectItemPageViewControllerFragment extends Fragment {

    //model
    private Integer postID = null;
    private Integer categoryID = null;
    private Integer shopID = null;

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private ViewPager viewPager;
    private TextView txtCreateReply;

    //helper

    public void setPostID(Integer postID) {
        this.postID = postID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            shopID = LoginManager.getsIntstance().me().shopID;
        } catch (Exception e) {
            shopID = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_reply_select_item_page_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        viewPager = (ViewPager) view.findViewById(R.id.vpReplySellectItem);
        txtCreateReply = (TextView) view.findViewById(R.id.txtCreateReply);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);
        navigation.getTitleNavigation().setText("過去の投稿から選択");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        if (shopID == null || postID == null || categoryID == null)
            return;
        addTab();

        addAction();
    }

    private void addTab() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        STReplySelectPastItemViewControllerFragment fm1 = new STReplySelectPastItemViewControllerFragment();
        fm1.ItemFindByCategoryFragment(postID, categoryID);
        adapter.addFrag(fm1, DataLoader.getCategory(categoryID).name + "");


        STReplySelectAllItemViewControllerFragment fm2 = new STReplySelectAllItemViewControllerFragment();
        fm2.AllItemFragment(postID);
        adapter.addFrag(fm2, "全アイテム");

        viewPager.setAdapter(adapter);
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        txtCreateReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // don't convert
                Intent intent = new Intent(getActivity(), STCreateReplyViewControllerActivity.class);
                intent.putExtra("postID", postID);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STReplySelectItemPVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STReplySelectItemPVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STReplySelectItemPVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STReplySelectItemPVCF", "onDestroy");
    }
}
