package link.styler.styler_android.New_TabHost;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.Utils.LoginManager;
import link.styler.models.STPost;
import link.styler.styler_android.New_TabHost.Create.STCreatePostViewControllerActivity;
import link.styler.styler_android.R;

/**
 * Created by macOS on 4/11/17.
 */

public class PopUpMoreActionPost extends Dialog {
    //model
    STPost post = new STPost();
    //view
    public Context context;
    public Button editButton, reportDeleteButton, cancelButton;

    //helper
    int meID = 0;
    int userID = 0;

    public PopUpMoreActionPost(Context context, STPost post) {
        super(context);
        this.context = context;
        this.post = post;
        this.userID = post.user.userID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_more_action_post);
        addControls();
        if (userID == 0)
            dismiss();

        initView();
        addEvents();

    }

    private void addControls() {
        editButton = (Button) findViewById(R.id.editButton);
        reportDeleteButton = (Button) findViewById(R.id.reportButtonDialog);
        cancelButton = (Button) findViewById(R.id.cancelButton);

    }

    private void initView() {
        try {
            meID = LoginManager.getsIntstance().me().userID;
        } catch (Exception e) {
        }
        if (meID != post.user.userID) {                     //report
            editButton.setVisibility(View.GONE);
            reportDeleteButton.setText("報告");
        } else {                                            // delete
            editButton.setVisibility(View.VISIBLE);

            if (post.repliesCount <= 0) {
                reportDeleteButton.setVisibility(View.VISIBLE);
                reportDeleteButton.setText("削除");
            } else {
                reportDeleteButton.setVisibility(View.GONE);
            }
        }
    }

    private void addEvents() {

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAction();
            }
        });

        reportDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDeleteAction();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void editAction() {
        // TODO: 4/11/17 need refactor createPost
        STCreatePostViewControllerActivity.post = post;
        Intent intent = new Intent(context, STCreatePostViewControllerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        dismiss();
    }

    private void reportDeleteAction() {
        if (meID != userID){
            //report
            Toast.makeText(context, "報告されたPostは運営で調査致します。", Toast.LENGTH_LONG).show();
            dismiss();
        } else {
            //detele
            if (post.repliesCount<=0){
                DataSender.deletePost(post.postID, new STCompletion.Base() {
                    @Override
                    public void onRequestComplete(boolean isSuccess, STError err) {
                        dismiss();
                    }
                });
            }
        }
    }

}
