package link.styler.styler_android.New_TabHost.Home.Event;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import link.styler.Utils.GeocodeAddressString;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.New_TabHost.Common.TabsPagerfragment;
import link.styler.styler_android.R;

public class STMapViewController extends AppCompatActivity {

    String address = "";

    private STBaseNavigationController navigation;
    private ViewPager viewPagerMap;
    private ImageButton regionBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_map_view_controller);

        Intent intent = getIntent();
        address = intent.getStringExtra("address");
        viewDidLoad();
        addControls();
    }

    private void viewDidLoad() {
        setupView();
    }

    private void setupView() {
        navigation = new STBaseNavigationController(STMapViewController.this);
        navigation.titleNavigation.setText(address);
        navigation.subTileNavigation.setVisibility(View.GONE);
        navigation.backButton.setImageResource(R.drawable.button_close);
        navigation.backButton.setPadding(0, 35, 0, 35);

        viewPagerMap = (ViewPager) findViewById(R.id.mapViewPager);
        setUpMapFragment(address);

        regionBtn = (ImageButton) findViewById(R.id.regionBtn);
    }

    private void setUpMapFragment(String location) {
        new GeocodeAddressString(new GeocodeAddressString.AsyncResponse() {
            @Override
            public void processFinish(double lng, double lnt) {
                TabsPagerfragment tabsPagerfragment = new TabsPagerfragment(getSupportFragmentManager(), lng, lnt);
                viewPagerMap.setAdapter(tabsPagerfragment);
            }
        }).execute(location);
    }

    private void addControls() {
        regionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 5/11/17  regionBtn action
                Log.i("STMapViewController","regionBtn action");
            }
        });
    }
}
