package link.styler.styler_android.New_TabHost.MyPage.Account;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;

import link.styler.Utils.LoginManager;
import link.styler.styler_android.New_TabHost.Common.STWebViewControllerFragment;
import link.styler.styler_android.New_TabHost.MainActivity;
import link.styler.styler_android.New_TabHost.Common.STBaseNavigationController;
import link.styler.styler_android.R;

public class STAccountSettingViewControllerFragment extends Fragment {

    //model

    //view
    private LinearLayout linearLayoutFragment;
    private STBaseNavigationController navigation;
    private LinearLayout profileInfo, loginInfo, termsOfService, privacyPolicy, help, aboutUs, license, logout;
    private Switch swNotification, swEmailNotification, swMailMagazine;

    //helper


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_st_account_setting_view_controller, container, false);
        linearLayoutFragment = (LinearLayout) view.findViewById(R.id.linearLayoutFragment);

        navigation = new STBaseNavigationController(view);
        profileInfo = (LinearLayout) view.findViewById(R.id.profileInfo);
        loginInfo = (LinearLayout) view.findViewById(R.id.loginInfo);
        termsOfService = (LinearLayout) view.findViewById(R.id.termsOfService);
        privacyPolicy = (LinearLayout) view.findViewById(R.id.privacyPolicy);
        help = (LinearLayout) view.findViewById(R.id.help);
        aboutUs = (LinearLayout) view.findViewById(R.id.aboutUs);
        license = (LinearLayout) view.findViewById(R.id.license);
        logout = (LinearLayout) view.findViewById(R.id.logout);

        swNotification = (Switch) view.findViewById(R.id.swNotification);
        swEmailNotification = (Switch) view.findViewById(R.id.swEmailNotification);
        swMailMagazine = (Switch) view.findViewById(R.id.swMailMagazine);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navigation.getTitleNavigation().setText("アカウント設定");
        navigation.getSubTileNavigation().setVisibility(View.GONE);
        navigation.getBackButton().setImageResource(R.drawable.button_previous);

        addAction();
    }

    private void addAction() {
        navigation.getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        profileInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STProfileInfoViewControllerFragment fragment = new STProfileInfoViewControllerFragment();
                fragment.setViewType("edit");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        loginInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STLoginInfoViewControllerFragment fragment = new STLoginInfoViewControllerFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        //// TODO: 3/23/17 switch flagReload

        termsOfService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STWebViewControllerFragment fragment = new STWebViewControllerFragment();
                fragment.setDefaultTitle("利用規約");
                fragment.setDefaultURL("http://styler.link/use_policy?app_webview");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STWebViewControllerFragment fragment = new STWebViewControllerFragment();
                fragment.setDefaultTitle("プライバシーポリシー");
                fragment.setDefaultURL("http://styler.link/privacy_policy?app_webview");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STWebViewControllerFragment fragment = new STWebViewControllerFragment();
                fragment.setDefaultTitle("ヘルプ");
                fragment.setDefaultURL("http://styler.link/help?app_webview");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STWebViewControllerFragment fragment = new STWebViewControllerFragment();
                fragment.setDefaultTitle("会社情報");
                fragment.setDefaultURL("http://styler.link/company?app_webview");
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        license.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STLicenseViewControllerFragment fragment = new STLicenseViewControllerFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.tabcontent, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Log.i("STAccountSettingVCF01", "logout OK");
                        LoginManager.getsIntstance().logout();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("ログアウト")
                .setMessage("ログアウトしてもよろしいですか？")
                .setNegativeButton("キャンセル", dialogClickListener)
                .setPositiveButton("OK", dialogClickListener).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("STAccountSettingVCF", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("STAccountSettingVCF", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("STAccountSettingVCF", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("STAccountSettingVCF", "onDestroy");
    }
}
