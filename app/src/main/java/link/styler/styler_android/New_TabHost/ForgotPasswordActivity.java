package link.styler.styler_android.New_TabHost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import link.styler.styler_android.R;

public class ForgotPasswordActivity extends AppCompatActivity {
    TextView txtCancel;
    TextView txtEmail;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        addControls();
        addEvents();
    }

    private void addControls() {
        txtCancel = (TextView) findViewById(R.id.txtCancel);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        btnSend = (Button) findViewById(R.id.btnSend);
    }

    private void addEvents() {
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ForgotPasswordActivity", "click send");
            }
        });
    }
}
