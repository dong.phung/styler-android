package link.styler.STStructs;

import android.graphics.Bitmap;

import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dongphung on 1/12/17.
 */

public class STStructs {
    public class StylerReplyAndItemStruct {
        public int postID;
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public int categoryID;
        public int itemID;
    }

    public class StylerReplyStruct {
        public int postID;
        public String comment;
        public int itemID;
    }

    public class StylerUpdateItemAndReplyStruct {
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public int categoryID;
        public int replyID;
    }

    public class StylerUpdateItemStruct {
        public int itemID;
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public Integer categoryID;
    }

    public class Favorite {
        public String objectId;
        public String imageUrl;
    }

    public class StylerCreateEventParam {
        public File photoData;
        public String title;
        public String text;
        public String start_date;
        public String finish_date;
        public String location;

        public RequestParams appendContentDataEvent()
        {
            RequestParams params = new RequestParams();
            params.put("title",title);
            params.put("text", text);
            params.put("start_date", start_date);
            params.put("finish_date", finish_date);
            params.put("lcation", location);

            try
            {
                if(photoData.exists())
                {
                    params.put("photo",photoData,"image/jpg","image.jpeg");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return  params;
        }

        /*
        public Map<String, Object> createBodyDataExceptPhotoDataFromSelf() {
            Map<String, Object> result = new HashMap<>();
            result.put("title", title);
            result.put("text", text);
            result.put("start_date", start_date);
            result.put("finish_date", finish_date);
            result.put("lcation", location);

            return result;
        }
        public Bitmap createPhotoDataFromSelf() {
            return photoData;
        }
        */
    }

    public class StylerPatchEventParam {
        public int eventID;
        public File photoData;
        public String title;
        public String text;
        public String start_date;
        public String finish_date;
        public String location;

        public RequestParams appendContentPatchEvent()
        {
            RequestParams params = new RequestParams();
            params.put("title", title);
            params.put("text", text);
            params.put("start_date", start_date);
            params.put("finish_date", finish_date);
            params.put("location", location);

            try
            {
                if(photoData.exists())
                {
                    params.put("photo",photoData,"image/jpg","image.jpeg");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return  params;
        }

        /*

        public Map<String, Object> createBodyDataExceptPhotoDataFromSelf() {
            Map<String, Object> result = new HashMap<>();
            result.put("title", title);
            result.put("text", text);
            result.put("start_date", start_date);
            result.put("finish_date", finish_date);
            result.put("lcation", location);

            return result;
        }

        public Bitmap createPhotoDataFromSelf() {
            return photoData;
        }

        */
    }

    public class StylerShopStruct{
        public String name ;
        public String profile;
        public String address;
        public String tel;
        public String station;
        public Integer location;
        public String website;
        public String business_hours;
    }
}
