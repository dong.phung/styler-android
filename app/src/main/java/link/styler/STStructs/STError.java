package link.styler.STStructs;

/**
 * Created by dongphung on 1/9/17.
 */

public class STError {
    public String domain = "styler.link";
    public int code = 401;
    public String message = "";

    public STError() {
        message = "";
    }

    public STError(String _msg) {
        message = _msg;
    }
    public STError(int _code, String _msg) {
        code = _code;
        message = _msg;
    }

    public void addErrorInfo(String errInfo, boolean isAtHead) {
        message = isAtHead ? errInfo+ " " + message : message + " " + errInfo;
    }

    @Override
    public String toString() {
        return "STError{" +
                "domain='" + domain + '\'' +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
