package link.styler.STStructs;

/**
 * Created by dongphung on 1/12/17.
 */

public class STEnums {

    public enum FavoriteSortType {
        date,
        date_rev,
        shop,
        shop_rev;
    }

    public enum ReportType {
        Post,
        Reply,
        Comment,
        Item;
    }

    public enum HTTPRequestType {
        GET,
        POST,
        PUT,
        PATCH,
        DELETE;
    }

    // create by Tan
    public enum PostSortType {
        None("none"),
        Popular("popular"),
        Latest("latest"),
        Recommend("recommend"),
        Watch("watch"),
        User("user"),
        Unreplied("unreplied");
        private String postSortType;

        private PostSortType(String postSortType) {
            this.postSortType = postSortType;
        }

        public String getPostSortType() {
            return postSortType;
        }
    }

    public enum CategoryType {
        None("none"),
        Post("post"),
        Article("article"),
        ArticleChild("article_child");
        private String categoryType;

        private CategoryType(String categoryType) {
            this.categoryType = categoryType;
        }

        public String getCategoryType() {
            return categoryType;
        }
    }

    public enum UserSortType {
        None("none"),
        Following("following"),
        Follower("follower"),
        Staff("staff"),
        Interest("interest");
        private String userSortType;

        private UserSortType(String userSortType) {
            this.userSortType = userSortType;
        }

        public String getUserSortType() {
            return userSortType;
        }
    }

    public enum ItemSortType {
        None("none"),
        Post("post"),
        Popular("popular"),
        Like("like"),
        Shop("shop"),
        Reply("reply"),
        Message("messaage");
        private String itemSortType;

        private ItemSortType(String itemSortType) {
            this.itemSortType = itemSortType;
        }

        public String getItemSortType() {
            return itemSortType;
        }
    }

    public enum EventSortType {
        None("none"),
        Feture("feture"),
        Ongoing("ongoing"),
        Shop("shop"),
        Interest("interest");
        private String eventSortType;

        private EventSortType(String eventSortType) {
            this.eventSortType = eventSortType;
        }

        public String getEventSortType() {
            return eventSortType;
        }
    }

    public enum ArticleSortType {
        None("none"),
        Timeline("timeline"),
        Categorized("categorized"),
        Feature("feature"),
        Tagged("tagged");
        private String articleSortType;

        private ArticleSortType(String articleSortType) {
            this.articleSortType = articleSortType;
        }

        public String getArticleSortType() {
            return articleSortType;
        }
    }

    public enum ShopSortType {
        None("none"),
        Active("active"),
        New("new"),
        List("list");
        private String shopSortType;

        private ShopSortType(String shopSortType) {
            this.shopSortType = shopSortType;
        }

        public String getShopSortType() {
            return shopSortType;
        }
    }

    public enum UserStatus {
        None("none"),
        Customer("customer"),
        Staff("staff");
        private String userStatus;

        private UserStatus(String userStatus) {
            this.userStatus = userStatus;
        }

        public String getUserStatus() {
            return userStatus;
        }

        static UserStatus checkStatus(String status) {
            switch (status) {
                case "customer":
                    return UserStatus.Customer;
                case "staff":
                    return UserStatus.Staff;
                default:
                    return UserStatus.None;
            }
        }
    }

    public enum MyPageType {
        MyCustomerPage,
        MyStaffPage,
        MyShopPage,
        OtherCustomerPage,
        OtherStaffPage,
        OtherShopPage,
        NotLogin,
        None,
    }

    public enum MessageType {
        None("none"),
        User("user"),
        Link("link"),
        Reply("reply"),
        Item("item"),
        Notification("notification");

        private String messageType;

        private MessageType(String messageType) {
            this.messageType = messageType;
        }

        public String getMessageType() {
            return messageType;
        }
    }

    public enum STItemListCollectionViewType {
        defaultType,
        noAvatar,
        selectLike;
    }

    public enum STItemDetailViewType {
        Item("item"),
        Reply("reply");

        private String stItemDetailViewType;

        private STItemDetailViewType(String stItemDetailViewType) {
            this.stItemDetailViewType = stItemDetailViewType;
        }

        public String getSTItemDetailViewType() {
            return stItemDetailViewType;
        }
    }

    public enum STCreateReplyViewType {
        create("create"),
        itemUpdate("itemUpdate"),
        itemAndReplyUpdate("itemAndReplyUpdate");

        private String stCreateReplyViewType;

        private STCreateReplyViewType(String stItemDetailViewType) {
            this.stCreateReplyViewType = stItemDetailViewType;
        }

        public String getSTCreateReplyViewType() {
            return stCreateReplyViewType;
        }
    }

    public enum MyAdapterViewMode {
        normal,
        createReply;
    }

    public enum STArticleListType {
        newList,
        category,
        tag;
    }

    public enum resumeType {
        createPost,
        createReply,
        none,
        createEvent, patchEvent, editUser, editShop, createAccount, firstSetting, editItem;
    }
    // end create by Tan
}
