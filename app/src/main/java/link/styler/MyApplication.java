package link.styler;

import android.app.Application;
import android.content.res.Configuration;

import com.deploygate.sdk.DeployGate;

/**
 * Created by admin on 4/6/17.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        Foreground.init(this);

        // deploygate SDK to collect crash report
        DeployGate.install(this);
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onTerminate(){
        super.onTerminate();
    }

}
