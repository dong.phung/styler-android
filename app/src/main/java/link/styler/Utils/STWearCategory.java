package link.styler.Utils;

/**
 * Created by macOS on 3/13/17.
 */

public class STWearCategory {
    public static final String japName[] = {"ジャケット・アウター",
            "スーツ・セットアップ",
            "シャツ・ブラウス",
            "Tシャツ・カットソー",
            "ニット・スウェット",
            "パンツ",
            "ドレス・ワンピース・スカート",
            "シューズ",
            "バッグ",
            "帽子",
            "アクセサリー・時計",
            "メガネ・サングラス",
            "財布・小物・雑貨",
            "その他"};

    public static final String engName[] = {   ".Outer", //2
            ".Suit", // 3
            ".Shirts",//4
            ".Cutsew",//5
            ".Knit",//6
            ".Pants",//7
            ".Dress",//8
            ".Shoes",//9
            ".Bag",//10
            ".Hat",//11
            ".Accessory",//12
            ".Glasses",//13
            ".Wallet",//14
            ".NoCategory"};      //1
    public static final int ID[] = {2,  // 2
            3,  // 3
            4,  // 4
            5,  // 5
            6,  // 6
            7,  // 7
            8,  // 8
            9,  // 9
            10, //10
            11, //11
            12, //12
            13, //13
            14, //14
            1}; // 1
}
