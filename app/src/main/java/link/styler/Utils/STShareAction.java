package link.styler.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import link.styler.styler_android.New_TabHost.Constants;

/**
 * Created by macOS on 5/15/17.
 */

public class STShareAction {
    private Activity activity;
    private Context context;
    private String path;

    public void ShareAction (Activity activity, String path){
        this.activity = activity;
        this.path = path;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String appLink = Constants.serverUrl(path);
        Log.i("ShareView: ", appLink);
        intent.putExtra(Intent.EXTRA_TEXT, appLink);

        //   boolean facebookAppFound = false;
        //   List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        //    for (ResolveInfo info : matches) {
        //       if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
        //            intent.setPackage(info.activityInfo.packageName);
        //              facebookAppFound = true;
        //             break;
        //         }
        //     }
        //     if (!facebookAppFound) {
        //       String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + appLink;
        //        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        //     }

        activity.startActivity(Intent.createChooser(intent,"Share"));
    }

}
