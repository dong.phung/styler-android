package link.styler.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by admin on 2/28/17.
 */

public class TransformationUtils {

    public class CircleTransform implements com.squareup.picasso.Transformation{

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public class RoundedCornersTransform implements com.squareup.picasso.Transformation{

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(),source.getHeight());
            int x    = (source.getWidth() - size)/2;
            int y    = (source.getHeight() - size)/2;

            Bitmap squareBitmap = Bitmap.createBitmap(source,x,y,size,size);
            if(squareBitmap != source){
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size,size,source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint  = new Paint();
            BitmapShader shader = new BitmapShader(squareBitmap,BitmapShader.TileMode.CLAMP,BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size/8f;
            canvas.drawRoundRect(new RectF(0,0,source.getWidth(),source.getHeight()),r,r,paint);
            squareBitmap.recycle();

            return bitmap;
        }

        @Override
        public String key() {
            return "rounder_corners";
        }
    }

}
