package link.styler.Utils;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by admin on 3/6/17.
 */

public class GeocodeAddressString extends AsyncTask<String, Void, String[]> {
    public interface AsyncResponse {
        void processFinish(double lng, double lnt);
    }

    public AsyncResponse delegate = null;

    public GeocodeAddressString(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected String[] doInBackground(String... params) {
        String response;
        String location = params[0];
        Logger.print("location " + location);
        try {
            response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address=" + location + "&sensor=false");
            Logger.print("response " + response);
            return new String[]{response};
        } catch (Exception e) {
            e.printStackTrace();
            return new String[]{"error"};
        }
    }

    @Override
    protected void onPostExecute(String... result) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        double lng;
        double lat;
        try {
            lng = jsonObject.optJSONArray("results").optJSONObject(0).optJSONObject("geometry").optJSONObject("location").optDouble("lng");
            lat = jsonObject.optJSONArray("results").optJSONObject(0).optJSONObject("geometry").optJSONObject("location").optDouble("lat");
        } catch (Exception e) {
            lng = 0;
            lat = 0;
        }
        Logger.print("lng " + lng);
        Logger.print("lat " + lat);
        delegate.processFinish(lng, lat);
    }

    private String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(15000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoOutput(true);

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
