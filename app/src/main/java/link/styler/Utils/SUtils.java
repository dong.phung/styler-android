package link.styler.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import link.styler.styler_android.R;

/**
 * Created by admin on 3/2/17.
 */

public class SUtils {

    public static void LoadImageIntoViewImageRoundedCornersTransform(Context context, String url, ImageView imageView){
        Logger.print("LoadImageIntoViewImage url "+url+" imageView "+imageView);
        Picasso.with(context).load(url).error(R.drawable.icon_user).transform(new TransformationUtils().new RoundedCornersTransform()).into(imageView);
    }

    public static void LoadImageIntoViewImageAndWHRoundedCornersTransform(Context context, String url, ImageView imageView, int width, int height){
        Picasso.with(context).load(url).error(R.drawable.icon_user).transform(new TransformationUtils().new RoundedCornersTransform()).resize(width,height).placeholder(R.drawable.default_avatar).into(imageView);
    }

    public static void LoadImageIntoViewImageCircleTransform(Context context, String url, ImageView imageView){
        Logger.print("LoadImageIntoViewImage url "+url+" imageView "+imageView);
        Picasso.with(context).load(url).error(R.drawable.icon_user).transform(new TransformationUtils().new CircleTransform()).into(imageView);
    }

    public static void LoadImageIntoViewImagAndWHCircleTransform(Context context, String url, ImageView imageView, int width, int height){
        Logger.print("LoadImageIntoViewImage url "+url+" imageView "+imageView);
        Picasso.with(context).load(url).error(R.drawable.icon_user).transform(new TransformationUtils().new CircleTransform()).resize(width,height).into(imageView);
    }



}
