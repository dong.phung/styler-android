package link.styler.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.ref.WeakReference;

import link.styler.StylerApi.StylerApi;

/**
 * Created by admin on 2/13/17.
 */

public class DataSharing {

    private final String kAccessTokenKey = "kAccessTokenKey";
    private final String kUserIDKey = "kUserIDKey";
    private final String kUserEmailKey = "kUserEmailKey";
    private final String kUserPasswordKey = "kUserPasswordKey";
    private final String kUserShopIdKey = "kUserShopIdKey";

    private final String kTokenGCM = "kTokenGCM";

    private final String kIsNotFirstLaunch  = "isNotFirstLaunch";



    private static DataSharing sIntstance;
    public static DataSharing getsIntstance()
    {
        if(sIntstance == null){
            sIntstance = new DataSharing(StylerApi.getCurrentActivity().getApplicationContext());
        }
        return sIntstance;
    }

    private SharedPreferences mSharedPreferences;

    public DataSharing(Context context){
        Logger.print("before "+mSharedPreferences);
        mSharedPreferences = context.getSharedPreferences("styler.atlassian.net",Context.MODE_PRIVATE);
        Logger.print("after "+mSharedPreferences);
    }

    public String getTokenGCM(){
        return mSharedPreferences.getString(kTokenGCM,null);
    }

    public boolean setTokenGCM(String tokenGCM){
        return mSharedPreferences.edit().putString(kTokenGCM,tokenGCM).commit();
    }


    public boolean isNotFirstLaunch(){
        return  mSharedPreferences.getBoolean(kIsNotFirstLaunch,true);
    }

    public boolean setIsNotFirstLauch(){
        return mSharedPreferences.edit().putBoolean(kIsNotFirstLaunch,false).commit();
    }

    public String getAccessToken() {

        return mSharedPreferences.getString(kAccessTokenKey,null);
    }

    public boolean setAccessToken(String accessToken){
        return mSharedPreferences.edit().putString(kAccessTokenKey,accessToken).commit();
    }

    public int getUserID(){
        return mSharedPreferences.getInt(kUserIDKey,-1); //default -1 : No user return
    }

    public boolean setUserID(Integer userID){
        if(userID !=  null)
            return mSharedPreferences.edit().putInt(kUserIDKey,userID).commit();
        else
            return false;
    }

    public String getUserEmail(){
        return mSharedPreferences.getString(kUserEmailKey,null);
    }

    public boolean setUserEmail(String user_name){
        if(user_name != null)
            return mSharedPreferences.edit().putString(kUserEmailKey,user_name).commit();
        else
            return false;
    }

    public String getUserPassword(){
        return mSharedPreferences.getString(kUserPasswordKey,null);
    }

    public boolean setUserPassword(String password){
        if(password != null)
            return mSharedPreferences.edit().putString(kUserPasswordKey,password).commit();
        else
            return false;
    }

    public void deleteSharePreference(){
        mSharedPreferences.edit().clear().commit();
    }
}
