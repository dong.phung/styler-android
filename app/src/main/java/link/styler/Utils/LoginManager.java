package link.styler.Utils;

import android.util.Log;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.DataManager.Preserver.DataPreserver;
import link.styler.DataManager.Sender.DataSender;
import link.styler.STStructs.STError;
import link.styler.StylerApi.StylerApi;
import link.styler.models.STUser;

/**
 * Created by admin on 2/10/17.
 */

public class LoginManager {

    public static Boolean isLogin;
    public static String status;
    public static Integer shopID;
    //public static Context context;

    private final String kAccessTokenKey = "kAccessTokenKey";
    private final String kUserIDKey = "kUserIDKey";
    private final String kUserEmailKey = "kUserEmailKey";
    private final String kUserPasswordKey = "kUserPasswordKey";
    private final String kUserShopIdKey = "kUserShopIdKey";

    public boolean hasUnOpenNotification = false;


    private static LoginManager sIntstance;

    public static LoginManager getsIntstance()
    {
        if(sIntstance == null){
            sIntstance = new LoginManager();
        }
        return sIntstance;
    }

    public LoginManager()
    {
        isLogin = false;
        this.me();
        StylerApi.getAccessTokenBlock = DataSharing.getsIntstance().getAccessToken();
        StylerApi.getUserEmailBlock = DataSharing.getsIntstance().getUserEmail();
        StylerApi.getUserPasswordBlock = DataSharing.getsIntstance().getUserPassword();
    }

    public void signUp(final String email, String password, final STCompletion.Base completion){
        DataSender.signUp(email, password, new STCompletion.WithNullableIntString() {
            @Override
            public void onRequestComplete(STError err, Integer nullableInt, String token) {
                if (err != null){
                    isLogin = false;
                    if (me() != null){
                        STUser user = me();
                        status = user.status;
                        shopID = user.shopID;
                    }
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (!token.isEmpty() && nullableInt != null){
                    isLogin = true;
                    setUserID(nullableInt);
                    setAccessToken(token);
                    status = me().status;
                    //// TODO: 3/6/17 in swift self?.shopID = self?.shopID
                    shopID = me().shopID;
                    setUserEmail(email);
                    completion.onRequestComplete(true, null);
                }else {
                    deleteKeyChains();
                    completion.onRequestComplete(false, null);
                }
            }
        });
    }

    public void login(final String email, String password, final STCompletion.Base completion){
        DataSender.login(email, password, new STCompletion.WithNullableIntString() {
            @Override
            public void onRequestComplete(STError err, Integer nullableInt, String token) {
                if (err != null){
                    isLogin = false;
                    if (me() != null){
                        STUser user = me();
                        status = user.status;
                        shopID = user.shopID;
                    }
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (!token.isEmpty() && nullableInt != null){
                    isLogin = true;
                    setUserID(nullableInt);
                    setAccessToken(token);
                    status = me().status;
                    //// TODO: 3/6/17 in swift self?.shopID = self?.shopID
                    shopID = me().shopID;
                    Log.i("Tan018","shopID: " + me().shopID );
                    setUserEmail(email);
                    completion.onRequestComplete(true, null);
                }else {
                    deleteKeyChains();
                    completion.onRequestComplete(false, null);
                }
            }
        });
    }

    public void facebookLogin(String accessToken, final STCompletion.Base completion){
        DataSender.facebookLogin(accessToken, new STCompletion.WithNullableIntString() {
            @Override
            public void onRequestComplete(STError err, Integer nullableInt, String token) {
                if (err != null){
                    isLogin = false;
                    if (me() != null){
                        STUser user = me();
                        status = user.status;
                        shopID = user.shopID;
                    }
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (!token.isEmpty() && nullableInt != null){
                    isLogin = true;
                    setUserID(nullableInt);
                    setAccessToken(token);
                    status = me().status;
                    shopID = me().shopID;
                    completion.onRequestComplete(true, null);
                }else {
                    deleteKeyChains();
                    completion.onRequestComplete(false, null);
                }
            }
        });
    }

    public void refreshToken(String versionNum, final STCompletion.Base completion){
        DataSender.refreshToken(new STCompletion.WithNullableIntString() {
            @Override
            public void onRequestComplete(STError err, Integer nullableInt, String token) {
                if (err != null){
                    isLogin = false;
                    if (me() != null){
                        STUser user = me();
                        status = user.status;
                        shopID = user.shopID;
                    }
                    completion.onRequestComplete(false, err);
                    return;
                }
                if (!token.isEmpty() && nullableInt != null){
                    isLogin = true;
                    setUserID(nullableInt);
                    setAccessToken(token);
                    status = me().status;
                    shopID = me().shopID;
                    completion.onRequestComplete(true, null);
                }else {
                    deleteKeyChains();
                    completion.onRequestComplete(false, null);
                }
            }
        });
    }

    public void logout(){
        DataPreserver.deleteAll();
        resetStatus();
    }

    private void resetStatus(){
        isLogin = false;
        status  = null;
        shopID  = null;

        setAccessToken(null);
        setUserID(null);
        setUserEmail(null);
        setUserPassword(null);
    }

    public STUser me()
    {
        Integer userID = getUserID();
        if(userID != null){
            STUser me = DataLoader.getUser(userID);
            if(userID != null && me != null){
                this.isLogin = true;
                this.status  = me.status;
                this.shopID  = me.shopID;
                return me;
            }
        }
        return null;
    }

    public void meInAsync(){
        //// TODO: 3/6/17 IOS not using 
    }

    //KeyChain

    public String getAccessToken() {
        return DataSharing.getsIntstance().getAccessToken();
    }

    public void setAccessToken(String token){
        DataSharing.getsIntstance().setAccessToken(token);
        StylerApi.getAccessTokenBlock=token;
    }

    public Integer getUserID(){
        if (DataSharing.getsIntstance().getUserID() != -1)
            return DataSharing.getsIntstance().getUserID();
        else
            return null;
    }

    public void setUserID(Integer userID){
        if(userID != null){
            DataSharing.getsIntstance().setUserID(userID);
        }
        else {
            DataSharing.getsIntstance().setUserID(-1);
        }
    }

    public String getUserEmail(){
        return DataSharing.getsIntstance().getUserEmail();
    }

    public void setUserEmail(String userEmail){
        DataSharing.getsIntstance().setUserEmail(userEmail);
    }

    public String getUserPassword(){
        return DataSharing.getsIntstance().getUserPassword();
    }

    public void setUserPassword(String password){
        DataSharing.getsIntstance().setUserPassword(password);
    }

    private void deleteKeyChains(){
        DataSharing.getsIntstance().deleteSharePreference();
    }

}
