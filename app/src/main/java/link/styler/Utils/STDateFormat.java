package link.styler.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by macOS on 5/19/17.
 */

public class STDateFormat {
    private String stringDateFormat = "";

    public String DateFormat(String stringDate) {
        String s = stringDate != null ? stringDate : "";
        java.text.DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(s);
            format = new SimpleDateFormat("yyyy年MM月dd日");
            this.stringDateFormat = format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return stringDateFormat;
    }
}
