package link.styler.CompletionInterfaces;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import link.styler.STStructs.STError;
import link.styler.models.STEvent;
import link.styler.models.STImage;

/**
 * Created by dongphung on 1/6/17.
 */

/* Interfaces for call back methods */
public interface STCompletion {
    public interface Base{
        public void onRequestComplete ( boolean isSuccess, STError err);
    }

    public interface Void{
        public void onRequestComplete ( );
    }

    public interface WithJson {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err);
    }

    public interface WithJsonInt {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int intValue);
    }

    public interface WithJsonNullableInt {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt);
    }

    // create by tan
    public interface WithJsonArrayNullableInt {
        public void onRequestComplete(JSONArray json, boolean isSuccess, STError err, Integer nullableInt);
    }
    // end create by tan

    public interface WithJsonToken {
        public  void onRequestComplete(JSONObject json, boolean isSuccess, STError err, String token);
    }

    public interface WithNullableInt {
        public void onRequestComplete(boolean isSuccess, Integer nullableInt);
    }

    //Quang Dinh add
    //Use func : signUp
    public interface WithNullableIntString{
        public void onRequestComplete(STError err,Integer nullableInt,String token);
    }

    public interface WithSTEvent{
        public void onRequestComplete(STEvent event);
    }

    // create by Tan
    public interface WithSTImage {
        public void onRequestComplete(STImage image);
    }

    public interface WithByteArray {
        public void onRequestComplete(byte[] data);
    }

    public interface WithLastObjectID {
        public void onRequestComplete(boolean isSuccess, STError err, Integer lastObjectID);
    }

    public interface WithPostIDAndItemID {
        public void onRequestComplete(boolean isSuccess, STError err, Integer postID, Integer itemID);
    }
    // end create by Tan
}

/*
public abstract class STCompletionHandler {
    //public abstract void onRequestComplete(CompletionParams params);

    public abstract void onRequestComplete(JSONObject json, STError err, int statusCode);
    public abstract void onRequestComplete(JSONObject json, STError err);
}
*/
/* call back interface end */
