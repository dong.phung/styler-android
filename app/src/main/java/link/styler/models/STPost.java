package link.styler.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.STStructs.STEnums;
import link.styler.Utils.Logger;


/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STPost extends RealmObject {
    @PrimaryKey
    public int      postID          = 0;
    public STUser   user            = new STUser();
    public int      commentsCount   = 0;
    public int      watchesCount    = 0;
    public int      repliesCount    = 0;
    public String   text            = "";
    public int      watchID         = 0;
    public int      replyID         = 0;
    public RealmList<STItemSort>   itemSorts     = new RealmList<>();
    public RealmList<STImageURL>   itemImageURLs = new RealmList<>();
    public int      categoryID      = 0;
    public String   categoryName    = "";
    public STCategory category      = new STCategory();
    public String   createdAt       = "";
    public int   newestTime      = 0;
    public STComment comment    = new STComment();

    public void fillInfo(Realm realm, final JSONObject info)  {
        Logger.print("Edit post json "+info);
        this.postID         = info.optInt("id");
        Logger.print("postID "+this.postID);
        this.commentsCount  = info.optInt("comments_count");
        Logger.print("commentsCount "+this.commentsCount);
        this.watchesCount   = info.optInt("watches_count");
        Logger.print("watchesCount "+this.watchesCount);
        this.repliesCount   = info.optInt("replies_count");
        Logger.print("repliesCount "+this.repliesCount);
        this.text           = info.optString("text",null);
        Logger.print("text "+this.text);
        this.watchID        = info.optInt("watch_id");
        Logger.print("watchID "+this.watchID);
        this.replyID        = info.optInt("reply_id");
        Logger.print("replyID "+this.replyID);
        this.createdAt      = info.optString("created_at",null);
        Logger.print("createdAt "+this.createdAt);
        this.newestTime     = info.optInt("newest_time");
        Logger.print("newestTime "+this.newestTime);
        this.categoryID     = info.optInt("category_id");
        Logger.print("categoryID "+this.categoryID);
        this.categoryName   = info.optString("category_name");

        if(info.has("category_id")){
            Logger.print("STCategory begin");
            final STCategory stCategory = new STCategory();
            stCategory.categoryID = this.categoryID;
            stCategory.type       = STEnums.CategoryType.Post.toString();
            stCategory.name       = this.categoryName;
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stCategory);
                }
            });

            this.category         = stCategory;
            Logger.print("STCategory end");
        }

        if(info.has("item_images_url")){
            JSONArray imagesURL = info.optJSONArray("item_images_url");
            saveImages(realm,this.postID,imagesURL);
        }

        STImageURL imageURL = realm.where(STImageURL.class).equalTo("postID",info.optInt("id")).findFirst();
        if(imageURL != null){
            Logger.print("imageURL begin");
            this.itemImageURLs.add(imageURL);
            Logger.print("imageURL end");
        }

        if(info.has("comment")){
            Logger.print("Comment begin");
            final STComment stComment = new STComment();
            stComment.fillInfo(realm,info.optJSONObject("comment"));
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stComment);
                }
            });

            this.comment = stComment;
            Logger.print("Comment end");
        }

        if(info.has("user")){
            Logger.print("User begin");
            final STUser stUser = new STUser();
            stUser.fillInfo(realm,info.optJSONObject("user"));
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stUser);
                }
            });

            this.user = stUser;
            Logger.print("User end");
        }

        Logger.print("STpost fillinfo end");
    }
    private void saveImages(Realm realm, final int postID, final JSONArray imagesURL){
        Logger.print("Save Image begin");
        for( int i = 0;i<imagesURL.length();i++){
            final STImageURL stImageURL = new STImageURL();
            stImageURL.fillInfoByPost(imagesURL.optString(i),postID,i+1);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stImageURL);
                }
            });
        }
        Logger.print("Save Image end");
    }

    public void fillAllInfo(Realm realm, JSONObject info, STUser user, RealmList<STItemSort>itemSorts, STCategory category ) throws JSONException {
        fillInfo(realm,info);
        this.user = user;
        this.itemSorts = itemSorts;
        this.category = category;
    }
}
