package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import link.styler.DataManager.Loader.DataLoader;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
public class STItem extends RealmObject {
    @PrimaryKey
    public int          itemID      = 0;
    public int          shopID      = 0;
    public STShop       shop        = new STShop();
    public String       text        = "";
    public String      mainImageURL   = "";
    public RealmList<STImageURL>   imageURLs   = new RealmList<>();
    public int          likeCount   = 0;
    public String       brand       = "";
    public String       name        = "";
    public String       price       = "";
    public int          likeID      = 0;
    public int          categoryID  = 0;
    public STCategory   category  = new STCategory();
     public RealmList<STReply>      replies    = new RealmList<>();
    public String       createdAt   = "";

    public void fillInfo(Realm realm, JSONObject info) {
        Logger.print("STItem json "+info);
            this.itemID         = info.optInt("id");
            this.shopID         = info.optInt("shop_id");
            this.text           = info.optString("text");

            if (info.has("images_url"))
            {
                if(info.optJSONObject("images_url").has("image_url_1")){
                    this.mainImageURL = info.optJSONObject("images_url").optString("image_url_1");
                }
                saveImages(realm,info.optInt("id"),info.optJSONObject("images_url"));

            }

            RealmResults<STImageURL> imageURL = realm.where(STImageURL.class).equalTo("itemID",info.optInt("info")).findAll();
            if(imageURL != null)
                this.imageURLs.addAll(imageURL);

            this.likeCount      = info.optInt("likes_count");
            this.brand          = info.optString("brand");
            this.name           = info.optString("name");
            if (info.has("price"))
                this.price = info.optInt("price") +"";
            this.likeID         = info.optInt("like_id");
            this.categoryID     = info.optInt("category_id");
            this.createdAt      = info.optString("created_at");
            this.category       = DataLoader.getCategory(this.categoryID);
        Logger.print("STItem complete");
    }

    private void saveImages(Realm realm, int itemID,JSONObject imagesURL){
        Iterator<String> keys1 = imagesURL.keys();
        ArrayList<String> keys = new ArrayList<String>();
        while (keys1.hasNext()){
            keys.add(keys1.next());
        }
        for (String key : keys){
            if(imagesURL.has(key)){
                final String imageURL = imagesURL.optString(key);
                if (imageURL.length()>0){
                    int order = 1;
                    switch (key){
                        case "image_url_1":
                            order = 1;
                            break;
                        case "image_url_2":
                            order = 2;
                            break;
                        case "image_url_3":
                            order = 3;
                            break;
                        case "image_url_4":
                            order = 4;
                            break;
                        default:
                            order = 1;
                            break;
                    }
                    final STImageURL stImageURL = new STImageURL();
                    stImageURL.fillInfoByItem(imageURL, itemID, order);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(stImageURL);
                        }
                    });
                }
            }
        }
    }
    //List<STReply> replies // code switf:  replies: Results<STReply>  // <============
    public void fillAllInfo(Realm realm, JSONObject info, STShop shop, RealmResults<STReply> replies, STCategory category) {
        fillInfo(realm, info);
        this.shop       = shop;
        this.replies.addAll(replies);
        this.category   = category;
    }

    public STItem() {
    }
}