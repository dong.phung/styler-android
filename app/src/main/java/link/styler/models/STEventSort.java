package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

enum EventSortType {
    None("none"),
    Feture("feture"),
    Ongoing("ongoing"),
    Shop("shop"),
    Interest("interest");

    private String eventSortType;
    private EventSortType(String eventSortType){
        this.eventSortType = eventSortType;
    }

    public String getEventSortType() {
        return eventSortType;
    }
}

public class STEventSort extends RealmObject {
    @PrimaryKey
    public String   eventSortKey    = "";
    public String   type            = EventSortType.None.getEventSortType();
    public int      eventID         = 0;
    public STEvent  event           = new STEvent();
    public int      shopID          = 0;
    public int      userID          = 0;
    public String   startDate       = "";
}
