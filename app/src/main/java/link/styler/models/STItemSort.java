package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

enum ItemSortType{
    None("none"),
    Post("post"),
    Popular("popular"),
    Like("like"),
    Shop("shop"),
    Reply("reply"),
    Message("messaage");
    private String itemSortType;
    private ItemSortType(String itemSortType){
        this.itemSortType = itemSortType;
    }

    public String getItemSortType() {
        return itemSortType;
    }
}

public class STItemSort extends RealmObject {
    @PrimaryKey
    public String   itemSortKey = "";
    public String   type        = ItemSortType.None.getItemSortType();
    public int      itemID      = 0;
    public int      postID      = 0;
    public int      replyID     = 0;
    public int      shopID      = 0;
    public int      userID      = 0;
    public int      categoryID  = 0;
    public STItem   item        = new STItem();
    public String   itemCreatedAt = "";
}
