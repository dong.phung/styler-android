package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.STStructs.STEnums;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
enum UserStatus{
    None("none"),Customer("customer"),Staff("staff");
    private String userStatus;
    private UserStatus (String userStatus){
        this.userStatus = userStatus;
    }

    public String getUserStatus() {
        return userStatus;
    }
    static UserStatus checkStatus(String status){
        /*if (status == null)
            return null;*/
        switch (status){
            case "customer":
                return UserStatus.Customer;
            case "staff":
                return UserStatus.Staff;
            default:
                return UserStatus.None;
        }
    }
}

public class STUser extends RealmObject {
    @PrimaryKey
    public int      userID          = 0;
    public String   name            = "";
    public String   apiDetail       = "";
    public String   imageURL        = "";
    public String   profile         = "";
    public int      likeCount       = 0;
    public int      interestCount   = 0;
    public int      postCount       = 0;
    public int      watchCount      = 0;
    public int      followCount     = 0;
    public int      replyCount      = 0;
    public int      followerCount   = 0;
    public int      followID        = 0;
    public int      shopID          = 0;
    public STShop   shop            = new STShop();
    public STUserData userData      = new STUserData();
    public String   location        = "";
    public int      locationID      = 0;
    public String   birthday        = "";
    public String   gender          = "";
    public String   status          = STEnums.UserStatus.None.getUserStatus();
     public RealmList<STPost> watches= new RealmList<>();
     public RealmList<STPost> posts  = new RealmList<>();

    final static private String[] list = new String[]{"id","name","api_detail","profile","image_url","likes_count","interests_count","posts_count","watches_count",
            "follows_count","replies_count","followers_count","follow_id","location","todo : id から locationを取得","birthday","gender","shop_id","status"};
    
    private void appendMissinKeys(JSONObject json){

        //Iterator<String> temp = json.keys();

        for(int i = 0;i<list.length;i++)
        {
            if(!json.has(list[i].toString())){
                try {
                    json.put(list[i].toString(),JSONObject.NULL);
                }
                catch (JSONException e)
                {
                    Logger.print("checkIsNullJson " +e.getMessage());
                    e.printStackTrace();
                }
            }
        }


    }
    

    public void fillInfo(Realm realm, JSONObject info) {
        //appendMissinKeys(info);
        Logger.print("STUser fillInfo json "+info);

            if (this.userID == 0)
                this.userID = info.optInt("id",0);
            this.name           = info.optString("name","");
            this.apiDetail      = info.optString("user_api_detail","");
            this.profile        = info.optString("profile","");
            this.imageURL       = info.optString("image_url","");
            this.likeCount      = info.optInt("likes_count",0);
            this.interestCount  = info.optInt("interests_count",0);
            this.postCount      = info.optInt("posts_count",0);
            this.watchCount     = info.optInt("watches_count",0);
            this.followCount    = info.optInt("follows_count",0);
            this.replyCount     = info.optInt("replies_count",0);
            this.followerCount  = info.optInt("followers_count",0);
            this.followID       = info.optInt("follow_id",0);
            this.locationID     = info.optInt("location",0);
            this.location       = "todo : id から locationを取得";
            this.birthday       = info.optString("birthday","");
            this.gender         = info.optString("gender","");
            this.shopID         = info.optInt("shop_id",0);
            switch (info.optString("status","")){
                case "customer":
                    this.status = STEnums.UserStatus.Customer.getUserStatus();
                    break;
                case "staff":
                    this.status = STEnums.UserStatus.Staff.getUserStatus();
                    break;
                default:
                    this.status = STEnums.UserStatus.None.getUserStatus();
                    break;
            }
        Logger.print("STUser fill info complete");
    }

    public void fillAllInfo(Realm realm, JSONObject info, STShop shop, STUserData userData) throws JSONException {
        fillInfo(realm,info);
        this.shop       = shop;
        this.userData   = userData;
    }
}
