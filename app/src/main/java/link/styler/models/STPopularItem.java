package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by macOS on 2/6/17.
 */

public class STPopularItem extends RealmObject {
    @PrimaryKey
    public int      itemID          = 0;
    public double   updateAt        = 0;
    public int      latestLikeCount = 0;
    public STItem item            = new STItem();

    public void fillInfo(Realm realm, JSONObject data) {
        this.itemID     = data.optInt("id");
        this.updateAt   = System.currentTimeMillis()/1000;
        this.item = new STItem();
        item.fillInfo(realm,data);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }
        });

    }
}
