package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

enum ShopSortType{
    None("none"),
    Active("active"),
    New("new"),
    List("list");

    private String shopSortType;
    private ShopSortType(String shopSortType){
        this.shopSortType = shopSortType;
    }

    public String getShopSortType() {
        return shopSortType;
    }
}

public class STShopSort extends RealmObject{
    @PrimaryKey
    public int      objectID    = 0;
    public String   type        = ShopSortType.None.getShopSortType();
    public int      locationID  = 0;
    public int      shopID      = 0;
    public STShop   shop        = new STShop();
}
