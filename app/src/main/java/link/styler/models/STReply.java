package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */

public class STReply extends RealmObject {
    @PrimaryKey
    public int      replyID         = 0;
    public int      itemID          = 0;
    public int      postID          = 0;
    public int      conversationID  = 0;
    public String   comment         = "";
    public String   createdAt       = "";
    public boolean  feedbackFlag    = false;
    public STShop   shop            = new STShop();
    public STUser   user            = new STUser();

    public void fillInfo(JSONObject info, int itemID) {
        Logger.print("STReply fillInfo json "+info);
        this.replyID            = info.optInt("id");
        this.itemID             = itemID;
        this.postID             = info.optInt("post_id");
        this.comment            = info.optString("comment");
        this.createdAt          = info.optString("created_at");
        this.conversationID     = info.optInt("conversation_id");
        this.feedbackFlag       = info.optBoolean("feedback_flag");
        Logger.print("STReply fillInfo complete");
    }

    public void update(JSONObject info) {
        comment         = info.optString("comment");
        createdAt       = info.optString("created_at");
        conversationID  = info.optInt("conversation_id");
        feedbackFlag    = info.optBoolean("feedback_flag");
    }

    public void fillAllInfo(JSONObject info, int itemID, STShop shop, STUser user){
        fillInfo(info,itemID);
        this.shop = shop;
        this.user = user;
    }
}
