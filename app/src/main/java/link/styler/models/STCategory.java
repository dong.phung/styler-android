package link.styler.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */
enum CategoryType{
    None("none"),
    Post("post"),
    Article("article"),
    ArticleChild("article_child");
    private String categoryType;
    private CategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryType() {
        return categoryType;
    }
}

public class STCategory extends RealmObject{
    @PrimaryKey
    public int      categoryID  = 0;
    public String   name        = "";
    public String   type        = CategoryType.None.getCategoryType();

    public void fillInfo(JSONObject info) {
        this.categoryID = info.optInt("id");
        final String type = info.optString("type");
        if( info.optString("child_name") != "null"){
            final String childName = info.optString("child_name");
            this.name = childName;
            Log.i("Tan009"," " + this.name);
            switch (type){
                case "ArticleCategory":
                    this.type = CategoryType.ArticleChild.getCategoryType();
                    break;
                default:
                    this.type = CategoryType.None.getCategoryType();
            }
        }else {
            this.name = info.optString("name");
            Log.i("Tan010"," " + this.name);
            switch (type){
                case "PostCategory":
                    this.type = CategoryType.Post.getCategoryType();
                    break;
                case "ArticleCategory":
                    this.type = CategoryType.Article.getCategoryType();
                    break;
                default:
                    this.type = CategoryType.None.getCategoryType();
            }
        }
    }
}
