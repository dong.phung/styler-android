package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STLocation extends RealmObject {
    @PrimaryKey
    public int      locationID  = 0;
    public String   name        = "";
    public int      count       = 0;
    public boolean  newShopFlag = false;

    public void fillInfo(JSONObject info) {
        this.locationID = info.optInt("id");
        this.name       = info.optString("name");
        if (this.name == "未設定" ){
            this.name = "オンライン";
        }
        this.count      = info.optInt("count");
        this.newShopFlag= info.optBoolean("new_shop_flag");
    }
}
