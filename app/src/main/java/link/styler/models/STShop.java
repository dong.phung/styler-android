package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */
public class STShop extends RealmObject {
    @PrimaryKey
    public int      shopID      = 0;
    public int      locationID  = 0;
    public String   name        = "";
    public String   station     = "";
    public String   profile     = "";
    public String   address     = "";
    public String   tel         = "";
    public String   website     = "";
    public String   businessHour= "";
    public String   imageURL    = "";
    public int      itemCount   = 0;
    public int      eventCount  = 0;;
    public int      staffCount  = 0;;
    public String   createdAt   = "";

    public void fillInfo(Realm realm, JSONObject info){
        Logger.print("stShop fillInfo");
            this.shopID     = info.optInt("id");
            this.locationID = info.optInt("location");
            this.name       = info.optString("name");
            this.station    = info.optString("station");
            this.address    = info.optString("address");
            this.tel        = info.optString("tel");
            this.website    = info.optString("website");
            this.businessHour = info.optString("business_hours");
            this.profile    = info.optString("profile");
            this.imageURL   = info.optString("image_url");
            this.itemCount  = info.optInt("items_count");
            this.eventCount = info.optInt("events_count");
            this.staffCount = info.optInt("staffs_count");
            this.createdAt  = info.optString("created_at");
        Logger.print("stop fillInfo complete");

    }
}
