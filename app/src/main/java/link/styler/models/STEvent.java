package link.styler.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STEvent extends RealmObject {
    @PrimaryKey
    public int eventID = 0;
    public int userID = 0;
    public int shopID = 0;
    public int interestID = 0;
    public int interestsCount = 0;
    public String title = "";
    public String text = "";
    // code swift:   dynamic var imageData: NSData? = nil // change NSdata <======
    public byte[] imageData = null;
    public String imageURL = "";
    public String location = "";
    public String startDate = "";
    public String finishDate = "";
    public STShop shop = new STShop();
    public RealmList<STUserSort> interestUsers = new RealmList<>();

    public void fillInfo(Realm realm, JSONObject info, int shopID) {
        this.eventID = info.optInt("id");
        this.userID = info.optInt("user_id");
        this.shopID = shopID;
        this.interestID = info.optInt("interest_id");
        this.interestsCount = info.optInt("interests_count");
        this.title = info.optString("title");
        this.text = info.optString("text");
        this.imageURL = info.optString("image_url");
        /*
        URL url = null;
        try {
            url = new URL(this.imageURL);
            if (url != null) {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                this.imageData = stream.toByteArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //new ImageLoadTask(this.imageURL.toString(),this.imageData);

        this.location = info.optString("location");
        this.startDate = info.optString("start_date");
        this.finishDate = info.optString("finish_date");
    }

    private class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
        private String url;
        private byte[] imageData;

        public ImageLoadTask(String url,byte[] imageData) {
            this.url = url;
            this.imageData = imageData;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoInput(true);
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(inputStream);
                return myBitmap;
            } catch (Exception e) {
                Logger.print("ImageLoadTask " + e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            result.compress(Bitmap.CompressFormat.PNG, 100, stream);
            this.imageData = stream.toByteArray();
        }
    }
}

