package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/9/2017.
 */

public class STArticle extends RealmObject{
    @PrimaryKey
    public int          articleID   = 0;
    public int          categoryID  = 0;
    public int          authorID    = 0;
    public String       title       = "";
    public String       desc        = "";
    public String       articleURL  = "";
    public String       imageURL    = "";
    public String       publishedAt = "";
    public STCategory   category    = new STCategory();
    public RealmList<STTag> tags = new RealmList<>();

    public void fillInfo(Realm realm, JSONObject info) {
        Logger.print("STArticle fillInfo");
        this.articleID      = info.optInt("id");
        this.categoryID     = info.optInt("category_id");
        this.authorID       = info.optInt("author_id");
        this.title          = info.optString("title");
        this.desc           = info.optString("description");
        this.articleURL     = info.optString("url");
        this.imageURL       = info.optString("thumbnail");
        this.publishedAt    = info.optString("created_at");
        this.category       = realm.where(STCategory.class).equalTo("categoryID",categoryID).findFirst();
    }
}
