package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STComment extends RealmObject {
    @PrimaryKey
    public int      commentID   = 0;
    public int      postID      = 0;
    public STUser   user        = new STUser();
    public STItem   item        = new STItem();
    public String   text        = "";
    public String   createdAt   = "";

    public void fillInfo(JSONObject info){
        this.commentID  = info.optInt("id");
        this.postID     = info.optInt("post_id");
        this.text       = info.optString("text",null);
        this.createdAt  = info.optString("created_at",null);
    }

    public void fillInfo(Realm realm,JSONObject info){
        Logger.print("ST Commnet fill info json "+info);
        this.commentID  = info.optInt("id");
        this.postID     = info.optInt("post_id");
        this.text       = info.optString("text",null);
        this.createdAt  = info.optString("created_at",null);
        int itemID      = info.optInt("item_id");
        if(itemID != 0){
            STItem itemObj = realm.where(STItem.class).equalTo("itemID",itemID).findFirst();
            if(itemObj != null)
                this.item   =  itemObj;
            else {
                final STItem stItem = new STItem();
                stItem.itemID  = itemID;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(stItem);
                    }
                });
                this.item    = stItem;
            }
        }

        JSONObject userObj = info.optJSONObject("user");
        if(userObj != null){
            final STUser stUser = new STUser();
            stUser.fillInfo(realm,userObj);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(stUser);
                }
            });
            this.user    = stUser;
        }

    }

}
