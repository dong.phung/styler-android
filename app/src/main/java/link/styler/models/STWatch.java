package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */

public class STWatch extends RealmObject {
    @PrimaryKey
    public int      watchID     = 0;
    public int      userID      = 0;
    public String   createdAt   = "";
    public STPost   post        = new STPost();

    public void fillInfo(JSONObject info, STPost post)  {
        watchID     = info.optInt("id");
        userID      = info.optInt("user_id");
        createdAt   = info.optString("created_at");
        this.post   = post;
    }
}
