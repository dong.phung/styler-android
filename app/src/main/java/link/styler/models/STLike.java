package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STLike extends RealmObject{
    @PrimaryKey
    public int      likeID      =0;
    public int      userID      =0;
    public String   createdAt   ="";
    public STItem   item        = new STItem();

    public void fillInfo(JSONObject info, STItem item) {

            likeID      = info.optInt("id");
            userID      = info.optInt("user_id");
            createdAt   =info.optString("created_at");
            this.item = item;
    }
}
