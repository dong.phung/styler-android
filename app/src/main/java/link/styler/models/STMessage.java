package link.styler.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

enum MessageType {
    None("none"),
    User("user"),
    Link("link"),
    Reply("reply"),
    Item("item"),
    Notification("notification");

    private String messageType;

    private MessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }
}

public class STMessage extends RealmObject {
    @PrimaryKey
    public int          messageID           = 0;
    public boolean      isReadMessage       = false;

    public int          userID              = 0;
    public int          conversationID      = 0;
    public int          destinationUserID   = 0;
    public String       text                = "";
    public String       type                = MessageType.None.getMessageType(); // <=>code swift: MessageType.None.rawValue
    public String       createdAt           = "";
    // code swift: dynamic var userImage: NSData? = nil // dùng bitmap thay thế để set image ????????????????
    public byte[]       userImage;
    public String       userImageURL;
    //link
    public String       imageURL            = "";
    public String       linkURL             = "";
    public String       title               = "";
    public String       linkDescription     = "";
    //Item
    public STItem       item            = new STItem();

    public void fillInfo(Realm realm, JSONObject info) {
        Logger.print("STMessage fillInfo");
        if (this.messageID == 0)
            this.messageID = info.optInt("id");
        this.userID = info.optInt("user_id");
        this.conversationID = info.optInt("conversation_id");
        this.text = info.optString("text");
        /*try {
            URL userImageURL = new URL(info.optString("user_image_url"));
            this.userImageURL = info.optString("user_image_url");
            if (userImageURL != null) {
                HttpURLConnection connection = (HttpURLConnection) userImageURL.openConnection();
                final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                this.userImage = stream.toByteArray();
            }
        } catch (IOException e) {
            Logger.print("STMessage exception " + e.getMessage());
            e.printStackTrace();
        }*/

        switch (info.optString("type")) {
            case "UserMessage":
                this.type = MessageType.User.getMessageType();
                break;
            case "LinkMessage":
                this.type = MessageType.Link.getMessageType();
                if (info.has("objects")) {
                    JSONObject object = info.optJSONObject("objects");
                    this.imageURL = object.optString("image_url");
                    this.linkURL = object.optString("url");
                    this.title = object.optString("title");
                    this.linkDescription = object.optString("description");
                }
                break;
            case "ReplyMessage":
                this.type = MessageType.Reply.getMessageType();
                break;
            case "NotificationMessage":
                this.type = MessageType.Notification.getMessageType();
                break;
            case "ItemMessage":
                this.type = MessageType.Item.getMessageType();
                if (info.has("objects")) {
                    if (info.has("objects")) {

                        final JSONObject object = info.optJSONObject("objects");
                        int itemID = object.optInt("id");
                        STItem item = realm.where(STItem.class).equalTo("itemID", itemID).findFirst();
                        if (item != null) {
                            final STItem finalItem = item;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    finalItem.brand = object.optString("brand");
                                    finalItem.name = object.optString("name");
                                    finalItem.mainImageURL = object.optString("image_url");
                                    finalItem.price = "" + object.optInt("price");
                                    realm.copyToRealmOrUpdate(finalItem);
                                }
                            });
                            this.item = item;
                        } else {
                            item = new STItem();

                            item.fillInfo(realm, info.optJSONObject("objects"));


                            final STItem finalItem1 = item;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(finalItem1);
                                }
                            });
                            this.item = item;
                        }
                    }
                }
                break;
            case "":
                this.type = MessageType.None.getMessageType();
                break;
            default:
                this.type = MessageType.None.getMessageType();
                break;
        }
        this.createdAt = info.optString("created_at");
        Logger.print("STMessage fill info commplete");
    }

    public STMessage() {
    }
}
