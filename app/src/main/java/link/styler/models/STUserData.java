package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */
public class STUserData extends RealmObject{
    @PrimaryKey
    public int  userID  = 0;
    public int  height  = 0;
    public int  weight  = 0;

    public void fillInfo(JSONObject info){
        this.userID = info.optInt("id");
        this.height = info.optInt("height");
        this.weight = info.optInt("weight");
    }
}
