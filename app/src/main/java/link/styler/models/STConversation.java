package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STConversation extends RealmObject{
    @PrimaryKey
    public int     conversationID = 0;
    public String  newestText = "";
    public boolean unreadFlag = false;
    public String  newestTime = "";
    public int     destinationID = 0;
    public String  destinationName = "";
    public String  destinationImageURL = "";
     public RealmList<STMessage> messages = new RealmList<>();

    public void fillInfo(JSONObject info) {
        this.conversationID         = info.optInt("id");
        this.newestText             = info.optString("newest_text");
        this.unreadFlag             = info.optBoolean("unread_flag");
        this.newestTime             = info.optString("newest_time");
        this.destinationID          = info.optInt("destination_id");
        this.destinationName        = info.optString("destination_name");
        this.destinationImageURL    = info.optString("destination_image_url");
    }

    public void fillAllInfo(JSONObject info,RealmList<STMessage> messages) {
        fillInfo(info);
       this.messages.addAll(messages);
    }
}
