package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
public class STImageURL extends RealmObject {
    @PrimaryKey
    public String   url     = "";
    public int      order   = 1;
    public int      postID  = 0;
    public int      itemID  = 0;
    public int      eventID = 0;
    public int      userID  = 0;
    public int      shopID  = 0;
    public int      articleID       = 0;
    public int      notificationID  = 0;

    public void fillInfoByPost(String url, int postID, int order){
        this.url    = url;
        this.postID = postID;
        this.order  = order;
    }

    public void fillInfoByItem(String imageURL, int itemID, int order) {
        this.url    = imageURL;
        this.itemID = itemID;
        this.order  = order;
    }

    public void fillInfoByEvent(String url,int eventID){
        this.url        = url;
        this.eventID    = eventID;
    }

    public void fillInfoByUser(String url, int userID){
        this.url        = url;
        this.userID     = userID;
    }

    public void fillInfoByShop(String url, int shopID){
        this.url        = url;
        this.shopID     = shopID;
    }
    public void fillInfoByNotification (String url, int notificationID) {
        this.url            = url;
        this.notificationID = notificationID;
    }

    public void fillInfoByArticle(String url, int articleID) {
        this.url        = url;
        this.articleID  = articleID;
    }
}
