package link.styler.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by admin on 1/6/2017.
 */
public class STImage extends RealmObject{
    @PrimaryKey
    public String   url             = "";
    public byte[] data;
    public double   registeredAt    = System.currentTimeMillis()/1000;

    public void fillByURL(Realm realm, String url) throws IOException {
        this.url = url;
        saveImageData(realm,this.url);
    }

    public void fillByURL(Realm realm, String url, final STCompletion.Void completion){
        this.url = url;
        saveImageData(realm, url, completion);
    }

    private void saveImageData(Realm realm, String url,STCompletion.Void completion){
        try {
            URL _url =new URL(url);
            HttpURLConnection connection = (HttpURLConnection) _url.openConnection();
            Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            data = stream.toByteArray();

            completion.onRequestComplete();
        }catch (Exception ex)
        {
            Log.e("Image data load error: ",ex.toString());

            completion.onRequestComplete();
        }

    }

    private void saveImageData(Realm realm, String url) throws IOException {
        if(url != null){
           // URL _url =new URL(url);
           // HttpURLConnection connection = (HttpURLConnection) _url.openConnection();
            //Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());

            try {
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                data = stream.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
