package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STFollow extends RealmObject{
    @PrimaryKey
    public int      followID    =0;
    public int      userID      =0;
    public int      staffID     =0;
    public String   createdAt   ="";
    public STUser   user        = new STUser();
    public STUser   staff       = new STUser();

    public void fillInfoUser(JSONObject info, STUser user) {
        this.followID   = info.optInt("id");
        this.userID     = info.optInt("user_id");
        this.staffID    = info.optInt("staff_id");
        this.createdAt  = info.optString("created_at");
        this.user = user;
    }

    public void fillInfoStaff(JSONObject info, STUser user) {
        this.followID   = info.optInt("id");
        this.userID     = info.optInt("user_id");
        this.staffID    = info.optInt("staff_id");
        this.createdAt  = info.optString("created_at");
        this.staff = user;
    }
}
