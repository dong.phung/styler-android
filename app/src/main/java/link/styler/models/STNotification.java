package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import link.styler.Utils.Logger;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
/*
enum NotificationType{
    Comment(1),Event(2),Feedback(3),Follow(4),Interest(5),Like(6),Reply(7),;
    private int notificationType;
    private NotificationType(int notificationType){
        this.notificationType = notificationType;
    }
    public int getNotificationType() {
        return notificationType;
    }
}
*/
public class STNotification extends RealmObject {

    //Common
    @PrimaryKey
    public int      notificationID  = 0;
    public int      type            = 0;
    public int      authorID        = 0;
    public String   authorName      = "";
    public String   authorImageURL  = "";
    public String   notificationText= "";

    //Comment
    public int      commentID       = 0;
    public String   commentText     = "";
    public int      postID          = 0;

    //FeedBack
    public int      feedbackRate    = 0;

    //Item
    public int      itemID          = 0;
    public String   itemBrand       = "";
    public String   itemName        = "";
    public int      itemPrice       = 0;
    public String   itemImageURL    = "";

    //Event
    public int      eventID         = 0;
    public String   eventTitle      = "";
    public String   eventStartDate  = "";
    public String   eventFinishDate = "";
    public String   eventImageURL   = "";

    //Watch
    //postID: int
    public String   postText        = "";

    public void fillInfo(Realm realm, JSONObject info){
        Logger.print("STNotication fillInfo "+info);
        //Common
        this.notificationID = info.optInt("notification_id");
        if(info.has("notification_type"))
            this.type           = info.optInt("notification_type");
        else
            this.type           = 1;
        this.authorID       = info.optInt("author_id");
        this.authorName     = info.optString("author_name");
        this.authorImageURL = info.optString("author_image_url");
        this.notificationText = info.optString("notification_text");

        //Comment
        this.commentID      = info.optInt("comment_id");
        this.commentText    = info.optString("comment_text");
        this.postID         = info.optInt("post_id");

        //FeedBack
        this.feedbackRate   = info.optInt("feedback_rate");

        //Item
        this.itemID         = info.optInt("item_id");
        this.itemBrand      = info.optString("item_brand");
        this.itemName       = info.optString("item_name");
        this.itemPrice      = info.optInt("item_price");
        this.itemImageURL   = info.optString("item_image_url");

        //Event
        this.eventID        = info.optInt("event_id");
        this.eventTitle     = info.optString("event_title");
        this.eventStartDate = info.optString("event_start_date");
        this.eventFinishDate= info.optString("event_finish_date");
        this.eventImageURL  = info.optString("event_image_url");

        //Watch
        this.postText       = info.optString("post_text");
        Logger.print("STNotication fillInfo complete");
    }
}
